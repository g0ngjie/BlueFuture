package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * RecipeSchool entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="recipe_school"
    ,catalog="edudb"
)

public class RecipeSchool  implements java.io.Serializable {


    // Fields    

     private String recipeSchoolId;
     private School school;
     private RecipeRecord recipeRecord;


    // Constructors

    /** default constructor */
    public RecipeSchool() {
    }

    
    /** full constructor */
    public RecipeSchool(School school, RecipeRecord recipeRecord) {
        this.school = school;
        this.recipeRecord = recipeRecord;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="recipeSchoolId", unique=true, nullable=false, length=32)

    public String getRecipeSchoolId() {
        return this.recipeSchoolId;
    }
    
    public void setRecipeSchoolId(String recipeSchoolId) {
        this.recipeSchoolId = recipeSchoolId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="schoolId")

    public School getSchool() {
        return this.school;
    }
    
    public void setSchool(School school) {
        this.school = school;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="recipeRecordId")

    public RecipeRecord getRecipeRecord() {
        return this.recipeRecord;
    }
    
    public void setRecipeRecord(RecipeRecord recipeRecord) {
        this.recipeRecord = recipeRecord;
    }
   








}