package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Banner entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="banner"
    ,catalog="edudb"
)

public class Banner  implements java.io.Serializable {


    // Fields    

     private String bannerId;
     private String imageUrl;
     private String linkUrl;
     private Integer enabel;


    // Constructors

    /** default constructor */
    public Banner() {
    }

    
    /** full constructor */
    public Banner(String imageUrl, String linkUrl, Integer enabel) {
        this.imageUrl = imageUrl;
        this.linkUrl = linkUrl;
        this.enabel = enabel;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="bannerId", unique=true, nullable=false, length=32)

    public String getBannerId() {
        return this.bannerId;
    }
    
    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }
    
    @Column(name="imageUrl", length=32)

    public String getImageUrl() {
        return this.imageUrl;
    }
    
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    
    @Column(name="linkUrl", length=32)

    public String getLinkUrl() {
        return this.linkUrl;
    }
    
    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }
    
    @Column(name="enabel")

    public Integer getEnabel() {
        return this.enabel;
    }
    
    public void setEnabel(Integer enabel) {
        this.enabel = enabel;
    }
   








}