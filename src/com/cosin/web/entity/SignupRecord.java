package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * SignupRecord entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="signup_record"
    ,catalog="edudb"
)

public class SignupRecord  implements java.io.Serializable {


    // Fields    

     private String signupRecordId;
     private Notice notice;
     private Student student;
     private Timestamp createDate;


    // Constructors

    /** default constructor */
    public SignupRecord() {
    }

    
    /** full constructor */
    public SignupRecord(Notice notice, Student student, Timestamp createDate) {
        this.notice = notice;
        this.student = student;
        this.createDate = createDate;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="signupRecordId", unique=true, nullable=false, length=32)

    public String getSignupRecordId() {
        return this.signupRecordId;
    }
    
    public void setSignupRecordId(String signupRecordId) {
        this.signupRecordId = signupRecordId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="noticeId")

    public Notice getNotice() {
        return this.notice;
    }
    
    public void setNotice(Notice notice) {
        this.notice = notice;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="studentId")

    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
   








}