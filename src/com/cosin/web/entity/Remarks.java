package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Remarks entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="remarks"
    ,catalog="edudb"
)

public class Remarks  implements java.io.Serializable {


    // Fields    

     private String remarksId;
     private SysUser sysUserByRemarkUserId;
     private SysUser sysUserByUserId;
     private String content;


    // Constructors

    /** default constructor */
    public Remarks() {
    }

    
    /** full constructor */
    public Remarks(SysUser sysUserByRemarkUserId, SysUser sysUserByUserId, String content) {
        this.sysUserByRemarkUserId = sysUserByRemarkUserId;
        this.sysUserByUserId = sysUserByUserId;
        this.content = content;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="remarksId", unique=true, nullable=false, length=32)

    public String getRemarksId() {
        return this.remarksId;
    }
    
    public void setRemarksId(String remarksId) {
        this.remarksId = remarksId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="remarkUserId")

    public SysUser getSysUserByRemarkUserId() {
        return this.sysUserByRemarkUserId;
    }
    
    public void setSysUserByRemarkUserId(SysUser sysUserByRemarkUserId) {
        this.sysUserByRemarkUserId = sysUserByRemarkUserId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="userId")

    public SysUser getSysUserByUserId() {
        return this.sysUserByUserId;
    }
    
    public void setSysUserByUserId(SysUser sysUserByUserId) {
        this.sysUserByUserId = sysUserByUserId;
    }
    
    @Column(name="content", length=1024)

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
   








}