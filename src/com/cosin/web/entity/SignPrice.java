package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * SignPrice entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="sign_price"
    ,catalog="edudb"
)

public class SignPrice  implements java.io.Serializable {


    // Fields    

     private String signPriceId;
     private Notice notice;
     private SysUser sysUser;
     private Student student;
     private String price;
     private Timestamp createTime;
     private String orderNo;
     private Integer state;


    // Constructors

    /** default constructor */
    public SignPrice() {
    }

    
    /** full constructor */
    public SignPrice(Notice notice, SysUser sysUser, Student student, String price, Timestamp createTime, String orderNo, Integer state) {
        this.notice = notice;
        this.sysUser = sysUser;
        this.student = student;
        this.price = price;
        this.createTime = createTime;
        this.orderNo = orderNo;
        this.state = state;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="signPriceId", unique=true, nullable=false, length=32)

    public String getSignPriceId() {
        return this.signPriceId;
    }
    
    public void setSignPriceId(String signPriceId) {
        this.signPriceId = signPriceId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="noticeId")

    public Notice getNotice() {
        return this.notice;
    }
    
    public void setNotice(Notice notice) {
        this.notice = notice;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="userId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="studentId")

    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }
    
    @Column(name="price", length=32)

    public String getPrice() {
        return this.price;
    }
    
    public void setPrice(String price) {
        this.price = price;
    }
    
    @Column(name="createTime", length=0)

    public Timestamp getCreateTime() {
        return this.createTime;
    }
    
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
    
    @Column(name="orderNo", length=32)

    public String getOrderNo() {
        return this.orderNo;
    }
    
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
    
    @Column(name="state")

    public Integer getState() {
        return this.state;
    }
    
    public void setState(Integer state) {
        this.state = state;
    }
   








}