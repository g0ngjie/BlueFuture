package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Parentsstudent entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="parentsstudent"
    ,catalog="edudb"
)

public class Parentsstudent  implements java.io.Serializable {


    // Fields    

     private String parentsStudentId;
     private Parents parents;
     private Student student;
     private String relativeShip;
     private String emobCode;
     private Timestamp activityNum;
     private Timestamp noticeNum;
     private Timestamp recipeNum;
     private Timestamp kaoqinNum;
     private Timestamp leaveNum;


    // Constructors

    /** default constructor */
    public Parentsstudent() {
    }

    
    /** full constructor */
    public Parentsstudent(Parents parents, Student student, String relativeShip, String emobCode, Timestamp activityNum, Timestamp noticeNum, Timestamp recipeNum, Timestamp kaoqinNum, Timestamp leaveNum) {
        this.parents = parents;
        this.student = student;
        this.relativeShip = relativeShip;
        this.emobCode = emobCode;
        this.activityNum = activityNum;
        this.noticeNum = noticeNum;
        this.recipeNum = recipeNum;
        this.kaoqinNum = kaoqinNum;
        this.leaveNum = leaveNum;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="parentsStudentId", unique=true, nullable=false, length=32)

    public String getParentsStudentId() {
        return this.parentsStudentId;
    }
    
    public void setParentsStudentId(String parentsStudentId) {
        this.parentsStudentId = parentsStudentId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="parentId")

    public Parents getParents() {
        return this.parents;
    }
    
    public void setParents(Parents parents) {
        this.parents = parents;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="studentId")

    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }
    
    @Column(name="relativeShip", length=32)

    public String getRelativeShip() {
        return this.relativeShip;
    }
    
    public void setRelativeShip(String relativeShip) {
        this.relativeShip = relativeShip;
    }
    
    @Column(name="emobCode", length=32)

    public String getEmobCode() {
        return this.emobCode;
    }
    
    public void setEmobCode(String emobCode) {
        this.emobCode = emobCode;
    }
    
    @Column(name="activityNum", length=0)

    public Timestamp getActivityNum() {
        return this.activityNum;
    }
    
    public void setActivityNum(Timestamp activityNum) {
        this.activityNum = activityNum;
    }
    
    @Column(name="noticeNum", length=0)

    public Timestamp getNoticeNum() {
        return this.noticeNum;
    }
    
    public void setNoticeNum(Timestamp noticeNum) {
        this.noticeNum = noticeNum;
    }
    
    @Column(name="recipeNum", length=0)

    public Timestamp getRecipeNum() {
        return this.recipeNum;
    }
    
    public void setRecipeNum(Timestamp recipeNum) {
        this.recipeNum = recipeNum;
    }
    
    @Column(name="kaoqinNum", length=0)

    public Timestamp getKaoqinNum() {
        return this.kaoqinNum;
    }
    
    public void setKaoqinNum(Timestamp kaoqinNum) {
        this.kaoqinNum = kaoqinNum;
    }
    
    @Column(name="leaveNum", length=0)

    public Timestamp getLeaveNum() {
        return this.leaveNum;
    }
    
    public void setLeaveNum(Timestamp leaveNum) {
        this.leaveNum = leaveNum;
    }
   








}