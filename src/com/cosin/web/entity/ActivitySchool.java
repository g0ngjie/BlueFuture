package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * ActivitySchool entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="activity_school"
    ,catalog="edudb"
)

public class ActivitySchool  implements java.io.Serializable {


    // Fields    

     private String avtivitySchoolId;
     private School school;
     private Activity activity;


    // Constructors

    /** default constructor */
    public ActivitySchool() {
    }

    
    /** full constructor */
    public ActivitySchool(School school, Activity activity) {
        this.school = school;
        this.activity = activity;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="avtivity_schoolId", unique=true, nullable=false, length=32)

    public String getAvtivitySchoolId() {
        return this.avtivitySchoolId;
    }
    
    public void setAvtivitySchoolId(String avtivitySchoolId) {
        this.avtivitySchoolId = avtivitySchoolId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="schoolId")

    public School getSchool() {
        return this.school;
    }
    
    public void setSchool(School school) {
        this.school = school;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="activityId")

    public Activity getActivity() {
        return this.activity;
    }
    
    public void setActivity(Activity activity) {
        this.activity = activity;
    }
   








}