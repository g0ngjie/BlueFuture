package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Contain entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="contain"
    ,catalog="edudb"
)

public class Contain  implements java.io.Serializable {


    // Fields    

     private String containId;
     private String content;


    // Constructors

    /** default constructor */
    public Contain() {
    }

    
    /** full constructor */
    public Contain(String content) {
        this.content = content;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="containId", unique=true, nullable=false, length=32)

    public String getContainId() {
        return this.containId;
    }
    
    public void setContainId(String containId) {
        this.containId = containId;
    }
    
    @Column(name="content", length=16777215)

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
   








}