package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Relative entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="relative"
    ,catalog="edudb"
)

public class Relative  implements java.io.Serializable {


    // Fields    

     private String relativeId;
     private String relativeName;


    // Constructors

    /** default constructor */
    public Relative() {
    }

    
    /** full constructor */
    public Relative(String relativeName) {
        this.relativeName = relativeName;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="relativeId", unique=true, nullable=false, length=32)

    public String getRelativeId() {
        return this.relativeId;
    }
    
    public void setRelativeId(String relativeId) {
        this.relativeId = relativeId;
    }
    
    @Column(name="relativeName", length=32)

    public String getRelativeName() {
        return this.relativeName;
    }
    
    public void setRelativeName(String relativeName) {
        this.relativeName = relativeName;
    }
   








}