package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Parents entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="parents"
    ,catalog="edudb"
)

public class Parents  implements java.io.Serializable {


    // Fields    

     private String parentId;
     private SysUser sysUser;
     private Student student;
     private Timestamp createDate;
     private Integer enable;
     private Integer isDel;
     private String relation;
     private Set<Parentsstudent> parentsstudents = new HashSet<Parentsstudent>(0);
     private Set<Student> students = new HashSet<Student>(0);


    // Constructors

    /** default constructor */
    public Parents() {
    }

    
    /** full constructor */
    public Parents(SysUser sysUser, Student student, Timestamp createDate, Integer enable, Integer isDel, String relation, Set<Parentsstudent> parentsstudents, Set<Student> students) {
        this.sysUser = sysUser;
        this.student = student;
        this.createDate = createDate;
        this.enable = enable;
        this.isDel = isDel;
        this.relation = relation;
        this.parentsstudents = parentsstudents;
        this.students = students;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="parentId", unique=true, nullable=false, length=32)

    public String getParentId() {
        return this.parentId;
    }
    
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="userId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="studentId")

    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="enable")

    public Integer getEnable() {
        return this.enable;
    }
    
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
    
    @Column(name="relation", length=32)

    public String getRelation() {
        return this.relation;
    }
    
    public void setRelation(String relation) {
        this.relation = relation;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="parents")

    public Set<Parentsstudent> getParentsstudents() {
        return this.parentsstudents;
    }
    
    public void setParentsstudents(Set<Parentsstudent> parentsstudents) {
        this.parentsstudents = parentsstudents;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="parents")

    public Set<Student> getStudents() {
        return this.students;
    }
    
    public void setStudents(Set<Student> students) {
        this.students = students;
    }
   








}