package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Employee entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="employee"
    ,catalog="edudb"
)

public class Employee  implements java.io.Serializable {


    // Fields    

     private String employeeId;
     private School school;
     private SysUser sysUserByUserId;
     private SysUser sysUserByCreateUserId;
     private String employeeName;
     private Timestamp createDate;
     private Integer enable;
     private Integer isDel;
     private Integer grade;
     private Set<Classteacher> classteachers = new HashSet<Classteacher>(0);
     private Set<EmpSchool> empSchools = new HashSet<EmpSchool>(0);
     private Set<StudentLeave> studentLeaves = new HashSet<StudentLeave>(0);
     private Set<School> schools = new HashSet<School>(0);
     private Set<Classes> classeses = new HashSet<Classes>(0);


    // Constructors

    /** default constructor */
    public Employee() {
    }

    
    /** full constructor */
    public Employee(School school, SysUser sysUserByUserId, SysUser sysUserByCreateUserId, String employeeName, Timestamp createDate, Integer enable, Integer isDel, Integer grade, Set<Classteacher> classteachers, Set<EmpSchool> empSchools, Set<StudentLeave> studentLeaves, Set<School> schools, Set<Classes> classeses) {
        this.school = school;
        this.sysUserByUserId = sysUserByUserId;
        this.sysUserByCreateUserId = sysUserByCreateUserId;
        this.employeeName = employeeName;
        this.createDate = createDate;
        this.enable = enable;
        this.isDel = isDel;
        this.grade = grade;
        this.classteachers = classteachers;
        this.empSchools = empSchools;
        this.studentLeaves = studentLeaves;
        this.schools = schools;
        this.classeses = classeses;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="employeeId", unique=true, nullable=false, length=32)

    public String getEmployeeId() {
        return this.employeeId;
    }
    
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="schoolId")

    public School getSchool() {
        return this.school;
    }
    
    public void setSchool(School school) {
        this.school = school;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="userId")

    public SysUser getSysUserByUserId() {
        return this.sysUserByUserId;
    }
    
    public void setSysUserByUserId(SysUser sysUserByUserId) {
        this.sysUserByUserId = sysUserByUserId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="createUserId")

    public SysUser getSysUserByCreateUserId() {
        return this.sysUserByCreateUserId;
    }
    
    public void setSysUserByCreateUserId(SysUser sysUserByCreateUserId) {
        this.sysUserByCreateUserId = sysUserByCreateUserId;
    }
    
    @Column(name="employeeName", length=32)

    public String getEmployeeName() {
        return this.employeeName;
    }
    
    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="enable")

    public Integer getEnable() {
        return this.enable;
    }
    
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
    
    @Column(name="grade")

    public Integer getGrade() {
        return this.grade;
    }
    
    public void setGrade(Integer grade) {
        this.grade = grade;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="employee")

    public Set<Classteacher> getClassteachers() {
        return this.classteachers;
    }
    
    public void setClassteachers(Set<Classteacher> classteachers) {
        this.classteachers = classteachers;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="employee")

    public Set<EmpSchool> getEmpSchools() {
        return this.empSchools;
    }
    
    public void setEmpSchools(Set<EmpSchool> empSchools) {
        this.empSchools = empSchools;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="employee")

    public Set<StudentLeave> getStudentLeaves() {
        return this.studentLeaves;
    }
    
    public void setStudentLeaves(Set<StudentLeave> studentLeaves) {
        this.studentLeaves = studentLeaves;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="employee")

    public Set<School> getSchools() {
        return this.schools;
    }
    
    public void setSchools(Set<School> schools) {
        this.schools = schools;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="employee")

    public Set<Classes> getClasseses() {
        return this.classeses;
    }
    
    public void setClasseses(Set<Classes> classeses) {
        this.classeses = classeses;
    }
   








}