package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * PayBill entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="pay_bill"
    ,catalog="edudb"
)

public class PayBill  implements java.io.Serializable {


    // Fields    

     private String payBillId;
     private Notice notice;
     private Student student;
     private String orderNo;
     private Double price;
     private Integer payType;
     private Timestamp createDate;


    // Constructors

    /** default constructor */
    public PayBill() {
    }

    
    /** full constructor */
    public PayBill(Notice notice, Student student, String orderNo, Double price, Integer payType, Timestamp createDate) {
        this.notice = notice;
        this.student = student;
        this.orderNo = orderNo;
        this.price = price;
        this.payType = payType;
        this.createDate = createDate;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="payBillId", unique=true, nullable=false, length=32)

    public String getPayBillId() {
        return this.payBillId;
    }
    
    public void setPayBillId(String payBillId) {
        this.payBillId = payBillId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="noticeId")

    public Notice getNotice() {
        return this.notice;
    }
    
    public void setNotice(Notice notice) {
        this.notice = notice;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="studentId")

    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }
    
    @Column(name="orderNo", length=32)

    public String getOrderNo() {
        return this.orderNo;
    }
    
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
    
    @Column(name="price", precision=8)

    public Double getPrice() {
        return this.price;
    }
    
    public void setPrice(Double price) {
        this.price = price;
    }
    
    @Column(name="payType")

    public Integer getPayType() {
        return this.payType;
    }
    
    public void setPayType(Integer payType) {
        this.payType = payType;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
   








}