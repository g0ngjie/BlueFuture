package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Grade entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="grade"
    ,catalog="edudb"
)

public class Grade  implements java.io.Serializable {


    // Fields    

     private String gradeId;
     private String gradeName;
     private Timestamp createDate;
     private Set<GradeClass> gradeClasses = new HashSet<GradeClass>(0);


    // Constructors

    /** default constructor */
    public Grade() {
    }

    
    /** full constructor */
    public Grade(String gradeName, Timestamp createDate, Set<GradeClass> gradeClasses) {
        this.gradeName = gradeName;
        this.createDate = createDate;
        this.gradeClasses = gradeClasses;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="gradeId", unique=true, nullable=false, length=32)

    public String getGradeId() {
        return this.gradeId;
    }
    
    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }
    
    @Column(name="gradeName", length=32)

    public String getGradeName() {
        return this.gradeName;
    }
    
    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="grade")

    public Set<GradeClass> getGradeClasses() {
        return this.gradeClasses;
    }
    
    public void setGradeClasses(Set<GradeClass> gradeClasses) {
        this.gradeClasses = gradeClasses;
    }
   








}