package com.cosin.web.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * SysSubrole entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="sys_subrole"
    ,catalog="edudb"
)

public class SysSubrole  implements java.io.Serializable {


    // Fields    

     private String subRoleId;
     private SysRole sysRole;
     private String typeName;
     private Integer isDel;
     private String roleLevel;
     private Set<SysRole> sysRoles = new HashSet<SysRole>(0);
     private Set<SysUser> sysUsers = new HashSet<SysUser>(0);
     private Set<Classteacher> classteachers = new HashSet<Classteacher>(0);


    // Constructors

    /** default constructor */
    public SysSubrole() {
    }

    
    /** full constructor */
    public SysSubrole(SysRole sysRole, String typeName, Integer isDel, String roleLevel, Set<SysRole> sysRoles, Set<SysUser> sysUsers, Set<Classteacher> classteachers) {
        this.sysRole = sysRole;
        this.typeName = typeName;
        this.isDel = isDel;
        this.roleLevel = roleLevel;
        this.sysRoles = sysRoles;
        this.sysUsers = sysUsers;
        this.classteachers = classteachers;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="subRoleId", unique=true, nullable=false, length=32)

    public String getSubRoleId() {
        return this.subRoleId;
    }
    
    public void setSubRoleId(String subRoleId) {
        this.subRoleId = subRoleId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="roleId")

    public SysRole getSysRole() {
        return this.sysRole;
    }
    
    public void setSysRole(SysRole sysRole) {
        this.sysRole = sysRole;
    }
    
    @Column(name="typeName", length=32)

    public String getTypeName() {
        return this.typeName;
    }
    
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
    
    @Column(name="roleLevel", length=32)

    public String getRoleLevel() {
        return this.roleLevel;
    }
    
    public void setRoleLevel(String roleLevel) {
        this.roleLevel = roleLevel;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysSubrole")

    public Set<SysRole> getSysRoles() {
        return this.sysRoles;
    }
    
    public void setSysRoles(Set<SysRole> sysRoles) {
        this.sysRoles = sysRoles;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysSubrole")

    public Set<SysUser> getSysUsers() {
        return this.sysUsers;
    }
    
    public void setSysUsers(Set<SysUser> sysUsers) {
        this.sysUsers = sysUsers;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysSubrole")

    public Set<Classteacher> getClassteachers() {
        return this.classteachers;
    }
    
    public void setClassteachers(Set<Classteacher> classteachers) {
        this.classteachers = classteachers;
    }
   








}