package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * RelativeStudent entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="relative_student"
    ,catalog="edudb"
)

public class RelativeStudent  implements java.io.Serializable {


    // Fields    

     private String relativeStudentId;
     private SysUser sysUser;
     private String attentions;
     private String phone;
     private String relationship;
     private String relativeName;


    // Constructors

    /** default constructor */
    public RelativeStudent() {
    }

    
    /** full constructor */
    public RelativeStudent(SysUser sysUser, String attentions, String phone, String relationship, String relativeName) {
        this.sysUser = sysUser;
        this.attentions = attentions;
        this.phone = phone;
        this.relationship = relationship;
        this.relativeName = relativeName;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="relativeStudentId", unique=true, nullable=false, length=32)

    public String getRelativeStudentId() {
        return this.relativeStudentId;
    }
    
    public void setRelativeStudentId(String relativeStudentId) {
        this.relativeStudentId = relativeStudentId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="userId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
    
    @Column(name="attentions")

    public String getAttentions() {
        return this.attentions;
    }
    
    public void setAttentions(String attentions) {
        this.attentions = attentions;
    }
    
    @Column(name="phone", length=32)

    public String getPhone() {
        return this.phone;
    }
    
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    @Column(name="relationship", length=32)

    public String getRelationship() {
        return this.relationship;
    }
    
    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }
    
    @Column(name="relativeName", length=32)

    public String getRelativeName() {
        return this.relativeName;
    }
    
    public void setRelativeName(String relativeName) {
        this.relativeName = relativeName;
    }
   








}