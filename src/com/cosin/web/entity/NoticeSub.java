package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * NoticeSub entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="notice_sub"
    ,catalog="edudb"
)

public class NoticeSub  implements java.io.Serializable {


    // Fields    

     private String niticesubId;
     private Notice notice;
     private Classes classes;


    // Constructors

    /** default constructor */
    public NoticeSub() {
    }

    
    /** full constructor */
    public NoticeSub(Notice notice, Classes classes) {
        this.notice = notice;
        this.classes = classes;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="niticesubId", unique=true, nullable=false, length=32)

    public String getNiticesubId() {
        return this.niticesubId;
    }
    
    public void setNiticesubId(String niticesubId) {
        this.niticesubId = niticesubId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="noticeId")

    public Notice getNotice() {
        return this.notice;
    }
    
    public void setNotice(Notice notice) {
        this.notice = notice;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="classesId")

    public Classes getClasses() {
        return this.classes;
    }
    
    public void setClasses(Classes classes) {
        this.classes = classes;
    }
   








}