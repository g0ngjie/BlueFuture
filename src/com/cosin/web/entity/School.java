package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * School entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="school"
    ,catalog="edudb"
)

public class School  implements java.io.Serializable {


    // Fields    

     private String schoolId;
     private Employee employee;
     private SysArea sysAreaByProvinceId;
     private SysArea sysAreaByCityId;
     private SysUser sysUser;
     private String schoolName;
     private Timestamp createDate;
     private Integer enable;
     private Integer isDel;
     private Set<Employee> employees = new HashSet<Employee>(0);
     private Set<SchoolFackback> schoolFackbacks = new HashSet<SchoolFackback>(0);
     private Set<EmpSchool> empSchools = new HashSet<EmpSchool>(0);
     private Set<Notice> notices = new HashSet<Notice>(0);
     private Set<ActivitySchool> activitySchools = new HashSet<ActivitySchool>(0);
     private Set<HistoryClass> historyClasses = new HashSet<HistoryClass>(0);
     private Set<Student> students = new HashSet<Student>(0);
     private Set<RecipeSchool> recipeSchools = new HashSet<RecipeSchool>(0);
     private Set<SysRoleSchoolPower> sysRoleSchoolPowers = new HashSet<SysRoleSchoolPower>(0);
     private Set<Classes> classeses = new HashSet<Classes>(0);
     private Set<Activity> activities = new HashSet<Activity>(0);


    // Constructors

    /** default constructor */
    public School() {
    }

    
    /** full constructor */
    public School(Employee employee, SysArea sysAreaByProvinceId, SysArea sysAreaByCityId, SysUser sysUser, String schoolName, Timestamp createDate, Integer enable, Integer isDel, Set<Employee> employees, Set<SchoolFackback> schoolFackbacks, Set<EmpSchool> empSchools, Set<Notice> notices, Set<ActivitySchool> activitySchools, Set<HistoryClass> historyClasses, Set<Student> students, Set<RecipeSchool> recipeSchools, Set<SysRoleSchoolPower> sysRoleSchoolPowers, Set<Classes> classeses, Set<Activity> activities) {
        this.employee = employee;
        this.sysAreaByProvinceId = sysAreaByProvinceId;
        this.sysAreaByCityId = sysAreaByCityId;
        this.sysUser = sysUser;
        this.schoolName = schoolName;
        this.createDate = createDate;
        this.enable = enable;
        this.isDel = isDel;
        this.employees = employees;
        this.schoolFackbacks = schoolFackbacks;
        this.empSchools = empSchools;
        this.notices = notices;
        this.activitySchools = activitySchools;
        this.historyClasses = historyClasses;
        this.students = students;
        this.recipeSchools = recipeSchools;
        this.sysRoleSchoolPowers = sysRoleSchoolPowers;
        this.classeses = classeses;
        this.activities = activities;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="schoolId", unique=true, nullable=false, length=32)

    public String getSchoolId() {
        return this.schoolId;
    }
    
    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="mgrId")

    public Employee getEmployee() {
        return this.employee;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="provinceId")

    public SysArea getSysAreaByProvinceId() {
        return this.sysAreaByProvinceId;
    }
    
    public void setSysAreaByProvinceId(SysArea sysAreaByProvinceId) {
        this.sysAreaByProvinceId = sysAreaByProvinceId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="cityId")

    public SysArea getSysAreaByCityId() {
        return this.sysAreaByCityId;
    }
    
    public void setSysAreaByCityId(SysArea sysAreaByCityId) {
        this.sysAreaByCityId = sysAreaByCityId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="createUserId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
    
    @Column(name="schoolName", length=64)

    public String getSchoolName() {
        return this.schoolName;
    }
    
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="enable")

    public Integer getEnable() {
        return this.enable;
    }
    
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="school")

    public Set<Employee> getEmployees() {
        return this.employees;
    }
    
    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="school")

    public Set<SchoolFackback> getSchoolFackbacks() {
        return this.schoolFackbacks;
    }
    
    public void setSchoolFackbacks(Set<SchoolFackback> schoolFackbacks) {
        this.schoolFackbacks = schoolFackbacks;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="school")

    public Set<EmpSchool> getEmpSchools() {
        return this.empSchools;
    }
    
    public void setEmpSchools(Set<EmpSchool> empSchools) {
        this.empSchools = empSchools;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="school")

    public Set<Notice> getNotices() {
        return this.notices;
    }
    
    public void setNotices(Set<Notice> notices) {
        this.notices = notices;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="school")

    public Set<ActivitySchool> getActivitySchools() {
        return this.activitySchools;
    }
    
    public void setActivitySchools(Set<ActivitySchool> activitySchools) {
        this.activitySchools = activitySchools;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="school")

    public Set<HistoryClass> getHistoryClasses() {
        return this.historyClasses;
    }
    
    public void setHistoryClasses(Set<HistoryClass> historyClasses) {
        this.historyClasses = historyClasses;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="school")

    public Set<Student> getStudents() {
        return this.students;
    }
    
    public void setStudents(Set<Student> students) {
        this.students = students;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="school")

    public Set<RecipeSchool> getRecipeSchools() {
        return this.recipeSchools;
    }
    
    public void setRecipeSchools(Set<RecipeSchool> recipeSchools) {
        this.recipeSchools = recipeSchools;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="school")

    public Set<SysRoleSchoolPower> getSysRoleSchoolPowers() {
        return this.sysRoleSchoolPowers;
    }
    
    public void setSysRoleSchoolPowers(Set<SysRoleSchoolPower> sysRoleSchoolPowers) {
        this.sysRoleSchoolPowers = sysRoleSchoolPowers;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="school")

    public Set<Classes> getClasseses() {
        return this.classeses;
    }
    
    public void setClasseses(Set<Classes> classeses) {
        this.classeses = classeses;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="school")

    public Set<Activity> getActivities() {
        return this.activities;
    }
    
    public void setActivities(Set<Activity> activities) {
        this.activities = activities;
    }
   








}