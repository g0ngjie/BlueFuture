package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * EmpSchool entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="emp_school"
    ,catalog="edudb"
)

public class EmpSchool  implements java.io.Serializable {


    // Fields    

     private String empSchoolId;
     private Employee employee;
     private School school;


    // Constructors

    /** default constructor */
    public EmpSchool() {
    }

    
    /** full constructor */
    public EmpSchool(Employee employee, School school) {
        this.employee = employee;
        this.school = school;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="emp_schoolId", unique=true, nullable=false, length=32)

    public String getEmpSchoolId() {
        return this.empSchoolId;
    }
    
    public void setEmpSchoolId(String empSchoolId) {
        this.empSchoolId = empSchoolId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="employeeId")

    public Employee getEmployee() {
        return this.employee;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="schoolId")

    public School getSchool() {
        return this.school;
    }
    
    public void setSchool(School school) {
        this.school = school;
    }
   








}