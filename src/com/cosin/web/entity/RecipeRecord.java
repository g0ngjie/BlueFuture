package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * RecipeRecord entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="recipe_record"
    ,catalog="edudb"
)

public class RecipeRecord  implements java.io.Serializable {


    // Fields    

     private String recipeRecordId;
     private SysUser sysUser;
     private String content;
     private Timestamp createDate;
     private Integer lookNum;
     private Timestamp beginTime;
     private Integer isDraft;
     private Integer isDel;
     private Set<Recipe> recipes = new HashSet<Recipe>(0);
     private Set<RecipeSchool> recipeSchools = new HashSet<RecipeSchool>(0);


    // Constructors

    /** default constructor */
    public RecipeRecord() {
    }

    
    /** full constructor */
    public RecipeRecord(SysUser sysUser, String content, Timestamp createDate, Integer lookNum, Timestamp beginTime, Integer isDraft, Integer isDel, Set<Recipe> recipes, Set<RecipeSchool> recipeSchools) {
        this.sysUser = sysUser;
        this.content = content;
        this.createDate = createDate;
        this.lookNum = lookNum;
        this.beginTime = beginTime;
        this.isDraft = isDraft;
        this.isDel = isDel;
        this.recipes = recipes;
        this.recipeSchools = recipeSchools;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="recipeRecordId", unique=true, nullable=false, length=32)

    public String getRecipeRecordId() {
        return this.recipeRecordId;
    }
    
    public void setRecipeRecordId(String recipeRecordId) {
        this.recipeRecordId = recipeRecordId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="createUserId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
    
    @Column(name="content", length=256)

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="lookNum")

    public Integer getLookNum() {
        return this.lookNum;
    }
    
    public void setLookNum(Integer lookNum) {
        this.lookNum = lookNum;
    }
    
    @Column(name="beginTime", length=0)

    public Timestamp getBeginTime() {
        return this.beginTime;
    }
    
    public void setBeginTime(Timestamp beginTime) {
        this.beginTime = beginTime;
    }
    
    @Column(name="isDraft")

    public Integer getIsDraft() {
        return this.isDraft;
    }
    
    public void setIsDraft(Integer isDraft) {
        this.isDraft = isDraft;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="recipeRecord")

    public Set<Recipe> getRecipes() {
        return this.recipes;
    }
    
    public void setRecipes(Set<Recipe> recipes) {
        this.recipes = recipes;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="recipeRecord")

    public Set<RecipeSchool> getRecipeSchools() {
        return this.recipeSchools;
    }
    
    public void setRecipeSchools(Set<RecipeSchool> recipeSchools) {
        this.recipeSchools = recipeSchools;
    }
   








}