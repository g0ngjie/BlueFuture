package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * SysMenu entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="sys_menu"
    ,catalog="edudb"
)

public class SysMenu  implements java.io.Serializable {


    // Fields    

     private String menuId;
     private SysMenu sysMenu;
     private String menuCode;
     private String menuName;
     private String icon;
     private String url;
     private Integer orderNo;
     private Integer enable;
     private Timestamp createDate;
     private Set<SysMenu> sysMenus = new HashSet<SysMenu>(0);
     private Set<SysPowerItem> sysPowerItems = new HashSet<SysPowerItem>(0);


    // Constructors

    /** default constructor */
    public SysMenu() {
    }

    
    /** full constructor */
    public SysMenu(SysMenu sysMenu, String menuCode, String menuName, String icon, String url, Integer orderNo, Integer enable, Timestamp createDate, Set<SysMenu> sysMenus, Set<SysPowerItem> sysPowerItems) {
        this.sysMenu = sysMenu;
        this.menuCode = menuCode;
        this.menuName = menuName;
        this.icon = icon;
        this.url = url;
        this.orderNo = orderNo;
        this.enable = enable;
        this.createDate = createDate;
        this.sysMenus = sysMenus;
        this.sysPowerItems = sysPowerItems;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="menuId", unique=true, nullable=false, length=32)

    public String getMenuId() {
        return this.menuId;
    }
    
    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="parentMenuId")

    public SysMenu getSysMenu() {
        return this.sysMenu;
    }
    
    public void setSysMenu(SysMenu sysMenu) {
        this.sysMenu = sysMenu;
    }
    
    @Column(name="menuCode", length=32)

    public String getMenuCode() {
        return this.menuCode;
    }
    
    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }
    
    @Column(name="menuName", length=32)

    public String getMenuName() {
        return this.menuName;
    }
    
    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
    
    @Column(name="icon", length=32)

    public String getIcon() {
        return this.icon;
    }
    
    public void setIcon(String icon) {
        this.icon = icon;
    }
    
    @Column(name="url", length=64)

    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    @Column(name="orderNo")

    public Integer getOrderNo() {
        return this.orderNo;
    }
    
    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }
    
    @Column(name="enable")

    public Integer getEnable() {
        return this.enable;
    }
    
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysMenu")

    public Set<SysMenu> getSysMenus() {
        return this.sysMenus;
    }
    
    public void setSysMenus(Set<SysMenu> sysMenus) {
        this.sysMenus = sysMenus;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysMenu")

    public Set<SysPowerItem> getSysPowerItems() {
        return this.sysPowerItems;
    }
    
    public void setSysPowerItems(Set<SysPowerItem> sysPowerItems) {
        this.sysPowerItems = sysPowerItems;
    }
   








}