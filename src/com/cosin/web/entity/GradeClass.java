package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * GradeClass entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="grade_class"
    ,catalog="edudb"
)

public class GradeClass  implements java.io.Serializable {


    // Fields    

     private String gradeClassId;
     private Grade grade;
     private Classes classes;


    // Constructors

    /** default constructor */
    public GradeClass() {
    }

    
    /** full constructor */
    public GradeClass(Grade grade, Classes classes) {
        this.grade = grade;
        this.classes = classes;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="gradeClassId", unique=true, nullable=false, length=32)

    public String getGradeClassId() {
        return this.gradeClassId;
    }
    
    public void setGradeClassId(String gradeClassId) {
        this.gradeClassId = gradeClassId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="gradeId")

    public Grade getGrade() {
        return this.grade;
    }
    
    public void setGrade(Grade grade) {
        this.grade = grade;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="classesId")

    public Classes getClasses() {
        return this.classes;
    }
    
    public void setClasses(Classes classes) {
        this.classes = classes;
    }
   








}