package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Attendance entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="attendance"
    ,catalog="edudb"
)

public class Attendance  implements java.io.Serializable {


    // Fields    

     private String attendanceId;
     private Classes classesByClassId;
     private Student student;
     private Classes classesByClassesId;
     private Timestamp createDate;
     private Integer type;


    // Constructors

    /** default constructor */
    public Attendance() {
    }

    
    /** full constructor */
    public Attendance(Classes classesByClassId, Student student, Classes classesByClassesId, Timestamp createDate, Integer type) {
        this.classesByClassId = classesByClassId;
        this.student = student;
        this.classesByClassesId = classesByClassesId;
        this.createDate = createDate;
        this.type = type;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="attendanceId", unique=true, nullable=false, length=32)

    public String getAttendanceId() {
        return this.attendanceId;
    }
    
    public void setAttendanceId(String attendanceId) {
        this.attendanceId = attendanceId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="classId")

    public Classes getClassesByClassId() {
        return this.classesByClassId;
    }
    
    public void setClassesByClassId(Classes classesByClassId) {
        this.classesByClassId = classesByClassId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="studentId")

    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="classesId")

    public Classes getClassesByClassesId() {
        return this.classesByClassesId;
    }
    
    public void setClassesByClassesId(Classes classesByClassesId) {
        this.classesByClassesId = classesByClassesId;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="type")

    public Integer getType() {
        return this.type;
    }
    
    public void setType(Integer type) {
        this.type = type;
    }
   








}