package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * SysRole entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="sys_role"
    ,catalog="edudb"
)

public class SysRole  implements java.io.Serializable {


    // Fields    

     private String roleId;
     private SysUser sysUser;
     private SysSubrole sysSubrole;
     private String rolerName;
     private Integer type;
     private Timestamp createDate;
     private Integer enable;
     private Integer isDel;
     private String userName;
     private Integer grade;
     private Set<SysUser> sysUsers = new HashSet<SysUser>(0);
     private Set<SysUserRole> sysUserRoles = new HashSet<SysUserRole>(0);
     private Set<SysSubrole> sysSubroles = new HashSet<SysSubrole>(0);
     private Set<SysRoleSchoolPower> sysRoleSchoolPowers = new HashSet<SysRoleSchoolPower>(0);
     private Set<SysRolePower> sysRolePowers = new HashSet<SysRolePower>(0);


    // Constructors

    /** default constructor */
    public SysRole() {
    }

    
    /** full constructor */
    public SysRole(SysUser sysUser, SysSubrole sysSubrole, String rolerName, Integer type, Timestamp createDate, Integer enable, Integer isDel, String userName, Integer grade, Set<SysUser> sysUsers, Set<SysUserRole> sysUserRoles, Set<SysSubrole> sysSubroles, Set<SysRoleSchoolPower> sysRoleSchoolPowers, Set<SysRolePower> sysRolePowers) {
        this.sysUser = sysUser;
        this.sysSubrole = sysSubrole;
        this.rolerName = rolerName;
        this.type = type;
        this.createDate = createDate;
        this.enable = enable;
        this.isDel = isDel;
        this.userName = userName;
        this.grade = grade;
        this.sysUsers = sysUsers;
        this.sysUserRoles = sysUserRoles;
        this.sysSubroles = sysSubroles;
        this.sysRoleSchoolPowers = sysRoleSchoolPowers;
        this.sysRolePowers = sysRolePowers;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="roleId", unique=true, nullable=false, length=32)

    public String getRoleId() {
        return this.roleId;
    }
    
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="userId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="subRoleId")

    public SysSubrole getSysSubrole() {
        return this.sysSubrole;
    }
    
    public void setSysSubrole(SysSubrole sysSubrole) {
        this.sysSubrole = sysSubrole;
    }
    
    @Column(name="rolerName", length=32)

    public String getRolerName() {
        return this.rolerName;
    }
    
    public void setRolerName(String rolerName) {
        this.rolerName = rolerName;
    }
    
    @Column(name="type")

    public Integer getType() {
        return this.type;
    }
    
    public void setType(Integer type) {
        this.type = type;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="enable")

    public Integer getEnable() {
        return this.enable;
    }
    
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
    
    @Column(name="userName", length=32)

    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    @Column(name="grade")

    public Integer getGrade() {
        return this.grade;
    }
    
    public void setGrade(Integer grade) {
        this.grade = grade;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysRole")

    public Set<SysUser> getSysUsers() {
        return this.sysUsers;
    }
    
    public void setSysUsers(Set<SysUser> sysUsers) {
        this.sysUsers = sysUsers;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysRole")

    public Set<SysUserRole> getSysUserRoles() {
        return this.sysUserRoles;
    }
    
    public void setSysUserRoles(Set<SysUserRole> sysUserRoles) {
        this.sysUserRoles = sysUserRoles;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysRole")

    public Set<SysSubrole> getSysSubroles() {
        return this.sysSubroles;
    }
    
    public void setSysSubroles(Set<SysSubrole> sysSubroles) {
        this.sysSubroles = sysSubroles;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysRole")

    public Set<SysRoleSchoolPower> getSysRoleSchoolPowers() {
        return this.sysRoleSchoolPowers;
    }
    
    public void setSysRoleSchoolPowers(Set<SysRoleSchoolPower> sysRoleSchoolPowers) {
        this.sysRoleSchoolPowers = sysRoleSchoolPowers;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysRole")

    public Set<SysRolePower> getSysRolePowers() {
        return this.sysRolePowers;
    }
    
    public void setSysRolePowers(Set<SysRolePower> sysRolePowers) {
        this.sysRolePowers = sysRolePowers;
    }
   








}