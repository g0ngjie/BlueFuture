package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * HistoryClass entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="history_class"
    ,catalog="edudb"
)

public class HistoryClass  implements java.io.Serializable {


    // Fields    

     private String historyClassId;
     private School school;
     private Classes classes;
     private String schoolName;
     private String className;
     private String intoYear;
     private String gradeName;
     private String classTeacherName;
     private String otherTeachers;
     private Integer isDel;
     private Integer type;
     private String otherTypeTeacherAndTel;


    // Constructors

    /** default constructor */
    public HistoryClass() {
    }

    
    /** full constructor */
    public HistoryClass(School school, Classes classes, String schoolName, String className, String intoYear, String gradeName, String classTeacherName, String otherTeachers, Integer isDel, Integer type, String otherTypeTeacherAndTel) {
        this.school = school;
        this.classes = classes;
        this.schoolName = schoolName;
        this.className = className;
        this.intoYear = intoYear;
        this.gradeName = gradeName;
        this.classTeacherName = classTeacherName;
        this.otherTeachers = otherTeachers;
        this.isDel = isDel;
        this.type = type;
        this.otherTypeTeacherAndTel = otherTypeTeacherAndTel;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="historyClassId", unique=true, nullable=false, length=32)

    public String getHistoryClassId() {
        return this.historyClassId;
    }
    
    public void setHistoryClassId(String historyClassId) {
        this.historyClassId = historyClassId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="schoolId")

    public School getSchool() {
        return this.school;
    }
    
    public void setSchool(School school) {
        this.school = school;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="classesId")

    public Classes getClasses() {
        return this.classes;
    }
    
    public void setClasses(Classes classes) {
        this.classes = classes;
    }
    
    @Column(name="schoolName", length=32)

    public String getSchoolName() {
        return this.schoolName;
    }
    
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
    
    @Column(name="className", length=32)

    public String getClassName() {
        return this.className;
    }
    
    public void setClassName(String className) {
        this.className = className;
    }
    
    @Column(name="intoYear", length=32)

    public String getIntoYear() {
        return this.intoYear;
    }
    
    public void setIntoYear(String intoYear) {
        this.intoYear = intoYear;
    }
    
    @Column(name="gradeName", length=32)

    public String getGradeName() {
        return this.gradeName;
    }
    
    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
    
    @Column(name="classTeacherName", length=32)

    public String getClassTeacherName() {
        return this.classTeacherName;
    }
    
    public void setClassTeacherName(String classTeacherName) {
        this.classTeacherName = classTeacherName;
    }
    
    @Column(name="otherTeachers", length=1024)

    public String getOtherTeachers() {
        return this.otherTeachers;
    }
    
    public void setOtherTeachers(String otherTeachers) {
        this.otherTeachers = otherTeachers;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
    
    @Column(name="type")

    public Integer getType() {
        return this.type;
    }
    
    public void setType(Integer type) {
        this.type = type;
    }
    
    @Column(name="otherTypeTeacherAndTel", length=256)

    public String getOtherTypeTeacherAndTel() {
        return this.otherTypeTeacherAndTel;
    }
    
    public void setOtherTypeTeacherAndTel(String otherTypeTeacherAndTel) {
        this.otherTypeTeacherAndTel = otherTypeTeacherAndTel;
    }
   








}