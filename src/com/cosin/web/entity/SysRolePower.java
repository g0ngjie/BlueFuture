package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * SysRolePower entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="sys_role_power"
    ,catalog="edudb"
)

public class SysRolePower  implements java.io.Serializable {


    // Fields    

     private String rolePowerId;
     private SysRole sysRole;
     private String powerCode;
     private String parentId;


    // Constructors

    /** default constructor */
    public SysRolePower() {
    }

    
    /** full constructor */
    public SysRolePower(SysRole sysRole, String powerCode, String parentId) {
        this.sysRole = sysRole;
        this.powerCode = powerCode;
        this.parentId = parentId;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="rolePowerId", unique=true, nullable=false, length=32)

    public String getRolePowerId() {
        return this.rolePowerId;
    }
    
    public void setRolePowerId(String rolePowerId) {
        this.rolePowerId = rolePowerId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="roleId")

    public SysRole getSysRole() {
        return this.sysRole;
    }
    
    public void setSysRole(SysRole sysRole) {
        this.sysRole = sysRole;
    }
    
    @Column(name="powerCode", length=32)

    public String getPowerCode() {
        return this.powerCode;
    }
    
    public void setPowerCode(String powerCode) {
        this.powerCode = powerCode;
    }
    
    @Column(name="parentId", length=32)

    public String getParentId() {
        return this.parentId;
    }
    
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
   








}