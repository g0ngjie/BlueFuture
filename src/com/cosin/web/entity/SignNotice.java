package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * SignNotice entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="sign_notice"
    ,catalog="edudb"
)

public class SignNotice  implements java.io.Serializable {


    // Fields    

     private String signNoticeId;
     private Notice notice;
     private SysUser sysUser;
     private Student student;
     private Timestamp creareDate;


    // Constructors

    /** default constructor */
    public SignNotice() {
    }

    
    /** full constructor */
    public SignNotice(Notice notice, SysUser sysUser, Student student, Timestamp creareDate) {
        this.notice = notice;
        this.sysUser = sysUser;
        this.student = student;
        this.creareDate = creareDate;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="signNoticeId", unique=true, nullable=false, length=32)

    public String getSignNoticeId() {
        return this.signNoticeId;
    }
    
    public void setSignNoticeId(String signNoticeId) {
        this.signNoticeId = signNoticeId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="noticeId")

    public Notice getNotice() {
        return this.notice;
    }
    
    public void setNotice(Notice notice) {
        this.notice = notice;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="userId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="studentId")

    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }
    
    @Column(name="creareDate", length=0)

    public Timestamp getCreareDate() {
        return this.creareDate;
    }
    
    public void setCreareDate(Timestamp creareDate) {
        this.creareDate = creareDate;
    }
   








}