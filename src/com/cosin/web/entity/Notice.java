package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Notice entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="notice"
    ,catalog="edudb"
)

public class Notice  implements java.io.Serializable {


    // Fields    

     private String noticeId;
     private School school;
     private SysUser sysUser;
     private Integer isDraft;
     private Integer type;
     private String title;
     private String content;
     private String img;
     private Integer lookNum;
     private Timestamp createDate;
     private Integer enable;
     private Integer isDel;
     private String price;
     private Set<Unreadnotice> unreadnotices = new HashSet<Unreadnotice>(0);
     private Set<PayBill> payBills = new HashSet<PayBill>(0);
     private Set<SignupRecord> signupRecords = new HashSet<SignupRecord>(0);
     private Set<SignPrice> signPrices = new HashSet<SignPrice>(0);
     private Set<SignNotice> signNotices = new HashSet<SignNotice>(0);
     private Set<NoticeSub> noticeSubs = new HashSet<NoticeSub>(0);


    // Constructors

    /** default constructor */
    public Notice() {
    }

    
    /** full constructor */
    public Notice(School school, SysUser sysUser, Integer isDraft, Integer type, String title, String content, String img, Integer lookNum, Timestamp createDate, Integer enable, Integer isDel, String price, Set<Unreadnotice> unreadnotices, Set<PayBill> payBills, Set<SignupRecord> signupRecords, Set<SignPrice> signPrices, Set<SignNotice> signNotices, Set<NoticeSub> noticeSubs) {
        this.school = school;
        this.sysUser = sysUser;
        this.isDraft = isDraft;
        this.type = type;
        this.title = title;
        this.content = content;
        this.img = img;
        this.lookNum = lookNum;
        this.createDate = createDate;
        this.enable = enable;
        this.isDel = isDel;
        this.price = price;
        this.unreadnotices = unreadnotices;
        this.payBills = payBills;
        this.signupRecords = signupRecords;
        this.signPrices = signPrices;
        this.signNotices = signNotices;
        this.noticeSubs = noticeSubs;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="noticeId", unique=true, nullable=false, length=32)

    public String getNoticeId() {
        return this.noticeId;
    }
    
    public void setNoticeId(String noticeId) {
        this.noticeId = noticeId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="schoolId")

    public School getSchool() {
        return this.school;
    }
    
    public void setSchool(School school) {
        this.school = school;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="createUserId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
    
    @Column(name="isDraft")

    public Integer getIsDraft() {
        return this.isDraft;
    }
    
    public void setIsDraft(Integer isDraft) {
        this.isDraft = isDraft;
    }
    
    @Column(name="type")

    public Integer getType() {
        return this.type;
    }
    
    public void setType(Integer type) {
        this.type = type;
    }
    
    @Column(name="title", length=64)

    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    @Column(name="content", length=2048)

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    @Column(name="img", length=64)

    public String getImg() {
        return this.img;
    }
    
    public void setImg(String img) {
        this.img = img;
    }
    
    @Column(name="lookNum")

    public Integer getLookNum() {
        return this.lookNum;
    }
    
    public void setLookNum(Integer lookNum) {
        this.lookNum = lookNum;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="enable")

    public Integer getEnable() {
        return this.enable;
    }
    
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
    
    @Column(name="price", length=32)

    public String getPrice() {
        return this.price;
    }
    
    public void setPrice(String price) {
        this.price = price;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="notice")

    public Set<Unreadnotice> getUnreadnotices() {
        return this.unreadnotices;
    }
    
    public void setUnreadnotices(Set<Unreadnotice> unreadnotices) {
        this.unreadnotices = unreadnotices;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="notice")

    public Set<PayBill> getPayBills() {
        return this.payBills;
    }
    
    public void setPayBills(Set<PayBill> payBills) {
        this.payBills = payBills;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="notice")

    public Set<SignupRecord> getSignupRecords() {
        return this.signupRecords;
    }
    
    public void setSignupRecords(Set<SignupRecord> signupRecords) {
        this.signupRecords = signupRecords;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="notice")

    public Set<SignPrice> getSignPrices() {
        return this.signPrices;
    }
    
    public void setSignPrices(Set<SignPrice> signPrices) {
        this.signPrices = signPrices;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="notice")

    public Set<SignNotice> getSignNotices() {
        return this.signNotices;
    }
    
    public void setSignNotices(Set<SignNotice> signNotices) {
        this.signNotices = signNotices;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="notice")

    public Set<NoticeSub> getNoticeSubs() {
        return this.noticeSubs;
    }
    
    public void setNoticeSubs(Set<NoticeSub> noticeSubs) {
        this.noticeSubs = noticeSubs;
    }
   








}