package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Recipe entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="recipe"
    ,catalog="edudb"
)

public class Recipe  implements java.io.Serializable {


    // Fields    

     private String recipeId;
     private RecipeRecord recipeRecord;
     private String content;
     private Timestamp createDate;


    // Constructors

    /** default constructor */
    public Recipe() {
    }

    
    /** full constructor */
    public Recipe(RecipeRecord recipeRecord, String content, Timestamp createDate) {
        this.recipeRecord = recipeRecord;
        this.content = content;
        this.createDate = createDate;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="recipeId", unique=true, nullable=false, length=32)

    public String getRecipeId() {
        return this.recipeId;
    }
    
    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="recipeRecordId")

    public RecipeRecord getRecipeRecord() {
        return this.recipeRecord;
    }
    
    public void setRecipeRecord(RecipeRecord recipeRecord) {
        this.recipeRecord = recipeRecord;
    }
    
    @Column(name="content", length=256)

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
   








}