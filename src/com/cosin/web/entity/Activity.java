package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Activity entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="activity"
    ,catalog="edudb"
)

public class Activity  implements java.io.Serializable {


    // Fields    

     private String activityId;
     private School school;
     private SysUser sysUser;
     private Integer isDraft;
     private Integer type;
     private String title;
     private String content;
     private String img;
     private Integer lookNum;
     private Timestamp createDate;
     private Integer enable;
     private Integer isDel;
     private Set<ActivitySchool> activitySchools = new HashSet<ActivitySchool>(0);


    // Constructors

    /** default constructor */
    public Activity() {
    }

    
    /** full constructor */
    public Activity(School school, SysUser sysUser, Integer isDraft, Integer type, String title, String content, String img, Integer lookNum, Timestamp createDate, Integer enable, Integer isDel, Set<ActivitySchool> activitySchools) {
        this.school = school;
        this.sysUser = sysUser;
        this.isDraft = isDraft;
        this.type = type;
        this.title = title;
        this.content = content;
        this.img = img;
        this.lookNum = lookNum;
        this.createDate = createDate;
        this.enable = enable;
        this.isDel = isDel;
        this.activitySchools = activitySchools;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="activityId", unique=true, nullable=false, length=32)

    public String getActivityId() {
        return this.activityId;
    }
    
    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="schoolId")

    public School getSchool() {
        return this.school;
    }
    
    public void setSchool(School school) {
        this.school = school;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="createUserId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
    
    @Column(name="isDraft")

    public Integer getIsDraft() {
        return this.isDraft;
    }
    
    public void setIsDraft(Integer isDraft) {
        this.isDraft = isDraft;
    }
    
    @Column(name="type")

    public Integer getType() {
        return this.type;
    }
    
    public void setType(Integer type) {
        this.type = type;
    }
    
    @Column(name="title", length=64)

    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    @Column(name="content", length=2048)

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    @Column(name="img", length=64)

    public String getImg() {
        return this.img;
    }
    
    public void setImg(String img) {
        this.img = img;
    }
    
    @Column(name="lookNum")

    public Integer getLookNum() {
        return this.lookNum;
    }
    
    public void setLookNum(Integer lookNum) {
        this.lookNum = lookNum;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="enable")

    public Integer getEnable() {
        return this.enable;
    }
    
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="activity")

    public Set<ActivitySchool> getActivitySchools() {
        return this.activitySchools;
    }
    
    public void setActivitySchools(Set<ActivitySchool> activitySchools) {
        this.activitySchools = activitySchools;
    }
   








}