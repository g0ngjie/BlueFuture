package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;


/**
 * StudentLeave entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="student_leave"
    ,catalog="edudb"
)

public class StudentLeave  implements java.io.Serializable {


    // Fields    

     private String studentLeaveId;
     private Employee employee;
     private SysUser sysUser;
     private Classes classesByClassId;
     private Student student;
     private Classes classesByClassesId;
     private Integer state;
     private String content;
     private Date startDate;
     private Date endDate;
     private Timestamp createDate;
     private Timestamp clTime;


    // Constructors

    /** default constructor */
    public StudentLeave() {
    }

    
    /** full constructor */
    public StudentLeave(Employee employee, SysUser sysUser, Classes classesByClassId, Student student, Classes classesByClassesId, Integer state, String content, Date startDate, Date endDate, Timestamp createDate, Timestamp clTime) {
        this.employee = employee;
        this.sysUser = sysUser;
        this.classesByClassId = classesByClassId;
        this.student = student;
        this.classesByClassesId = classesByClassesId;
        this.state = state;
        this.content = content;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createDate = createDate;
        this.clTime = clTime;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="studentLeaveId", unique=true, nullable=false, length=32)

    public String getStudentLeaveId() {
        return this.studentLeaveId;
    }
    
    public void setStudentLeaveId(String studentLeaveId) {
        this.studentLeaveId = studentLeaveId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="employeeId")

    public Employee getEmployee() {
        return this.employee;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="userId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="classId")

    public Classes getClassesByClassId() {
        return this.classesByClassId;
    }
    
    public void setClassesByClassId(Classes classesByClassId) {
        this.classesByClassId = classesByClassId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="studentId")

    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="classesId")

    public Classes getClassesByClassesId() {
        return this.classesByClassesId;
    }
    
    public void setClassesByClassesId(Classes classesByClassesId) {
        this.classesByClassesId = classesByClassesId;
    }
    
    @Column(name="state")

    public Integer getState() {
        return this.state;
    }
    
    public void setState(Integer state) {
        this.state = state;
    }
    
    @Column(name="content", length=2048)

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="startDate", length=0)

    public Date getStartDate() {
        return this.startDate;
    }
    
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="endDate", length=0)

    public Date getEndDate() {
        return this.endDate;
    }
    
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="clTime", length=0)

    public Timestamp getClTime() {
        return this.clTime;
    }
    
    public void setClTime(Timestamp clTime) {
        this.clTime = clTime;
    }
   








}