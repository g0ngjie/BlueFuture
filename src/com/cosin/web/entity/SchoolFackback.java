package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * SchoolFackback entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="school_fackback"
    ,catalog="edudb"
)

public class SchoolFackback  implements java.io.Serializable {


    // Fields    

     private String schoolFackbackId;
     private School school;
     private Student student;
     private String content;
     private Timestamp createDate;
     private Integer enable;
     private Integer isDel;


    // Constructors

    /** default constructor */
    public SchoolFackback() {
    }

    
    /** full constructor */
    public SchoolFackback(School school, Student student, String content, Timestamp createDate, Integer enable, Integer isDel) {
        this.school = school;
        this.student = student;
        this.content = content;
        this.createDate = createDate;
        this.enable = enable;
        this.isDel = isDel;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="schoolFackbackId", unique=true, nullable=false, length=32)

    public String getSchoolFackbackId() {
        return this.schoolFackbackId;
    }
    
    public void setSchoolFackbackId(String schoolFackbackId) {
        this.schoolFackbackId = schoolFackbackId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="schoolId")

    public School getSchool() {
        return this.school;
    }
    
    public void setSchool(School school) {
        this.school = school;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="studentId")

    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }
    
    @Column(name="content", length=256)

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="enable")

    public Integer getEnable() {
        return this.enable;
    }
    
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
   








}