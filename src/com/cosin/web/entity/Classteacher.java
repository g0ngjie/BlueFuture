package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Classteacher entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="classteacher"
    ,catalog="edudb"
)

public class Classteacher  implements java.io.Serializable {


    // Fields    

     private String classTeacherId;
     private Employee employee;
     private Classes classes;
     private SysSubrole sysSubrole;


    // Constructors

    /** default constructor */
    public Classteacher() {
    }

	/** minimal constructor */
    public Classteacher(Employee employee) {
        this.employee = employee;
    }
    
    /** full constructor */
    public Classteacher(Employee employee, Classes classes, SysSubrole sysSubrole) {
        this.employee = employee;
        this.classes = classes;
        this.sysSubrole = sysSubrole;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="classTeacherId", unique=true, nullable=false, length=32)

    public String getClassTeacherId() {
        return this.classTeacherId;
    }
    
    public void setClassTeacherId(String classTeacherId) {
        this.classTeacherId = classTeacherId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="employeeId", nullable=false)

    public Employee getEmployee() {
        return this.employee;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="classesId")

    public Classes getClasses() {
        return this.classes;
    }
    
    public void setClasses(Classes classes) {
        this.classes = classes;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="subRoleId")

    public SysSubrole getSysSubrole() {
        return this.sysSubrole;
    }
    
    public void setSysSubrole(SysSubrole sysSubrole) {
        this.sysSubrole = sysSubrole;
    }
   








}