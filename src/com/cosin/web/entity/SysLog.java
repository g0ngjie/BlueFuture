package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * SysLog entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="sys_log"
    ,catalog="edudb"
)

public class SysLog  implements java.io.Serializable {


    // Fields    

     private String logId;
     private SysUser sysUser;
     private String funName;
     private String action;
     private String content;
     private Timestamp createDate;


    // Constructors

    /** default constructor */
    public SysLog() {
    }

    
    /** full constructor */
    public SysLog(SysUser sysUser, String funName, String action, String content, Timestamp createDate) {
        this.sysUser = sysUser;
        this.funName = funName;
        this.action = action;
        this.content = content;
        this.createDate = createDate;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="logId", unique=true, nullable=false, length=32)

    public String getLogId() {
        return this.logId;
    }
    
    public void setLogId(String logId) {
        this.logId = logId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="userId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
    
    @Column(name="funName", length=32)

    public String getFunName() {
        return this.funName;
    }
    
    public void setFunName(String funName) {
        this.funName = funName;
    }
    
    @Column(name="action", length=32)

    public String getAction() {
        return this.action;
    }
    
    public void setAction(String action) {
        this.action = action;
    }
    
    @Column(name="content", length=32)

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
   








}