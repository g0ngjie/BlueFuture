package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Student entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="student"
    ,catalog="edudb"
)

public class Student  implements java.io.Serializable {


    // Fields    

     private String studentId;
     private Parents parents;
     private School school;
     private SysUser sysUser;
     private Classes classes;
     private String studentName;
     private String studentCode;
     private Timestamp createDate;
     private Integer enable;
     private Integer isDefault;
     private Integer isDel;
     private String studentIcon;
     private String embCode;
     private Set<SignupRecord> signupRecords = new HashSet<SignupRecord>(0);
     private Set<Attendance> attendances = new HashSet<Attendance>(0);
     private Set<Unreadnotice> unreadnotices = new HashSet<Unreadnotice>(0);
     private Set<PayBill> payBills = new HashSet<PayBill>(0);
     private Set<StudentLeave> studentLeaves = new HashSet<StudentLeave>(0);
     private Set<Parentsstudent> parentsstudents = new HashSet<Parentsstudent>(0);
     private Set<SchoolFackback> schoolFackbacks = new HashSet<SchoolFackback>(0);
     private Set<Parents> parentses = new HashSet<Parents>(0);
     private Set<SignNotice> signNotices = new HashSet<SignNotice>(0);
     private Set<SignPrice> signPrices = new HashSet<SignPrice>(0);


    // Constructors

    /** default constructor */
    public Student() {
    }

    
    /** full constructor */
    public Student(Parents parents, School school, SysUser sysUser, Classes classes, String studentName, String studentCode, Timestamp createDate, Integer enable, Integer isDefault, Integer isDel, String studentIcon, String embCode, Set<SignupRecord> signupRecords, Set<Attendance> attendances, Set<Unreadnotice> unreadnotices, Set<PayBill> payBills, Set<StudentLeave> studentLeaves, Set<Parentsstudent> parentsstudents, Set<SchoolFackback> schoolFackbacks, Set<Parents> parentses, Set<SignNotice> signNotices, Set<SignPrice> signPrices) {
        this.parents = parents;
        this.school = school;
        this.sysUser = sysUser;
        this.classes = classes;
        this.studentName = studentName;
        this.studentCode = studentCode;
        this.createDate = createDate;
        this.enable = enable;
        this.isDefault = isDefault;
        this.isDel = isDel;
        this.studentIcon = studentIcon;
        this.embCode = embCode;
        this.signupRecords = signupRecords;
        this.attendances = attendances;
        this.unreadnotices = unreadnotices;
        this.payBills = payBills;
        this.studentLeaves = studentLeaves;
        this.parentsstudents = parentsstudents;
        this.schoolFackbacks = schoolFackbacks;
        this.parentses = parentses;
        this.signNotices = signNotices;
        this.signPrices = signPrices;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="studentId", unique=true, nullable=false, length=32)

    public String getStudentId() {
        return this.studentId;
    }
    
    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="parentId")

    public Parents getParents() {
        return this.parents;
    }
    
    public void setParents(Parents parents) {
        this.parents = parents;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="schoolId")

    public School getSchool() {
        return this.school;
    }
    
    public void setSchool(School school) {
        this.school = school;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="createUserId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="classesId")

    public Classes getClasses() {
        return this.classes;
    }
    
    public void setClasses(Classes classes) {
        this.classes = classes;
    }
    
    @Column(name="studentName", length=24)

    public String getStudentName() {
        return this.studentName;
    }
    
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
    
    @Column(name="studentCode", length=32)

    public String getStudentCode() {
        return this.studentCode;
    }
    
    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="enable")

    public Integer getEnable() {
        return this.enable;
    }
    
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
    @Column(name="isDefault")

    public Integer getIsDefault() {
        return this.isDefault;
    }
    
    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
    
    @Column(name="studentIcon", length=32)

    public String getStudentIcon() {
        return this.studentIcon;
    }
    
    public void setStudentIcon(String studentIcon) {
        this.studentIcon = studentIcon;
    }
    
    @Column(name="embCode", length=32)

    public String getEmbCode() {
        return this.embCode;
    }
    
    public void setEmbCode(String embCode) {
        this.embCode = embCode;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="student")

    public Set<SignupRecord> getSignupRecords() {
        return this.signupRecords;
    }
    
    public void setSignupRecords(Set<SignupRecord> signupRecords) {
        this.signupRecords = signupRecords;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="student")

    public Set<Attendance> getAttendances() {
        return this.attendances;
    }
    
    public void setAttendances(Set<Attendance> attendances) {
        this.attendances = attendances;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="student")

    public Set<Unreadnotice> getUnreadnotices() {
        return this.unreadnotices;
    }
    
    public void setUnreadnotices(Set<Unreadnotice> unreadnotices) {
        this.unreadnotices = unreadnotices;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="student")

    public Set<PayBill> getPayBills() {
        return this.payBills;
    }
    
    public void setPayBills(Set<PayBill> payBills) {
        this.payBills = payBills;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="student")

    public Set<StudentLeave> getStudentLeaves() {
        return this.studentLeaves;
    }
    
    public void setStudentLeaves(Set<StudentLeave> studentLeaves) {
        this.studentLeaves = studentLeaves;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="student")

    public Set<Parentsstudent> getParentsstudents() {
        return this.parentsstudents;
    }
    
    public void setParentsstudents(Set<Parentsstudent> parentsstudents) {
        this.parentsstudents = parentsstudents;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="student")

    public Set<SchoolFackback> getSchoolFackbacks() {
        return this.schoolFackbacks;
    }
    
    public void setSchoolFackbacks(Set<SchoolFackback> schoolFackbacks) {
        this.schoolFackbacks = schoolFackbacks;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="student")

    public Set<Parents> getParentses() {
        return this.parentses;
    }
    
    public void setParentses(Set<Parents> parentses) {
        this.parentses = parentses;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="student")

    public Set<SignNotice> getSignNotices() {
        return this.signNotices;
    }
    
    public void setSignNotices(Set<SignNotice> signNotices) {
        this.signNotices = signNotices;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="student")

    public Set<SignPrice> getSignPrices() {
        return this.signPrices;
    }
    
    public void setSignPrices(Set<SignPrice> signPrices) {
        this.signPrices = signPrices;
    }
   








}