package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * SysRoleSchoolPower entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="sys_role_school_power"
    ,catalog="edudb"
)

public class SysRoleSchoolPower  implements java.io.Serializable {


    // Fields    

     private String subRoleSchoolPowerId;
     private School school;
     private SysRole sysRole;


    // Constructors

    /** default constructor */
    public SysRoleSchoolPower() {
    }

    
    /** full constructor */
    public SysRoleSchoolPower(School school, SysRole sysRole) {
        this.school = school;
        this.sysRole = sysRole;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="subRoleSchoolPowerId", unique=true, nullable=false, length=32)

    public String getSubRoleSchoolPowerId() {
        return this.subRoleSchoolPowerId;
    }
    
    public void setSubRoleSchoolPowerId(String subRoleSchoolPowerId) {
        this.subRoleSchoolPowerId = subRoleSchoolPowerId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="schoolId")

    public School getSchool() {
        return this.school;
    }
    
    public void setSchool(School school) {
        this.school = school;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="roleId")

    public SysRole getSysRole() {
        return this.sysRole;
    }
    
    public void setSysRole(SysRole sysRole) {
        this.sysRole = sysRole;
    }
   








}