package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * SysUser entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="sys_user"
    ,catalog="edudb"
)

public class SysUser  implements java.io.Serializable {


    // Fields    

     private String userId;
     private SysRole sysRole;
     private SysSubrole sysSubrole;
     private String userName;
     private Integer type;
     private String loginName;
     private String pwd;
     private String mobile;
     private String email;
     private Integer sex;
     private String icon;
     private Timestamp createDate;
     private Integer isDel;
     private Integer enable;
     private Timestamp activityNum;
     private String emobCode;
     private Timestamp noticeNum;
     private Timestamp recipeNum;
     private Timestamp leaveNum;
     private Timestamp kaoqinNum;
     private Set<Employee> employeesForCreateUserId = new HashSet<Employee>(0);
     private Set<Unreadnotice> unreadnotices = new HashSet<Unreadnotice>(0);
     private Set<School> schools = new HashSet<School>(0);
     private Set<Student> students = new HashSet<Student>(0);
     private Set<RelativeStudent> relativeStudents = new HashSet<RelativeStudent>(0);
     private Set<Notice> notices = new HashSet<Notice>(0);
     private Set<RecipeRecord> recipeRecords = new HashSet<RecipeRecord>(0);
     private Set<Classes> classeses = new HashSet<Classes>(0);
     private Set<Remarks> remarksesForUserId = new HashSet<Remarks>(0);
     private Set<SysRole> sysRoles = new HashSet<SysRole>(0);
     private Set<SysLog> sysLogs = new HashSet<SysLog>(0);
     private Set<SignPrice> signPrices = new HashSet<SignPrice>(0);
     private Set<SignNotice> signNotices = new HashSet<SignNotice>(0);
     private Set<SysUserRole> sysUserRoles = new HashSet<SysUserRole>(0);
     private Set<StudentLeave> studentLeaves = new HashSet<StudentLeave>(0);
     private Set<Parents> parentses = new HashSet<Parents>(0);
     private Set<Activity> activities = new HashSet<Activity>(0);
     private Set<Remarks> remarksesForRemarkUserId = new HashSet<Remarks>(0);
     private Set<Employee> employeesForUserId = new HashSet<Employee>(0);


    // Constructors

    /** default constructor */
    public SysUser() {
    }

    
    /** full constructor */
    public SysUser(SysRole sysRole, SysSubrole sysSubrole, String userName, Integer type, String loginName, String pwd, String mobile, String email, Integer sex, String icon, Timestamp createDate, Integer isDel, Integer enable, Timestamp activityNum, String emobCode, Timestamp noticeNum, Timestamp recipeNum, Timestamp leaveNum, Timestamp kaoqinNum, Set<Employee> employeesForCreateUserId, Set<Unreadnotice> unreadnotices, Set<School> schools, Set<Student> students, Set<RelativeStudent> relativeStudents, Set<Notice> notices, Set<RecipeRecord> recipeRecords, Set<Classes> classeses, Set<Remarks> remarksesForUserId, Set<SysRole> sysRoles, Set<SysLog> sysLogs, Set<SignPrice> signPrices, Set<SignNotice> signNotices, Set<SysUserRole> sysUserRoles, Set<StudentLeave> studentLeaves, Set<Parents> parentses, Set<Activity> activities, Set<Remarks> remarksesForRemarkUserId, Set<Employee> employeesForUserId) {
        this.sysRole = sysRole;
        this.sysSubrole = sysSubrole;
        this.userName = userName;
        this.type = type;
        this.loginName = loginName;
        this.pwd = pwd;
        this.mobile = mobile;
        this.email = email;
        this.sex = sex;
        this.icon = icon;
        this.createDate = createDate;
        this.isDel = isDel;
        this.enable = enable;
        this.activityNum = activityNum;
        this.emobCode = emobCode;
        this.noticeNum = noticeNum;
        this.recipeNum = recipeNum;
        this.leaveNum = leaveNum;
        this.kaoqinNum = kaoqinNum;
        this.employeesForCreateUserId = employeesForCreateUserId;
        this.unreadnotices = unreadnotices;
        this.schools = schools;
        this.students = students;
        this.relativeStudents = relativeStudents;
        this.notices = notices;
        this.recipeRecords = recipeRecords;
        this.classeses = classeses;
        this.remarksesForUserId = remarksesForUserId;
        this.sysRoles = sysRoles;
        this.sysLogs = sysLogs;
        this.signPrices = signPrices;
        this.signNotices = signNotices;
        this.sysUserRoles = sysUserRoles;
        this.studentLeaves = studentLeaves;
        this.parentses = parentses;
        this.activities = activities;
        this.remarksesForRemarkUserId = remarksesForRemarkUserId;
        this.employeesForUserId = employeesForUserId;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="userId", unique=true, nullable=false, length=32)

    public String getUserId() {
        return this.userId;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="roleId")

    public SysRole getSysRole() {
        return this.sysRole;
    }
    
    public void setSysRole(SysRole sysRole) {
        this.sysRole = sysRole;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="subRoleId")

    public SysSubrole getSysSubrole() {
        return this.sysSubrole;
    }
    
    public void setSysSubrole(SysSubrole sysSubrole) {
        this.sysSubrole = sysSubrole;
    }
    
    @Column(name="userName", length=32)

    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    @Column(name="type")

    public Integer getType() {
        return this.type;
    }
    
    public void setType(Integer type) {
        this.type = type;
    }
    
    @Column(name="loginName", length=64)

    public String getLoginName() {
        return this.loginName;
    }
    
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }
    
    @Column(name="pwd", length=32)

    public String getPwd() {
        return this.pwd;
    }
    
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    
    @Column(name="mobile", length=64)

    public String getMobile() {
        return this.mobile;
    }
    
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    
    @Column(name="email", length=32)

    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    @Column(name="sex")

    public Integer getSex() {
        return this.sex;
    }
    
    public void setSex(Integer sex) {
        this.sex = sex;
    }
    
    @Column(name="icon", length=16)

    public String getIcon() {
        return this.icon;
    }
    
    public void setIcon(String icon) {
        this.icon = icon;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="isDel")

    public Integer getIsDel() {
        return this.isDel;
    }
    
    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }
    
    @Column(name="enable")

    public Integer getEnable() {
        return this.enable;
    }
    
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
    @Column(name="activityNum", length=0)

    public Timestamp getActivityNum() {
        return this.activityNum;
    }
    
    public void setActivityNum(Timestamp activityNum) {
        this.activityNum = activityNum;
    }
    
    @Column(name="emobCode", length=32)

    public String getEmobCode() {
        return this.emobCode;
    }
    
    public void setEmobCode(String emobCode) {
        this.emobCode = emobCode;
    }
    
    @Column(name="noticeNum", length=0)

    public Timestamp getNoticeNum() {
        return this.noticeNum;
    }
    
    public void setNoticeNum(Timestamp noticeNum) {
        this.noticeNum = noticeNum;
    }
    
    @Column(name="recipeNum", length=0)

    public Timestamp getRecipeNum() {
        return this.recipeNum;
    }
    
    public void setRecipeNum(Timestamp recipeNum) {
        this.recipeNum = recipeNum;
    }
    
    @Column(name="leaveNum", length=0)

    public Timestamp getLeaveNum() {
        return this.leaveNum;
    }
    
    public void setLeaveNum(Timestamp leaveNum) {
        this.leaveNum = leaveNum;
    }
    
    @Column(name="kaoqinNum", length=0)

    public Timestamp getKaoqinNum() {
        return this.kaoqinNum;
    }
    
    public void setKaoqinNum(Timestamp kaoqinNum) {
        this.kaoqinNum = kaoqinNum;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUserByCreateUserId")

    public Set<Employee> getEmployeesForCreateUserId() {
        return this.employeesForCreateUserId;
    }
    
    public void setEmployeesForCreateUserId(Set<Employee> employeesForCreateUserId) {
        this.employeesForCreateUserId = employeesForCreateUserId;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<Unreadnotice> getUnreadnotices() {
        return this.unreadnotices;
    }
    
    public void setUnreadnotices(Set<Unreadnotice> unreadnotices) {
        this.unreadnotices = unreadnotices;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<School> getSchools() {
        return this.schools;
    }
    
    public void setSchools(Set<School> schools) {
        this.schools = schools;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<Student> getStudents() {
        return this.students;
    }
    
    public void setStudents(Set<Student> students) {
        this.students = students;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<RelativeStudent> getRelativeStudents() {
        return this.relativeStudents;
    }
    
    public void setRelativeStudents(Set<RelativeStudent> relativeStudents) {
        this.relativeStudents = relativeStudents;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<Notice> getNotices() {
        return this.notices;
    }
    
    public void setNotices(Set<Notice> notices) {
        this.notices = notices;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<RecipeRecord> getRecipeRecords() {
        return this.recipeRecords;
    }
    
    public void setRecipeRecords(Set<RecipeRecord> recipeRecords) {
        this.recipeRecords = recipeRecords;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<Classes> getClasseses() {
        return this.classeses;
    }
    
    public void setClasseses(Set<Classes> classeses) {
        this.classeses = classeses;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUserByUserId")

    public Set<Remarks> getRemarksesForUserId() {
        return this.remarksesForUserId;
    }
    
    public void setRemarksesForUserId(Set<Remarks> remarksesForUserId) {
        this.remarksesForUserId = remarksesForUserId;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<SysRole> getSysRoles() {
        return this.sysRoles;
    }
    
    public void setSysRoles(Set<SysRole> sysRoles) {
        this.sysRoles = sysRoles;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<SysLog> getSysLogs() {
        return this.sysLogs;
    }
    
    public void setSysLogs(Set<SysLog> sysLogs) {
        this.sysLogs = sysLogs;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<SignPrice> getSignPrices() {
        return this.signPrices;
    }
    
    public void setSignPrices(Set<SignPrice> signPrices) {
        this.signPrices = signPrices;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<SignNotice> getSignNotices() {
        return this.signNotices;
    }
    
    public void setSignNotices(Set<SignNotice> signNotices) {
        this.signNotices = signNotices;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<SysUserRole> getSysUserRoles() {
        return this.sysUserRoles;
    }
    
    public void setSysUserRoles(Set<SysUserRole> sysUserRoles) {
        this.sysUserRoles = sysUserRoles;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<StudentLeave> getStudentLeaves() {
        return this.studentLeaves;
    }
    
    public void setStudentLeaves(Set<StudentLeave> studentLeaves) {
        this.studentLeaves = studentLeaves;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<Parents> getParentses() {
        return this.parentses;
    }
    
    public void setParentses(Set<Parents> parentses) {
        this.parentses = parentses;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUser")

    public Set<Activity> getActivities() {
        return this.activities;
    }
    
    public void setActivities(Set<Activity> activities) {
        this.activities = activities;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUserByRemarkUserId")

    public Set<Remarks> getRemarksesForRemarkUserId() {
        return this.remarksesForRemarkUserId;
    }
    
    public void setRemarksesForRemarkUserId(Set<Remarks> remarksesForRemarkUserId) {
        this.remarksesForRemarkUserId = remarksesForRemarkUserId;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="sysUserByUserId")

    public Set<Employee> getEmployeesForUserId() {
        return this.employeesForUserId;
    }
    
    public void setEmployeesForUserId(Set<Employee> employeesForUserId) {
        this.employeesForUserId = employeesForUserId;
    }
   








}