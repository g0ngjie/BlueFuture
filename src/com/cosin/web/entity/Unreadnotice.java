package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 * Unreadnotice entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="unreadnotice"
    ,catalog="edudb"
)

public class Unreadnotice  implements java.io.Serializable {


    // Fields    

     private String unReadNoticeId;
     private Notice notice;
     private SysUser sysUser;
     private Student student;


    // Constructors

    /** default constructor */
    public Unreadnotice() {
    }

    
    /** full constructor */
    public Unreadnotice(Notice notice, SysUser sysUser, Student student) {
        this.notice = notice;
        this.sysUser = sysUser;
        this.student = student;
    }

   
    // Property accessors
    @GenericGenerator(name="generator", strategy="uuid.hex")@Id @GeneratedValue(generator="generator")
    
    @Column(name="unReadNoticeId", unique=true, nullable=false, length=32)

    public String getUnReadNoticeId() {
        return this.unReadNoticeId;
    }
    
    public void setUnReadNoticeId(String unReadNoticeId) {
        this.unReadNoticeId = unReadNoticeId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="noticeId")

    public Notice getNotice() {
        return this.notice;
    }
    
    public void setNotice(Notice notice) {
        this.notice = notice;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="userId")

    public SysUser getSysUser() {
        return this.sysUser;
    }
    
    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="studentId")

    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }
   








}