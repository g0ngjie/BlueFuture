package com.cosin.web.service.blue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.utils.DateUtils;
import com.cosin.utils.HttpBodyClient;
import com.cosin.web.dao.blue.IEmployeeDao;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

@Service
@Transactional
public class EmployeeManager implements IEmployeeManager {

	@Autowired
	private IEmployeeDao employeeDao;

	/**
	 * 分页
	 */
	@Override
	public List getEmployeeLimit(String SsName,String SsTel, int start, int limit) {
		return employeeDao.getEmployeeLimit(SsName,SsTel, start, limit);
	}

	/**
	 * 数量
	 */
	@Override
	public int getEmployeeCount(String SsName,String SsTel) {
		return employeeDao.getEmployeeCount(SsName,SsTel);
	}

	/**
	 * 获取角色名称
	 */
	@Override
	public List<SysRole> getlistRole(String schoolId) {
		List listRes = new ArrayList();
		List<SysRoleSchoolPower> roleSchoolPowerList = employeeDao.getSysRoleSchoolPowerBySchoolId(schoolId);
		for (int i = 0; i < roleSchoolPowerList.size(); i++) {
			SysRoleSchoolPower roleSchoolPower = roleSchoolPowerList.get(i);
			SysRole sysRole = roleSchoolPower.getSysRole();
			if(sysRole.getRolerName().equals("管理员")){
				continue;
			}
			if(sysRole.getRolerName().equals("总园长")){
				continue;
			}
			Map map = new HashMap();
			if(sysRole.getIsDel() == 0){
				
				map.put("roleName", sysRole.getRolerName());
				map.put("roleId", sysRole.getRoleId());
				listRes.add(map);
			}
			
		}
		return listRes;
	}

	/**
	 * 通过 roleId 获取 类型
	 */
	@Override
	public List<SysSubrole> getlistSubRole(String roleId) {
		List listRes = new ArrayList();
		List<SysSubrole> listSysSubrole = employeeDao.getlistSubRole(roleId);
		for (int i = 0; i < listSysSubrole.size(); i++) {
			SysSubrole sysSubrole = listSysSubrole.get(i);
			Map map = new HashMap();
			map.put("typeName", sysSubrole.getTypeName());
			map.put("subRoleId", sysSubrole.getSubRoleId());
			listRes.add(map);
		}
		return listRes;
	}


	/**
	 * 获取school 园区   操作者级别
	 * 思路：控制层
	 */
	@Override
	public List getlistSchool(String createUserId) {
		List listRes = new ArrayList();
		SysUser sysUser = employeeDao.getSysUserById(createUserId);
		//通过createUserId 获取 role角色 
		String roleId = sysUser.getSysRole().getRoleId();
		//通过roleId 查询中间表sysRoleSchoolPower
		List<SysRoleSchoolPower> listsysRoleSchoolPower = employeeDao.getListsysRoleSchoolPower(roleId);
		for (int i = 0; i < listsysRoleSchoolPower.size(); i++) {
			SysRoleSchoolPower sysRoleSchoolPower = listsysRoleSchoolPower.get(i);
			Map map = new HashMap();
			map.put("schoolName", sysRoleSchoolPower.getSchool().getSchoolName());
			map.put("schoolId", sysRoleSchoolPower.getSchool().getSchoolId());
			listRes.add(map);
		}
		return listRes;
	}

	/**
	 * 保存职工
	 */
	@Override
	public void saveSchool(String isNianJ,String userId, String employeeId, String schoolId,
			String roleId, String subRoleId, String sysUserName, String sex,
			String tel, String mode,String radom) {

		if (mode.equals("edit")) {
			
			Employee employee = employeeDao.getEmployeeById(employeeId);
			employee.setCreateDate(new Timestamp(new Date().getTime()));
			
			if(isNianJ!= null&&!"".equals(isNianJ)){
				employee.setGrade(new Integer(isNianJ));
			}
			//保存园区
			if(schoolId != null && schoolId != ""){
				
				School school = employeeDao.getSchoolById(schoolId);
				employee.setSchool(school);
			}
			
			SysUser sysUser = employee.getSysUserByUserId();
			if(sex.equals("男")){
				sysUser.setSex(1);
			}else if(sex.equals("女")){
				sysUser.setSex(0);
			}
			sysUser.setUserName(sysUserName);
			String loginName = sysUser.getLoginName();
			//查询所有type = 1的 家长端
			SysUser chaUser = employeeDao.getsysUserParentByLoginName(loginName);
			if(chaUser != null&&!"".equals(chaUser)){
				chaUser.setMobile(tel);
				chaUser.setLoginName(tel);
				sysUser.setMobile(tel);
				sysUser.setLoginName(tel);
				employeeDao.saveSysUser(chaUser);
			}else{
				sysUser.setMobile(tel);
				sysUser.setLoginName(tel);
			}
			//职工随机数
			//String radom1 = DateUtils.getCharAndNumr(6);
			//sysUser.setEmobCode(radom1);
			//查出sysSubRole表
			if("".equals(subRoleId))
			{
				SysSubrole sysSubrole = employeeDao.getSysSubRoleByRoleId(roleId);
				sysUser.setSysSubrole(sysSubrole);
				employeeDao.saveSysUser(sysUser);
			}else{
				SysSubrole sysSubrole = employeeDao.getSysSubRoleById(subRoleId);
				sysUser.setSysSubrole(sysSubrole);
				employeeDao.saveSysUser(sysUser);
			}
			
//			SysSubrole sysSubrole = employeeDao.getSysSubRoleByRoleId(roleId);
			
			//查出sysRole表
			SysRole sysRole = employeeDao.getSysRoleById(roleId);
			String useId = employee.getSysUserByUserId().getUserId();
			List<SysUserRole> sysUserRoleList = employeeDao.getSysUserRoleByUserId(useId); 
			for (int i = 0; i < sysUserRoleList.size(); i++) {
				SysUserRole sysUserRole1 = sysUserRoleList.get(i);
				employeeDao.delObj(sysUserRole1);
			}
			//保存角色
			SysUserRole sysUserRole = new SysUserRole();
			sysUserRole.setSysUser(sysUser);
			sysUserRole.setSysRole(sysRole);
			employeeDao.saveObj(sysUserRole);
			
			employee.setSysUserByUserId(sysUser);
			SysUser createUser = employeeDao.getSysUserById(userId);
			//随机数
			//String radom = DateUtils.getCharAndNumr(6);
			//createUser.setEmobCode(radom);
			employee.setSysUserByCreateUserId(createUser);
			
			employeeDao.save(employee);
			
		}else{
			
			Employee employee = new Employee();
			if(isNianJ!= null&&!"".equals(isNianJ)){
				employee.setGrade(new Integer(isNianJ));
			}
			employee.setIsDel(0);
			employee.setCreateDate(new Timestamp(new Date().getTime()));
			employee.setEnable(0);
			//保存园区
			if(schoolId != null && schoolId != ""){
				
				School school = employeeDao.getSchoolById(schoolId);
				employee.setSchool(school);
			}
			//创建sysUser表
			SysUser sysUser = new SysUser();
			sysUser.setIsDel(0);
			sysUser.setCreateDate(new Timestamp(new Date().getTime()));
			
			sysUser.setActivityNum(new Timestamp(new Date().getTime()));
			sysUser.setNoticeNum(new Timestamp(new Date().getTime()));
			sysUser.setRecipeNum(new Timestamp(new Date().getTime()));
			sysUser.setLeaveNum(new Timestamp(new Date().getTime()));
			sysUser.setKaoqinNum(new Timestamp(new Date().getTime()));
			//职工登录名
			sysUser.setLoginName(tel);
			
			//查询 老师姓名是否存在
			SysUser parenUser = employeeDao.getParenUseByLogin(tel);
			if(parenUser != null&&!"".equals(parenUser)){
				String pwd = parenUser.getPwd();
				if("000000".equals(pwd)){
					sysUser.setPwd("000000");
				}else{
					sysUser.setPwd(pwd);
				}
			}else{
				
				//职工 密码
				sysUser.setPwd("000000");
			}
			
			//职工随机数
//			String radom1 = DateUtils.getCharAndNumr(6);
			sysUser.setEmobCode(radom);
			//默认头像
			sysUser.setIcon("headIcon.png");
			//职工 为2
			sysUser.setType(2);
			sysUser.setEnable(0);
			//查出sysSubRole表
			if("".equals(subRoleId))
			{
				SysSubrole sysSubrole = employeeDao.getSysSubRoleByRoleId(roleId);
				sysUser.setSysSubrole(sysSubrole);
			}else{
				SysSubrole sysSubrole = employeeDao.getSysSubRoleById(subRoleId);
				sysUser.setSysSubrole(sysSubrole);
			}
			
//			SysSubrole sysSubrole = employeeDao.getSysSubRoleById(subRoleId);
//			SysSubrole sysSubrole = employeeDao.getSysSubRoleByRoleId(roleId);
			//保存类型
			//保存性别 0女 1男
			int sexx =new Integer (sex);
			sysUser.setSex(sexx);
			sysUser.setMobile(tel);
			sysUser.setUserName(sysUserName);
			//保存 添加的user 到sysUser表
			employeeDao.saveSysUser(sysUser);
			
			
			//查出sysRole表
			SysRole sysRole = employeeDao.getSysRoleById(roleId);
			//保存角色
			SysUserRole sysUserRole = new SysUserRole();
			sysUserRole.setSysUser(sysUser);
			sysUserRole.setSysRole(sysRole);
			employeeDao.saveObj(sysUserRole);
			
			employee.setSysUserByUserId(sysUser);
			//保存employee表中的 createId 创建者
			SysUser createUser = employeeDao.getSysUserById(userId);
			//随机数
//			String radom = DateUtils.getCharAndNumr(6);
//			createUser.setEmobCode(radom);
			employee.setSysUserByCreateUserId(createUser);
			
			employeeDao.save(employee);
			
			if(sysRole.getRolerName().equals("分园长")){
				School school = employeeDao.getSchoolById(schoolId);
				school.setEmployee(employee);
				employeeDao.saveSchool(school);
				
			}
			
		}
		
	}

	/**
	 * 删除操作
	 */
	@Override
	public void delEmployee(String delKeys) {
		
		
		Employee employee = employeeDao.getEmployeeById(delKeys);
		employee.setIsDel(1);
		//删除关联的sysUser表
		SysUser sysUser = employee.getSysUserByUserId();
		sysUser.setIsDel(1);
		employeeDao.saveSysUser(sysUser);
		SysUserRole sysUserRole = employeeDao.getSysUserRoleByUseri(sysUser.getUserId());
		if(sysUserRole!=null){
			employeeDao.delSysUserRole(sysUserRole);
		}
		
		
		employeeDao.save(employee);
		
	}

	/* 
	 * 根据EmployeeId 查出对象Employee
	 * 下午4:46:52
	 */
	@Override
	public Employee getEmployeeById(String employeeId) {
		return employeeDao.getEmployeeById(employeeId);
	}

	/**
	 * 通过userId  查询 user
	 */
	@Override
	public SysUser getSysUserById(String userId) {
		return employeeDao.getSysUserById(userId);
	}

	@Override
	public void save(Employee employee) {
		employeeDao.save(employee);
		
	}

	/**
	 * 判断导入
	 */
	@Override
	public String chaReport(String userId,String dirPre, InputStream inputStream) {
		String listData ="";
		List list = new ArrayList();
        try {  
  
            Workbook wb = WorkbookFactory.create(inputStream);  
            
            Sheet sheet =  wb.getSheetAt(0);
            
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            	
            	if(!"".equals(listData)){
            		listData +="数据异常";
            		break;
            	}
                Row row = sheet.getRow(i); 
               
                if (row == null) {
                    
                    continue;
                }  
                
              
                int x = row.getLastCellNum();
                Cell cellLast = row.getCell(0); 
                String cc = ConvertCellStr(cellLast, null);
                if(cc==null){
                	break;
                }
                if(x<6){
                	break;
                }
                for (int j = 0; j <= row.getLastCellNum(); j++) {  

                	String cellStr = null; 
                	Cell cell = row.getCell(j); 
                	
                    cellStr = ConvertCellStr(cell, cellStr);
                    
                    
                    if(j == 0){//视频名称测试
                    	School school = employeeDao.getByName(cellStr);
                    	if("".equals(school)||school==null){
                			listData +="第"+i+"条,查无此园区,";
                			break;
                		}
                    }else if(j == 1){//英文标题
                    	String[] str = cellStr.split("——");
                    	if(str.length>1){
                    		//查出sysRole表
                    		SysRole sysRole = employeeDao.getSysRoleByName(str[0]);
                    		if("".equals(sysRole)||sysRole==null){
                    			listData +="第"+i+"条,查无此角色,";
                    			break;
                    		}
                    		SysSubrole sysSubrole = employeeDao.getSysSubRoleByName(str[1]);
                    		if("".equals(sysSubrole)||sysSubrole==null){
                    			listData +="第"+i+"条,查无此子角色,";
                    			break;
                    		}
                    	}else{
                    		SysRole sysRole = employeeDao.getSysRoleByName(cellStr);
                    		if("".equals(sysRole)||sysRole==null){
                    			listData +="第"+i+"条,查无此角色,";
                    			break;
                    		}
                    	}
            			
                    	
                    }else if(j == 2){//所属频道
                    	
                    }else if(j == 3){//所属媒体
                    	
                    }else if(j == 4){//标签
                    	boolean isFind = false;
                    	for (int k = 0; k < list.size(); k++) {
							String tel = list.get(k).toString();
							if(tel.equals(cellStr)){
								isFind = true;
							}
						}
                    	if(isFind){
                    		listData +="此数据重复:"+cellStr;
                			break;
                    	}
                    	SysUser chaUser = employeeDao.chaSysUserByLogin(cellStr);
                    	list.add(cellStr);
                    	if(!"".equals(chaUser)&&chaUser!=null){
                			listData +="第"+i+"条,该职工已存在,";
                			break;
                		}
                    }else if(j == 5){//关键词
                    	
                    }else if(j == 6){
                    	if(!"".equals(listData))
                    		break;
                    	
                    }
                }
               
            }
        } catch (InvalidFormatException e) {  
            e.printStackTrace();  
            listData = "请导入正确表格";
        } catch (IOException e) {  
            e.printStackTrace();  
            listData = "请导入正确表格";
        } finally {  
            if (inputStream != null) {  
                try { 
                    inputStream.close();  
                } catch (IOException e) {  
                    e.printStackTrace();  
                    listData = "请导入正确表格";
                }  
            } else {  
                
            }  
        }
		return listData;  
	}
	
	/**
	 * 批量导入
	 */
	@Override
	public String readReport(String userId,String dirPre, InputStream inputStream) {
		String listData ="";
        try {  
  
            Workbook wb = WorkbookFactory.create(inputStream);  
            Sheet sheet =  wb.getSheetAt(0);
            
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {  
            	int  p = 0;
            	String eveData = "";
            	String schoolNam = "";
            	String roleLevel = "";
            	Employee employee = new Employee();  
            	SysUser sysUser = new SysUser();
            	SysUserRole sysUserRole = new SysUserRole();
            	
                Row row = sheet.getRow(i); 
               // System.out.println(row);
                if (row == null) {
                    
                    continue;
                    
                }  
                
              //  employee = saveVcVideoData(employee);
                int x = row.getLastCellNum();
                Cell cellLast = row.getCell(0); 
                String cc = ConvertCellStr(cellLast, null);
                if(cc==null){
                	break;
                }
                if(x<6){
                	break;
                }
                for (int j = 0; j <= row.getLastCellNum(); j++) {  

                	String cellStr = null; 
                	Cell cell = row.getCell(j); 
                	
                    cellStr = ConvertCellStr(cell, cellStr);
                    
                    
                    if(j == 0){// 
                    	School school = employeeDao.getByName(cellStr);
                    	if("".equals(school)||school==null){
                			listData +="第"+i+"条,查无此园区,";
                			break;
                		}
                    	schoolNam = school.getSchoolName();
                    	employee.setSchool(school);
                    }else if(j == 1){//英文标题
                    	String[] str = cellStr.split("——");
//                    	if(str[0]=="")
                    	if(str.length>1){
                    		//查出sysRole表
                    		SysRole sysRole = employeeDao.getSysRoleByName(str[0]);
                    		if("".equals(sysRole)||sysRole==null){
                    			listData +="第"+i+"条,查无此角色,";
                    			break;
                    		}
                    		sysUserRole.setSysRole(sysRole);
                    		SysSubrole sysSubrole = employeeDao.getSysSubRoleByName(str[1]);
                    		if("".equals(sysSubrole)||sysSubrole==null){
                    			listData +="第"+i+"条,查无此子角色,";
                    			break;
                    		}
                    		roleLevel = sysSubrole.getRoleLevel();
                    		sysUser.setSysSubrole(sysSubrole);
                    		//sysUser.setSysRole(sysRole);
                    	}else{
                    		SysRole sysRole = employeeDao.getSysRoleByName(cellStr);
                    		if("".equals(sysRole)||sysRole==null){
                    			listData +="第"+i+"条,查无此角色,";
                    			break;
                    		}
                    		if(sysRole.getRolerName().equals("分园长")){
                				School school = employeeDao.getSchoolByName(schoolNam);
                				employeeDao.save(employee);
                				school.setEmployee(employee);
                				employeeDao.saveSchool(school);
                			}
                    		String roleId = sysRole.getRoleId();
                    		SysSubrole sysSubrole = employeeDao.getSysSubRoleByRoleId(sysRole.getRoleId());
                    		sysUser.setSysSubrole(sysSubrole);
                    		sysUserRole.setSysRole(sysRole);
                    		roleLevel = sysSubrole.getRoleLevel();
                    	}
            			
                    	
                    }else if(j == 2){// 
                    	sysUser.setUserName(cellStr);
                    	
                    }else if(j == 3){// 
                    	if("男".equals(cellStr)){
                    		sysUser.setSex(1);
                    	}else{
                    		sysUser.setSex(0);
                    	}
                    	
                    }else if(j == 4){//标签
                    	SysUser chaUser = employeeDao.chaSysUserByLogin(cellStr);
                    	if(!"".equals(chaUser)&&chaUser!=null){
                			listData +="第"+i+"条,该职工已存在,";
                			break;
                		}
                    	
            			//查询 老师姓名是否存在
            			SysUser parenUser = employeeDao.getParenUseByLogin(cellStr);
            			if(parenUser != null&&!"".equals(parenUser)){
            				String pwd = parenUser.getPwd();
            				if("000000".equals(pwd)){
            					sysUser.setPwd("000000");
            				}else{
            					sysUser.setPwd(pwd);
            				}
            			}else{
            				
            				//职工 密码
            				sysUser.setPwd("000000");
            			}
                    	sysUser.setMobile(cellStr);
                    	sysUser.setLoginName(cellStr);
                    	
                    }else if(j == 5){//关键词
                    	if("启用".equals(cellStr)){
                    		employee.setEnable(0);
                    	}else{
                    		employee.setEnable(1);
                    	}
                    	
                    }else if(j == 6){
                    	if(!roleLevel.equals("年级")){
                    		//listData +="第"+i+"条,该角色级别不为年级,";
                			//break;
                    		sysUser.setCreateDate(new Timestamp(new Date().getTime()));
                        	
                        	sysUser.setActivityNum(new Timestamp(new Date().getTime()));
                			sysUser.setNoticeNum(new Timestamp(new Date().getTime()));
                			sysUser.setRecipeNum(new Timestamp(new Date().getTime()));
                			sysUser.setLeaveNum(new Timestamp(new Date().getTime()));
                			sysUser.setKaoqinNum(new Timestamp(new Date().getTime()));
                        	
                        	sysUser.setType(2);
                        	sysUser.setIsDel(0);
                        	sysUser.setEnable(0);
                        	sysUser.setPwd("000000");
                        	String num = DateUtils.getCharAndNumr(6);
                        	sysUser.setEmobCode(num);
                        	try {
                        		HttpBodyClient.postBody("https://a1.easemob.com/bluefuture/future/users", "{\"username\":\""+num+"\", \"password\":\"111\"}");
                        	} catch (Exception e) {
                        		e.printStackTrace();
                        	}
                        	sysUser.setIcon("headIcon.png");
                        	employeeDao.saveSysUser(sysUser);
                        	
                        	
                        	sysUserRole.setSysUser(sysUser);
                        	employeeDao.saveUserRole(sysUserRole);
                        	
                        	
                        	
                        	employee.setSysUserByUserId(sysUser);
                        	employee.setCreateDate(new Timestamp(new Date().getTime()));
                        	employee.setIsDel(0);
                        	SysUser sysUser2 = employeeDao.getSysUserById(userId);
                        	employee.setSysUserByCreateUserId(sysUser2);
                        	employeeDao.save(employee);
                    	}
                    	else{
                    		if(cellStr.equals("托班")){
                        		employee.setGrade(1);
                        	}else if(cellStr.equals("小班")){
                        		employee.setGrade(2);
                        	}else if(cellStr.equals("中班")){
                        		employee.setGrade(3);
                        	}else if(cellStr.equals("大班")){
                        		employee.setGrade(4);
                        	}else if(cellStr.equals("学前班")){
                        		employee.setGrade(5);
                        	}
                    		sysUser.setCreateDate(new Timestamp(new Date().getTime()));
                        	
                        	sysUser.setActivityNum(new Timestamp(new Date().getTime()));
                			sysUser.setNoticeNum(new Timestamp(new Date().getTime()));
                			sysUser.setRecipeNum(new Timestamp(new Date().getTime()));
                			sysUser.setLeaveNum(new Timestamp(new Date().getTime()));
                			sysUser.setKaoqinNum(new Timestamp(new Date().getTime()));
                        	
                        	sysUser.setType(2);
                        	sysUser.setIsDel(0);
                        	sysUser.setEnable(0);
                        	sysUser.setPwd("000000");
                        	String num = DateUtils.getCharAndNumr(6);
                        	sysUser.setEmobCode(num);
                        	
							try {
								HttpBodyClient.postBody("https://a1.easemob.com/bluefuture/future/users", "{\"username\":\""+num+"\", \"password\":\"111\"}");
							} catch (Exception e) {
								e.printStackTrace();
							}
                        	sysUser.setIcon("headIcon.png");
                        	employeeDao.saveSysUser(sysUser);
                        	
                        	sysUserRole.setSysUser(sysUser);
                        	employeeDao.saveUserRole(sysUserRole);
                        	
                        	employee.setSysUserByUserId(sysUser);
                        	employee.setCreateDate(new Timestamp(new Date().getTime()));
                        	employee.setIsDel(0);
                        	SysUser sysUser2 = employeeDao.getSysUserById(userId);
                        	employee.setSysUserByCreateUserId(sysUser2);
                        	employeeDao.save(employee);
                    	}
                    	
                    }
                }
               
            }
        } catch (InvalidFormatException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            if (inputStream != null) {  
                try { 
                    inputStream.close();  
                } catch (IOException e) {  
                    e.printStackTrace();  
                }  
            } else {  
                
            }  
        }
		return listData;  
	}
	/** 
     * 把单元格内的类型转换至String类型 
     */ 
	 private String ConvertCellStr(Cell cell, String cellStr) {  
		  	
		 if(cell==null){
			 return "";
		 }
	        switch (cell.getCellType()) {  
	  
	        case Cell.CELL_TYPE_STRING:  
	        	// 读取String  
	            cellStr = cell.getStringCellValue().toString();  
	            break;
	  
	        case Cell.CELL_TYPE_BOOLEAN:  
	        	 // 得到Boolean对象的方法  
	            cellStr = String.valueOf(cell.getBooleanCellValue());  
	            break;  
	  
	        case Cell.CELL_TYPE_NUMERIC:  
	  
	        	// 先看是否是日期格式  
	            if (DateUtil.isCellDateFormatted(cell)) {  
	            	// 读取日期格式  
	                cellStr = cell.getDateCellValue().toString();  
	  
	            } else {  
	  
	            	// 读取数字  
	                cellStr = String.valueOf(cell.getNumericCellValue());  
	            }  
	            break;  
	  
	        case Cell.CELL_TYPE_FORMULA:  
	        	// 读取公式  
	            cellStr = cell.getCellFormula().toString();  
	            break;  
	        }  
	        return cellStr;  
	    }

	/**
	 * 2016年9月29日下午5:56:01
	 *  龚杰
	 *  注释：根据userId 查询role
	 */
	@Override
	public List<SysUserRole> getSysUserRoleByUserId(String useId) {
		
		return employeeDao.getSysUserRoleByUserId(useId);
	}

	/**
	 * 2016年10月10日下午6:32:12
	 *  龚杰
	 *  注释：
	 */
	@Override
	public SysSubrole getSysSubRoleByRoleId(String roleId) {
		return employeeDao.getSysSubRoleByRoleId(roleId);
	}

	/**
	 * 2016年10月14日下午2:29:17
	 *  阳朔
	 *  注释:查询是否存在
	 */
	@Override
	public SysUser chaSysUserByLogin(String tel) {
		return employeeDao.chaSysUserByLogin(tel);
	}

	/**
	 * 2016年10月22日下午3:08:09
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUser getSysUserBySubRoleId(String subRoleId) {
		
		return employeeDao.getSysUserBySubRoleId(subRoleId);
	}

	/**
	 * 2016年10月22日下午3:24:17
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysRoleSchoolPower getSysRoleSchoolPowerByRoleId(String roleId) {
		return employeeDao.getSysRoleSchoolPowerByRoleId(roleId);
	}

	/**
	 * 2016年10月22日下午3:57:45
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<Employee> getEmployeeBySchoolId(String schoolId, String ssName,
			String ssTel, Integer integer, Integer integer2) {
		return employeeDao.getEmployeeBySchoolId(schoolId,ssName,ssTel,integer,integer2);
	}

	/**
	 * 2016年10月22日下午4:15:25
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Employee getEmployeeByUserId(String userId) {
		// TODO Auto-generated method stub
		Employee employee = employeeDao.getEmployeeByIUserId(userId);
		return employee;
	}

	/**
	 * 2016年10月26日下午1:48:28
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUserRole chaSysUserRoleByUserId(String userId) {
		return employeeDao.chaSysUserRoleByUserId(userId);
	}

	/**
	 * 2016年10月26日下午2:58:45
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getEmployeeCountBySchoolId(String schoolId, String ssName,
			String ssTel) {
		return employeeDao.getEmployeeCountBySchoolId(schoolId,ssName,ssTel);
	}

	/**
	 * 2016年10月26日下午6:24:05
	 *  阳朔
	 *  注释:
	 */
	@Override
	public String getRoleLevelName(String roleId) {
		List<SysSubrole> listSysSubrole = employeeDao.getlistSubRole(roleId);
		SysSubrole subrole = listSysSubrole.get(0);
		if(subrole != null){
			String roleLe = subrole.getRoleLevel();
			return roleLe;
		}else{
			
			return null;
		}
	}

	/**
	 * 2016年10月30日下午12:55:47
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List getEmployeeCountGai(String ssName, String ssTel) {
		// TODO Auto-generated method stub
		return employeeDao.getEmployeeCountGai(ssName,ssTel);
	}

	/**
	 * 2016年10月30日下午1:06:43
	 *  阳朔
	 *  注释: 分页数量改
	 */
	@Override
	public List<Employee> getEmployeeBySchoolIdCount(String schoolId,
			String ssName, String ssTel) {
		// TODO Auto-generated method stub
		return employeeDao.getEmployeeBySchoolIdCount(schoolId,ssName,ssTel);
	}
	
	
	
}
