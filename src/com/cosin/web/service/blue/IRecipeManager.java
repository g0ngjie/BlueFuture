package com.cosin.web.service.blue;

import java.text.ParseException;
import java.util.List;

import com.cosin.web.entity.Recipe;
import com.cosin.web.entity.RecipeRecord;
import com.cosin.web.entity.RecipeSchool;
import com.cosin.web.entity.SysRoleSchoolPower;

public interface IRecipeManager {

	//分页
    public List getRecipeLimit(String startDate, int start, int limit) throws ParseException;
    //数量
    public int getRecipeCount(String startDate) throws ParseException;
    //获取SubjectId
  	public RecipeRecord getRecipeId(String recipeId);
    //添加 
  	public void saveRecipe(RecipeRecord recipe);
  	 //删除
    public void recipeDelId(String recipeId);
    //添加数据
//    public void addRecipe(String mode,String title,String user,String text,String recipeId);
    //根据id查询
//    public SysUser getUser2(String user);
  	
  	//草稿箱列表查询 
	public List<RecipeRecord> getDraftLimit(String startDate, Integer integer,
			Integer integer2) throws ParseException;
	
	/**
	 * 保存 食谱
	 */
	public void saveRecipe(String recipeId, String userId, String beginDate,
			String breakfast, String light1, String lunch, String light2,
			String dinner, String mode,String schoolVar,String draf);
	
	/**
	 *通过recipeRecordId获取 对象
	 * @param recipeRecordId
	 * @return
	 */
	public List<RecipeSchool> getRecipeSchoolById(String recipeRecordId);
	
	/**
	 * 撤回操作 存至草稿箱
	 * @param recipeId
	 */
	public void roolBackRecipe(String recipeId);
	
	/**
	 * 通过recipeRecordId 查询 recipeRecordd
	 * @param recipeId
	 * @return
	 */
	public RecipeRecord getRecipeRecordById(String recipeId);
	
	/**
	 * 根据recipeRecordId  查询  食谱
	 * @param recipeId
	 * @return
	 */
	public List<Recipe> getRecipeById(String recipeId);
	/**
	 * 2016年10月10日下午3:04:28
	 *  龚杰
	 *  注释：
	 * @throws ParseException 
	 */
	public int getDraftCount(String startDate) throws ParseException;
	/**
	 * 2016年10月23日下午4:55:18
	 *  阳朔
	 *  注释:
	 * @throws ParseException 
	 */
	public List<RecipeSchool> getRecipeSchoolLimit(String schoolsId,
			String startDate, Integer integer, Integer integer2) throws ParseException;
	/**
	 * 2016年12月8日下午5:24:32
	 *  阳朔
	 *  注释:
	 * @throws ParseException 
	 */
	public List<RecipeSchool> getRecipeDraftSchoolLimit(String schoolsId,
			String startDate, Integer integer, Integer integer2) throws ParseException;
	

}
