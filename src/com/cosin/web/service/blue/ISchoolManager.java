package com.cosin.web.service.blue;

import java.util.List;

import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysArea;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUserRole;

public interface ISchoolManager {

	// 获取园区管理列表
	List<School> getSchoolList(int integer, int integer2);

	// 获取园区管理分页
	int getSchoolfenye(String ssName);

	// 编辑园区
	School findbjSchool(String schoolId);

	// 查询园区是否有重复
	List<School> chaSchool(String schoolName);

	// schoolName,enable, schooluserName,city,provinceName,schoolId, mode
	// 添加园区
	void saveSchool(String schoolName, String schooluserName, String cityId,
			String provinceName,String schoolId, String mode, String userId);

	// 通过id 获取 园区 （删除）
	School getBySchoolId(String key);

	// 删除园区
	void delSchool(String key);

	List<SysArea> getSysArea();

	List<SysArea> getSysCity(String cityId);

	List<Employee> getEmployee(String schoolId);

	/**
	 * 2016年10月22日下午1:36:36
	 *  阳朔
	 *  注释:
	 */
	SysUserRole getSysUserRoleByUserId(String userId);

	/**
	 * 2016年10月22日下午1:40:22
	 *  阳朔
	 *  注释:
	 */
	SysSubrole getSysSubRoleByRoleId(String roleId);

	/**
	 * 2016年10月22日下午1:55:21
	 *  阳朔
	 *  注释:
	 */
	SysRoleSchoolPower getRoleSchoolPowerByRoleId(String roleId);

	/**
	 * 2016年10月24日下午8:03:01
	 *  阳朔
	 *  注释:
	 */
	List<School> getSchoolList(String schoolsId, Integer integer,
			Integer integer2);

	/**
	 * 2016年10月26日下午4:38:26
	 *  阳朔
	 *  注释:
	 */
	List getSchoolfenyeBySchoolId(String schoolsId, String ssName);

	/**
	 * 2016年11月15日下午12:08:36
	 *  阳朔
	 *  注释:
	 */
	List<Classes> getClassesBySchoolId(String schoolId);

	/**
	 * 2016年11月15日下午12:17:03
	 *  阳朔
	 *  注释:
	 */
	List<Employee> getEmployeeBySchoolId(String schoolId);

}
