/*package com.cosin.web.service.blue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.blue.IAreaListDao;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.SysArea;


@Service
@Transactional
public class AreaManager implements IAreaManager{

	@Autowired
	private IAreaListDao areaListDao;
	
	@Override
	public List<SysArea> getSysArea() {
		List listRes = new ArrayList();
		List<SysArea> listSysAreaData = areaListDao.getSysArea();
		for (int i = 0; i < listSysAreaData.size(); i++) {
			SysArea sysArea = listSysAreaData.get(i);
			Map map = new HashMap();
			map.put("areaName", sysArea.getAreaName());
			map.put("areaId", sysArea.getAreaId());
			listRes.add(map);
		}
		return listRes;
	}
	
	@Override
	public List<SysArea> getSysCity(String cityId) {
		List listRes = new ArrayList();
		List<SysArea> listSysAreaData = areaListDao.getSysCity(cityId);
		for (int i = 0; i < listSysAreaData.size(); i++) {
			SysArea sysArea = listSysAreaData.get(i);
			Map map = new HashMap();
			map.put("cityName", sysArea.getAreaName());
			map.put("cityId", sysArea.getAreaId());
			listRes.add(map);
		}
		return listRes;
	}
	
	@Override
	public List<Employee> getEmployee() {
		List listRes = new ArrayList();
		List<Employee> listEmployeeData = areaListDao.getEmployee();
		for (int i = 0; i < listEmployeeData.size(); i++) {
			Employee employee = listEmployeeData.get(i);
			Map map = new HashMap();
			map.put("employeeName", employee.getEmployeeName());
			map.put("employeeId", employee.getEmployeeId());
			listRes.add(map);
		}
		return listRes;
	}
	
	
	
}
*/