package com.cosin.web.service.blue;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.blue.ISchoolDao;
import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysArea;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;
import com.cosin.web.service.system.ISysManager;

@Service
@Transactional
public class SchoolManager implements ISchoolManager {

	@Autowired
	private ISchoolDao schoolDao;
	@Autowired
	private ISysManager sysDao;

	public void setSchoolDao(ISchoolDao schoolDao) {
		this.schoolDao = schoolDao;
	}

	// 获取园区管理列表
	@Override
	public List<School> getSchoolList(int start, int limit) {

		return schoolDao.getSchool(start, limit);
	}

	// 获取园区管理分页数据
	@Override
	public int getSchoolfenye(String ssName) {
		return schoolDao.getRolefenye(ssName);
	}

	// 编辑园区School
	@Override
	public School findbjSchool(String schoolId) {

		return schoolDao.getBySchool(schoolId);
	}

	// 查询是否有重复园区
	@Override
	public List<School> chaSchool(String schoolName) {
		return schoolDao.chaSchool(schoolName);
	}

	
	@Override
	public void saveSchool(String schoolName,String employeeId,String cityId,String provinceId,
			String schoolId,String mode,String userId) {
		if (mode.equals("edit")) {

			School school = schoolDao.getBySchoolId(schoolId);
			school.setSchoolName(schoolName);
			school.setEnable(0);
			

			SysArea sys = schoolDao.getSysAreaById(provinceId);
			school.setSysAreaByProvinceId(sys);

			SysArea city1 = schoolDao.getSysCityById(cityId);
			school.setSysAreaByCityId(city1);
		
			if(employeeId != null&&!"".equals(employeeId)){
				Employee employee = schoolDao.getEmployeeByUserId(employeeId);
				school.setEmployee(employee);
			}
			
			
			schoolDao.save(school);
			

		} else {
//省份 城市 园长
			School school = new School();
			school.setSchoolName(schoolName);
			school.setEnable(0);
			school.setCreateDate(new Timestamp(new Date().getTime()));
			school.setIsDel(0);
			
			SysArea city1 = schoolDao.getSysCityById(cityId);
			school.setSysAreaByCityId(city1);
			SysArea province = schoolDao.getSysAreaById(provinceId);
			school.setSysAreaByProvinceId(province);
			
			SysUser user = schoolDao.findUserByUserId(userId);
			school.setSysUser(user);
			
			
			if(employeeId != null&&!"".equals(employeeId)){
				
				Employee employee = schoolDao.getEmployeeByUserId(employeeId);
				school.setEmployee(employee);
			}
			schoolDao.save(school);
			/*List<SysSubrole> subList = schoolDao.getSysSubroleList();
			for (int i = 0; i < subList.size(); i++) {
				SysSubrole subrole = subList.get(i);
				SysRole sysRole = subrole.getSysRole();
				SysRoleSchoolPower sysRoleSchoolPower = new SysRoleSchoolPower();
				sysRoleSchoolPower.setSchool(school);
				sysRoleSchoolPower.setSysRole(sysRole);
				schoolDao.saveSysRoleSchoolPower(sysRoleSchoolPower);
			}*/
			
			List<SysRole> sysRoleList = schoolDao.getSysRoleAll();
			for (int i = 0; i < sysRoleList.size(); i++) {
				SysRole sysRole = sysRoleList.get(i);
				int type = sysRole.getType();
				if(type == 1){
					SysRoleSchoolPower roleSchoolPower = new SysRoleSchoolPower();
					roleSchoolPower.setSchool(school);
					roleSchoolPower.setSysRole(sysRole);
					schoolDao.saveSysRoleSchoolPower(roleSchoolPower);
				}
			}
			
		}

	}

	public School getBySchoolId(String schoolId) {
		return schoolDao.getBySchoolId(schoolId);
	}

	
	//删除园区
	@Override
	public void delSchool(String schoolId) {

		schoolDao.delSchool(schoolId);
	}

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>城市省会
	@Override
	public List<SysArea> getSysArea() {
		List listRes = new ArrayList();
		List<SysArea> listSysAreaData = schoolDao.getSysArea();
		for (int i = 0; i < listSysAreaData.size(); i++) {
			SysArea sysArea = listSysAreaData.get(i);
			Map map = new HashMap();
			map.put("areaName", sysArea.getAreaName());
			map.put("areaId", sysArea.getAreaId());
			listRes.add(map);
		}
		return listRes;
	}
	
	@Override
	public List<SysArea> getSysCity(String cityId) {
		List listRes = new ArrayList();
		List<SysArea> listSysAreaData = schoolDao.getSysCity(cityId);
		for (int i = 0; i < listSysAreaData.size(); i++) {
			SysArea sysArea = listSysAreaData.get(i);
			Map map = new HashMap();
			map.put("cityName", sysArea.getAreaName());
			map.put("cityId", sysArea.getAreaId());
			listRes.add(map);
		}
		return listRes;
	}
	
	@Override
	public List<Employee> getEmployee(String schoolId) {
		List listRes = new ArrayList();
		List<Employee> listEmployeeData = schoolDao.getEmployee(schoolId);
		for (int i = 0; i < listEmployeeData.size(); i++) {
			Employee employee = listEmployeeData.get(i);
			String leave = employee.getSysUserByUserId().getSysSubrole().getRoleLevel();
			if(leave.equals("分园")){
				Map map = new HashMap();
				map.put("employeeName", employee.getSysUserByUserId().getUserName());
				map.put("employeeId", employee.getSysUserByUserId().getUserId());
				listRes.add(map);
			}
			
		}
		return listRes;
	}

	/**
	 * 2016年10月22日下午1:36:56
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUserRole getSysUserRoleByUserId(String userId) {
		return schoolDao.getSysUserRoleByUserId(userId);
	}

	/**
	 * 2016年10月22日下午1:40:27
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysSubrole getSysSubRoleByRoleId(String roleId) {
		
		return schoolDao.getSysSubRoleByRoleId(roleId);
	}

	/**
	 * 2016年10月22日下午1:55:27
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysRoleSchoolPower getRoleSchoolPowerByRoleId(String roleId) {
		
		return schoolDao.getRoleSchoolPowerByRoleId(roleId);
	}

	/**
	 * 2016年10月24日下午8:03:13
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<School> getSchoolList(String schoolsId, Integer integer,
			Integer integer2) {
		// TODO Auto-generated method stub
		return schoolDao.getSchool(schoolsId,integer, integer2);
	}

	/**
	 * 2016年10月26日下午4:38:34
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List getSchoolfenyeBySchoolId(String schoolsId, String ssName) {
		// TODO Auto-generated method stub
		return schoolDao.getSchoolfenyeBySchoolId(schoolsId,ssName);
	}

	/**
	 * 2016年11月15日下午12:09:49
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<Classes> getClassesBySchoolId(String schoolId) {
		// TODO Auto-generated method stub
		return schoolDao.getClassesBySchoolId(schoolId);
	}

	/**
	 * 2016年11月15日下午12:17:07
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<Employee> getEmployeeBySchoolId(String schoolId) {
		// TODO Auto-generated method stub
		return schoolDao.getEmployeeBySchoolId(schoolId);
	}
	
	
}
