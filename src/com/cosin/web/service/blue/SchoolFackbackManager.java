package com.cosin.web.service.blue;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.blue.ISchoolFackbackDao;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.SchoolFackback;
import com.cosin.web.entity.Student;

@Service
@Transactional
public class SchoolFackbackManager implements ISchoolFackbackManager {

	@Autowired
	private ISchoolFackbackDao schoolFackbackDao;

	//分页
		@Override
		public List getSchoolFackbackLimit(String startDate,String endDate,String schoolName,String enable, int start, int limit) {
			return schoolFackbackDao.getSchoolFackbackLimit(startDate,endDate,schoolName,enable, start, limit);
		}
		//数量
		@Override
		public int getSchoolFackbackCount() {
			return schoolFackbackDao.getSchoolFackbackCount();
		}
		//获取SubjectId
		@Override
		public SchoolFackback getSchoolFackbackId(String schoolFackbackId) {
			return schoolFackbackDao.getSchoolFackbackId(schoolFackbackId);
		}
		//添加
		@Override
		public void saveSchoolFackback(SchoolFackback schoolFackback) {
			
		}
		//删除
		@Override
		public void schoolFackbackDelId(String schoolFackbackId) {
			schoolFackbackDao.schoolFackbackDelId(schoolFackbackId);
		}
		
		/**
		 * 当点击详情时候  改变 查看状态
		 */
		@Override
		public void updataSchoolFackback(String schoolFackbackId) {

			schoolFackbackDao.save(schoolFackbackId);
			
		}
		@Override
		public List<SchoolFackback> getSchoolFackbackLimit(int mode,
				String order, String sort, String SsName, int start, int limit,
				String startDate, String endDate) {
			return schoolFackbackDao.getSchoolFackbackLimit(mode,order,sort ,SsName, start, limit, startDate, endDate);
		}
		/**
		 * 2016年10月23日下午5:14:46
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List<SchoolFackback> getSchoolFackbackLimit(String schoolsId,
				String startDate, String endDate, String ischoolName,
				String enable, Integer integer, Integer integer2) {
			// TODO Auto-generated method stub
			return schoolFackbackDao.getSchoolFackbackLimit(schoolsId,startDate,endDate,ischoolName,enable, integer, integer2);
		}
		/**
		 * 2016年10月26日下午4:15:16
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List getSchoolFackbackCount(String startDate, String endDate,
				String ischoolName, String enable) {
			return schoolFackbackDao.getSchoolFackbackCount(startDate,endDate,ischoolName,enable);
		}
		/**
		 * 2016年10月26日下午4:31:53
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List getSchoolFackbackCountBySchoolId(String schoolsId,String startDate,
				String endDate, String ischoolName, String enable) {
			// TODO Auto-generated method stub
			return schoolFackbackDao.getSchoolFackbackCountBySchoolId(schoolsId,startDate,endDate,ischoolName,enable);
		}
		/**
		 * 2016年11月1日下午7:13:39
		 *  阳朔
		 *  注释:
		 */
		@Override
		public Parentsstudent getParentsstudentByParentId(String parentId) {
			// TODO Auto-generated method stub
			return schoolFackbackDao.getParentsstudentByParentId(parentId);
		}
		
		


}
