package com.cosin.web.service.blue;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import com.cosin.web.entity.Employee;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

public interface IEmployeeManager {

	/**
	 * 分页
	 */
	public List getEmployeeLimit(String employeeId,String SsTel, int start, int limit);

	/**
	 * 数量
	 */
	public int getEmployeeCount(String SsName,String SsTel);

	/**
	 * 获取角色名称 二级联动
	 */
	public List<SysRole> getlistRole(String schoolId);

	/**
	 * 获取类型 Name 二级联动
	 */
	public List<SysSubrole> getlistSubRole(String roleId);

	/**
	 * 获取 园区 操作者级别
	 */
	public List getlistSchool(String createUserId);

	/**
	 * 保存职工
	 */
	public void saveSchool(String isNianJ,String userId, String employeeId, String schoolId,
			String roleId, String subRoleId, String sysUserName, String sex,
			String tel, String mode,String radom);

	/**
	 * 删除操作
	 */
	public void delEmployee(String delKeys);

	/**
	 * 通过EmployeeId 查出对象Employee
	 * 下午4:46:22
	 */
	public Employee getEmployeeById(String employeeId);

	/**
	 * 通过userId  查询user
	 * @param userId
	 * @return
	 */
	public SysUser getSysUserById(String userId);

	
	public void save(Employee employee);

	/**
	 * 批量导入
	 * @param path
	 * @param inputStream
	 * @return
	 */
	public String readReport(String userId,String path, InputStream inputStream);

	/**
	 * 2016年9月29日下午5:55:43
	 *  龚杰
	 *  注释：根据userId  查询 roleId
	 */
	public List<SysUserRole> getSysUserRoleByUserId(String useId);

	/**
	 * 2016年10月10日下午6:32:06
	 *  龚杰
	 *  注释：
	 */
	public SysSubrole getSysSubRoleByRoleId(String roleId);

	/**
	 * 2016年10月14日下午2:29:05
	 *  阳朔
	 *  注释:查询是否存在
	 */
	public SysUser chaSysUserByLogin(String tel);

	/**
	 * 2016年10月22日下午3:08:03
	 *  阳朔
	 *  注释:
	 */
	public SysUser getSysUserBySubRoleId(String subRoleId);

	/**
	 * 2016年10月22日下午3:24:12
	 *  阳朔
	 *  注释:
	 */
	public SysRoleSchoolPower getSysRoleSchoolPowerByRoleId(String roleId);

	/**
	 * 2016年10月22日下午3:57:38
	 *  阳朔
	 *  注释:
	 */
	public List<Employee> getEmployeeBySchoolId(String schoolId, String ssName,
			String ssTel, Integer integer, Integer integer2);

	/**
	 * 2016年10月22日下午4:15:09
	 *  阳朔
	 *  注释:
	 */
	public Employee getEmployeeByUserId(String userId);

	/**
	 * 2016年10月26日下午1:47:54
	 *  阳朔
	 *  注释：如果为管理员  continue
	 */
	public SysUserRole chaSysUserRoleByUserId(String userId);

	/**
	 * 2016年10月26日下午2:58:20
	 *  阳朔
	 *  注释:分页
	 */
	public int getEmployeeCountBySchoolId(String schoolId, String ssName,
			String ssTel);

	/**
	 * 2016年10月26日下午6:23:56
	 *  阳朔
	 *  注释:
	 */
	public String getRoleLevelName(String roleId);

	String chaReport(String userId, String dirPre, InputStream inputStream);

	/**
	 * 2016年10月30日下午12:55:39
	 *  阳朔
	 *  注释:
	 */
	public List getEmployeeCountGai(String ssName, String ssTel);

	/**
	 * 2016年10月30日下午1:06:24
	 *  阳朔
	 *  注释: 分页数量改 角色级别
	 */
	public List<Employee> getEmployeeBySchoolIdCount(String schoolId,
			String ssName, String ssTel);

	
	
	

}
