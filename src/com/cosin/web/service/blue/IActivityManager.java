package com.cosin.web.service.blue;

import java.util.List;

import com.cosin.web.entity.Activity;
import com.cosin.web.entity.ActivitySchool;
import com.cosin.web.entity.Notice;

public interface IActivityManager {

	//分页
    public List getActivityLimit(String activityId, int start, int limit);
    //数量
    public int getActivityCount(String SsName);
    //获取SubjectId
  	public Activity getActivityId(String activityId);
  	 //删除
    public void activityDelId(String activityId);
    //查询用户表 user
//    public List getUser();
    //草稿箱列表查询
	public List<Activity> getDraftLimit(String ssName, Integer integer,
			Integer integer2);
	
	/**
	 * 保存 精彩活动 新增发布 
	 */
	public void saveActivity(String activityId, String userId, String ntitle,
			String filePath, String txtContent,String data, String mode,String draf);
	
	/**
	 * 中间表查询
	 * @param activityId
	 * @return
	 */
	public List<ActivitySchool> getActivitySchoolByActivityId(String activityId);
	
	/**
	 * 撤回操作
	 * @param activityId
	 * @param userId
	 */
	public void rollBackActivity(String activityId, String userId);
	/**
	 * 2016年10月8日下午4:01:52
	 *  龚杰
	 *  注释：
	 */
	public List<ActivitySchool> getSchoolByActivityId(String activityId);
	/**
	 * 2016年10月12日下午8:17:39
	 *  阳朔
	 *  注释:
	 */
	public int getActivityDraftCount(String ssName);
	/**
	 * 2016年10月23日下午4:40:14
	 *  阳朔
	 *  注释:
	 */
	public List<Activity> getActivityLimitBySchools(String ssName,
			String schoolsId, Integer integer, Integer integer2);
	/**
	 * 2016年10月26日下午3:34:21
	 *  阳朔
	 *  注释:
	 */
	public List<Activity> getActivityCountBySchoolId(String ssName, String schoolsId);
	/**
	 * 2016年12月8日下午3:13:15
	 *  阳朔
	 *  注释:
	 */
	public List<Activity> getActivityDraftLimitBySchools(String ssName,
			String schoolsId, Integer integer, Integer integer2);
	/**
	 * 2016年12月8日下午3:13:24
	 *  阳朔
	 *  注释:
	 */
	public List<Activity> getActivityDraftCountBySchoolId(String ssName,
			String schoolsId);

}
