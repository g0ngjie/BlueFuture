package com.cosin.web.service.blue;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.utils.DateUtils;
import com.cosin.utils.JPushUtils;
import com.cosin.web.dao.blue.INoticeDao;
import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Classteacher;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Notice;
import com.cosin.web.entity.NoticeSub;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.SignNotice;
import com.cosin.web.entity.SignPrice;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;
import com.cosin.web.entity.Unreadnotice;

@Service
@Transactional
public class NoticeManager implements INoticeManager {

	@Autowired
	private INoticeDao noticeDao;

	    //分页
		@Override
		public List getNoticeLimit(String ssName, int start, int limit) {
			return noticeDao.getNoticeLimit(ssName, start, limit);
		}
		//数量
		@Override
		public int getNoticeCount(String SsName) {
			return noticeDao.getNoticeCount(SsName);
		}
		//获取SubjectId
		@Override
		public Notice getNoticeId(String noticeId) {
			return noticeDao.getNoticeId(noticeId);
		}
		//删除
		@Override
		public void noticeDelId(String noticeId) {
			noticeDao.noticeDelId(noticeId);
		}
		
		
		/**
		 * 保存 公告
		 */
		@Override
		public void saveNotice(String payBill,String noticeId,String userId, String ntitle, String filePath,
				String txtContent, String ntype, String mode,String data,String draf) {
			if("edit".equals(mode)){
				
				Notice notice = getNoticeId(noticeId);
				notice.setCreateDate(new Timestamp(new Date().getTime()));
				//保存类型 1报名 2支付 3查看
				notice.setType(new Integer(ntype));
				//保存 标题
				if(!"".equals(ntitle)&&!"null".equals(ntitle)){
					notice.setTitle(ntitle);
				}
				if(!"".equals(payBill)&&!"null".equals(payBill)){
					notice.setPrice(payBill);
				}
				//修改人
				SysUser sysUser = noticeDao.getSysUserById(userId);
				notice.setSysUser(sysUser);
				//修改的正文
				if(!"".equals(txtContent)&&!"null".equals(txtContent)){
					
					notice.setContent(txtContent);
				}
				//保存图片地址
				notice.setImg(filePath);
				List<NoticeSub> listNoticeSub = noticeDao.getNoticeSubByNoId(noticeId);
				noticeDao.delNoticeSub(listNoticeSub);

				List listId = classId(data);
				
				for  ( int  ii  =   0 ; ii  <  listId.size()  -   1 ; ii ++ )   { 
				    for  ( int  jj  =  listId.size()  -   1 ; jj  >  ii; jj -- )   { 
				      if  (listId.get(jj).equals(listId.get(ii)))   { 
				    	  listId.remove(jj); 
				      } 
				    } 
				  }
				
				if("saveDra".equals(draf)){
					notice.setIsDraft(1);
					noticeDao.saveNotice(notice);
				}else{
					notice.setIsDraft(0);
					noticeDao.saveNotice(notice);
					Map<String,String> map = new HashMap<String, String>();
					map.put("type","1");
					//推送
					String userList = alias(data);
					JPushUtils.testSendPush(ntitle, map, userList);
					
					for (int l = 0; l < listId.size(); l++) {
						String cId = (String) listId.get(l);
						List<Student> studentList = noticeDao.getStudentListByClassesId(cId);
						if(studentList != null&&!"".equals(studentList)){
							for (int i = 0; i < studentList.size(); i++) {
								Student student = studentList.get(i);
								
								String stuId = student.getStudentId();
								Parentsstudent parentsstudent = noticeDao.getParentsstudentByStudentId(stuId);
								SysUser use = parentsstudent.getParents().getSysUser();
								
								Unreadnotice unreadnotice = new Unreadnotice();
								unreadnotice.setStudent(student);
								unreadnotice.setNotice(notice);
								unreadnotice.setSysUser(use);
								noticeDao.saveObj(unreadnotice);
								
								if(new Integer(ntype).equals(2)){
									
									SignPrice signPrice = new SignPrice();
									signPrice.setCreateTime(new Timestamp(new Date().getTime()));
									signPrice.setNotice(notice);
									signPrice.setState(0);
									signPrice.setStudent(student);
									
									signPrice.setOrderNo(DateUtils.getOutTradeNo());
									noticeDao.saveObj(signPrice);
								}
							}
						}
					}
				}
				
				
				for (int i = 0; i < listId.size(); i++) {
					NoticeSub  noticeSub = new NoticeSub();
					String classeId = (String) listId.get(i);
					Classes  classes = noticeDao.getByIdClasses(classeId);
					noticeSub.setClasses(classes);
					noticeSub.setNotice(notice);
					noticeDao.saveObj(noticeSub);
				}
				
			}else{
				
				Notice notice = new Notice();
				notice.setIsDel(0);
				notice.setEnable(1);
				notice.setCreateDate(new Timestamp(new Date().getTime()));
				notice.setLookNum(0);
				if(!"".equals(payBill)&&!"null".equals(payBill)){
					notice.setPrice(payBill);
				}
				//保存图片地址
				notice.setImg(filePath);
				String tit = "<div align="+"center"+"><p>"+ntitle+"</p>";
				String timem = DateUtils.parseDateToStr(DateUtils.DEF_FORMAT, new Timestamp(new Date().getTime()));
				timem = timem.substring(0, 10);
				String time = "<p align="+"right"+">"+timem+"&nbsp;&nbsp;&nbsp;&nbsp;</p></div>";
				//保存 正文
				notice.setContent(txtContent);
				//保存 标题
				notice.setTitle(ntitle);
				//保存类型 1报名 2支付 3查看
				notice.setType(new Integer(ntype));
				//保存 创建人
				SysUser sysUser =noticeDao.getSysUserById(userId);
				notice.setSysUser(sysUser);
				
				List listId = classId(data);
				
				for  ( int  ii  =   0 ; ii  <  listId.size()  -   1 ; ii ++ )   { 
				    for  ( int  jj  =  listId.size()  -   1 ; jj  >  ii; jj -- )   { 
				      if  (listId.get(jj).equals(listId.get(ii)))   { 
				    	  listId.remove(jj); 
				      } 
				    } 
				  }
				
				
				if("saveDra".equals(draf)){
					notice.setIsDraft(1);
					noticeDao.saveNotice(notice);
				}else{
					notice.setIsDraft(0);
					noticeDao.saveNotice(notice);
					Map<String,String> map = new HashMap<String, String>();
					map.put("type","1");
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>						
					String userList = alias(data);
					JPushUtils.testSendPush(ntitle, map, userList);
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>				
					
					for (int l = 0; l < listId.size(); l++) {
						String cId = (String) listId.get(l);
						List<Student> studentList = noticeDao.getStudentListByClassesId(cId);
						if(studentList != null&&!"".equals(studentList)){
							for (int i = 0; i < studentList.size(); i++) {
								Student student = studentList.get(i);
								String stuId = student.getStudentId();
								Parentsstudent parentsstudent = noticeDao.getParentsstudentByStudentId(stuId);
								SysUser use = parentsstudent.getParents().getSysUser();
								Unreadnotice unreadnotice = new Unreadnotice();
								unreadnotice.setStudent(student);
								unreadnotice.setNotice(notice);
								unreadnotice.setSysUser(use);
								noticeDao.saveObj(unreadnotice);
								
								if(new Integer(ntype).equals(2)){
									
									SignPrice signPrice = new SignPrice();
									signPrice.setCreateTime(new Timestamp(new Date().getTime()));
									signPrice.setNotice(notice);
									signPrice.setState(0);
									signPrice.setStudent(student);
									
									signPrice.setOrderNo(DateUtils.getOutTradeNo());
									noticeDao.saveObj(signPrice);
								}
							}
						}
					}
					
				}
				
				
				for (int i = 0; i < listId.size(); i++) {
					NoticeSub  noticeSub = new NoticeSub();
					String classeId = (String) listId.get(i);
					Classes  classes = noticeDao.getByIdClasses(classeId);
					noticeSub.setClasses(classes);
					noticeSub.setNotice(notice);
					noticeDao.saveObj(noticeSub);
				}
			}
		}
		
/**
 * 通过拼接 id字符串 筛选出 classesId
 */
public List classId(String data){
	
	List<Classes> allClassesList = noticeDao.getAllClasses();
	Map mapAllClassId = new HashMap();
	for (int j = 0; j < allClassesList.size(); j++) {
		Classes classes = allClassesList.get(j);
		String classesId = classes.getClassesId();
		mapAllClassId.put(classesId, "ok");
	}
	
	String[] atrAr = data.split(",");
	Map map = new HashMap();
	List listId = new ArrayList();
	for (int k = 0; k < atrAr.length; k++) {
		String atr = atrAr[k];
		Boolean flag = mapAllClassId.containsKey(atr);
		if(flag==true){
			listId.add(atrAr[k]);
		}
	}
	
	return listId;
}
		/**
		 * 2016年9月30日下午4:46:00
		 *  龚杰
		 *  注释：拼接 推送字符串
		 */
public String alias(String data){
	
	/*
	 * 筛选 classesId
	 */
	List listId = classId(data);

	for  ( int  ii  =   0 ; ii  <  listId.size()  -   1 ; ii ++ )   { 
	    for  ( int  jj  =  listId.size()  -   1 ; jj  >  ii; jj -- )   { 
	      if  (listId.get(jj).equals(listId.get(ii)))   { 
	    	  listId.remove(jj); 
	      } 
	    } 
	  }
	
	String allId = "";
	
	List listUserId = new ArrayList();
	//推送 根据班级 关联的家长
	for (int i = 0; i < listId.size(); i++) {
		
		
		String cId = (String) listId.get(i);
		
		Classes  classes = noticeDao.getByIdClasses(cId);
		if(classes!=null){
			
			Student student = noticeDao.getStudentByClassesId(classes.getClassesId());
			if(student!=null){
				
				List<Parentsstudent> parentsstudent = noticeDao.getParentListByStudentId(student.getStudentId());
				for (int j = 0; j < parentsstudent.size(); j++) {
					Parentsstudent parentsstudent2 = parentsstudent.get(j);
					String parentsId = parentsstudent2.getParents().getSysUser().getUserId();
					listUserId.add(parentsId);
				}
			}
			
			
			
			//推送 根据班级 关联的 老师 园长
			//普通老师
			String clasId = classes.getClassesId();
			List<Classteacher> classteacherList = noticeDao.getClassteacherListByClassId(clasId);
			for (int k = 0; k < classteacherList.size(); k++) {
				Classteacher classteacher = classteacherList.get(k);
				//普通老师Id
				String otherTeacherId = classteacher.getEmployee().getSysUserByUserId().getUserId();

				SysUserRole sysUserRole = noticeDao.getSysUserRoleByEmpUserId(otherTeacherId);
				String emp_RoleId = sysUserRole.getSysRole().getRoleId();
				List<SysRolePower> powerList = noticeDao.getSysRolePowerByEmp_RoleId(emp_RoleId);
				for (int j = 0; j < powerList.size(); j++) {
					SysRolePower power = powerList.get(j);
					if("appGengduo_gonggao".equals(power.getPowerCode())){
						listUserId.add(otherTeacherId);
					}
				}
			}
			
			//班主任
			String classTeacherId = classes.getEmployee().getSysUserByUserId().getUserId();
//			listUserId.add(classTeacherId);
			
			SysUserRole sysUserRole = noticeDao.getSysUserRoleByEmpUserId(classTeacherId);
			String emp_RoleId1 = sysUserRole.getSysRole().getRoleId();
			List<SysRolePower> powerList1 = noticeDao.getSysRolePowerByEmp_RoleId(emp_RoleId1);
			for (int j = 0; j < powerList1.size(); j++) {
				SysRolePower power = powerList1.get(j);
				if("appGengduo_gonggao".equals(power.getPowerCode())){
					listUserId.add(classTeacherId);
				}
			}
			
			//园长
			String schoolId = classes.getSchool().getSchoolId();
			Employee employee = classes.getSchool().getEmployee();
			if(employee != null){
				
				String schoolUserNameId = classes.getSchool().getEmployee().getSysUserByUserId().getUserId();
				
				SysUserRole sysUserRole2 = noticeDao.getSysUserRoleByEmpUserId(schoolUserNameId);
				String emp_RoleId2 = sysUserRole2.getSysRole().getRoleId();
				List<SysRolePower> powerList2 = noticeDao.getSysRolePowerByEmp_RoleId(emp_RoleId2);
				for (int j = 0; j < powerList2.size(); j++) {
					SysRolePower power = powerList2.get(j);
					if("appGengduo_gonggao".equals(power.getPowerCode())){
						listUserId.add(schoolUserNameId);
					}
				}
//				listUserId.add(schoolUserNameId);
			}
		}
	}
	
	for  ( int  ii  =   0 ; ii  <  listUserId.size()  -   1 ; ii ++ )   { 
	    for  ( int  jj  =  listUserId.size()  -   1 ; jj  >  ii; jj -- )   { 
	      if  (listUserId.get(jj).equals(listUserId.get(ii)))   { 
	    	  listUserId.remove(jj); 
	      } 
	    } 
	  } 
	for (int ij = 0; ij < listUserId.size(); ij++) {
		String userList = (String) listUserId.get(ij);
		allId +=userList+",";
	}
	
	return allId;
}
		
		
		
	//撤回操作
	public void rollNotice(String noticeId, String userId){
		
		Notice notice = noticeDao.getNoticeId(noticeId);
		notice.setIsDraft(1);
			if(userId.equals("0") || "".equals(userId) || userId.equals("全部")){
				notice.setSysUser(null);
			}else{
				SysUser sysUser=noticeDao.getUser2(userId);
				if(sysUser != null){
					notice.setSysUser(sysUser);
					}
				}
		noticeDao.rollNotice(notice);
	}
	

	/**
	 * 获取草稿箱 列表
	 */
	@Override
	public List<Notice> getDraftLimit(String ssName, int start, int limit) {
		return noticeDao.getDraftLimit(ssName, start, limit);
	}
	/**
	 * 
	 */
	@Override
	public List<Object> getNoticeTree(String id) {
		
		return noticeDao.noticeDao(id);
	}
	@Override
	public List<Classes> getListClasses(Object id) {
		return noticeDao.getListClasses(id);
	}
	//年级查询
	@Override
	public List<Object> getListClas(Object id) {			
		return noticeDao.getListClas(id);
	}
	@Override
	public List<Classes> getClass(Object id,Object schoolId) {			
		return noticeDao.getClass(id,schoolId);
	}
	
	/**
	 * 根据noticeId  查询NoticeSub对象
	 */
	@Override
	public List<NoticeSub> getNoticeSubByNoticeId(String noticeId) {
		return noticeDao.getNoticeSubByNoticeId(noticeId);
	}
	
	/**
	 * 根据noticeId  查询 noticesub  中间表  关联班级
	 */
	@Override
	public List<NoticeSub> getNoticeSubBynoticeId(String noticeId) {
		
		return noticeDao.getNoticeSubBynoticeId(noticeId);
	}
	
	/**
	 * 已报名
	 */
	@Override
	public List<NoticeSub> getNoticeLimit(String noticeId,String schoolName, String grade,String className, Integer start, Integer limit)
	{
		return noticeDao.getNoticeLimit(noticeId,schoolName,grade,className, start, limit);
	}
	
	/**
	 * 通过classId 查询  学生
	 * 
	 */
	@Override
	public Student getStudentByClassesId(String classesId) {
		return noticeDao.getStudentByClassesId(classesId);
	}
	
	/**
	 * 数量
	 */
	@Override
	public int getNoticeCount(String noticeId,String schoolName, String grade,
			String className) {
		return noticeDao.getNoticeCount(noticeId,schoolName,grade,className);
	}
	
	/**
	 * 二级联动
	 */
	@Override
	public List getListGradeBySchoolId(String schoolId) {
		List list = new ArrayList();
		List<Classes> className = noticeDao.getListGradeBySchoolId(schoolId);
		for (int i = 0; i < className.size(); i++) {
			Classes classes = className.get(i);
			Map map = new HashMap();
			String gra = classes.getGrade();
			if(gra.equals("1")){
				map.put("gradeName","托班");
				map.put("clasId", classes.getGrade());
			}else if(gra.equals("2")){
				map.put("gradeName","小班");
				map.put("clasId", classes.getGrade());
			}else if(gra.equals("3")){
				map.put("gradeName","中班");
				map.put("clasId", classes.getGrade());
			}else if(gra.equals("4")){
				map.put("gradeName","大班");
				map.put("clasId", classes.getGrade());
			}else if(gra.equals("5")){
				map.put("gradeName","学前班");
				map.put("clasId", classes.getGrade());
			}
			list.add(map);
		}
		
		for  ( int  i  =   0 ; i  <  list.size()  -   1 ; i ++ )   { 
		    for  ( int  j  =  list.size()  -   1 ; j  >  i; j -- )   { 
		      if  (list.get(j).equals(list.get(i)))   { 
		        list.remove(j); 
		      } 
		    } 
		  } 		
		return list;
	}
	
	/**
	 * 二级联动
	 */
	@Override
	public List getListClassNameByGradeName(String gradeName) {
		List list = new ArrayList();

		List<Classes> gradeList = noticeDao.getListClassNameByGradeName(gradeName);
		
		for (int i = 0; i < gradeList.size(); i++) {
			Classes classes = gradeList.get(i);
			Map map = new HashMap();
			map.put("classesId", classes.getClassesName());
			map.put("classesName", classes.getClassesName());
			list.add(map);
		}
		
		for  ( int  i  =   0 ; i  <  list.size()  -   1 ; i ++ )   { 
		    for  ( int  j  =  list.size()  -   1 ; j  >  i; j -- )   { 
		      if  (list.get(j).equals(list.get(i)))   { 
		        list.remove(j); 
		      } 
		    } 
		  } 
		return list;
	}
	
	/**
	 * 
	 */
	@Override
	public List<Employee> getEmployeeBySchoolId(String schoolId) {
		return noticeDao.getEmployeeBySchoolId(schoolId);
	}
	
	/**
	 * 通过classId 查询 所有学生
	 */
	@Override
	public List<Student> getStudentListByClassesId(String classId) {
		return noticeDao.getStudentListByClassesId(classId);
	}
	
	/**
	 * 通过studentId  查询家长
	 */
	@Override
	public List<Parentsstudent> getParentListByStudentId(String studentId) {
		return noticeDao.getParentListByStudentId(studentId);
	}
	
	/**
	 * 未报名
	 */
	@Override
	public List<NoticeSub> getNoticeSubLimit(String noticeId,Integer start,
			Integer limit) {
		
		return noticeDao.getNoticeSubLimit(noticeId,start, limit);
	}
	@Override
	public int getNoticeSubCount(String noticeId) {
		return noticeDao.getNoticeSubCount(noticeId);
	}
	/**
	 * 2016年10月10日上午9:27:41
	 *  龚杰
	 *  注释：
	 */
	@Override
	public int getNoticeDraftCount(String ssName) {
		// TODO Auto-generated method stub
		return noticeDao.getNoticeDraftCount(ssName);
	}
	/**
	 * 2016年10月17日上午11:44:21
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<NoticeSub> getNotice2Limit(String noticeId, Integer integer,
			Integer integer2) {
		return noticeDao.getNotice2Limit(noticeId,integer,integer2);
	}
	/**
	 * 2016年10月17日下午1:35:07
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<NoticeSub> getNoticeByNoticeId(String noticeId) {
		return noticeDao.getNoticeByNoticeId(noticeId);
	}
	/**
	 * 2016年10月17日下午2:51:17
	 *  阳朔
	 *  注释:查询已报名 新改
	 */
	@Override
	public List<SignNotice> getSignNoticeLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2) {
		return noticeDao.getSignNoticeLimit(noticeId,schoolName,grade,className,integer,integer2);
	}
	/**
	 * 2016年10月17日下午2:58:11
	 *  阳朔
	 *  注释:查询已报名数量 新改
	 */
	@Override
	public int getSignNoticeCount(String noticeId, String schoolName,
			String grade, String className) {
		return noticeDao.getSignNoticeCount(noticeId,schoolName,grade,className);
	}
	/**
	 * 2016年10月17日下午3:07:32
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Parentsstudent getParentsstudentByStudentId(String studentId) {
		return noticeDao.getParentsstudentByStudentId(studentId);
	}
	/**
	 * 2016年10月17日下午3:14:01
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SignNotice> getSignNoticeByNoticeId(String noticeId) {
		
		return noticeDao.getSignNoticeByNoticeId(noticeId);
	}
	/**
	 * 2016年10月23日下午4:28:38
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<Classes> getClassess(String schoolId) {
		// TODO Auto-generated method stub
		return noticeDao.getClassess(schoolId);
	}
	/**
	 * 2016年10月23日下午4:30:51
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<NoticeSub> getNoticeSubLimit(String classIds, String ssName,
			Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		
		return noticeDao.getNoticeSubLimit(classIds,ssName,integer,integer2);
	}
	/**
	 * 2016年10月27日下午7:50:35
	 *  阳朔
	 *  注释:已支付
	 */
	@Override
	public List<SignPrice> getSignPriceLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2) {
		return noticeDao.getSignPriceLimit(noticeId,schoolName,grade,className,integer,integer2);
	}
	/**
	 * 2016年10月27日下午7:59:56
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getSignPriceCount(String noticeId, String schoolName,
			String grade, String className) {
		// TODO Auto-generated method stub
		return noticeDao.getSignPriceCount(noticeId,schoolName,grade,className);
	}
	/**
	 * 2016年10月27日下午8:07:16
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<NoticeSub> getNoticePriceLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2) {
		// TODO Auto-generated method stub
		return noticeDao.getNoticePriceLimit(noticeId,schoolName,grade,className,integer,integer2);
	}
	/**
	 * 2016年10月27日下午8:09:16
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getNoticeSubPriceCount(String noticeId,String schoolName,String grade,String className) {
		// TODO Auto-generated method stub
		return noticeDao.getNoticeSubPriceCount(noticeId,schoolName,grade,className);
	}
	/**
	 * 2016年10月28日下午4:24:13
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SignPrice> getNoSignPriceLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2) {
		// TODO Auto-generated method stub
		return noticeDao.getNoSignPriceLimit(noticeId,schoolName,grade,className,integer,integer2);
	}
	/**
	 * 2016年10月28日下午4:29:48
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getNoPriceCount(String noticeId, String schoolName,
			String grade, String className) {
		return noticeDao.getNoPriceCount(noticeId,schoolName,grade,className);
	}
	/**
	 * 2016年12月8日下午5:54:33
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<NoticeSub> getNoticeSubDraftLimit(String classIds,
			String ssName, Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		return noticeDao.getNoticeSubDraftLimit(classIds,ssName,integer,integer2);
	}
		

}
