package com.cosin.web.service.blue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.utils.JsonUtils;
import com.cosin.web.dao.blue.IContainDao;
import com.cosin.web.entity.Contain;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.School;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysUser;

@Service
@Transactional
public class ContainManager implements IContainManager {

	@Autowired
	private IContainDao containDao;

	/**
	 * 获取正文
	 */
	@Override
	public Contain getlistContain() {
		 
		return containDao.getContainList();
	}

	/**
	 * 园区数量
	 */
	@Override
	public int selectSchoolNum() {
		return containDao.getSchoolNum();
	}

	/**
	 * 班级数量
	 */
	@Override
	public int selectClassesNum() {
		return containDao.getClassesNum();
	}

	/**
	 * 老师数量   班主任 数量 + 普通 老师数量
	 */
	@Override
	public int selectTeacherNum() {
		
		int classTeacher = containDao.getClassTeacherNum();
		//int otherTeacher = containDao.getOtherTeacherNum();
		int teacherNum = classTeacher;
		
		return teacherNum;
	}

	/**
	 * 学生数量
	 */
	@Override
	public int selectStudentNum() {
		return containDao.getStudentNum();
	}

	/**
	 * 家长数量
	 */
	@Override
	public int selectParentNum() {
		return containDao.getParentNum();
	}

	/**
	 * 保存 内容 
	 * @throws InterruptedException 
	 */
	@Override
	public void saveContain(String content) throws InterruptedException {
		Contain contain2 = containDao.getContain();
		if(contain2!= null&&!"".equals(contain2)){
			containDao.delContain(contain2);
		}
		Thread.sleep(500);
		Contain contain = new Contain();
		contain.setContent(content);
		containDao.saveContain(contain);
	}

	/**
	 * 查询内容
	 */
	@Override
	public Contain selectContent() {
		 return containDao.getContain();
	}

	@Override
	public SysUser getSysUserById(String userId) {
		return containDao.getSysUserById(userId);
	}


	@Override
	public Map saveSysUser(String userId, String beginPassword,
			String endPassword, String doublePassword) {
		
		SysUser sysUser = containDao.getSysUserById(userId);
		String pwd = sysUser.getPwd();
		
		Map map = new HashMap();
		if(pwd.equals(beginPassword)){
			if(pwd.equals(endPassword)||pwd.equals(doublePassword)){
				map.put("code", 103);
				map.put("msg", "修改失败，与旧密码一致!");
				return map;
			}else{
				if(endPassword.equals(doublePassword)&&!"".equals(endPassword)&&!"null".equals(endPassword)){
					sysUser.setPwd(endPassword);
					containDao.saveSysUser(sysUser);
					map.put("code", 100);
					map.put("msg", "保存成功");
					return map;
				}else{
					map.put("code", 102);
					map.put("msg", "两次密码输入不一致");
					return map;
				}
			}
		}else{
			map.put("code", 101);
			map.put("msg", "原密码输入错误!");
		}
		
		return map;
	}

	/**
	 * 2016年10月23日下午3:10:27
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map forgetPassWord(String phone, String newPassWord) {
		// TODO Auto-generated method stub
		List<SysUser> list = containDao.getSysUserByPhone(phone);
		
		for(int i=0;i<list.size();i++){
			SysUser sysUser = list.get(i);
			sysUser.setPwd(newPassWord);
			containDao.saveUser(sysUser);
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "修改成功");
		return map;
	}

	/**
	 * 2016年10月23日下午3:53:47
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<School> getSchools(String userId) {
		// TODO Auto-generated method stub
		List<Employee> list = containDao.getEmployees(userId);
		List<School> ll = new ArrayList<School>();
		for (int i = 0; i < list.size(); i++) {
			Employee employee = list.get(i);
			ll.add(employee.getSchool());
		}
		return ll;
	}

	/**
	 * 2016年10月23日下午3:58:59
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int selectClassesNum(String schoolsId) {
		// TODO Auto-generated method stub
		return containDao.getClassesNum(schoolsId);
	}

	/**
	 * 2016年10月23日下午4:02:25
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getTeacherBySchool(String schoolsId) {
		// TODO Auto-generated method stub
		int sx = containDao.getTeacherBySchool(schoolsId);
		return sx;
	}

	/**
	 * 2016年10月23日下午4:06:09
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getStudentsBySchool(String schoolsId) {
		// TODO Auto-generated method stub
		int sx = containDao.getStudentsBySchool(schoolsId);
		return sx;
	}

	/**
	 * 2016年10月23日下午4:08:17
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getParentsBySchools(String schoolsId) {
		// TODO Auto-generated method stub
		List<Student> studentlist = containDao.getParentStudents(schoolsId);
		int count = 0;
		for (int i = 0; i < studentlist.size(); i++) {
			Student student = studentlist.get(i);
			List<Parentsstudent> ll =  containDao.getParentsstudent(student.getStudentId());
			count+=ll.size();
		}
		return count;
	}

}
