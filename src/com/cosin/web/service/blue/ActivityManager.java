package com.cosin.web.service.blue;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.utils.JPushUtils;
import com.cosin.web.dao.blue.IActivityDao;
import com.cosin.web.entity.Activity;
import com.cosin.web.entity.ActivitySchool;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Notice;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.School;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

@Service
@Transactional
public class ActivityManager implements IActivityManager {

	@Autowired
	private IActivityDao activityDao;

	//分页
		@Override
		public List getActivityLimit(String SsName, int start, int limit) {
			return activityDao.getActivityLimit(SsName, start, limit);
		}
		//数量
		@Override
		public int getActivityCount(String SsName) {
			return activityDao.getActivityCount(SsName);
		}
		//获取SubjectId
		@Override
		public Activity getActivityId(String activityId) {
			return activityDao.getActivityId(activityId);
		}
		//删除
		@Override
		public void activityDelId(String activityId) {
			activityDao.activityDelId(activityId);
		}


		/**
		 * 保存 精彩活动 新增发布 
		 */
		@Override
		public void saveActivity(String activityId, String userId, String ntitle,
				String filePath, String txtContent,String data, String mode,String draf) {
			if("edit".equals(mode)){
				Activity activity = getActivityId(activityId);
				//添加标题
				activity.setTitle(ntitle);
				//添加 图片 地址
				activity.setImg(filePath);
				//添加正文
				activity.setContent(txtContent);
				
				if("saveDra".equals(draf)){
					activity.setIsDraft(1);
				}else{
					activity.setIsDraft(0);
					Map<String,String> map = new HashMap<String, String>();
					map.put("type","2");
					String userList = alias(data);
					JPushUtils.testSendPush(ntitle, map, userList);
				}
				//添加 创建人
				SysUser sysUser = activityDao.getSysUserById(userId);
				activity.setSysUser(sysUser);
				
				activityDao.saveActivity(activity);
				
				List<ActivitySchool> activitySchoolList = activityDao.getActivitySchoolByActivityId(activityId);
				
				activityDao.delActivitySchoolById(activitySchoolList);
				
				//添加 园区
				if(data!=null){
					String[] keyArr = data.split(",");
					for (int i = 0; i < keyArr.length; i++) {
						String key = keyArr[i];
						if (!"".equals(key) && key != null) {
							ActivitySchool activitySchool = new ActivitySchool();
							//添加园区 外键
							School school = activityDao.getSchoolById(key);
							activitySchool.setSchool(school);
							activitySchool.setActivity(activity);
							//保存 外键表
							activityDao.saveActivitySchool(activitySchool);
						}
					}
				}
				
		}else{
				Activity activity = new Activity();
				activity.setIsDel(0);
				activity.setEnable(1);
				activity.setCreateDate(new Timestamp(new Date().getTime()));
				activity.setLookNum(0);
				activity.setType(1);
				
				if("saveDra".equals(draf)){
					activity.setIsDraft(1);
				}else{
					activity.setIsDraft(0);
					String userList = alias(data);
					Map<String,String> map = new HashMap<String, String>();
					map.put("type","2");
					JPushUtils.testSendPush(ntitle, map, userList);
				}
				//添加 标题
				activity.setTitle(ntitle);
				//添加 正文
				activity.setContent(txtContent);
				//添加 图片 地址
				activity.setImg(filePath);
				//添加 创建人
				SysUser sysUser = activityDao.getSysUserById(userId);
				activity.setSysUser(sysUser);
				activityDao.saveActivity(activity);
				//添加 园区
				if(data!=null){
					String[] keyArr = data.split(",");
					for (int i = 0; i < keyArr.length; i++) {
						String key = keyArr[i];
						if (!"".equals(key) && key != null) {
							ActivitySchool activitySchool = new ActivitySchool();
							//添加园区 外键
							School school = activityDao.getSchoolById(key);
							activitySchool.setSchool(school);
							activitySchool.setActivity(activity);
							//保存 外键表
							activityDao.saveActivitySchool(activitySchool);
						}
					}
				}
			}
		}
		
		
		/**
		 * 2016年10月25日下午5:19:10
		 *  阳朔
		 *  注释:推送所需要的数据
		 */
		public String alias(String data){
			
			String tuiId = "";
			List listUserId = new ArrayList();
			String[] schoolStr = data.split(",");
			for (int i = 0; i < schoolStr.length; i++) {
				String schoolId = schoolStr[i];
				if(!"".equals(schoolId)){
					//根据schoolId 获取 关联的职工Id
					List<Employee> employeeList = activityDao.getEmployeeBySchoolId(schoolId);
					for (int m = 0; m < employeeList.size(); m++) {
						Employee employee = employeeList.get(m);
						String empUserId = employee.getSysUserByUserId().getUserId();
						
						SysUserRole sysUserRole = activityDao.getSysUserRoleByEmpUserId(employee.getSysUserByUserId().getUserId());
						String emp_RoleId = sysUserRole.getSysRole().getRoleId();
						List<SysRolePower> powerList = activityDao.getSysRolePowerByEmp_RoleId(emp_RoleId);
						for (int j = 0; j < powerList.size(); j++) {
							SysRolePower power = powerList.get(j);
							if("appGengduo_huodong".equals(power.getPowerCode())){
								listUserId.add(empUserId);
							}
						}
						
					}
					
					
					//根据schoolId 获取 关联 的所有家长Id
					List<Student> studentList = activityDao.getStudentBySchoolId(schoolId);
					for (int j = 0; j < studentList.size(); j++) {
						Student student = studentList.get(j);
						List<Parentsstudent> parentsstudentList = activityDao.getParentsstudentByStudentId(student.getStudentId());
						for (int k = 0; k < parentsstudentList.size(); k++) {
							Parentsstudent parentsstudent = parentsstudentList.get(k);
							String sysUserParentId = parentsstudent.getParents().getSysUser().getUserId();
							listUserId.add(sysUserParentId);
						}
					}
					
					
					
				}
			}
			for (int ij = 0; ij < listUserId.size(); ij++) {
				String userList = (String) listUserId.get(ij);
				tuiId +=userList +",";
			}
			
			return tuiId;
		}
		
		
		//草稿箱列表查询
		@Override
		public List<Activity> getDraftLimit(String ssName, Integer start,
				Integer limit) {
			return activityDao.getDraftLimit(ssName, start, limit);
		}
		
		/**
		 * 中间表查询
		 */
		@Override
		public List<ActivitySchool> getActivitySchoolByActivityId(
				String activityId) {
			
			return activityDao.getActivitySchoolByActivityId(activityId);
		}
		
		/**
		 * 撤回操作
		 */
		@Override
		public void rollBackActivity(String activityId, String userId) {
			Activity activity = activityDao.getActivityId(activityId);
			activity.setIsDraft(1);
				if(userId.equals("0") || "".equals(userId) || userId.equals("全部")){
					activity.setSysUser(null);
				}else{
					SysUser sysUser=activityDao.getUser2(userId);
					if(sysUser != null){
						activity.setSysUser(sysUser);
						}
					}
				activityDao.saveActivity(activity);
		}
		/**
		 * 2016年10月8日下午4:02:00
		 *  龚杰
		 *  注释：
		 */
		@Override
		public List<ActivitySchool> getSchoolByActivityId(String activityId) {
			return activityDao.getSchoolByActivityId(activityId);
		}
		/**
		 * 2016年10月12日下午8:17:47
		 *  阳朔
		 *  注释:
		 */
		@Override
		public int getActivityDraftCount(String ssName) {
			return activityDao.getActivityDraftCount(ssName);
		}
		/**
		 * 2016年10月23日下午4:40:22
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List<Activity> getActivityLimitBySchools(String ssName,
				String schoolsId, Integer integer, Integer integer2) {
			// TODO Auto-generated method stub
			List<ActivitySchool> list = activityDao.getActivityLimitBySchools(ssName,schoolsId, integer, integer2);
			List<Activity> ll = new ArrayList<Activity>();
			for (int i = 0; i < list.size(); i++) {
				ActivitySchool as = list.get(i);
				ll.add(as.getActivity());
			}
			return ll;
		}
		/**
		 * 2016年10月26日下午3:34:27
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List<Activity> getActivityCountBySchoolId(String ssName, String schoolsId) {
			List<ActivitySchool> list = activityDao.getActivityCountBySchoolId(ssName,schoolsId);
			List<Activity> ll = new ArrayList<Activity>();
			for (int i = 0; i < list.size(); i++) {
				ActivitySchool as = list.get(i);
				ll.add(as.getActivity());
			}
			return ll;
		}
		/**
		 * 2016年12月8日下午3:13:34
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List<Activity> getActivityDraftLimitBySchools(String ssName,
				String schoolsId, Integer integer, Integer integer2) {
			List<ActivitySchool> list = activityDao.getActivityDraftLimitBySchools(ssName,schoolsId, integer, integer2);
			List<Activity> ll = new ArrayList<Activity>();
			for (int i = 0; i < list.size(); i++) {
				ActivitySchool as = list.get(i);
				ll.add(as.getActivity());
			}
			return ll;
		}
		/**
		 * 2016年12月8日下午3:13:34
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List<Activity> getActivityDraftCountBySchoolId(String ssName,
				String schoolsId) {
			List<ActivitySchool> list = activityDao.getActivityDraftCountBySchoolId(ssName,schoolsId);
			List<Activity> ll = new ArrayList<Activity>();
			for (int i = 0; i < list.size(); i++) {
				ActivitySchool as = list.get(i);
				ll.add(as.getActivity());
			}
			return ll;
		}

}
