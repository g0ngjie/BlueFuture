package com.cosin.web.service.blue;

import java.util.List;

import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.SchoolFackback;
import com.cosin.web.entity.Student;

public interface ISchoolFackbackManager {

	//分页  ;搜索列表 下拉数据
    public List getSchoolFackbackLimit(String startDate,String endDate,String schoolName, String enable ,int start, int limit);
    //数量
    public int getSchoolFackbackCount();
    //获取SubjectId
  	public SchoolFackback getSchoolFackbackId(String schoolFackbackId);
    //添加 
  	public void saveSchoolFackback(SchoolFackback schoolFackback);
  	 //删除
    public void schoolFackbackDelId(String schoolFackbackId);
    //查询用户表 user
//    public List getUser();
    //添加数据
//    public void addSchoolFackback(String mode,String title,String user,String text,String schoolFackbackId);
    //根据id查询
//    public SysUser getUser2(String user);
    
    //当点击详情按钮时候  改变状态
	public void updataSchoolFackback(String schoolFackbackId);
	
	/*
	 * 
	 */
	public List<SchoolFackback> getSchoolFackbackLimit(int mode, String order,
			String sort, String ssName, int i, int j, String startDate,
			String endDate);
	/**
	 * 2016年10月23日下午5:14:36
	 *  阳朔
	 *  注释:
	 */
	public List<SchoolFackback> getSchoolFackbackLimit(String schoolsId,
			String startDate, String endDate, String ischoolName,
			String enable, Integer integer, Integer integer2);
	/**
	 * 2016年10月26日下午4:14:59
	 *  阳朔
	 *  注释:
	 */
	public List getSchoolFackbackCount(String startDate, String endDate,
			String ischoolName, String enable);
	/**
	 * 2016年10月26日下午4:31:45
	 *  阳朔
	 *  注释:
	 */
	public List getSchoolFackbackCountBySchoolId(String schoolsId,String startDate,
			String endDate, String ischoolName, String enable);
	/**
	 * 2016年11月1日下午7:13:28
	 *  阳朔
	 *  注释:
	 */
	public Parentsstudent getParentsstudentByParentId(String parentId);
	

}
