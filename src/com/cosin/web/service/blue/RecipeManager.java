package com.cosin.web.service.blue;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.utils.DateUtils;
import com.cosin.utils.JPushUtils;
import com.cosin.web.dao.blue.IActivityDao;
import com.cosin.web.dao.blue.IRecipeDao;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.Recipe;
import com.cosin.web.entity.RecipeRecord;
import com.cosin.web.entity.RecipeSchool;
import com.cosin.web.entity.School;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

@Service
@Transactional
public class RecipeManager implements IRecipeManager {

	@Autowired
	private IRecipeDao recipeDao;
	@Autowired
	private IActivityDao activityDao;

    /** 
     * 根据日期获得所在周的日期  
     * @param mdate 
     * @return 
     */  
    @SuppressWarnings("deprecation")  
    public static List<Date> dateToWeek(Date mdate) {  
        int b = mdate.getDay();  
        Date fdate;  
        List<Date> list = new ArrayList<Date>();  
        Long fTime = mdate.getTime() - b * 24 * 3600000;  
        for (int a = 1; a <= 7; a++) {  
            fdate = new Date();  
            fdate.setTime(fTime + (a * 24 * 3600000));  
            list.add(a-1, fdate);  
        }  
        return list;  
    }
	//分页
		@Override
		public List getRecipeLimit(String startDate, int start, int limit) throws ParseException {
			 // 定义输出日期格式  
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        // 比如今天是2012-12-25  
	        if(startDate!=null){
	        	List<Date> days = dateToWeek(sdf.parse(startDate));  
	        	String monday = sdf.format(days.get(0));
	        	Date mondayDate = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, monday);
	        	Calendar calendar = Calendar.getInstance();
	    		calendar.setTime(mondayDate);
	    		calendar.add(Calendar.DAY_OF_MONTH, -1);
	    		Date date = calendar.getTime();
	    		monday = DateUtils.parseDateToStr(DateUtils.defaultDatePattern, date);
	    		String fri = sdf.format(days.get(4));
	    		Date friDate = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, fri);
	    		calendar.setTime(friDate);
	    		calendar.add(Calendar.DAY_OF_MONTH, 1);
	    		Date datedf = calendar.getTime();
	    		fri = DateUtils.parseDateToStr(DateUtils.defaultDatePattern, datedf);
	        	return recipeDao.getRecipeLimit(monday,fri, start, limit);
	        }
			return recipeDao.getRecipeLimit(null,null, start, limit);
	        
		}
		//数量
		@Override
		public int getRecipeCount(String startDate) throws ParseException {
			 // 定义输出日期格式  
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        // 比如今天是2012-12-25  
	        if(startDate!=null){
	        	List<Date> days = dateToWeek(sdf.parse(startDate));  
	        	String monday = sdf.format(days.get(0));
	        	Date mondayDate = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, monday);
	        	Calendar calendar = Calendar.getInstance();
	    		calendar.setTime(mondayDate);
	    		calendar.add(Calendar.DAY_OF_MONTH, -1);
	    		Date date = calendar.getTime();
	    		monday = DateUtils.parseDateToStr(DateUtils.defaultDatePattern, date);
	    		String fri = sdf.format(days.get(4));
	    		Date friDate = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, fri);
	    		calendar.setTime(friDate);
	    		calendar.add(Calendar.DAY_OF_MONTH, 1);
	    		Date datedf = calendar.getTime();
	    		fri = DateUtils.parseDateToStr(DateUtils.defaultDatePattern, datedf);
				return recipeDao.getRecipeCount(monday,fri);
	        }
	        return recipeDao.getRecipeCount(null,null);
		}
		//获取SubjectId
		@Override
		public RecipeRecord getRecipeId(String recipeId) {
			return recipeDao.getRecipeId(recipeId);
		}
		//添加
		@Override
		public void saveRecipe(RecipeRecord recipe) {
			
		}
		
		
		//草稿箱列表查询 
		@Override
		public List<RecipeRecord> getDraftLimit(String startDate, Integer start,
				Integer limit) throws ParseException {
			 // 定义输出日期格式  
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        // 比如今天是2012-12-25  
	        if(startDate!=null){
	        	List<Date> days = dateToWeek(sdf.parse(startDate));  
	        	String monday = sdf.format(days.get(0));
	        	String fri = sdf.format(days.get(4));
	        	return recipeDao.getDraftLimit(monday,fri, start, limit);
	        }
			return recipeDao.getDraftLimit(null,null, start, limit);
		}
		
		
		//删除
		@Override
		public void recipeDelId(String recipeId) {
			recipeDao.recipeDelId(recipeId);
		}
		
		
		/**
		 * 保存 食谱
		 */
		@Override
		public void saveRecipe(String recipeId, String userId,
				String beginDate, String breakfast, String light1,
				String lunch, String light2, String dinner, String mode,String schoolVar, String draf) {

			
			if("edit".equals(mode)){
				//周一
				breakfast =  breakfast.replaceAll(",", "|");
				//周二
				light1 =  light1.replaceAll(",", "|");
				//周三
				lunch =  lunch.replaceAll(",", "|");
				//周四
				light2 =  light2.replaceAll(",", "|");
				//周五
				dinner =  dinner.replaceAll(",", "|");
				
				String strFirstDate = DateUtils.getWeekBegin(beginDate);				

				RecipeRecord recipeRecord = recipeDao.getRecipeId(recipeId);
				recipeRecord.setBeginTime(DateUtils.dateToTs(DateUtils.parseStrToDate(DateUtils.DEF_FORMAT, beginDate + " 00:00:00")));
				recipeRecord.setCreateDate(DateUtils.dateToTs(new Date()));
				recipeRecord.setIsDel(0);
				if("saveDra".equals(draf)){
					recipeRecord.setIsDraft(1);
				}else{
					recipeRecord.setIsDraft(0);
					Map<String,String> map = new HashMap<String, String>();
					map.put("type","3");
					String userList = alias(schoolVar);
					JPushUtils.testSendPush("食谱发布通知", map, userList);
				}
				recipeRecord.setLookNum(0);
				SysUser sysUser = recipeDao.getSysUserById(userId);
				recipeRecord.setSysUser(sysUser);
				
				List<Recipe> recipeList = recipeDao.getRecipeById(recipeId);
				recipeDao.delRecipe(recipeList);
				
				Date date = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, beginDate);
				
				List listNew = new ArrayList();
				List list = DateUtils.dateToWeek(date);
				for(int i=0; i<list.size(); i++)
				{
					Date str = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, list.get(i).toString());
					Recipe recipe = new Recipe();
					if(i == 0 && breakfast!="" && breakfast != null)
						recipe.setContent(breakfast);
					if(i == 1 && light1!="" && light1 != null)
						recipe.setContent(light1);
					if(i == 2 && lunch!="" && lunch != null)
						recipe.setContent(lunch);
					if(i == 3 && light2!="" && light2 != null)
						recipe.setContent(light2);
					if(i == 4 && dinner!="" && dinner != null)
						recipe.setContent(dinner);
					recipe.setCreateDate(new Timestamp(str.getTime()));
					recipe.setRecipeRecord(recipeRecord);
					recipeDao.saveRecipe(recipe);
					
				}
/*				Date firstDate = DateUtils.parseStrToDate(DateUtils.DEF_FORMAT, strFirstDate + " 00:00:00");
				
				List<Recipe> recipeList = recipeDao.getRecipeById(recipeId);
				recipeDao.delRecipe(recipeList);
				
				for(int i=0; i<5; i++)
				{
					long dd = firstDate.getTime() + i*24*60*60*1000;
					Recipe recipe = new Recipe();
					if(i == 0 && breakfast!="" && breakfast != null)
						recipe.setContent(breakfast);
					if(i == 1 && light1!="" && light1 != null)
						recipe.setContent(light1);
					if(i == 2 && lunch!="" && lunch != null)
						recipe.setContent(lunch);
					if(i == 3 && light2!="" && light2 != null)
						recipe.setContent(light2);
					if(i == 4 && dinner!="" && dinner != null)
						recipe.setContent(dinner);
					recipe.setCreateDate(DateUtils.dateToTs(new Date(dd)));
					recipe.setRecipeRecord(recipeRecord);
					recipeDao.saveRecipe(recipe);
				}*/
				
					

				recipeDao.saveRecipeRecord(recipeRecord);
				
				//真删  RecipeSchool中间表
				List<RecipeSchool> recipeSchoolList = recipeDao.getRecipeSchoolById(recipeId);
				recipeDao.delRecipeSchool(recipeSchoolList);
					
				//RecipeSchool 
				if(schoolVar!=null){
					
					String[] keyArr = schoolVar.split(",");
					for (int i = 0; i < keyArr.length; i++) {
						String key = keyArr[i];
						if (!"".equals(key) && key != null) {
							RecipeSchool recipeSchool = new RecipeSchool();
							recipeSchool.setRecipeRecord(recipeRecord);
							//添加园区 外键
							School school = recipeDao.getSchoolById(key);
							recipeSchool.setSchool(school);
							//保存 外键表
							recipeDao.saveRecipeSchool(recipeSchool);
						}
					}
				}				
				
			}else{
				
				
				//周一
				breakfast =  breakfast.replaceAll(",", "|");
				//周二
				light1 =  light1.replaceAll(",", "|");
				//周三
				lunch =  lunch.replaceAll(",", "|");
				//周四
				light2 =  light2.replaceAll(",", "|");
				//周五
				dinner =  dinner.replaceAll(",", "|");
				
				
				
				String strFirstDate = DateUtils.getWeekBegin(beginDate);
				
				
				RecipeRecord recipeRecord = new RecipeRecord();
				recipeRecord.setBeginTime(DateUtils.dateToTs(DateUtils.parseStrToDate(DateUtils.DEF_FORMAT, beginDate + " 00:00:00")));
				recipeRecord.setCreateDate(DateUtils.dateToTs(new Date()));
				recipeRecord.setIsDel(0);
				if("saveDra".equals(draf)){
					recipeRecord.setIsDraft(1);
				}else{
					recipeRecord.setIsDraft(0);
					Map<String,String> map = new HashMap<String, String>();
					map.put("type","3");
					String userList = alias(schoolVar);
					JPushUtils.testSendPush("食谱发布通知", map, userList);
				}
				recipeRecord.setLookNum(0);
				SysUser sysUser = recipeDao.getSysUserById(userId);
				recipeRecord.setSysUser(sysUser);
				
				
				
				//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				//String bDate = dateFormat.format(beginDate);
				Date date = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, beginDate);
				
				List listNew = new ArrayList();
				List list = DateUtils.dateToWeek(date);
				for(int i=0; i<list.size(); i++)
				{
					Date str = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, list.get(i).toString());
					Recipe recipe = new Recipe();
					if(i == 0 && breakfast!="" && breakfast != null)
						recipe.setContent(breakfast);
					if(i == 1 && light1!="" && light1 != null)
						recipe.setContent(light1);
					if(i == 2 && lunch!="" && lunch != null)
						recipe.setContent(lunch);
					if(i == 3 && light2!="" && light2 != null)
						recipe.setContent(light2);
					if(i == 4 && dinner!="" && dinner != null)
						recipe.setContent(dinner);
					recipe.setCreateDate(new Timestamp(str.getTime()));
					recipe.setRecipeRecord(recipeRecord);
					recipeDao.saveRecipe(recipe);
					
				}
				/*Date firstDate = DateUtils.parseStrToDate(DateUtils.DEF_FORMAT, strFirstDate + " 00:00:00");
				for(int i=0; i<5; i++)
				{
					//
					long dd = firstDate.getTime() + i*24*60*60*1000;
					Recipe recipe = new Recipe();
					if(i == 0 && breakfast!="" && breakfast != null)
						recipe.setContent(breakfast);
					if(i == 1 && light1!="" && light1 != null)
						recipe.setContent(light1);
					if(i == 2 && lunch!="" && lunch != null)
						recipe.setContent(lunch);
					if(i == 3 && light2!="" && light2 != null)
						recipe.setContent(light2);
					if(i == 4 && dinner!="" && dinner != null)
						recipe.setContent(dinner);
					recipe.setCreateDate(DateUtils.dateToTs(new Date(dd)));
					recipe.setRecipeRecord(recipeRecord);
					recipeDao.saveRecipe(recipe);
				}*/
				
					

				recipeDao.saveRecipeRecord(recipeRecord);
				
				if(schoolVar!=null){
					
					String[] keyArr = schoolVar.split(",");
					for (int i = 0; i < keyArr.length; i++) {
						String key = keyArr[i];
						if (!"".equals(key) && key != null) {
							RecipeSchool recipeSchool = new RecipeSchool();
							recipeSchool.setRecipeRecord(recipeRecord);
							//添加园区 外键
							School school = recipeDao.getSchoolById(key);
							recipeSchool.setSchool(school);
							//保存 外键表
							recipeDao.saveRecipeSchool(recipeSchool);
						}
					}
				}
			}
		}
		
		/**
		 * 2016年10月25日下午5:19:10
		 *  阳朔
		 *  注释:推送所需要的数据
		 */
		public String alias(String data){
			
			String tuiId = "";
			List listUserId = new ArrayList();
			String[] schoolStr = data.split(",");
			for (int i = 0; i < schoolStr.length; i++) {
				String schoolId = schoolStr[i];
				if(!"".equals(schoolId)){
					//根据schoolId 获取 关联的职工Id
					List<Employee> employeeList = activityDao.getEmployeeBySchoolId(schoolId);
					for (int m = 0; m < employeeList.size(); m++) {
						Employee employee = employeeList.get(m);
						String empUserId = employee.getSysUserByUserId().getUserId();
						
						SysUserRole sysUserRole = activityDao.getSysUserRoleByEmpUserId(employee.getSysUserByUserId().getUserId());
						String emp_RoleId = sysUserRole.getSysRole().getRoleId();
						List<SysRolePower> powerList = activityDao.getSysRolePowerByEmp_RoleId(emp_RoleId);
						for (int j = 0; j < powerList.size(); j++) {
							SysRolePower power = powerList.get(j);
							if("appGengduo_shipu".equals(power.getPowerCode())){
								listUserId.add(empUserId);
							}
						}
					}
					
					
					//根据schoolId 获取 关联 的所有家长Id
					List<Student> studentList = activityDao.getStudentBySchoolId(schoolId);
					for (int j = 0; j < studentList.size(); j++) {
						Student student = studentList.get(j);
						List<Parentsstudent> parentsstudentList = activityDao.getParentsstudentByStudentId(student.getStudentId());
						for (int k = 0; k < parentsstudentList.size(); k++) {
							Parentsstudent parentsstudent = parentsstudentList.get(k);
							String sysUserParentId = parentsstudent.getParents().getSysUser().getUserId();
							listUserId.add(sysUserParentId);
						}
					}
					
					
					
				}
			}
			for (int ij = 0; ij < listUserId.size(); ij++) {
				String userList = (String) listUserId.get(ij);
				tuiId +=userList +",";
			}
			
			return tuiId;
		}
		
		
		/**
		 * 通过recipeRecordId获取对象
		 */
		@Override
		public List<RecipeSchool> getRecipeSchoolById(
				String recipeRecordId) {
			
			return recipeDao.getRecipeSchoolById(recipeRecordId);
		}
		
		/**
		 * 撤回操作 存至草稿箱
		 */
		@Override
		public void roolBackRecipe(String recipeId) {

			RecipeRecord recipeRecord = recipeDao.getRecipeRecordById(recipeId);
			recipeRecord.setIsDraft(1);
			recipeDao.saveRecipeRecord(recipeRecord);
		}
		
		/**
		 * 通过recipeRecordId 查询 recipeRecord
		 */
		@Override
		public RecipeRecord getRecipeRecordById(String recipeId) {
			return recipeDao.getRecipeRecordById(recipeId);
		}
		
		/**
		 * 通过recipeRecordId 查询食谱  Recipe  周一至 周五
		 */
		@Override
		public List<Recipe> getRecipeById(String recipeId) {

			return recipeDao.getRecipeById(recipeId);
		}
		/**
		 * 2016年10月10日下午3:04:49
		 *  龚杰
		 *  注释：
		 * @throws ParseException 
		 */
		@Override
		public int getDraftCount(String startDate) throws ParseException {
			 // 定义输出日期格式  
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        // 比如今天是2012-12-25  
	        if(startDate!=null){
	        	List<Date> days = dateToWeek(sdf.parse(startDate));  
	        	String monday = sdf.format(days.get(0));
	        	Date mondayDate = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, monday);
	        	Calendar calendar = Calendar.getInstance();
	    		calendar.setTime(mondayDate);
	    		calendar.add(Calendar.DAY_OF_MONTH, -1);
	    		Date date = calendar.getTime();
	    		monday = DateUtils.parseDateToStr(DateUtils.defaultDatePattern, date);
	    		String fri = sdf.format(days.get(4));
	    		Date friDate = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, fri);
	    		calendar.setTime(friDate);
	    		calendar.add(Calendar.DAY_OF_MONTH, 1);
	    		Date datedf = calendar.getTime();
	    		fri = DateUtils.parseDateToStr(DateUtils.defaultDatePattern, datedf);
				return recipeDao.getDraftCount(monday,fri);
	        }
	        return recipeDao.getDraftCount(null,null);
		}
		/**
		 * 2016年10月23日下午4:55:26
		 *  阳朔
		 *  注释:
		 * @throws ParseException 
		 */
		@Override
		public List<RecipeSchool> getRecipeSchoolLimit(String schoolsId,
				String startDate, Integer integer, Integer integer2) throws ParseException {
			// TODO Auto-generated method stub
			List<RecipeSchool> ll = new ArrayList<RecipeSchool>();
			 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        // 比如今天是2012-12-25  
	        if(startDate!=null){
	        	List<Date> days = dateToWeek(sdf.parse(startDate));  
	        	String monday = sdf.format(days.get(0));
	        	Date mondayDate = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, monday);
	        	Calendar calendar = Calendar.getInstance();
	    		calendar.setTime(mondayDate);
	    		calendar.add(Calendar.DAY_OF_MONTH, -1);
	    		Date date = calendar.getTime();
	    		monday = DateUtils.parseDateToStr(DateUtils.defaultDatePattern, date);
	    		String fri = sdf.format(days.get(4));
	    		Date friDate = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, fri);
	    		calendar.setTime(friDate);
	    		calendar.add(Calendar.DAY_OF_MONTH, 1);
	    		Date datedf = calendar.getTime();
	    		fri = DateUtils.parseDateToStr(DateUtils.defaultDatePattern, datedf);
	        	ll = recipeDao.getRecipeSchoolLimit(schoolsId, monday,fri,integer, integer2);
	        	
	        }
	        else{
	        	ll = recipeDao.getRecipeSchoolLimit(schoolsId, null,null,integer, integer2);
	        }
			return ll;
		}
		/**
		 * 2016年12月8日下午5:25:08
		 *  阳朔
		 *  注释:
		 * @throws ParseException 
		 */
		@Override
		public List<RecipeSchool> getRecipeDraftSchoolLimit(String schoolsId,
				String startDate, Integer integer, Integer integer2) throws ParseException {
			List<RecipeSchool> ll = new ArrayList<RecipeSchool>();
			 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        // 比如今天是2012-12-25  
	        if(startDate!=null){
	        	List<Date> days = dateToWeek(sdf.parse(startDate));  
	        	String monday = sdf.format(days.get(0));
	        	Date mondayDate = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, monday);
	        	Calendar calendar = Calendar.getInstance();
	    		calendar.setTime(mondayDate);
	    		calendar.add(Calendar.DAY_OF_MONTH, -1);
	    		Date date = calendar.getTime();
	    		monday = DateUtils.parseDateToStr(DateUtils.defaultDatePattern, date);
	    		String fri = sdf.format(days.get(4));
	    		Date friDate = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, fri);
	    		calendar.setTime(friDate);
	    		calendar.add(Calendar.DAY_OF_MONTH, 1);
	    		Date datedf = calendar.getTime();
	    		fri = DateUtils.parseDateToStr(DateUtils.defaultDatePattern, datedf);
	        	ll = recipeDao.getRecipeDraftSchoolLimit(schoolsId, monday,fri,integer, integer2);
	        	
	        }
	        else{
	        	ll = recipeDao.getRecipeDraftSchoolLimit(schoolsId, null,null,integer, integer2);
	        }
			return ll;
		}
		
		
		
		
		
		
		
		
		
		
		
}