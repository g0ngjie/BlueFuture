package com.cosin.web.service.blue;

import java.io.IOException;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import antlr.Utils;

import com.cosin.utils.DateUtils;
import com.cosin.utils.HttpBodyClient;
import com.cosin.web.dao.blue.IStudentDao;
import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Parents;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.School;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysArea;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;

@Service
@Transactional
public class StudentManager implements IStudentManager {

	@Autowired
	private IStudentDao studentDao;

	//分页
		@Override
		public List<Student> getStudentLimit(String schoolName,String SsName,String intoYear,String gradeName,String classesName, int start, int limit) {
			return studentDao.getStudentLimit(schoolName,SsName,intoYear,gradeName,classesName, start, limit);
		}
		//数量
		@Override
		public int getStudentCount(String schoolName,String SsName,String intoYear,String gradeName,String classesName) {
			return studentDao.getStudentCount(schoolName,SsName,intoYear,gradeName,classesName);
		}
		//获取SubjectId
		@Override
		public Student getStudentId(String studentId) {
			return studentDao.getStudentId(studentId);
		}
		
		//删除
		@Override
		public void studentDelId(String studentId) {
			
			List<Parentsstudent> parentsstudentList = studentDao.getParentsstudentByStudentId(studentId);
			for (int i = 0; i < parentsstudentList.size(); i++) {
				Parentsstudent parentsstudent = parentsstudentList.get(i);
				studentDao.delParentsstudent(parentsstudent);
//				Parents parents = parentsstudentList.get(i).getParents();
//				parents.setIsDel(1);
//				studentDao.save(parents);
//				SysUser sysUser = parents.getSysUser();
//				sysUser.setIsDel(1);
//				studentDao.save(sysUser);
			}
			studentDao.studentDelId(studentId);
		}
		
		/**
		 *下拉菜单  根据  查园区 选项 
		 */
		@Override
		public List<School> getselectSchool() {
			return studentDao.getSelectSchool();
		}
		@Override
		public List<Student> getselectStudent() {
			return null;
		}
		/**
		 * 下拉弹窗  查询 班级Classes 
		 */
		@Override
		public List<Classes> getselectClasses() {
			return studentDao.getselectClasses();
		}
		
		/**
		 * 下拉菜单 
		 * 添加 学生
		 * 
		 * 学生表 如果要添加 涉及到 外键 shcool 和 classes 表 
		 * 关联 sys_user 和 parents 表  都需要查出来 然后 在添加
		 */
		@Override
		public void saveStudent(String studentCode,Classes classes,String schoolId,String intoYear,String gradeName,String studentId,String parentId,String relationId,String classesName,String studentName,
				String parentName,String sex,String relation,String tel,String mode,String userId,String radom) {
			
			if (mode.equals("edit")) {
			
				Student student = studentDao.getStudentId(studentId);
				//添加学生姓名
				student.setStudentName(studentName);
				
				//获取到的 id 是 班级名 id
				//Classes classes = studentDao.getClassesByData(schoolId,intoYear,gradeName,classesName);
				student.setClasses(classes);
				
				School school = studentDao.getSchoolById(schoolId);
				student.setSchool(school);
				student.setStudentCode(studentCode);
				
				SysUser user = studentDao.findUserByUserId(userId);
				student.setSysUser(user);
				studentDao.saveStudent(student);
				
				//家长
				SysUser sysuser = studentDao.getParentById(parentId);
				//添加家长姓名
				sysuser.setUserName(parentName);
				//性别
				if("男".equals(sex)||"1".equals(sex)){
					sysuser.setSex(1);
				}else{
					sysuser.setSex(0);
				}
				//添加电话
				sysuser.setMobile(tel);
				sysuser.setLoginName(tel);
				SysUser teachUser = studentDao.chaTeacherUserByLoginName(tel);
				if(teachUser != null){
					teachUser.setLoginName(tel);
					teachUser.setMobile(tel);
					studentDao.save(teachUser);
				}
				studentDao.save(sysuser);
				
				String parenUserId = sysuser.getUserId();
				Parents parents =studentDao.getParentByUserId(parenUserId);
				parents.setSysUser(sysuser);
				studentDao.save(parents);
				
				String parenId = parents.getParentId();
				Parentsstudent parentsstudent = studentDao.getParentsstudentByParenId(parenId);
				parentsstudent.setRelativeShip(relation);
				parentsstudent.setStudent(student);
				parentsstudent.setParents(parents);
				studentDao.save(parentsstudent);
				
				
			}else{
					String classesId = classes.getClassesId();
					Student isStudent = studentDao.chaStudentByData(studentName,classesId,schoolId);
					if(isStudent != null){
						
						SysUser user = studentDao.findUserByUserId(userId);
						//添加 创建者
						isStudent.setSysUser(user);
						//保存 student
						studentDao.saveStudent(isStudent);
						
						//查询是否存在 根据tpye = 1 ，tel 
						SysUser sysuserParent = studentDao.getSysUserParentByTel(tel);
						if(sysuserParent!= null){//一个家长关联多个学生
							
							//亲戚关系
							/*Parents parents = new Parents();
							//没用
							parents.setRelation(relation);
							parents.setCreateDate(new Timestamp(new Date().getTime()));
							parents.setEnable(1);
							parents.setIsDel(0);
							parents.setSysUser(sysuserParent);
							//保存 
							studentDao.save(parents);*/
							Parents parents = studentDao.getParentByUserId(sysuserParent.getUserId());
							
							//中间表关联 parentId 和 studentId
							Parentsstudent parentsstudent = new Parentsstudent();
							parentsstudent.setStudent(isStudent);
							parentsstudent.setParents(parents);
							parentsstudent.setActivityNum(new Timestamp(new Date().getTime()));
							parentsstudent.setNoticeNum(new Timestamp(new Date().getTime()));
							parentsstudent.setRecipeNum(new Timestamp(new Date().getTime()));
							parentsstudent.setLeaveNum(new Timestamp(new Date().getTime()));
							parentsstudent.setKaoqinNum(new Timestamp(new Date().getTime()));
							//添加关系
							parentsstudent.setRelativeShip(relation);
							studentDao.save(parentsstudent);
							
						}else{
							
							SysUser sysuser = new SysUser();
							//添加家长姓名
							sysuser.setUserName(parentName);
							
							//查询 老师姓名是否存在
							SysUser teachUser = studentDao.getTeachUserByLogin(tel);
							if(teachUser != null&&!"".equals(teachUser)){
								String pwd = teachUser.getPwd();
								if("000000".equals(pwd)){
									sysuser.setPwd("000000");
								}else{
									sysuser.setPwd(pwd);
								}
							}else{
								
								//pwd
								sysuser.setPwd("000000");
							}
							//用户名
							sysuser.setLoginName(tel);
							//随机 生成6位
//							String emobCode = DateUtils.getCharAndNumr(6);
							sysuser.setEmobCode(radom);
							//添加电话
							sysuser.setMobile(tel);
							//性别
							if("男".equals(sex)||"1".equals(sex)){
								sysuser.setSex(1);
							}else{
								sysuser.setSex(0);
							}
							
							sysuser.setCreateDate(new Timestamp(new Date().getTime()));
							
							sysuser.setActivityNum(new Timestamp(new Date().getTime()));
							sysuser.setNoticeNum(new Timestamp(new Date().getTime()));
							sysuser.setRecipeNum(new Timestamp(new Date().getTime()));
							sysuser.setLeaveNum(new Timestamp(new Date().getTime()));
							sysuser.setKaoqinNum(new Timestamp(new Date().getTime()));
							
							sysuser.setIsDel(0);
							sysuser.setType(1);
							sysuser.setEnable(0);
							sysuser.setIcon("headIcon.png");
							
							studentDao.save(sysuser);
							
							//亲戚关系
							Parents parents = new Parents();
							//没用
							parents.setRelation(relation);
							parents.setCreateDate(new Timestamp(new Date().getTime()));
							parents.setEnable(1);
							parents.setIsDel(0);
							parents.setSysUser(sysuser);
							//保存 
							studentDao.save(parents);
							
							//中间表关联 parentId 和 studentId
							Parentsstudent parentsstudent = new Parentsstudent();
							parentsstudent.setStudent(isStudent);
							parentsstudent.setParents(parents);
							parentsstudent.setActivityNum(new Timestamp(new Date().getTime()));
							parentsstudent.setNoticeNum(new Timestamp(new Date().getTime()));
							parentsstudent.setRecipeNum(new Timestamp(new Date().getTime()));
							parentsstudent.setLeaveNum(new Timestamp(new Date().getTime()));
							parentsstudent.setKaoqinNum(new Timestamp(new Date().getTime()));
							//添加关系
							parentsstudent.setRelativeShip(relation);
							studentDao.save(parentsstudent);
							
						}
						
					}else{
						
						Student student = new Student();
						//添加学生姓名
						student.setStudentName(studentName);
						student.setIsDel(0);
						student.setEnable(1);
						student.setIsDefault(1);
						student.setEmbCode(DateUtils.getCharAndNumr(6));
						student.setCreateDate(new Timestamp(new Date().getTime()));
						student.setStudentIcon("headIcon.png");
						//随机数
						//String radom1 = DateUtils.getRodamNumr(6);
						student.setStudentCode(studentCode);
						
						//获取到的 id 是 班级名 id
						//Classes classes = studentDao.getClassesByData(schoolId,intoYear,gradeName,classesName);
						student.setClasses(classes);
						
						School school = studentDao.getSchoolById(classes.getSchool().getSchoolId());
						student.setSchool(school);
						
						SysUser user = studentDao.findUserByUserId(userId);
						//添加 创建者
						student.setSysUser(user);
						SysUser sysuserParent = studentDao.getSysUserParentByTel(tel);
					
//						Parents parentss = studentDao.getParentByUserId(sysuserParent.getUserId());
//						student.setParents(parentss);
//						
//						studentDao.saveStudent(student);
						//查询是否存在 根据tpye = 1 ，tel 
						
						if(sysuserParent!= null){
							
							
							//亲戚关系
							/*Parents parents = new Parents();
							//没用
							parents.setRelation(relation);
							parents.setCreateDate(new Timestamp(new Date().getTime()));
							parents.setEnable(1);
							parents.setIsDel(0);
							parents.setSysUser(sysuserParent);
							//保存 
							studentDao.save(parents);*/
							
							Parents parentss = studentDao.getParentByUserId(sysuserParent.getUserId());
							student.setParents(parentss);
							studentDao.saveStudent(student);
							
							//中间表关联 parentId 和 studentId
							Parentsstudent parentsstudent = new Parentsstudent();
							parentsstudent.setStudent(student);
							parentsstudent.setEmobCode(radom);
							parentsstudent.setActivityNum(new Timestamp(new Date().getTime()));
							parentsstudent.setNoticeNum(new Timestamp(new Date().getTime()));
							parentsstudent.setRecipeNum(new Timestamp(new Date().getTime()));
							parentsstudent.setLeaveNum(new Timestamp(new Date().getTime()));
							parentsstudent.setKaoqinNum(new Timestamp(new Date().getTime()));
							parentsstudent.setParents(parentss);
							//添加关系
							parentsstudent.setRelativeShip(relation);
							studentDao.save(parentsstudent);
							
						}else{
							
							SysUser sysuser = new SysUser();
							//添加家长姓名
							sysuser.setUserName(parentName);
							//查询 老师姓名是否存在
							SysUser teachUser = studentDao.getTeachUserByLogin(tel);
							if(teachUser != null&&!"".equals(teachUser)){
								String pwd = teachUser.getPwd();
								if("000000".equals(pwd)){
									sysuser.setPwd("000000");
								}else{
									sysuser.setPwd(pwd);
								}
							}else{
								
								//pwd
								sysuser.setPwd("000000");
							}
							//用户名
							sysuser.setLoginName(tel);
							//随机 生成6位
//							String emobCode = DateUtils.getCharAndNumr(6);
							sysuser.setEmobCode(radom);
							//添加电话
							sysuser.setMobile(tel);
							//性别
							if("男".equals(sex)||"1".equals(sex)){
								sysuser.setSex(1);
							}else{
								sysuser.setSex(0);
							}
							sysuser.setCreateDate(new Timestamp(new Date().getTime()));
							
							sysuser.setActivityNum(new Timestamp(new Date().getTime()));
							sysuser.setNoticeNum(new Timestamp(new Date().getTime()));
							sysuser.setRecipeNum(new Timestamp(new Date().getTime()));
							sysuser.setLeaveNum(new Timestamp(new Date().getTime()));
							sysuser.setKaoqinNum(new Timestamp(new Date().getTime()));
							
							sysuser.setIsDel(0);
							sysuser.setType(1);
							sysuser.setEnable(0);
							sysuser.setIcon("headIcon.png");
							
							studentDao.save(sysuser);
							
							//亲戚关系
							Parents parents = new Parents();
							//没用
							parents.setRelation(relation);
							parents.setCreateDate(new Timestamp(new Date().getTime()));
							parents.setEnable(1);
							parents.setIsDel(0);
							parents.setStudent(student);
							parents.setSysUser(sysuser);
							//保存 
							studentDao.save(parents);
							
							
							student.setParents(parents);
							
							//保存 student
							studentDao.saveStudent(student);
							
							//中间表关联 parentId 和 studentId
							Parentsstudent parentsstudent = new Parentsstudent();
							parentsstudent.setStudent(student);
							parentsstudent.setParents(parents);
							parentsstudent.setEmobCode(radom);
							parentsstudent.setActivityNum(new Timestamp(new Date().getTime()));
							parentsstudent.setNoticeNum(new Timestamp(new Date().getTime()));
							parentsstudent.setRecipeNum(new Timestamp(new Date().getTime()));
							parentsstudent.setLeaveNum(new Timestamp(new Date().getTime()));
							parentsstudent.setKaoqinNum(new Timestamp(new Date().getTime()));
							//添加关系
							parentsstudent.setRelativeShip(relation);
							studentDao.save(parentsstudent);
							
						}
					}
				
					
			}
			
			
		}
		
		/**
		 * 修改选项里面  查询学生表
		 */
		@Override
		public Student findStudentById(String studentId) {
			
			return studentDao.findStudentById(studentId);
		}
		
		/**
		 * 通过 userId  查询 user
		 */
		@Override
		public SysUser getSysUserById(String userId) {
			return studentDao.getSysUserById(userId);
		}
		
		/**
		 * 查询  班级 届数  去重复
		 */
		@Override
		public List getListIntoYear(String schoolId) {
			List list = new ArrayList();
			List<Classes> listIntoYear = studentDao.getClassesBySchoolId(schoolId);
			for (int i = 0; i < listIntoYear.size(); i++) {
				Classes classes = listIntoYear.get(i);
				Map map = new HashMap();
				map.put("intoYear", classes.getIntoYear());
				map.put("yearId", classes.getIntoYear());
				map.put("schoolId", schoolId);
				
				list.add(map);
			}
			
			for  ( int  i  =   0 ; i  <  list.size()  -   1 ; i ++ )   { 
			    for  ( int  j  =  list.size()  -   1 ; j  >  i; j -- )   { 
			      if  (list.get(j).equals(list.get(i)))   { 
			        list.remove(j); 
			      } 
			    } 
			  } 
			
			return list;
		}
		
		/**
		 * 根据 届数 查年级
		 */
		@Override
		public List getListGradeByIntoyear(String schoolId,String intoYear) {
			
			return studentDao.getListGradeByIntoyear(schoolId,intoYear);
		}
		
		/**
		 * 根据grade 查询 className
		 */
		@Override
		public List getListClassName(String schoolId,String intoYear,String grade) {

			List list = new ArrayList();
			String gra = null;
			if("托班".equals(grade)){
				gra = "1";
			}else if("小班".equals(grade)){
				gra = "2";
			}else if("中班".equals(grade)){
				gra = "3";
			}else if("大班".equals(grade)){
				gra = "4";
			}else if("学前班".equals(grade)){
				gra = "5";
			}
			List<Classes> className = studentDao.getListClassNameByGrade(schoolId,intoYear,gra);
			for (int i = 0; i < className.size(); i++) {
				Classes classes = className.get(i);
				Map map = new HashMap();
				map.put("classesId", classes.getClassesName());
				map.put("classesName", classes.getClassesName());
				map.put("schoolId", schoolId);
				map.put("intoYear", intoYear);
				list.add(map);
			}
			
			for  ( int  i  =   0 ; i  <  list.size()  -   1 ; i ++ )   { 
			    for  ( int  j  =  list.size()  -   1 ; j  >  i; j -- )   { 
			      if  (list.get(j).equals(list.get(i)))   { 
			        list.remove(j); 
			      } 
			    } 
			  } 
			return list;
		}
		
		/**
		 * 判断导入
		 */
		@Override
		public String chaReport(String userId,String dirPre, InputStream inputStream) {
			String listData ="";
	        try {  
	            String cellStr = null;  
	  
	            Workbook wb = WorkbookFactory.create(inputStream);  
	            
	            Sheet sheet =  wb.getSheetAt(0);
	            
	            for (int i = 1; i <= sheet.getLastRowNum(); i++) {  
	            	int  p = 0;
	            	String eveData = "";
	            	String relationData = "";
	            	School schoolData = null;
	            	Classes classData = null;
	            	String pareName = "";
	            	
	                Row row = sheet.getRow(i); 
	               // System.out.println(row);
	                if (row == null) {
	                    
	                    continue;
	                    
	                }  
	                
	                int x = row.getLastCellNum();
	                if(x<9){
	                	break;
	                }
	                
	                for (int j = 0; j < row.getLastCellNum(); j++) {  
	                	
	                    Cell cell = row.getCell(j); 
	                    if(cell != null && !"".equals(cell)){
	                    
	                    cellStr = ConvertCellStr(cell, cellStr);  
	                    
	                    if(j == 0){//园区名
	                    	School school = studentDao.getSchoolByName(cellStr);
	                    	if("".equals(school)||school==null){
                    			listData +="第"+i+"条,查无此园区,";
                    			break;
                    		}
	                    	schoolData = school;
	                    	
	                    }else if(j == 1){//届数
	                    	String[] grad = cellStr.split("届");
	                    	if(grad.length > 0){
	                    		eveData +=grad[0]+",";
	                    	}
	                    	
	                    }else if(j == 2){//年级
	                    	eveData +=cellStr+",";
	                    	
	                    }else if(j == 3){//班级
	                    	eveData +=cellStr+",";
	                    	String[] str = eveData.split(",");
	                    	if(!"".equals(str)&&!"null".equals(str)){
	                    			
                    			if("托班".equals(str[1])){
                    				String num = "1";
                    				Classes classes = studentDao.getClassesByDataName(schoolData.getSchoolId(),str[0],num,str[2]);
                    				if("".equals(classes)||classes==null){
                            			listData +="第"+i+"条,查无此班级,";
                            			break;
                            		}
                    				classData = classes;
                    				
                    			}else if("小班".equals(str[1])){
                    				String num = "2";
                    				Classes classes = studentDao.getClassesByDataName(schoolData.getSchoolId(),str[0],num,str[2]);
                    				if("".equals(classes)||classes==null){
                            			listData +="第"+i+"条,查无此班级,";
                            			break;
                            		}
                    				classData = classes;
                    			}else if("中班".equals(str[1])){
                    				String num = "3";
                    				Classes classes = studentDao.getClassesByDataName(schoolData.getSchoolId(),str[0],num,str[2]);
                    				if("".equals(classes)||classes==null){
                            			listData +="第"+i+"条,查无此班级,";
                            			break;
                            		}
                    				classData = classes;
                    			}else if("大班".equals(str[1])){
                    				String num = "4";
                    				Classes classes = studentDao.getClassesByDataName(schoolData.getSchoolId(),str[0],num,str[2]);
                    				if("".equals(classes)||classes==null){
                            			listData +="第"+i+"条,查无此班级,";
                            			break;
                            		}
                    				classData = classes;
                    			}else if("学前班".equals(str[1])){
                    				String num = "5";
                    				Classes classes = studentDao.getClassesByDataName(schoolData.getSchoolId(),str[0],num,str[2]);
                    				if("".equals(classes)||classes==null){
                            			listData +="第"+i+"条,查无此班级,";
                            			break;
                            		}
                    				classData = classes;
                    			}
	                    	}
	                    	
	                    }else if(j == 4){//学生姓名
	                    	String claId = classData.getClassesId();
	                    	String schId = schoolData.getSchoolId();
	                    	Student chaStudent = studentDao.chaStudentByData(cellStr, claId, schId);
	                    	if(!"".equals(chaStudent)&&chaStudent!=null){
                    			listData +="第"+i+"条,该学生已存在,";
                    			break;
                    		}
	                    }else if(j == 5){//家长姓名
	                    	
	                    	//添加家长姓名
	                    	pareName = cellStr;
	                    	
	                    }else if(j == 6){//亲戚关系
	                    	
	                    	relationData += cellStr;
	                    }else if(j == 7){//联系电话
	        				
	                    }else if(j == 9){//学生编号
	                    	int ss = cellStr.length();
	                    	if(cellStr.length() == 5){
	                    		
	                    		Student student = studentDao.chaStudentByCode(cellStr);
	                    		if(!"".equals(student)&&student!=null){
	                    			listData +="第"+i+"条,该学生编号已存在,";
	                    			break;
	                    		}
	                    	}else{
	                    		listData +="第"+i+"条,该条学生编号格式不正确,";
                    			break;
	                    	}
	                    }
	                }
	            }
            }
	        } catch (InvalidFormatException e) {  
	            e.printStackTrace();  
	            listData = "请导入正确表格";
	        } catch (IOException e) {  
	            e.printStackTrace();  
	            listData = "请导入正确表格";
	        } finally {  
	            if (inputStream != null) {  
	                try { 
	                    inputStream.close();  
	                } catch (IOException e) {  
	                    e.printStackTrace();  
	                    listData = "请导入正确表格";
	                }  
	            } else {  
	                
	            }  
	        }
			return listData;  
		}
		
		/**
		 * 批量导入
		 */
		@Override
		public String readReport(String userId,String dirPre, InputStream inputStream) {
			String listData ="";
	        try {  
	            String cellStr = null;  
	  
	            Workbook wb = WorkbookFactory.create(inputStream);  
	            
	            Sheet sheet =  wb.getSheetAt(0);
	            
	            for (int i = 1; i <= sheet.getLastRowNum(); i++) {  
	            	int  p = 0;
	            	String eveData = "";
	            	String relationData = "";
	            	School schoolData = null;
	            	Classes classData = null;
	            	String pareName = "";
	            	String phone = "";
	            	
	            	Student student = new Student();
	            	SysUser parentUser = new SysUser();
	            	//亲戚关系
					Parents parents = null;
					
					Parentsstudent parentsstudent = new Parentsstudent();
	            	
	                Row row = sheet.getRow(i); 
	               // System.out.println(row);
	                if (row == null) {
	                    
	                    continue;
	                    
	                }  
	                
	                int x = row.getLastCellNum();
	                if(x<9){
	                	break;
	                }
	                
	                for (int j = 0; j < row.getLastCellNum(); j++) {  
	                	
	                    Cell cell = row.getCell(j); 
	                    if(cell != null && !"".equals(cell)){
	                    
	                    cellStr = ConvertCellStr(cell, cellStr);  
	                    
	                    if(j == 0){//园区名
	                    	School school = studentDao.getSchoolByName(cellStr);
	                    	if("".equals(school)||school==null){
                    			listData +="第"+i+"条,查无此园区,";
                    			break;
                    		}
	                    	schoolData = school;
	                    	
	                    }else if(j == 1){//届数
	                    	String[] grad = cellStr.split("届");
	                    	if(grad.length > 0){
	                    		eveData +=grad[0]+",";
	                    	}
	                    	
	                    }else if(j == 2){//年级
	                    	eveData +=cellStr+",";
	                    	
	                    }else if(j == 3){//班级
	                    	eveData +=cellStr+",";
	                    	String[] str = eveData.split(",");
	                    	if(!"".equals(str)&&!"null".equals(str)){
	                    			
                    			if("托班".equals(str[1])){
                    				String num = "1";
                    				Classes classes = studentDao.getClassesByDataName(schoolData.getSchoolId(),str[0],num,str[2]);
                    				if("".equals(classes)||classes==null){
                            			listData +="第"+i+"条,查无此班级,";
                            			break;
                            		}
                    				classData = classes;
                    				
                    			}else if("小班".equals(str[1])){
                    				String num = "2";
                    				Classes classes = studentDao.getClassesByDataName(schoolData.getSchoolId(),str[0],num,str[2]);
                    				if("".equals(classes)||classes==null){
                            			listData +="第"+i+"条,查无此班级,";
                            			break;
                            		}
                    				classData = classes;
                    			}else if("中班".equals(str[1])){
                    				String num = "3";
                    				Classes classes = studentDao.getClassesByDataName(schoolData.getSchoolId(),str[0],num,str[2]);
                    				if("".equals(classes)||classes==null){
                            			listData +="第"+i+"条,查无此班级,";
                            			break;
                            		}
                    				classData = classes;
                    			}else if("大班".equals(str[1])){
                    				String num = "4";
                    				Classes classes = studentDao.getClassesByDataName(schoolData.getSchoolId(),str[0],num,str[2]);
                    				if("".equals(classes)||classes==null){
                            			listData +="第"+i+"条,查无此班级,";
                            			break;
                            		}
                    				classData = classes;
                    			}else if("学前班".equals(str[1])){
                    				String num = "5";
                    				Classes classes = studentDao.getClassesByDataName(schoolData.getSchoolId(),str[0],num,str[2]);
                    				if("".equals(classes)||classes==null){
                            			listData +="第"+i+"条,查无此班级,";
                            			break;
                            		}
                    				classData = classes;
                    			}
	                    	}
	                    	
	                    }else if(j == 4){//学生姓名
	                    	String claId = classData.getClassesId();
	                    	String schId = schoolData.getSchoolId();
	                    	Student chaStudent = studentDao.chaStudentByData(cellStr, claId, schId);
	                    	if(!"".equals(chaStudent)&&chaStudent!=null){
                    			listData +="第"+i+"条,该学生已存在,";
                    			break;
                    		}
	                    	student.setStudentName(cellStr);
	                    }else if(j == 5){//家长姓名
	                    	
	                    	//添加家长姓名
	                    	pareName = cellStr;
	                    	
	                    }else if(j == 6){//亲戚关系
	                    	
	                    	relationData += cellStr;
	                    }else if(j == 7){//联系电话
	                    	SysUser parUser = studentDao.getSysUserParentByTel(cellStr);//家长
	                    	phone = cellStr;
	                    	parents = new Parents();
	                    	if(!"".equals(parUser)&&parUser!=null){
                    			//listData +="第"+i+"条,此家长已存在,";
                    			//break;
	                    		parentUser = parUser;
	                    		parents = studentDao.getParentByUserId(parUser.getUserId());
                    		}
	                    	parentUser.setMobile(cellStr);
		                	parentUser.setLoginName(cellStr);
		                	
	                }else if(j == 8){//家长性别
	                	if("男".equals(cellStr)){
	                		parentUser.setSex(1);
                    	}else{
                    		parentUser.setSex(0);
                    	}
                    }else if(j == 9){
	                	parentUser.setUserName(pareName);
	                	parentUser.setPwd("000000");
	                	//家长保存
	                	SysUser teaUser = studentDao.getSysUserTeaByTel(phone);//老师
	                	if(teaUser!=null){
	                		parentUser.setPwd(teaUser.getPwd());
	                	}
	                	parentUser.setIcon("headIcon.png");
	                	String num = DateUtils.getCharAndNumr(6);
	                	parentUser.setEmobCode(num);
	                	parentUser.setCreateDate(new Timestamp(new Date().getTime()));
	                	parentUser.setIsDel(0);
	                	parentUser.setType(1);
	                	parentUser.setEnable(0);
	                	studentDao.save(parentUser);
	                	
	                	//关系保存 关系 不走这个表
	                	//parents.setRelation(relationData);
	                	parents.setCreateDate(new Timestamp(new Date().getTime()));
	                	parents.setSysUser(parentUser);
	                	parents.setEnable(1);
	                	parents.setIsDel(0);
	                	
	                	studentDao.save(parents);
	                	
	                	student.setClasses(classData);	
	                	student.setSchool(schoolData);	
	                	//编号
	                	student.setStudentCode(cellStr);
	                	student.setEmbCode(DateUtils.getCharAndNumr(6));
	                	student.setStudentIcon("headIcon.png");
	                	student.setIsDel(0);
	                	student.setEnable(1);
	                	student.setIsDefault(1);
	                	student.setCreateDate(new Timestamp(new Date().getTime()));
	                	SysUser createUser = studentDao.getSysUserById(userId);
	                	student.setSysUser(createUser);
	                	student.setParents(parents);
	                	
	                	studentDao.saveStudent(student);
	                	
	                	parents.setStudent(student);
	                	studentDao.save(parents);
	                	
	                	parentsstudent.setParents(parents);
	                	parentsstudent.setRelativeShip(relationData);
	                	parentsstudent.setStudent(student);
	                	
	                	String radom = DateUtils.getCharAndNumr(6);
	                	parentsstudent.setEmobCode(radom);
	                	try {
	                		HttpBodyClient.postBody("https://a1.easemob.com/bluefuture/future/users", "{\"username\":\""+radom+"\", \"password\":\"111\"}");
	                	} catch (Exception e) {
	                		e.printStackTrace();
	                	}
	                	parentsstudent.setActivityNum(new Timestamp(new Date().getTime()));
	                	parentsstudent.setNoticeNum(new Timestamp(new Date().getTime()));
	                	parentsstudent.setRecipeNum(new Timestamp(new Date().getTime()));
	                	parentsstudent.setLeaveNum(new Timestamp(new Date().getTime()));
	                	parentsstudent.setKaoqinNum(new Timestamp(new Date().getTime()));
	                	studentDao.saveParentsstudent(parentsstudent);
	                	
	                }
	            }
	            }
            }
	        } catch (InvalidFormatException e) {  
	            e.printStackTrace();  
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        } finally {  
	            if (inputStream != null) {  
	                try { 
	                    inputStream.close();  
	                } catch (IOException e) {  
	                    e.printStackTrace();  
	                }  
	            } else {  
	                
	            }  
	        }
			return listData;  
		}
		/** 
	     * 把单元格内的类型转换至String类型 
	     */ 
		 private String ConvertCellStr(Cell cell, String cellStr) {  
			  
		        switch (cell.getCellType()) {  
		  
		        case Cell.CELL_TYPE_STRING:  
		        	// 读取String  
		            cellStr = cell.getStringCellValue().toString();  
		            break;
		  
		        case Cell.CELL_TYPE_BOOLEAN:  
		        	 // 得到Boolean对象的方法  
		            cellStr = String.valueOf(cell.getBooleanCellValue());  
		            break;  
		  
		        case Cell.CELL_TYPE_NUMERIC:  
		  
		        	// 先看是否是日期格式  
		            if (DateUtil.isCellDateFormatted(cell)) {  
		            	// 读取日期格式  
		                cellStr = cell.getDateCellValue().toString();  
		  
		            } else {  
		  
		            	// 读取数字  
		                cellStr = String.valueOf(cell.getNumericCellValue());  
		            }  
		            break;  
		  
		        case Cell.CELL_TYPE_FORMULA:  
		        	// 读取公式  
		            cellStr = cell.getCellFormula().toString();  
		            break;  
		        }  
		        return cellStr;  
		    }
		 
		 /**
		  * 园区 全查
		  */
		@Override
		public List<School> getListSchool() {
			return studentDao.getListSchool();
		}
		/**
		 * 2016年10月8日上午11:18:17
		 *  龚杰
		 *  注释：
		 */
		@Override
		public Classes chaClasses(String schoolId, String intoYear,
				String gradeName, String classesName) {
			
			return studentDao.getClassesByData(schoolId, intoYear, gradeName, classesName);
			
		}
		/**
		 * 2016年10月14日下午4:11:16
		 *  阳朔
		 *  注释:通过parentId  查询 中间表
		 */
		@Override
		public List<Parentsstudent> getParentsstudentByParentId(String parentId) {
			return studentDao.getParentsstudentByParentId(parentId);
		}
		/**
		 * 2016年10月14日下午4:17:32
		 *  阳朔
		 *  注释:注释:通过studentId 查询 中间表
		 */
		@Override
		public List<Parentsstudent> getParentsstudentByStudentId(
				String studentId) {
			
			return studentDao.getParentsstudentByStudentId(studentId);
		}
		/**
		 * 2016年10月23日下午5:33:24
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List<Student> getStudentLimit(String classIds,
				String schoolName, String ssName, String intoYear,
				String gradeName, String classesName, Integer integer,
				Integer integer2) {
			// TODO Auto-generated method stub
			return studentDao.getStudentLimit(classIds,schoolName,ssName,intoYear,gradeName,classesName, integer, integer2);
		}
		/**
		 * 2016年10月26日下午4:48:14
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List getStudentCountByClassId(String classIds,
				String schoolName, String ssName, String intoYear,
				String gradeName, String classesName) {
			// TODO Auto-generated method stub
			return studentDao.getStudentCountByClassId(classIds,schoolName,ssName,intoYear,gradeName,classesName);
		}
		/**
		 * 2016年11月17日下午4:54:59
		 *  阳朔
		 *  注释:
		 */
		@Override
		public Student chaStudentByCode(String studentCode) {
			// TODO Auto-generated method stub
			return studentDao.chaStudentByCode(studentCode);
		}

}
