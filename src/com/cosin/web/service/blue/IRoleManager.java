package com.cosin.web.service.blue;

import java.util.List;

import javax.management.relation.Role;

import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;

public interface IRoleManager {

	// 分页
	public List<SysRole> getRoleLimit(int start, int limit);

	// 数量
	public int getRoleCount();

	// 获取sysrole Id  删除时可以用
	public SysRole getRoleById(String roleId);

	// 添加
	public void saveRole(String nianji,String appGengduo_shipu,String appGengduo_huodong,String appGengduo_gonggao,String appGengduo,String appCheckIn_chaxunlan,String appCheckIn_chuli,String appCheckIn_jieshou,String appCheckIn_qingjia,String appCheckIn_kejian,String appCheckIn,
			String subRoleIdd,String roleId,String roleName, String typeName,
			String schoolNameData, String rolelevel, String mode, String userId, String employee_daochu, String employee_enable, String employee_del, String employee_daoruList, String employee_daoru, String employee_add,
			String employee_kejian, String employee, String role_del, String role_xiugai, String role_add, String role_kejian, String role, String student_daochu, String student_del, String student_xiugai, String student_addList, String student_add,
			String student_kejian, String student, String classes_lishi, String classes_zhuxiao, String classes_up, String classes_del, String classes_xiugai, String classes_addList, String classes_add, String classes_kejian, String classes,
			String park_del, String park_xiugai, String park_add, String park_kejian, String park, String mailbox_daochu, String mailbox_del, String mailbox_chakan, String mailbox_kejian, String mailbox, String recipe_del, String recipe_chehui,
			String recipe_chakan, String recipe_new, String recipe_kejian, String recipe, String activity_del, String activity_chehui, String activity_chakan, String activity_new, String activity_kejian, String activity, String notice_daochu,
			String notice_del, String notice_chehui, String notice_chakan, String notice_caogao, String notice_new, String notice_kejian, String notice, String gaiLan_guanyu, String gaiLan_xinxi, String gaiLan_kejian,String gaiLan);

	// 删除
	public void delRole(String roleId);

	/**
	 * 查询中间表
	 * @param roleId
	 * @return
	 */
	public List<SysRoleSchoolPower> getsysRoleSchoolPowerByRoleId(String roleId);

	/**
	 * 根据id 查询 sysSubrole表里面的 角色级别
	 * @param roleId
	 * @return
	 */
	public SysSubrole getsysSubRoleById(String roleId);
	/**
	 * 通过roleId 全查关联所有的类型
	 * @param roleId
	 * @return
	 */
	public List<SysSubrole> getsysSubRoleListById(String roleId);

	/**
	 * 2016年10月9日下午12:01:20
	 *  龚杰
	 *  注释：
	 */
	public List<SysRolePower> getsysRolePowerByRoleId(String roleId);

	/**
	 * 2016年10月17日下午5:46:50
	 *  阳朔
	 *  注释:查询 角色名称是否存在
	 */
	public SysRole chaRolebyRoleName(String roleName);

	/**
	 * 2016年10月22日上午1:56:07
	 *  阳朔
	 *  注释:
	 */
	public List<SysUser> chaSysUserBySubId(String subRoleId);

	/**
	 * 2016年10月23日下午5:49:11
	 *  阳朔
	 *  注释:
	 */
	public List<SysRole> getRoleLimit(String schoolsId, Integer integer,
			Integer integer2);

	/**
	 * 2016年10月26日下午4:52:02
	 *  阳朔
	 *  注释:
	 */
	public List getRoleCountBySchoolId(String schoolsId);


	


	


}
