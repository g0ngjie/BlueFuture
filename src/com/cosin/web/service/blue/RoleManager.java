package com.cosin.web.service.blue;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.relation.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.blue.IRoleDao;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysPowerItem;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

@Service
@Transactional
public class RoleManager implements IRoleManager {

	@Autowired
	private IRoleDao roleDao;
		
	
		/**
		 * 分页
		 */
		@Override
		public List<SysRole> getRoleLimit(int start, int limit) {
			return roleDao.getRoleLimit(start, limit);
		}
		/**
		 * 根据id 查询 sysSubrole表里面的 角色级别
		 */
		@Override
		public SysSubrole getsysSubRoleById(String roleId) {
			return roleDao.getsysSubRoleById(roleId);
		}
		
		/**
		 * 数量
		 */
		@Override
		public int getRoleCount() {
			return roleDao.getRoleCount();
		}
		
		/**
		 * 查询中间表
		 */
		@Override
		public List<SysRoleSchoolPower> getsysRoleSchoolPowerByRoleId(
				String roleId) {
			return roleDao.getsysRoleSchoolPowerByRoleId(roleId);
		}
				
		/**
		 * 添加
		 */
		@Override
		public void saveRole(String nianji,String appGengduo_shipu,String appGengduo_huodong,
				String appGengduo_gonggao,String appGengduo,String appCheckIn_chaxunlan,
				String appCheckIn_chuli,String appCheckIn_jieshou,String appCheckIn_qingjia,String appCheckIn_kejian,String appCheckIn,
				String subRoleId,String roleId, String roleName, String typeName,
				String schoolNameData, String rolelevel, String mode,
				String userId, String employee_daochu, String employee_enable,
				String employee_del, String employee_xiugai,
				String employee_daoru, String employee_add,
				String employee_kejian, String employee, String role_del,
				String role_xiugai, String role_add, String role_kejian,
				String role, String student_daochu, String student_del,
				String student_xiugai, String student_addList,
				String student_add, String student_kejian, String student,
				String classes_lishi, String classes_zhuxiao,
				String classes_up, String classes_del, String classes_xiugai,
				String classes_addList, String classes_add,
				String classes_kejian, String classes, String park_del,
				String park_xiugai, String park_add, String park_kejian,
				String park, String mailbox_daochu, String mailbox_del,
				String mailbox_chakan, String mailbox_kejian, String mailbox,
				String recipe_del, String recipe_chehui, String recipe_chakan,
				String recipe_new, String recipe_kejian, String recipe,
				String activity_del, String activity_chehui,
				String activity_chakan, String activity_new,
				String activity_kejian, String activity, String notice_daochu,
				String notice_del, String notice_chehui, String notice_chakan,
				String notice_caogao, String notice_new, String notice_kejian,
				String notice, String gaiLan_guanyu, String gaiLan_xinxi,
				String gaiLan_kejian,String gaiLan) {
			
			
			
			if (mode.equals("edit")) {
				
				List<SysRoleSchoolPower> sysRoleSchoolPowerList = roleDao.getsysRoleSchoolPowerByRoleId(roleId);
				for (int i = 0; i < sysRoleSchoolPowerList.size(); i++) {
					SysRoleSchoolPower sysRoleSchoolPower = sysRoleSchoolPowerList.get(i);
					roleDao.delObj(sysRoleSchoolPower);
				}
				
				SysRole sysRole = roleDao.getRoleById(roleId);
				sysRole.setRolerName(roleName);
				SysUser sysUser = roleDao.findByUserId(userId);
//				if(nianji != null){
//					
//					sysRole.setGrade(new Integer(nianji));
//				}else{
//					sysRole.setGrade(null);
//				}
				
				//添加创建者
				sysRole.setSysUser(sysUser);
				
				int typ = 0;
				if(schoolNameData!=null){
					String[] keyArr = schoolNameData.split(",");
					for (int i = 0; i < keyArr.length; i++) {
						String key = keyArr[i];
						if (!"".equals(key) && key != null) {
							if(key.equals("0")){
								typ = 1;
								break;
							}else{
								typ = 0;
							}
						}
					}
				}		
				sysRole.setType(typ);
				roleDao.saveRole(sysRole);
				
				String[] subId = subRoleId.split(",");
				List<SysSubrole> subroleAll = roleDao.getsysSubRoleByRoleId(roleId);
				
				Map map1 = new HashMap();
				for(int m=0; m<subId.length; m++){
					map1.put(subId[m], "");
				}
				
				
				List listNew = new ArrayList();
				for(int m=0; m<subroleAll.size(); m++){
					SysSubrole sysSubrole = subroleAll.get(m);
					String subRoleId1 = sysSubrole.getSubRoleId();
					if(!map1.containsKey(subRoleId1))
					{
						listNew.add(sysSubrole);
					}
				}
				
				for(int m=0; m<listNew.size(); m++){
					SysSubrole sysSubrole = (SysSubrole)listNew.get(m);
					String subRoleId1 = sysSubrole.getSubRoleId();
					if(sysSubrole.getTypeName()!=null){
						roleDao.delSub(subRoleId1);
					}
				}
				if(subId.length==0){
					List<SysSubrole> sysSubroleList = roleDao.getsysSubRoleByRoleId(roleId);
					for (int i = 0; i < sysSubroleList.size(); i++) {
						SysSubrole subrole = sysSubroleList.get(i);
						if(subrole.getTypeName()!=null)
						{
							roleDao.delSysSubrole(subrole);
						}
					}
				}
				List<SysSubrole> sysSubroleList = roleDao.getsysSubRoleByRoleId(roleId);
				
				if(!"null".equals(typeName)&&!"".equals(typeName)){
					//添加类型 数理化  获取类型  拼接字符串 拆分
					String[] keyArr = typeName.split(",");
					for (int j = 1; j < keyArr.length; j++) {
						String key = keyArr[j];
						if (!"".equals(key) && key != null) {
							if(j<subId.length+1&&!subId[0].equals("")){
								String subRId = subId[j-1];
								SysSubrole sysSubrole = roleDao.getsysSubRoleListBysuId(subRId);
								sysSubrole.setTypeName(key);
								//添加角色级别
								sysSubrole.setRoleLevel(rolelevel);
								//SysRole roles = sysRole;//roleDao.getRoleById(roleId);
								sysSubrole.setSysRole(sysRole);
								roleDao.saveSysSubrole(sysSubrole);
							}else{
								SysSubrole sysSubrole1 = new SysSubrole();
								sysSubrole1.setTypeName(key);
								sysSubrole1.setIsDel(0);
								//添加角色级别
								sysSubrole1.setRoleLevel(rolelevel);
								//SysRole roles = sysRole;//roleDao.getRoleById(roleId);
								sysSubrole1.setSysRole(sysRole);
								roleDao.saveSysSubrole(sysSubrole1);
							}
						}
					}
				}else{
					//添加角色级别
					SysSubrole sysSubrole = sysSubroleList.get(0);
					sysSubrole.setRoleLevel(rolelevel);
					sysSubrole.setSysRole(sysRole);
					String key = null;
					sysSubrole.setTypeName(key);
					roleDao.saveSysSubrole(sysSubrole);
				}
				
				
				
				
				//添加 所有园区
				if(schoolNameData!=null){
					
					String[] keyArr = schoolNameData.split(",");
					for (int i = 0; i < keyArr.length; i++) {
						String key = keyArr[i];
						if (!"".equals(key) && key != null) {
							if(key.equals("0")){
								continue;
							}
							SysRoleSchoolPower sysRoleSchoolPower = new SysRoleSchoolPower();
							//添加园区 外键
							School school = roleDao.getSchoolById(key);
							sysRoleSchoolPower.setSchool(school);
							sysRoleSchoolPower.setSysRole(sysRole);
							//保存 外键表
							roleDao.saveSysRoleSchoolPower(sysRoleSchoolPower);
						}
					}
				}

				
				//权限处理
				//1、删除以前所有权限
				List<SysRolePower> list = roleDao.getBySysRolePower(roleId);
				for (int i = 0; i < list.size(); i++) {
					
					SysRolePower sysRolePow = list.get(i);
					roleDao.delObj(sysRolePow);
					
				}
				
				//2、权限表添加数据
				saveAddSysRolePower(appGengduo_shipu,appGengduo_huodong,appGengduo_gonggao,
						appGengduo,appCheckIn_chaxunlan,appCheckIn_chuli,appCheckIn_jieshou,
						appCheckIn_qingjia,appCheckIn_kejian,appCheckIn,
					 sysRole,employee_daochu, employee_enable,
					 employee_del, employee_xiugai,
					 employee_daoru, employee_add,
					 employee_kejian, employee, role_del,
					 role_xiugai, role_add, role_kejian,
					 role, student_daochu, student_del,
					 student_xiugai, student_addList,
					 student_add, student_kejian, student,
					 classes_lishi, classes_zhuxiao,
					 classes_up, classes_del, classes_xiugai,
					 classes_addList, classes_add,
					 classes_kejian, classes, park_del,
					 park_xiugai, park_add, park_kejian,
					 park, mailbox_daochu, mailbox_del,
					 mailbox_chakan, mailbox_kejian, mailbox,
					 recipe_del, recipe_chehui, recipe_chakan,
					 recipe_new, recipe_kejian, recipe,
					 activity_del, activity_chehui,
					 activity_chakan, activity_new,
					 activity_kejian, activity, notice_daochu,
					 notice_del, notice_chehui, notice_chakan,
					 notice_caogao, notice_new, notice_kejian,
					 notice, gaiLan_guanyu, gaiLan_xinxi,
					 gaiLan_kejian, gaiLan);
				
			}else {
				
				SysRole sysRole = new SysRole();
				sysRole.setIsDel(0);
				sysRole.setCreateDate(new Timestamp(new Date().getTime()));
				//添加角色名称  班主任  园长  普通老师  超级管理员
				sysRole.setRolerName(roleName);
				
//				if(nianji != null){
//					
//					sysRole.setGrade(new Integer(nianji));
//				}
				
				SysUser sysUser = roleDao.findByUserId(userId);
				
				//添加创建者
				sysRole.setSysUser(sysUser);
				
				int typ = 0;
				if(schoolNameData!=null){
					String[] keyArr = schoolNameData.split(",");
					for (int i = 0; i < keyArr.length; i++) {
						String key = keyArr[i];
						if (!"".equals(key) && key != null) {
							if(key.equals("0")){
								typ = 1;
								break;
							}else{
								typ = 0;
							}
						}
					}
				}		
				sysRole.setType(typ);
				roleDao.saveRole(sysRole);
				
				
				if(typeName!=null&&!typeName.equals("")){
					
					//添加类型 数理化  获取类型  拼接字符串 拆分
					String[] keyArr = typeName.split(",");
					for (int i = 0; i < keyArr.length; i++) {
						String key = keyArr[i];
						if (!"".equals(key) && key != null) {
							SysSubrole sysSubrole = new SysSubrole();
							sysSubrole.setTypeName(key);
							//添加角色级别
							sysSubrole.setRoleLevel(rolelevel);
							sysSubrole.setIsDel(0);
							//SysRole role = sysRole;//roleDao.getRoleById(roleId);
							sysSubrole.setSysRole(sysRole);
							roleDao.saveSysSubrole(sysSubrole);
						}
					}
				}else{
					SysSubrole sysSubrole = new SysSubrole();
					//添加角色级别
					sysSubrole.setRoleLevel(rolelevel);
					sysSubrole.setIsDel(0);
					sysSubrole.setSysRole(sysRole);
					roleDao.saveSysSubrole(sysSubrole);
				}
				
				//添加 所有园区
				if(schoolNameData!=null){
					String[] keyArr = schoolNameData.split(",");
					for (int i = 0; i < keyArr.length; i++) {
						String key = keyArr[i];
						if (!"".equals(key) && key != null) {
							if(key.equals("0")){
								continue;
							}
							SysRoleSchoolPower sysRoleSchoolPower = new SysRoleSchoolPower();
							//添加园区 外键
							School school = roleDao.getSchoolById(key);
							sysRoleSchoolPower.setSchool(school);
							
							//SysRole role = sysRole;//roleDao.getRoleById(roleId);
							sysRoleSchoolPower.setSysRole(sysRole);
							//保存 外键表
							roleDao.saveSysRoleSchoolPower(sysRoleSchoolPower);
						}
					}
				}
				
				
				//权限表添加数据
				saveAddSysRolePower(appGengduo_shipu,appGengduo_huodong,appGengduo_gonggao,
						appGengduo,appCheckIn_chaxunlan,appCheckIn_chuli,appCheckIn_jieshou,
						appCheckIn_qingjia,appCheckIn_kejian,appCheckIn,
					 sysRole,employee_daochu, employee_enable,
					 employee_del, employee_xiugai,
					 employee_daoru, employee_add,
					 employee_kejian, employee, role_del,
					 role_xiugai, role_add, role_kejian,
					 role, student_daochu, student_del,
					 student_xiugai, student_addList,
					 student_add, student_kejian, student,
					 classes_lishi, classes_zhuxiao,
					 classes_up, classes_del, classes_xiugai,
					 classes_addList, classes_add,
					 classes_kejian, classes, park_del,
					 park_xiugai, park_add, park_kejian,
					 park, mailbox_daochu, mailbox_del,
					 mailbox_chakan, mailbox_kejian, mailbox,
					 recipe_del, recipe_chehui, recipe_chakan,
					 recipe_new, recipe_kejian, recipe,
					 activity_del, activity_chehui,
					 activity_chakan, activity_new,
					 activity_kejian, activity, notice_daochu,
					 notice_del, notice_chehui, notice_chakan,
					 notice_caogao, notice_new, notice_kejian,
					 notice, gaiLan_guanyu, gaiLan_xinxi,
					 gaiLan_kejian, gaiLan);
				
				
			}
			
			
		}
		//添加权限处理
		public void saveAddSysRolePower(String appGengduo_shipu,String appGengduo_huodong,
				String appGengduo_gonggao,String appGengduo,String appCheckIn_chaxunlan,
				String appCheckIn_chuli,String appCheckIn_jieshou,String appCheckIn_qingjia,String appCheckIn_kejian,String appCheckIn,
				SysRole sysRole, String employee_daochu, String employee_enable,
				String employee_del, String employee_xiugai,
				String employee_daoru, String employee_add,
				String employee_kejian, String employee, String role_del,
				String role_xiugai, String role_add, String role_kejian,
				String role, String student_daochu, String student_del,
				String student_xiugai, String student_addList,
				String student_add, String student_kejian, String student,
				String classes_lishi, String classes_zhuxiao,
				String classes_up, String classes_del, String classes_xiugai,
				String classes_addList, String classes_add,
				String classes_kejian, String classes, String park_del,
				String park_xiugai, String park_add, String park_kejian,
				String park, String mailbox_daochu, String mailbox_del,
				String mailbox_chakan, String mailbox_kejian, String mailbox,
				String recipe_del, String recipe_chehui, String recipe_chakan,
				String recipe_new, String recipe_kejian, String recipe,
				String activity_del, String activity_chehui,
				String activity_chakan, String activity_new,
				String activity_kejian, String activity, String notice_daochu,
				String notice_del, String notice_chehui, String notice_chakan,
				String notice_caogao, String notice_new, String notice_kejian,
				String notice, String gaiLan_guanyu, String gaiLan_xinxi,
				String gaiLan_kejian,String gaiLan){
			
			if(appGengduo_shipu != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("appGengduo_shipu");
				sysRolePower.setParentId("appGengduo");
				roleDao.saveObj(sysRolePower);
			}
			if(appGengduo_huodong != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("appGengduo_huodong");
				sysRolePower.setParentId("appGengduo");
				roleDao.saveObj(sysRolePower);
			}
			if(appGengduo_gonggao != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("appGengduo_gonggao");
				sysRolePower.setParentId("appGengduo");
				roleDao.saveObj(sysRolePower);
			}
			if(appGengduo != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("appGengduo");
				roleDao.saveObj(sysRolePower);
			}
			
			
			if(appCheckIn_chaxunlan != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("appCheckIn_chaxunlan");
				sysRolePower.setParentId("appCheckIn");
				roleDao.saveObj(sysRolePower);
			}
			if(appCheckIn_chuli != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("appCheckIn_chuli");
				sysRolePower.setParentId("appCheckIn");
				roleDao.saveObj(sysRolePower);
			}
			if(appCheckIn_jieshou != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("appCheckIn_jieshou");
				sysRolePower.setParentId("appCheckIn");
				roleDao.saveObj(sysRolePower);
			}
			if(appCheckIn_qingjia != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("appCheckIn_qingjia");
				sysRolePower.setParentId("appCheckIn");
				roleDao.saveObj(sysRolePower);
			}
			if(appCheckIn_kejian != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("appCheckIn_kejian");
				sysRolePower.setParentId("appCheckIn");
				roleDao.saveObj(sysRolePower);
			}
			if(appCheckIn != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("appCheckIn");
				roleDao.saveObj(sysRolePower);
			}
			
			if(employee_daochu != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("employee_daochu");
				sysRolePower.setParentId("employee");
				roleDao.saveObj(sysRolePower);
			}
			if(employee_enable != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("employee");
				sysRolePower.setPowerCode("employee_enable");
				roleDao.saveObj(sysRolePower);
			}
			if(employee_del != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("employee_del");
				sysRolePower.setParentId("employee");
				roleDao.saveObj(sysRolePower);
			}
			if(employee_xiugai != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("employee_xiugai");
				sysRolePower.setParentId("employee");
				roleDao.saveObj(sysRolePower);
			}
			if(employee_daoru != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("employee_daoru");
				sysRolePower.setParentId("employee");
				roleDao.saveObj(sysRolePower);
			}
			if(employee_add != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("employee_add");
				sysRolePower.setParentId("employee");
				roleDao.saveObj(sysRolePower);
			}
			if(employee_kejian != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("employee_kejian");
				sysRolePower.setParentId("employee");
				roleDao.saveObj(sysRolePower);
			}
			if(employee != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("employee");
				roleDao.saveObj(sysRolePower);
			}
			if(role_del != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("role_del");
				sysRolePower.setParentId("role");
				roleDao.saveObj(sysRolePower);
			}
			if(role_xiugai != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("role_xiugai");
				sysRolePower.setParentId("role");
				roleDao.saveObj(sysRolePower);
			}
			if(role_add != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("role_add");
				sysRolePower.setParentId("role");
				roleDao.saveObj(sysRolePower);
			}
			if(role_kejian != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("role_kejian");
				sysRolePower.setParentId("role");
				roleDao.saveObj(sysRolePower);
			}
			if(role != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("role");
				roleDao.saveObj(sysRolePower);
			}
			if(student_daochu != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("student_daochu");
				sysRolePower.setParentId("student");
				roleDao.saveObj(sysRolePower);
			}
			if(student_del != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("student_del");
				sysRolePower.setParentId("student");
				roleDao.saveObj(sysRolePower);
			}
			if(student_xiugai != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("student_xiugai");
				sysRolePower.setParentId("student");
				roleDao.saveObj(sysRolePower);
			}
			if(student_addList != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("student_addList");
				sysRolePower.setParentId("student");
				roleDao.saveObj(sysRolePower);
			}
			if(student_add != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("student_add");
				sysRolePower.setParentId("student");
				roleDao.saveObj(sysRolePower);
			}
			if(student_kejian != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("student_kejian");
				sysRolePower.setParentId("student");
				roleDao.saveObj(sysRolePower);
			}
			if(student != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("student");
				roleDao.saveObj(sysRolePower);
			}
			if(classes_lishi != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("classes_lishi");
				sysRolePower.setParentId("classes");
				roleDao.saveObj(sysRolePower);
			}
			if(classes_zhuxiao != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("classes_zhuxiao");
				sysRolePower.setParentId("classes");
				roleDao.saveObj(sysRolePower);
			}
			if(classes_up != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("classes_up");
				sysRolePower.setParentId("classes_up");
				roleDao.saveObj(sysRolePower);
			}
			if(classes_del != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("classes");
				sysRolePower.setPowerCode("classes_del");
				roleDao.saveObj(sysRolePower);
			}
			if(classes_xiugai != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("classes_xiugai");
				sysRolePower.setParentId("classes");
				roleDao.saveObj(sysRolePower);
			}
			if(classes_addList != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("classes_addList");
				sysRolePower.setParentId("classes");
				roleDao.saveObj(sysRolePower);
			}
			if(classes_add != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("classes_add");
				sysRolePower.setParentId("classes");
				roleDao.saveObj(sysRolePower);
			}
			if(classes_kejian != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("classes_kejian");
				sysRolePower.setParentId("classes");
				roleDao.saveObj(sysRolePower);
			}
			if(classes != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("classes");
				roleDao.saveObj(sysRolePower);
			}
			if(park_del != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("park");
				sysRolePower.setPowerCode("park_del");
				roleDao.saveObj(sysRolePower);
			}
			if(park_xiugai != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("park_xiugai");
				sysRolePower.setParentId("park");
				roleDao.saveObj(sysRolePower);
			}
			if(park_add != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("park_add");
				sysRolePower.setParentId("park");
				roleDao.saveObj(sysRolePower);
			}
			if(park_kejian != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("park");
				sysRolePower.setPowerCode("park_kejian");
				roleDao.saveObj(sysRolePower);
			}
			if(park != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("park");
				roleDao.saveObj(sysRolePower);
			}
			if(mailbox_daochu != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("mailbox_daochu");
				sysRolePower.setParentId("mailbox");
				roleDao.saveObj(sysRolePower);
			}
			if(mailbox_del != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("mailbox");
				sysRolePower.setPowerCode("mailbox_del");
				roleDao.saveObj(sysRolePower);
			}
			if(mailbox_chakan != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("mailbox");
				sysRolePower.setPowerCode("mailbox_chakan");
				roleDao.saveObj(sysRolePower);
			}
			if(mailbox_kejian != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("mailbox");
				sysRolePower.setPowerCode("mailbox_kejian");
				roleDao.saveObj(sysRolePower);
			}
			if(mailbox != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("mailbox");
				roleDao.saveObj(sysRolePower);
			}
			if(recipe_del != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("recipe");
				sysRolePower.setPowerCode("recipe_del");
				roleDao.saveObj(sysRolePower);
			}
			if(recipe_chehui != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("recipe");
				sysRolePower.setPowerCode("recipe_chehui");
				roleDao.saveObj(sysRolePower);
			}
			if(recipe_chakan != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("recipe_chakan");
				sysRolePower.setParentId("recipe");
				roleDao.saveObj(sysRolePower);
			}
			if(recipe_new != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("recipe");
				sysRolePower.setPowerCode("recipe_new");
				roleDao.saveObj(sysRolePower);
			}
			if(recipe_kejian != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("recipe");
				sysRolePower.setPowerCode("recipe_kejian");
				roleDao.saveObj(sysRolePower);
			}
			if(recipe != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("recipe");
				roleDao.saveObj(sysRolePower);
			}
			if(activity_del != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("activity");
				sysRolePower.setPowerCode("activity_del");
				roleDao.saveObj(sysRolePower);
			}
			if(activity_chehui != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("activity");
				sysRolePower.setPowerCode("activity_chehui");
				roleDao.saveObj(sysRolePower);
			}
			if(activity_chakan != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("activity");
				sysRolePower.setPowerCode("activity_chakan");
				roleDao.saveObj(sysRolePower);
			}
			if(activity_new != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("activity_new");
				sysRolePower.setParentId("activity");
				roleDao.saveObj(sysRolePower);
			}
			if(activity_kejian != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("activity_kejian");
				
				sysRolePower.setParentId("activity");
				roleDao.saveObj(sysRolePower);
			}
			if(activity != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("activity");
				roleDao.saveObj(sysRolePower);
			}
			if(notice_daochu != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("notice_daochu");
				sysRolePower.setParentId("notice");
				roleDao.saveObj(sysRolePower);
			}
			if(notice_del != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("notice");
				sysRolePower.setPowerCode("notice_del");
				roleDao.saveObj(sysRolePower);
			}
			if(notice_chehui != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("notice");
				sysRolePower.setPowerCode("notice_chehui");
				roleDao.saveObj(sysRolePower);
			}
			if(notice_chakan != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("notice");
				sysRolePower.setPowerCode("notice_chakan");
				roleDao.saveObj(sysRolePower);
			}
			if(notice_caogao != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("notice_caogao");
				sysRolePower.setParentId("notice");
				roleDao.saveObj(sysRolePower);
			}
			if(notice_new != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("notice_new");
				sysRolePower.setParentId("notice");
				roleDao.saveObj(sysRolePower);
			}
			if(notice_kejian != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("notice");
				sysRolePower.setPowerCode("notice_kejian");
				roleDao.saveObj(sysRolePower);
			}
			if(notice != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				
				sysRolePower.setPowerCode("notice");
				roleDao.saveObj(sysRolePower);
			}
			if(gaiLan_guanyu != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("gaiLan");
				sysRolePower.setPowerCode("gaiLan_guanyu");
				roleDao.saveObj(sysRolePower);
			}
			if(gaiLan_xinxi != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setParentId("gaiLan");
				sysRolePower.setPowerCode("gaiLan_xinxi");
				roleDao.saveObj(sysRolePower);
			}
			if(gaiLan_kejian != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("gaiLan_kejian");
				sysRolePower.setParentId("gaiLan");
				roleDao.saveObj(sysRolePower);
			}
			if(gaiLan != null){
				SysRolePower sysRolePower = new SysRolePower();
				sysRolePower.setSysRole(sysRole);
				sysRolePower.setPowerCode("gaiLan");
				roleDao.saveObj(sysRolePower);
			}
		}
		
		
		/**
		 * 删除
		 */
		@Override
		public void delRole(String roleId) {
			//真删  类型表  一对多 改成伪删除
			List<SysSubrole> sysSubrole = roleDao.getsysSubRoleListById(roleId);
			for (int i = 0; i < sysSubrole.size(); i++) {
			 	sysSubrole.get(i).setIsDel(1);
				roleDao.delSysSubrole(sysSubrole);
			}
			
			roleDao.delRole(roleId);
		}
		
		
		/**
		 * 获取sysRole 通过id  删除时可以用
		 * 2.点击修改时 回调函数(角色只有一个 所以方法共用)
		 */
		@Override
		public SysRole getRoleById(String roleId) {
			return roleDao.getRoleById(roleId);
		}
		
		/**
		 * 通过roleId 全查关联所有的类型
		 */
		@Override
		public List<SysSubrole> getsysSubRoleListById(String roleId) {
			
			return roleDao.getsysSubRoleListById(roleId);
		}
		/**
		 * 2016年10月9日下午12:01:31
		 *  龚杰
		 *  注释：
		 */
		@Override
		public List<SysRolePower> getsysRolePowerByRoleId(String roleId) {
			
			return roleDao.getsysRolePowerByRoleId(roleId);
		}
		/**
		 * 2016年10月17日下午5:47:07
		 *  阳朔
		 *  注释: 查询 角色名称是否存在
		 */
		@Override
		public SysRole chaRolebyRoleName(String roleName) {
			return roleDao.chaRolebyRoleName(roleName);
		}
		/**
		 * 2016年10月22日上午1:56:13
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List<SysUser> chaSysUserBySubId(String subRoleId) {
			
			return roleDao.chaSysUserBySubId(subRoleId);
		}
		/**
		 * 2016年10月23日下午5:49:18
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List<SysRole> getRoleLimit(String schoolsId, Integer integer,
				Integer integer2) {
			// TODO Auto-generated method stub
			List<SysRoleSchoolPower> list = roleDao.getRoleLimit(schoolsId,integer, integer2);
			List<SysRole> listS = new ArrayList<SysRole>();
			for (int i = 0; i < list.size(); i++) {
				SysRoleSchoolPower sysRoleSchoolPower = list.get(i);
				SysRole s = sysRoleSchoolPower.getSysRole();
				if(s.getIsDel().equals(0)){
					
					listS.add(s);
				}
			}
			return listS;
		}
		/**
		 * 2016年10月26日下午4:52:08
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List getRoleCountBySchoolId(String schoolsId) {
			List<SysRoleSchoolPower> list = roleDao.getRoleCountBySchoolId(schoolsId);
			List<SysRole> listS = new ArrayList<SysRole>();
			for (int i = 0; i < list.size(); i++) {
				SysRoleSchoolPower sysRoleSchoolPower = list.get(i);
				SysRole s = sysRoleSchoolPower.getSysRole();
				if(s.getIsDel().equals(0)){
					
					listS.add(s);
				}
			}
			return listS;
		}
	
		
		
		
	
		
		
		

}
