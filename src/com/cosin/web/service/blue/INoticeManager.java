package com.cosin.web.service.blue;

import java.util.List;

import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Classteacher;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Notice;
import com.cosin.web.entity.NoticeSub;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SignNotice;
import com.cosin.web.entity.SignPrice;
import com.cosin.web.entity.Student;

public interface INoticeManager {

	/**
	 * 分页
	 */
    public List getNoticeLimit(String noticeId, int start, int limit);
    /**
     * 数量
     */
    public int getNoticeCount(String SsName);
    /**
     * 获取getNoticeId
     */
  	public Notice getNoticeId(String noticeId);
  	 /**
  	  * 删除
  	  */
    public void noticeDelId(String noticeId);

    
    List<Notice> getDraftLimit(String ssName, int start, int limit);
	/**
	 */
	public List<Object> getNoticeTree(String id);
	/**
	 * 
	 */
	public List<Classes> getListClasses(Object id);
	public void rollNotice(String userId, String noticeId);
	
	
	
	/**
	 * 保存 公告
	 */
	public void saveNotice(String payBill,String noticeId,String userId, String ntitle, String filePath,
			String txtContent, String ntype, String mode,String data,String draf);
	
	public List<Object> getListClas(Object id);
	
	public List<Classes> getClass(Object id,Object schoolId);
	
	/**
	 * 根据NoticeId 查询 NoticeSub 对象
	 */
	public List<NoticeSub> getNoticeSubByNoticeId(String noticeId);
	
	/**
	 * 根据noticeId  查询 noticesub  中间表  关联班级
	 * @param noticeId
	 * @return
	 */
	public List<NoticeSub> getNoticeSubBynoticeId(String noticeId);
	
	/**
	 * 已报名
	 * @param schoolName
	 * @param grade
	 * @param className
	 * @param integer
	 * @param integer2
	 * @return
	 */
	public List<NoticeSub> getNoticeLimit(String noticeId,String schoolName, String grade,
			String className, Integer integer, Integer integer2);
	
	/**
	 * 通过classId 查询 学生
	 * 
	 * @param classesId
	 * @return
	 */
	public Student getStudentByClassesId(String classesId);
	
	/**
	 * 数量
	 * @param schoolName
	 * @param grade
	 * @param className
	 * @return
	 */
	public int getNoticeCount(String noticeId,String schoolName, String grade, String className);
	
	/**
	 * 二级联动 
	 * @param schoolId
	 * @return
	 */
	public List getListGradeBySchoolId(String schoolId);
	
	/**
	 * 二级联动
	 * @param gradeName
	 * @return
	 */
	public List getListClassNameByGradeName(String gradeName);
	
	
	public List<Employee> getEmployeeBySchoolId(String schoolId);
	
	/**
	 * 通过classId  查询 所有学生
	 * @param classId
	 * @return
	 */
	public List<Student> getStudentListByClassesId(String classId);
	
	/**
	 * 通过studentId  查询 家长
	 * @param studentId
	 * @return
	 */
	public List<Parentsstudent> getParentListByStudentId(String studentId);
	
	/**
	 * 未报名
	 * @param integer
	 * @param integer2
	 * @return
	 */
	public List<NoticeSub> getNoticeSubLimit(String noticeId,Integer integer, Integer integer2);
	public int getNoticeSubCount(String noticeId);
	/**
	 * 2016年10月10日上午9:27:32
	 *  龚杰
	 *  注释：
	 */
	public int getNoticeDraftCount(String ssName);
	/**
	 * 2016年10月17日上午11:44:14
	 *  阳朔
	 *  注释:
	 */
	public List<NoticeSub> getNotice2Limit(String noticeId, Integer integer,
			Integer integer2);
	/**
	 * 2016年10月17日下午1:35:01
	 *  阳朔
	 *  注释:
	 */
	public List<NoticeSub> getNoticeByNoticeId(String noticeId);
	/**
	 * 2016年10月17日下午2:50:57
	 *  阳朔
	 *  注释:查询已报名 新改
	 */
	public List<SignNotice> getSignNoticeLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2);
	/**
	 * 2016年10月17日下午2:57:52
	 *  阳朔
	 *  注释:查询已报名数量 新改
	 */
	public int getSignNoticeCount(String noticeId, String schoolName,
			String grade, String className);
	/**
	 * 2016年10月17日下午3:07:26
	 *  阳朔
	 *  注释:
	 */
	public Parentsstudent getParentsstudentByStudentId(String studentId);
	/**
	 * 2016年10月17日下午3:13:48
	 *  阳朔
	 *  注释:查询 未报名
	 */
	public List<SignNotice> getSignNoticeByNoticeId(String noticeId);
	/**
	 * 2016年10月23日下午4:28:28
	 *  阳朔
	 *  注释:
	 */
	public List<Classes> getClassess(String schoolId);
	/**
	 * 2016年10月23日下午4:30:46
	 *  阳朔
	 *  注释:
	 */
	public List<NoticeSub> getNoticeSubLimit(String classIds, String ssName,
			Integer integer, Integer integer2);
	/**
	 * 2016年10月27日下午7:50:23
	 *  阳朔
	 *  注释:已支付
	 */
	public List<SignPrice> getSignPriceLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2);
	/**
	 * 2016年10月27日下午7:59:44
	 *  阳朔
	 *  注释:已支付 数量
	 */
	public int getSignPriceCount(String noticeId, String schoolName,
			String grade, String className);
	/**
	 * 2016年10月27日下午8:07:10
	 *  阳朔
	 *  注释:
	 */
	public List<NoticeSub> getNoticePriceLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2);
	/**
	 * 2016年10月27日下午8:09:03
	 *  阳朔
	 *  注释:支付
	 */
	public int getNoticeSubPriceCount(String noticeId,String schoolName,String grade,String className);
	/**
	 * 2016年10月28日下午4:24:07
	 *  阳朔
	 *  注释:
	 */
	public List<SignPrice> getNoSignPriceLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2);
	/**
	 * 2016年10月28日下午4:29:44
	 *  阳朔
	 *  注释:
	 */
	public int getNoPriceCount(String noticeId, String schoolName,
			String grade, String className);
	/**
	 * 2016年12月8日下午5:54:27
	 *  阳朔
	 *  注释:
	 */
	public List<NoticeSub> getNoticeSubDraftLimit(String classIds,
			String ssName, Integer integer, Integer integer2);
	

}
