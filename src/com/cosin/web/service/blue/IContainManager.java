package com.cosin.web.service.blue;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.Contain;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysUser;


public interface IContainManager {

	/**
	 * 获取正文
	 */
	Contain getlistContain();

	/**
	 * 学校数量
	 * @return
	 */
	int selectSchoolNum();

	/**
	 * 班级数量
	 * @return
	 */
	int selectClassesNum();

	/**
	 * 老师数量
	 * @return
	 */
	int selectTeacherNum();

	/**
	 * 学生数量
	 * @return
	 */
	int selectStudentNum();

	/**
	 * 家长数量
	 * @return
	 */
	int selectParentNum();

	/**
	 * 保存 内容
	 * @param content
	 * @throws InterruptedException 
	 */
	void saveContain(String content) throws InterruptedException;

	/**
	 * 查询内容
	 * @return
	 */
	Contain selectContent();

	SysUser getSysUserById(String userId);


	Map saveSysUser(String userId, String beginPassword, String endPassword,
			String doublePassword);

	/**
	 * 2016年10月23日下午3:10:16
	 *  阳朔
	 *  注释:
	 */
	Map forgetPassWord(String phone, String newPassWord);

	/**
	 * 2016年10月23日下午3:53:37
	 *  阳朔
	 *  注释:
	 */
	List<School> getSchools(String userId);

	/**
	 * 2016年10月23日下午3:58:51
	 *  阳朔
	 *  注释:
	 */
	int selectClassesNum(String schoolsId);

	/**
	 * 2016年10月23日下午4:02:12
	 *  阳朔
	 *  注释:
	 */
	int getTeacherBySchool(String schoolsId);

	/**
	 * 2016年10月23日下午4:06:01
	 *  阳朔
	 *  注释:
	 */
	int getStudentsBySchool(String schoolsId);

	/**
	 * 2016年10月23日下午4:08:11
	 *  阳朔
	 *  注释:
	 */
	int getParentsBySchools(String schoolsId);


	

}
