package com.cosin.web.service.blue;

import java.io.InputStream;
import java.util.List;

import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Classteacher;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.HistoryClass;
import com.cosin.web.entity.Recipe;
import com.cosin.web.entity.School;
import com.cosin.web.entity.Student;

public interface IClassesManager {

	/**
	 *  分页
	 */
	public List getClassesLimit(String classesId, int start, int limit);

	/**
	 *  数量
	 */
	public int getClassesCount(String SsName);

	/**
	 *  获取Classes
	 */
	public Classes getClassesId(String classesId);

	/**
	 *  删除
	 */
	public void classesDelId(String classesId);

	/**
	 *  历史班级列表查询
	 */
	public List<HistoryClass> getHistoryLimit(String ssName, Integer integer,
			Integer integer2);

	/**
	 *  历史班级数量查询
	 */
	public int getHistoryCount(String ssName);

	/**
	 * 获取 园区 操作者级别
	 */
	public List getlistSchool(String createUserId);
	
	/**
	 * 下拉框combobox 班主任
	 * @param ssName 
	 */
	public List getTeacherAndTel(String ssName,String schoolId);

	/**
	 * 查询subRole 的其他老师 typeName字段
	 * 下拉bombobox 框 
	 */
	public List getSubRole();

	/**
	 * OtherTeacher 姓名 +电话
	 * 通过sysUser表  来获取 sysRole 表里 关联其他老师 typeName字段的 sysUser——userName 
	 * 下拉bombobox 框 
	 */
	public List getOtherTeacherAndTel(String schoolId,String typeNameId);

	/**
	 * 保存班级
	 */
	public void saveClasses(String userId,String classesId, String schoolId, String intoYear,
			String grade, String className, String teacherAndTelId,
			String listTypeNameId, String listOtherTeacherAndTelId, String mode);

	/**
	 * 查询中间表
	 */
	public List<Classteacher> getClassteacherByclassesId(String classesId);

	/**
	 * 根据classesId 查询 老师类型数量
	 * @param classesId
	 * @return
	 */
	public int getTypeNumById(String classesId);

	/**
	 * 历史班级 查看功能实现
	 * @param classesId
	 * @return
	 */
	public HistoryClass getHistoryClassById(String historyClassId);

	/**
	 * 删除历史
	 * @param key
	 */
	public void historyDelId(String key);

	/**
	 * 批量导入
	 * @param userId
	 * @param path
	 * @param inputStream
	 * @return
	 */
	public String readReport(String userId, String path, InputStream inputStream);

	/**
	 * 2016年10月23日下午5:25:12
	 *  阳朔
	 *  注释:
	 */
	public List<Classes> getClassesLimit(String schoolsId, String ssName,
			Integer integer, Integer integer2);

	/**
	 * 2016年10月26日下午4:45:11
	 *  阳朔
	 *  注释:
	 */
	public List getClassesCountBySchoolId(String schoolsId, String ssName);

	String chaReport(String userId, String dirPre, InputStream inputStream);

	/**
	 * 2016年11月15日下午1:09:09
	 *  阳朔
	 *  注释:
	 */
	public List<Student> getStudentByClaId(String deleteKeys);

	/**
	 * 2016年11月16日下午2:36:23
	 *  阳朔
	 *  注释:
	 */
	public List getHistoryCountByGrad(String schoolName, String grad,
			String ssName);

	/**
	 * 2016年11月16日下午2:54:11
	 *  阳朔
	 *  注释:
	 */
	public Employee getEmpByUsrId(String teacherAndTelId);

	/**
	 * 2016年11月16日下午2:56:55
	 *  阳朔
	 *  注释:
	 */
	public Classes getClassByEmpId(String employeeId);

	/**
	 * 2016年11月28日上午9:37:12
	 *  阳朔
	 *  注释:
	 */
	public void delClassteacher(Classteacher classteacher);


	
	
	
	
}
