/**
 * 
 */
package com.cosin.web.service.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.system.ISystemDao;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysPowerItem;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

/**
 * @author 王思明
 * 2016年7月19日
 * 下午2:12:35
 */
@Service
@Transactional 
public class systemManager implements ISystemManager {

	@Autowired
	private ISystemDao systemDao;

	/* 
	 * 后台用户登录验证
	 * 王思明
	 * 2016年7月19日
	 * 下午3:15:46
	 */
	@Override
	public Map<String, Object> getSysUserYZ(String loginName, String pwd) {
		Map<String, Object> map = new HashMap<String,Object>();
		
		SysUser sysUser = systemDao.getByEmailNamePwd(loginName,pwd);
		if(sysUser == null)
		{
			map.put("code", 101);
			map.put("msg", "用户名或密码错误");
			
		}else{
//			sysUser.setLastLoginTime(new Timestamp(new Date().getTime()));
			Employee employee = systemDao.getEmployeeById(sysUser.getUserId());
			if(employee==null){
				map.put("code", 101);
				map.put("msg", "该用户不存在");
			}
			else{
				if(employee.getSchool()!=null&&employee.getSchool().getIsDel()==1){
					map.put("code", 101);
					map.put("msg", "该学校已被删除");
				}
				else{
					systemDao.saveObject(sysUser);
					
					//	UserSession userSession = new UserSession();
					//	userSession.token = sysUser.getToken();
						//返回值处理
						map.put("code", 100);
						map.put("msg", "登录成功");
						//map.put("token", sysUser.getToken());
						map.put("userName", sysUser.getUserName());
						map.put("userId", sysUser.getUserId());
						
						SysUserRole sysRole = systemDao.getByIdSysRole(sysUser.getUserId());
						if(sysRole != null){
							map.put("roleId", sysRole.getSysRole().getRoleId());
							SysRolePower sysRolePower = systemDao.getBySysRolePower(sysRole.getSysRole().getRoleId());
							if(sysRolePower == null){
								
								map.put("msg", "您没有权限登录此系统");
								
								map.put("code", 101);
								
							}
						}else{
							
							map.put("msg", "您没有权限登录此系统");
							
							map.put("code", 101);
							
						}
						//map.put("token", sysUser.getToken());
						map.put("icon", sysUser.getIcon());
						map.put("enable", sysUser.getEnable());
				}
			}
			
		}
		return map;
	}

	/* 
	 * 王思明
	 * 2016年9月26日
	 * 下午5:53:03
	 */
	@Override
	public List<SysRolePower> getSysRolePower(String roleId) {
		
		return systemDao.getSysRolePower(roleId);
		
	}

	/**
	 * 2016年10月8日下午5:37:41
	 *  龚杰
	 *  注释：查询园区 操作者级别
	 */
	@Override
	public List getlistSchool(String createUserId) {
		if("4028810a57bd26110157bd55fbd7005c".equals(createUserId)){
			List lists = new ArrayList();
			List<School> list = systemDao.getAllSchool();
			for (int k = 0; k < list.size(); k++) {
				School school = list.get(k);
				Map map = new HashMap();
				map.put("schoolName", school.getSchoolName());
				map.put("schoolId", school.getSchoolId());
				lists.add(map);
			}
			return lists;
		}else{
			
			List listRes = new ArrayList();
			
			
			//园长 对应一个园区
			List<Employee> employeeList = systemDao.getEmployeeByUserId(createUserId);
			School school = employeeList.get(0).getSchool();
			if(school == null){
				
				//一个人 可能对应多个园区 管理员
				List<SysUserRole> sysUserRoleList = systemDao.getRoleByUserId(createUserId);
				for (int i = 0; i < sysUserRoleList.size(); i++) {
					SysUserRole sysUserRole = sysUserRoleList.get(i);
					String roleId = sysUserRole.getSysRole().getRoleId();
					List<SysRoleSchoolPower> roleSchoolPowerList = systemDao.getSysRoleSchoolPowerListByRoleId(roleId);
					for (int j = 0; j < roleSchoolPowerList.size(); j++) {
						SysRoleSchoolPower sysRoleSchoolPower = roleSchoolPowerList.get(j);
						Map map = new HashMap();
						if(sysRoleSchoolPower.getSchool().getIsDel().equals(0)){
							
							map.put("schoolName", sysRoleSchoolPower.getSchool().getSchoolName());
							map.put("schoolId", sysRoleSchoolPower.getSchool().getSchoolId());
							listRes.add(map);
						}
					}
					return listRes;
				}
				
			}else if(employeeList.size() > 0&&employeeList.size() < 2){
				Employee employee = employeeList.get(0);
				Map map = new HashMap();
				map.put("schoolName", employee.getSchool().getSchoolName());
				map.put("schoolId", employee.getSchool().getSchoolId());
				listRes.add(map);
				return listRes;
			}
		}
		return null;
	}
	
	
	
}
