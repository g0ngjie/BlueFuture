package com.cosin.web.service.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.system.ISysDao;
import com.cosin.web.entity.SysLog;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

@Service  
@Transactional
public class SysManager implements ISysManager{

	@Autowired
	private ISysDao sysDao;
	
	//分页
	@Override
	public List getLogLimit(String fun,String operate,String SsName, String logId, int start, int limit,
			String startDate, String endDate) {
		// TODO Auto-generated method stub
		return sysDao.getLogLimit(SsName, logId, start, limit, startDate, endDate, fun, operate);
	}
	//数量
	@Override
	public int getLogCount(String SsName, String startDate, String endDate,String fun,String operate) {
		// TODO Auto-generated method stub
		return sysDao.getLogCount(SsName, startDate, endDate, fun, operate);
	}
	//id
	@Override
	public SysLog getLogId(String logId) {
		// TODO Auto-generated method stub
		return sysDao.getLogId(logId);
	}
	
	//导出
	@Override
	public List<SysLog> getLogLimitDaoChu(String SsName, String startDate,
			String endDate,String fun,String operate) {
		// TODO Auto-generated method stub
		return sysDao.getLogLimitDaoChu(SsName, startDate, endDate,fun,operate);
	}
	//搜索 导出
//	@Override
//	public List<VcVideo> getByIdVcVideo(String mediaId) {
//		// TODO Auto-generated method stub
//		return sysDao.getByIdVcVideo(mediaId);
//	}
	
	
	
	//操作日记
	@Override
	public SysUser getSysUserId(String id) {
		// TODO Auto-generated method stub
		return sysDao.getSysUserId(id);
	}
	@Override
	public void saveAdd(Object object, String token, String vcName,String ip) {
		sysDao.saveAdd(object, token, vcName,ip);
	}
	@Override
	public void look(Object object, String token, String vcName,String ip) {
		sysDao.look(object, token, vcName,ip);
	}
	@Override
	public void del(Object object, String token, String vcName,String ip) {
		sysDao.del(object, token, vcName,ip);
	}
	@Override
	public void using(Object object, String token, String vcName,String ip) {
		sysDao.using(object, token, vcName,ip);
	}
	@Override
	public void offing(Object object, String token, String vcName,String ip) {
		sysDao.offing(object, token, vcName,ip);
	}
	@Override
	public void updata( String token, String vcName,String ip) {
		sysDao.updata(token, vcName,ip);
	}
	
	//热词管理 上移
	@Override
	public void up(Object object, String token, String vcName,String ip) {
		sysDao.up(object, token, vcName,ip);
	}
	//热词管理 下移
	@Override
	public void down(Object object, String token, String vcName,String ip) {
		sysDao.down(object, token, vcName,ip);
	}
	//导出
	@Override
	public void daochu( String token, String vcName,String ip) {
		
		sysDao.daochu( token, vcName,ip);
	}
	//新建推送
	@Override
	public void push(Object object, String token, String vcName, String ip) {
		sysDao.push(object, token, vcName, ip);
	}
	//驳回
	@Override
	public void reject(Object object, String token, String vcName, String ip,String fun) {
		sysDao.reject(object, token, vcName, ip,fun);
	}
	//查询userRole
	@Override
	public SysUserRole getUr(String userId) {
		// TODO Auto-generated method stub
		return sysDao.getUr(userId);
	}
	

	
}
