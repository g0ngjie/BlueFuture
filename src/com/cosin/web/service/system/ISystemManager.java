/**
 * 
 */
package com.cosin.web.service.system;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.SysRolePower;

/**
 * @author 王思明
 * 2016年7月19日
 * 下午2:10:54
 */
public interface ISystemManager {

	/**
	 * 后台用户登录验证
	 * 王思明 
	 * 2016年7月19日
	 * 下午3:15:29
	 */
	Map<String, Object> getSysUserYZ(String loginName, String pwd);

	/**
	 * 王思明 
	 * 2016年9月26日
	 * 下午5:52:45
	 */
	List<SysRolePower> getSysRolePower(String roleId);

	/**
	 * 2016年10月8日下午5:37:24
	 *  龚杰
	 *  注释：查询园区 操作者级别
	 */
	List getlistSchool(String createUserId);
	
	

}
