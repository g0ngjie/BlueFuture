package com.cosin.web.service;

import java.util.List;

import com.cosin.web.dao.system.IMenuDao;
import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

public interface IMenuManager {
	public void setMenuDao(IMenuDao menuDao);
	
	
	public List getMainMenuList(int start, int limit);
	
	public SysMenu findMenuById(String id);
	
	public List findMenuByParentMenuId(String id,String roleId);
	
	//查询  接口
	public List getMainMenuList(String id);
	//保存  编辑  接口
	public void addMenu(String mode,String menukey,String name, String code, String url, String icon,
			Integer enable ,String type);
	//删除  接口
	public void delmenu(String menuId);
	//隐藏  接口
	public void hideById(String menuId);


	/**
	 * 根据token获取用户信息（用户名--用来在右上角显示）
	 * 王思明 
	 * 2016年7月19日
	 * 下午5:36:17
	 */
	public SysUser getByTokenUser(Object token);


	/**
	 * 2016年9月30日下午3:34:45
	 *  龚杰
	 *  注释：登陆者角色
	 */
	public SysUserRole getSysUserRoleByUserId(String userId);
}
