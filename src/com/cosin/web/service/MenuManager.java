package com.cosin.web.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.system.IMenuDao;
import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysPowerItem;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;
@Service  
@Transactional 
public class MenuManager implements IMenuManager{
	@Autowired
	private IMenuDao menuDao;
	
	
	public IMenuDao getMenuDao() {
		return menuDao;
	}

	public void setMenuDao(IMenuDao menuDao) {
		this.menuDao = menuDao;
	}

	/**
	 * 获取一级菜单
	 * @return
	 */
	
	public List getMainMenuList(int start, int limit)
	{
		return menuDao.getMainMenuList(start, limit);
	}
	
	public SysMenu findMenuById(String id)
	{
		return menuDao.findMenuById(id);
	}
	/**
	 * qunxianchuli 
	 */
	public List findMenuByParentMenuId(String menuId,String roleId)
	{
		
		List<SysMenu> listMenu =  menuDao.getByMenuId(menuId);
		
		List<Object> listObj = new ArrayList<Object>();
		for (int i = 0; i < listMenu.size(); i++) {
			
			SysMenu sysMenu = listMenu.get(i);
			
			SysPowerItem sysPoItem = menuDao.getSysPowerItemByMenuId(sysMenu.getMenuId());
			
			if ( sysPoItem != null )
			{
				SysRolePower sysRolePower = menuDao.getSysRolePowerByPower(roleId,sysPoItem.getPowerCode());
				
				if( sysRolePower != null )
				{
					Map<String,Object> map = new HashMap<String,Object>();
					map.put("url",sysMenu.getUrl());
					map.put("menuName",sysMenu.getMenuName());
					listObj.add(map);
				}
				
			}
			//SysRolePower sysRolePower = list.get(i);
			
			/*SysPowerItem sysPoItem = menuDao.getBySysPowerItemId(sysRolePower.getPowerCode());
			if(sysPoItem != null){
				SysMenu sysMenu = menuDao.getByMenuId(sysPoItem.getSysMenu().getMenuId());
				Map<String,Object> map = new HashMap<String,Object>();
				if(sysMenu != null){
					map.put("url",sysMenu.getUrl());
					map.put("menuName",sysMenu.getMenuName());
					listObj.add(map);
				}
			}*/
		}
		return listObj;
		//return menuDao.findMenuByParentMenuId(id);
	}
	//查询   方法
	@Override
	public List getMainMenuList(String id) {
		return menuDao.getSysMenu(id);
	}

	/**
	 * 保存方法      编辑方法
	 */
	@Override
	public void addMenu(String mode,String menukey,String name, String code, String url, String icon,
			Integer enable ,String type) {
		if("edit".equals(mode)){
			SysMenu sysMenus = findMenuById(menukey);
			sysMenus.setMenuName(name);
			sysMenus.setMenuCode(code);
			sysMenus.setUrl(url);
			sysMenus.setIcon(icon);
			sysMenus.setEnable(enable);
			menuDao.saveSysMenu(sysMenus);
		}else{
			SysMenu sysMenus = new SysMenu();
			sysMenus.setCreateDate(new Timestamp(new Date().getTime()));
			sysMenus.setOrderNo(0);
			SysMenu sysMenu = findMenuById(menukey);
			sysMenus.setSysMenu(sysMenu);
			sysMenus.setMenuName(name);
			sysMenus.setMenuCode(code);
			sysMenus.setUrl(url);
			sysMenus.setIcon(icon);
			sysMenus.setEnable(enable);
			menuDao.saveSysMenu(sysMenus);
		}
	}
	//删除   方法
	@Override
	public void delmenu(String menuId) {
		SysMenu sysMenu = findMenuById(menuId);
		menuDao.delSysMenu(sysMenu);
	}
	
	//隐藏  方法
	@Override
	public void hideById(String menuId) {
		SysMenu sysMenu = findMenuById(menuId);
		sysMenu.setEnable(0);
		menuDao.saveSysMenu(sysMenu);
	}

	/* 
	 * 根据token获取用户信息（用户名--用来在右上角显示）
	 * 王思明
	 * 2016年7月19日
	 * 下午5:37:12
	 */
	@Override
	public SysUser getByTokenUser(Object token) {
		
		return menuDao.getByTokenUser(token);
	}

	/**
	 * 2016年9月30日下午3:35:02
	 *  龚杰
	 *  注释：登陆者角色
	 */
	@Override
	public SysUserRole getSysUserRoleByUserId(String userId) {
		// TODO Auto-generated method stub
		return menuDao.getSysUserRoleByUserId(userId);
	}
		
}
