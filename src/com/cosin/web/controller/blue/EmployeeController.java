package com.cosin.web.controller.blue;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cosin.utils.DateUtils;
import com.cosin.utils.HttpBodyClient;
import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.dao.blue.IEmployeeDao;
import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Classteacher;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;
import com.cosin.web.service.blue.IEmployeeManager;

@Scope("prototype")
@Controller
public class EmployeeController {

	@Autowired
	private IEmployeeManager employeeManager;
	@Autowired
	private IEmployeeDao employeeDao;

	/**
	 * 跳转到职工管理页面
	 * 
	 * @return
	 */
	@RequestMapping(value="/employee/employeeData", produces = "text/html;charset=UTF-8")
	public ModelAndView employee(HttpServletRequest request,HttpServletResponse ronse){
		ModelAndView mav = new ModelAndView();
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String role = userSession.roleId;
		
		List<Object> listPw = userSession.list;
		
		for (int i = 0; i < listPw.size(); i++) {
			
			if("employee_add".equals(listPw.get(i))){//添加权限
				
				mav.addObject("employee_add", listPw.get(i));
				
			}
			if("employee_enable".equals(listPw.get(i))){
				
				mav.addObject("employee_enable", listPw.get(i));
				
			}
			if("employee_daoru".equals(listPw.get(i))){
				
				mav.addObject("employee_daoru", listPw.get(i));
				
			}
			if("employee_daochu".equals(listPw.get(i))){
				
				mav.addObject("employee_daochu", listPw.get(i));
				
			}
			if("employee_xiugai".equals(listPw.get(i))){
				
				mav.addObject("employee_xiugai", listPw.get(i));
				
			}
			if("employee_del".equals(listPw.get(i))){
				
				mav.addObject("employee_del", listPw.get(i));
				
			}
		}
		mav.setViewName("/employee/employee");
		
		return mav;
	}
	/*@RequestMapping(value = "/employee/employeeData")
	public String notice() {
		return "/employee/employee";
	}*/

	/**
	 * 查询功能 操作 分页功能 操作 搜索功能 操作
	 * 
	 * @param request
	 *            获取前台页面属性
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/employee/selectEmployee", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectsubject(HttpServletRequest request,HttpServletResponse response) {
		String SsName = request.getParameter("SsName");
		String SsTel = request.getParameter("SsTel");
		Map pageParam = PageUtils.getPageParam(request);
		
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		List<SysUserRole> chaSysUserRole = employeeManager.getSysUserRoleByUserId(userId);
		String roleId = chaSysUserRole.get(0).getSysRole().getRoleId();
		SysSubrole subrole = employeeManager.getSysSubRoleByRoleId(roleId);
		String subRoleId = subrole.getSubRoleId();
		SysUser chaSysUser = employeeManager.getSysUserBySubRoleId(subRoleId);
		String roleLevel = chaSysUser.getSysSubrole().getRoleLevel();
		SysRoleSchoolPower roleSchoolPower = employeeManager.getSysRoleSchoolPowerByRoleId(roleId);
		
		int num = 0;
		List list = new ArrayList();
		if("总园".equals(roleLevel)&&chaSysUserRole.get(0).getSysRole().getType()==1){
			
			List<Employee> listsubject = employeeManager.getEmployeeLimit(SsName,SsTel,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));

			//数量 分页
			List listCount = employeeManager.getEmployeeCountGai(SsName,SsTel);
			num = listCount.size();
			
				for (int i = 0; i < listsubject.size(); i++) {
					Employee vcsubject = listsubject.get(i);
					
					Employee listNum = (Employee) listCount.get(i);
					Map map = new HashMap();
					
					// 角色
					String useIdNum = listNum.getSysUserByUserId().getUserId();
					if("4028810a57bd26110157bd55fbd7005c".equals(useIdNum)){
						num--;
					}
					if(listNum.getSysUserByUserId().getIsDel().equals(1)){
						num--;
					}
					
					// 角色
					String useId = vcsubject.getSysUserByUserId().getUserId();
					if("4028810a57bd26110157bd55fbd7005c".equals(useId)){
						continue;
					}
					if(vcsubject.getSysUserByUserId().getIsDel().equals(1)){
						continue;
					}
					
					
					
					//Id
					map.put("employeeId", vcsubject.getEmployeeId());
					//园区名称
					
					if(vcsubject.getSchool() != null){
						
						map.put("schoolName", vcsubject.getSchool().getSchoolName());
					}
					List<SysUserRole> sysUserRole = employeeManager.getSysUserRoleByUserId(useId);
					if(sysUserRole.size() > 0){
						
						String rolerName = sysUserRole.get(0).getSysRole().getRolerName();
						SysSubrole sysSubrole = vcsubject.getSysUserByUserId().getSysSubrole();
						if(sysSubrole != null){
							if(sysSubrole.getTypeName()!=null&&sysSubrole.getTypeName()!=""){
								
								map.put("roleName", rolerName + "——" +sysSubrole.getTypeName());
							}else{
								
								map.put("roleName", rolerName);
							}
						}else {
							map.put("roleName", rolerName);
						}
					}
					// 姓名
					map.put("userName", vcsubject.getSysUserByUserId().getUserName());
					// 性别
					if(vcsubject.getSysUserByUserId().getSex()==1){
						map.put("sex", "男");
					}else{
						map.put("sex", "女");
					}
					// 联系电话
					map.put("tel", vcsubject.getSysUserByUserId().getMobile());
					// 可用
					map.put("enable", vcsubject.getEnable());
					
					list.add(map);
				}
				
				List<Employee> listZY = employeeDao.getEmployeeBySchoolNullLimit(SsName,SsTel,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
				//数量 分页
				List listCountZY = employeeDao.getEmployeeCountZY(SsName,SsTel);
				num += listCountZY.size();
				for (int i = 0; i < listZY.size(); i++) {
					Map map = new HashMap();
					Employee employee = listZY.get(i);
					
					Employee listNum = (Employee) listCount.get(i);
					// 角色
					String useIdNum = listNum.getSysUserByUserId().getUserId();
					
					String userEmpId = employee.getSysUserByUserId().getUserId();
					if("4028810a57bd26110157bd55fbd7005c".equals(useIdNum)){
						num--;
					}
					if(listNum.getSysUserByUserId().getIsDel().equals(1)){
						num--;
					}
					
					// 角色
					String useId = employee.getSysUserByUserId().getUserId();
					if("4028810a57bd26110157bd55fbd7005c".equals(useId)){
						continue;
					}
					if(employee.getSysUserByUserId().getIsDel().equals(1)){
						continue;
					}
					//Id
					map.put("employeeId", employee.getEmployeeId());
					
					List<SysUserRole> sysUserRole = employeeManager.getSysUserRoleByUserId(userEmpId);
					if(sysUserRole.size() > 0){
						
						String rolerName = sysUserRole.get(0).getSysRole().getRolerName();
						SysSubrole sysSubrole = employee.getSysUserByUserId().getSysSubrole();
						if(sysSubrole != null){
							if(sysSubrole.getTypeName()!=null&&sysSubrole.getTypeName()!=""){
								
								map.put("roleName", rolerName + "——" +sysSubrole.getTypeName());
							}else{
								
								map.put("roleName", rolerName);
							}
						}else {
							map.put("roleName", rolerName);
						}
					}
					// 姓名
					map.put("userName", employee.getSysUserByUserId().getUserName());
					// 性别
					if(employee.getSysUserByUserId().getSex()==1){
						map.put("sex", "男");
					}else{
						map.put("sex", "女");
					}
					// 联系电话
					map.put("tel", employee.getSysUserByUserId().getMobile());
					// 可用
					map.put("enable", employee.getEnable());
					list.add(map);
				}
				/*int total = employeeManager.getEmployeeCount(SsName,SsTel);
				num += total;*/
				
			}else{
				
				Employee employee1 = employeeManager.getEmployeeByUserId(userId);
				
				List<Employee> employeeList = employeeManager.getEmployeeBySchoolId(employee1.getSchool().getSchoolId(),SsName,SsTel,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
				
				List<Employee> employeeListCount = employeeManager.getEmployeeBySchoolIdCount(employee1.getSchool().getSchoolId(),SsName,SsTel);
				num = employeeListCount.size();
				
				for (int j = 0; j < employeeList.size(); j++) {
					Employee employee = employeeList.get(j);
					//数量
					Employee employeeCount = employeeListCount.get(j);
					
					if(employee != null&&!"".equals(employee)){
							
							Map map = new HashMap();
							
							// 角色
							String useIdd = employeeCount.getSysUserByUserId().getUserId();
							
							if("4028810a57bd26110157bd55fbd7005c".equals(useIdd)){
								num--;
							}
							if(employeeCount.getSysUserByUserId().getIsDel().equals(1)){
								num--;
							}
							
							// 角色
							String useId = employee.getSysUserByUserId().getUserId();
							
							if("4028810a57bd26110157bd55fbd7005c".equals(useId)){
								continue;
							}
							if(employee.getSysUserByUserId().getIsDel().equals(1)){
								continue;
							}
							
							
							
							List<SysUserRole> sysUserRole = employeeManager.getSysUserRoleByUserId(useId);
							if(sysUserRole.size() > 0){
								
								String rolerName = sysUserRole.get(0).getSysRole().getRolerName();
								SysSubrole sysSubrole = employee.getSysUserByUserId().getSysSubrole();
								if(sysSubrole != null){
									if(sysSubrole.getTypeName()!=null&&sysSubrole.getTypeName()!=""){
										
										map.put("roleName", rolerName + "——" +sysSubrole.getTypeName());
									}else{
										
										map.put("roleName", rolerName);
									}
								}else {
									map.put("roleName", rolerName);
								}
							}
							//Id
							map.put("employeeId", employee.getEmployeeId());
							//园区名称
							if(employee.getSchool() != null){
								
								map.put("schoolName", employee.getSchool().getSchoolName());
							}
							// 姓名
							map.put("userName", employee.getSysUserByUserId().getUserName());
							// 性别
							if(employee.getSysUserByUserId().getSex()==1){
								map.put("sex", "男");
							}else{
								map.put("sex", "女");
							}
							// 联系电话
							map.put("tel", employee.getSysUserByUserId().getMobile());
							// 可用
							map.put("enable", employee.getEnable());

							list.add(map);
						/*int total = employeeManager.getEmployeeCountBySchoolId(employee1.getSchool().getSchoolId(),SsName,SsTel);
							num += total;*/
					}
				}
				
			}
				
		Map mapRet = PageUtils.getPageParam(list, num);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
			
		
		
	}

	/**
	 * 获取角色-类型 二级联动
	 */
	@RequestMapping(value = "employee/listRole", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getSysRole(HttpServletResponse response,HttpServletRequest request) {
		List list = new ArrayList();
		String schoolId = request.getParameter("schoolId");
		if(schoolId==null||schoolId==""){
			List<SysRole> listRes = employeeDao.getlistRoleByZY();
			for (int i = 0; i < listRes.size(); i++) {
				SysRole role = listRes.get(i);
				String roleName = role.getRolerName();
				Map map = new HashMap();
				if(roleName.equals("总园长")){
					map.put("roleName", roleName);
					map.put("roleId", role.getRoleId());
					list.add(map);
					break;
				}
			}
		}else{
			list = employeeManager.getlistRole(schoolId);
		}

		return JsonUtils.fromArrayObject(list);
	}
	
	/**
	 * 获取类型
	 */
	@RequestMapping(value = "employee/listSubRole", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getSysSubRole(HttpServletResponse response,
			HttpServletRequest request) {
		String roleId = request.getParameter("roleId");
		List listRes = employeeManager.getlistSubRole(roleId);

		return JsonUtils.fromArrayObject(listRes);
	}
	
	/**
	 * 获取级别
	 */
	@RequestMapping(value = "employee/getRoleLevel", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getRoleLevel(HttpServletResponse response,HttpServletRequest request) {
		String roleId = request.getParameter("roleId");
		String roleLevelName = employeeManager.getRoleLevelName(roleId);
		Map map = new HashMap();
		if(roleLevelName.equals("年级")){
			map.put("roLevel", 1);
			return JsonUtils.fromObject(map);
		}else{
			map.put("roLevel", 0);
			return JsonUtils.fromObject(map);
		}
	}
	
	/**
	 * 查询园区 操作者级别
	 * 思路：
	 * 		获取userId————查sysUser 表 对应的 userId————得到 对应roleId 外键————查询sysRoleSchool 表 关联的园区
	 */
	@RequestMapping(value = "/employee/listSchool", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getSchoolList(HttpServletRequest request,
			HttpServletResponse response) {
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String createUserId = userSession.userId;
		List listRes = employeeManager.getlistSchool(createUserId);

		return JsonUtils.fromArrayObject(listRes);
	}
	
	/**
	 * 保存职工
	 */
	@RequestMapping(value = "/employee/saveEmployee", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveShop(HttpServletRequest request,
			HttpServletResponse response) {
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		//employeeId
		String employeeId = request.getParameter("employeeId");
		//园区Id
		String schoolId = request.getParameter("schoolId");
		//角色Id
		String roleId = request.getParameter("roleId");
		//类型Id
		String subRoleId = request.getParameter("subRoleId");
		
		//姓名 职工姓名 保存到 sysuser表
		String sysUserName = request.getParameter("sysUserName");
		//性别
		String sex = request.getParameter("sex");
		//联系电话
		String tel = request.getParameter("tel");
		
		String mode = request.getParameter("mode");
		
		String isNianJ = request.getParameter("isNianJ");
		
//		String username = request.getParameter("emobCode");
		String radom = DateUtils.getCharAndNumr(6);
		
		Map map = new HashMap();
		
		SysUser sysUser = employeeManager.chaSysUserByLogin(tel);
		
		//判断 是否为总园长
		SysRole sysRole = employeeDao.getSysRoleById(roleId);
		String roleName = sysRole.getRolerName();
		if(roleName.equals("总园长")&&!"4028810a57bd26110157bd55fbd7005c".equals(userId)){
			map.put("code", 101);
			map.put("msg", "您添加不了该角色!");
		}else if(sysUser!=null&&!"edit".equals(mode)){
			map.put("code", 101);
			map.put("msg", "用户已存在");
		}else{
			employeeManager.saveSchool(isNianJ,userId, employeeId, schoolId,roleId, subRoleId, sysUserName, sex,tel,mode,radom);
			try {
				HttpBodyClient.postBody("https://a1.easemob.com/bluefuture/future/users", "{\"username\":\""+radom+"\", \"password\":\"111\"}");
			} catch (Exception e) {
				e.printStackTrace();
			}
			map.put("code", 100);
			map.put("msg", "保存成功");
		}
		String resStr = JsonUtils.fromObject(map);
		return resStr;
		

	}
	
	/**
	 * 删除操作
	 */
	@RequestMapping(value = "/employee/delEmployee", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String delEmployee(HttpServletResponse response,HttpServletRequest request) {
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		String role = userSession.roleId;
		//delKeys就是employid
		
		String deleteKeys=request.getParameter("delKeys");
		String[] keyArr = deleteKeys.split(",");
		Map map = new HashMap();
		int num = 0;
		for(int i=0; i<keyArr.length; i++){
			String key = keyArr[i];
			if(!"".equals(key) && key !=null){
				
				Employee employee = employeeDao.getEmployeeById(key);
				
				//查询删除对象是否是总园
				SysUserRole sysUserRole = employeeDao.getSysUserRoleByUseri(employee.getSysUserByUserId().getUserId());
				String roleName = sysUserRole.getSysRole().getRolerName();
				if(roleName.equals("总园长")&&!userId.equals("4028810a57bd26110157bd55fbd7005c")){
					num++;
				}
				Classteacher classteacher = employeeDao.getClassesTeacherByEmId(employee.getEmployeeId());
				Classes classes = employeeDao.getClassesByEmId(employee.getEmployeeId());
				if(classteacher!= null || classes != null){
					num++;
				}
			}
		}
		if(num == 0){
			for(int i=0; i<keyArr.length; i++)
			{
				String key = keyArr[i];
				if(!"".equals(key) && key !=null){
					employeeManager.delEmployee(key);
				}
			}
			map.put("code", 100);
			map.put("msg", "删除成功");
		}else{
			map.put("code", 101);
			map.put("msg", "数据不可被删除！");
		}
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
	/**
	 * 编辑 员工
	 */ 
	@RequestMapping(value = "employee/employeeEdit", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String employeeEdit(HttpServletRequest request) {
		
		String employeeId = request.getParameter("employeeId");
		
		Employee employee = employeeManager.getEmployeeById(employeeId);
		Map map = new HashMap();
		//园区 Id
		map.put("employeeId", employee.getEmployeeId());
		//园区名称
		if(employee.getSchool() != null){
			
			map.put("schoolName", employee.getSchool().getSchoolName());
			map.put("schoolId", employee.getSchool().getSchoolId());
		}
		
		// 角色
		String useId = employee.getSysUserByUserId().getUserId();
		List<SysUserRole> sysUserRole = employeeManager.getSysUserRoleByUserId(useId);
		if(sysUserRole.size() > 0){
			
			map.put("roleName", sysUserRole.get(0).getSysRole().getRolerName());
			map.put("roleId", sysUserRole.get(0).getSysRole().getRoleId());
		}
		
		//类型
		SysSubrole subrole = employee.getSysUserByUserId().getSysSubrole();
		if(subrole != null){
			
			map.put("typeName", subrole.getTypeName());
			map.put("subRoleId", subrole.getSubRoleId());
		}
		
		// 姓名
		map.put("userName", employee.getSysUserByUserId().getUserName());
		// 性别
		if(employee.getSysUserByUserId().getSex()==1){
			map.put("sex", "男");
		}else{
			map.put("sex", "女");
		}
		// 联系电话
		map.put("tel", employee.getSysUserByUserId().getMobile());
		// 可用
		map.put("enable", employee.getSysUserByUserId().getEnable());
		
		map.put("isNi", employee.getGrade());
		
		map.put("code", "100");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 随机数 交互
	 */ 
	@RequestMapping(value = "employee/sysUserEmobCode", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String sysUserEmobCode(HttpServletRequest request) {
		String employeeId = request.getParameter("employeeId");
		Employee employee = employeeManager.getEmployeeById(employeeId);
		SysUser sysUser = employee.getSysUserByUserId();
		Map map = new HashMap();
		map.put("emobCode", sysUser.getEmobCode());
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 启用禁用
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/employee/enable", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String enable(HttpServletRequest request,HttpServletResponse response) {
		String disableKeys = request.getParameter("disableKeys");
		String[] str = disableKeys.split(",");
		for (int i = 0; i < str.length; i++) {
			Employee employee = employeeManager.getEmployeeById(str[i]);
			int enable = employee.getEnable();
			if(enable==1){
				employee.setEnable(0);
				employeeManager.save(employee);
			}else{
				employee.setEnable(1);
				employeeManager.save(employee);
			}
		}
		Map map = new HashMap();
		map.put("code", "100");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
	/**
	 * 导出
	 * @param response
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value="/employee/daochuEmployee",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public void daochuEmployee(HttpServletResponse response, HttpServletRequest request) throws IOException{
		
		String SsName = request.getParameter("SsName");
		String SsTel = request.getParameter("SsTel");
		Map pageParam = PageUtils.getPageParam(request);
		
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		List<SysUserRole> chaSysUserRole = employeeManager.getSysUserRoleByUserId(userId);
		String roleId = chaSysUserRole.get(0).getSysRole().getRoleId();
		SysSubrole subrole = employeeManager.getSysSubRoleByRoleId(roleId);
		String subRoleId = subrole.getSubRoleId();
		SysUser chaSysUser = employeeManager.getSysUserBySubRoleId(subRoleId);
		String roleLevel = chaSysUser.getSysSubrole().getRoleLevel();
		SysRoleSchoolPower roleSchoolPower = employeeManager.getSysRoleSchoolPowerByRoleId(roleId);
		
		int num = 0;
		List list = new ArrayList();
		if("总园".equals(roleLevel)&&chaSysUserRole.get(0).getSysRole().getType()==1){
			
			List<Employee> listsubject = employeeManager.getEmployeeLimit(SsName,SsTel,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
			
			//数量 分页
			List listCount = employeeManager.getEmployeeCountGai(SsName,SsTel);
			num = listCount.size();
			
				for (int i = 0; i < listsubject.size(); i++) {
					Employee vcsubject = listsubject.get(i);
					
					Employee listNum = (Employee) listCount.get(i);
					Map map = new HashMap();
					
					// 角色
					String useIdNum = listNum.getSysUserByUserId().getUserId();
					if("4028810a57bd26110157bd55fbd7005c".equals(useIdNum)){
						num--;
					}
					if(listNum.getSysUserByUserId().getIsDel().equals(1)){
						num--;
					}
					
					// 角色
					String useId = vcsubject.getSysUserByUserId().getUserId();
					if("4028810a57bd26110157bd55fbd7005c".equals(useId)){
						continue;
					}
					if(vcsubject.getSysUserByUserId().getIsDel().equals(1)){
						continue;
					}
					
					
					
					//Id
					map.put("employeeId", vcsubject.getEmployeeId());
					//园区名称
					if(vcsubject.getSchool() != null){
						
						map.put("schoolName", vcsubject.getSchool().getSchoolName());
					}
					List<SysUserRole> sysUserRole = employeeManager.getSysUserRoleByUserId(useId);
					if(sysUserRole.size() > 0){
						
						String rolerName = sysUserRole.get(0).getSysRole().getRolerName();
						SysSubrole sysSubrole = vcsubject.getSysUserByUserId().getSysSubrole();
						if(sysSubrole != null){
							if(sysSubrole.getTypeName()!=null&&sysSubrole.getTypeName()!=""){
								
								map.put("roleName", rolerName + "——" +sysSubrole.getTypeName());
							}else{
								
								map.put("roleName", rolerName);
							}
						}else {
							map.put("roleName", rolerName);
						}
					}
					// 姓名
					map.put("userName", vcsubject.getSysUserByUserId().getUserName());
					// 性别
					if(vcsubject.getSysUserByUserId().getSex()==1){
						map.put("sex", "男");
					}else{
						map.put("sex", "女");
					}
					// 联系电话
					map.put("tel", vcsubject.getSysUserByUserId().getMobile());
					// 可用
					map.put("enable", vcsubject.getEnable());
					
					if(vcsubject.getGrade() != null&&!"".equals(vcsubject.getGrade())){
						int grad = vcsubject.getGrade();
						if(grad == 1){
							map.put("grade", "托班");
						}else if(grad == 2){
							map.put("grade", "小班");
						}else if(grad == 3){
							map.put("grade", "中班");
						}else if(grad == 4){
							map.put("grade", "大班");
						}else if(grad == 5){
							map.put("grade", "学前班");
						}
					}else{
						
						map.put("grade", "");
					}
					
					list.add(map);
				}
				/*int total = employeeManager.getEmployeeCount(SsName,SsTel);
				num += total;*/
				
			}else{
				
				Employee employee1 = employeeManager.getEmployeeByUserId(userId);
				
				List<Employee> employeeList = employeeManager.getEmployeeBySchoolId(employee1.getSchool().getSchoolId(),SsName,SsTel,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
				
				List<Employee> employeeListCount = employeeManager.getEmployeeBySchoolIdCount(employee1.getSchool().getSchoolId(),SsName,SsTel);
				num = employeeListCount.size();
				
				for (int j = 0; j < employeeList.size(); j++) {
					Employee employee = employeeList.get(j);
					//数量
					Employee employeeCount = employeeListCount.get(j);
					
					if(employee != null&&!"".equals(employee)){
							
							Map map = new HashMap();
							
							// 角色
							String useIdd = employeeCount.getSysUserByUserId().getUserId();
							
							if("4028810a57bd26110157bd55fbd7005c".equals(useIdd)){
								num--;
							}
							if(employeeCount.getSysUserByUserId().getIsDel().equals(1)){
								num--;
							}
							
							// 角色
							String useId = employee.getSysUserByUserId().getUserId();
							
							if("4028810a57bd26110157bd55fbd7005c".equals(useId)){
								continue;
							}
							if(employee.getSysUserByUserId().getIsDel().equals(1)){
								continue;
							}
							
							
							
							List<SysUserRole> sysUserRole = employeeManager.getSysUserRoleByUserId(useId);
							if(sysUserRole.size() > 0){
								
								String rolerName = sysUserRole.get(0).getSysRole().getRolerName();
								SysSubrole sysSubrole = employee.getSysUserByUserId().getSysSubrole();
								if(sysSubrole != null){
									if(sysSubrole.getTypeName()!=null&&sysSubrole.getTypeName()!=""){
										
										map.put("roleName", rolerName + "——" +sysSubrole.getTypeName());
									}else{
										
										map.put("roleName", rolerName);
									}
								}else {
									map.put("roleName", rolerName);
								}
							}
							//Id
							map.put("employeeId", employee.getEmployeeId());
							//园区名称
							if(employee.getSchool() != null){
								
								map.put("schoolName", employee.getSchool().getSchoolName());
							}
							// 姓名
							map.put("userName", employee.getSysUserByUserId().getUserName());
							// 性别
							if(employee.getSysUserByUserId().getSex()==1){
								map.put("sex", "男");
							}else{
								map.put("sex", "女");
							}
							// 联系电话
							map.put("tel", employee.getSysUserByUserId().getMobile());
							// 可用
							map.put("enable", employee.getEnable());
							
							if(employee.getGrade() != null&&!"".equals(employee.getGrade())){
								int grad = employee.getGrade();
								if(grad == 1){
									map.put("grade", "托班");
								}else if(grad == 2){
									map.put("grade", "小班");
								}else if(grad == 3){
									map.put("grade", "中班");
								}else if(grad == 4){
									map.put("grade", "大班");
								}else if(grad == 5){
									map.put("grade", "学前班");
								}
							}else{
								
								map.put("grade", "");
							}

							list.add(map);
						/*int total = employeeManager.getEmployeeCountBySchoolId(employee1.getSchool().getSchoolId(),SsName,SsTel);
							num += total;*/
					}
				}
				
			}
		//创建一个新的 excel 
        HSSFWorkbook wb = new HSSFWorkbook();
        //创建 sheet 页
        HSSFSheet sheet = wb.createSheet("职工管理"); 
        HSSFRow row = sheet.createRow((int) 0);  
        //sheet.setColumnWidth((short) 0, (short) (35.7 * 150));
        //sheet.setColumnWidth((short)1, (short) 600);
       
        //设置水平居中
        HSSFCellStyle style = wb.createCellStyle();  
        
        
        //设置标题居中
        HSSFHeader header = sheet.getHeader();
        header.setCenter("职工管理");
        String[] excelHeader = new String[]{"园区名","角色","姓名","性别","联系电话","状态","年级级别"};
        for (int i = 0; i < excelHeader.length; i++) {  
            HSSFCell cell = row.createCell(i);  
            cell.setCellValue(excelHeader[i]);  
            cell.setCellStyle(style);  
            sheet.autoSizeColumn(i);  
        }  
        for (int i = 0; i < list.size(); i++) {  
            row = sheet.createRow(i + 1);  
            Map map = (Map)list.get(i);  
            if(map.get("schoolName")!=null&&!"".equals(map.get("schoolName"))){
            	
            	row.createCell(0).setCellValue(map.get("schoolName").toString());  
            }
            row.createCell(1).setCellValue(map.get("roleName").toString());
            row.createCell(2).setCellValue(map.get("userName").toString());
            row.createCell(3).setCellValue(map.get("sex").toString());
            row.createCell(4).setCellValue(map.get("tel").toString());
            row.createCell(5).setCellValue(map.get("enable").toString());
            row.createCell(6).setCellValue(map.get("grade").toString());
        }   
        sheet.autoSizeColumn((short)0);// 列表宽度自定义
        sheet.autoSizeColumn((short)1);// 列表宽度自定义
        sheet.autoSizeColumn((short)2);// 列表宽度自定义
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 居中
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);// 居中
        style.setWrapText(true); 
        response.setContentType("application/vnd.ms-excel");    
        String excelName = "职工管理";
        response.setHeader("Content-disposition", "attachment;filename="+new String(excelName.getBytes(), "ISO8859-1")+".xls");    
        OutputStream ouputStream = response.getOutputStream();    
        wb.write(ouputStream);    
        ouputStream.flush();    
        ouputStream.close();  
	}
	
	
	
	 /** 
     * 读取excel报表 
     */  
    @RequestMapping(value = "/employee/saveExcel", produces = "text/html;charset=UTF-8") 
    @ResponseBody
    public String getReadReport(@RequestParam MultipartFile file,HttpServletRequest request)  
            throws IOException {  
    	UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
    	/*(@RequestParam("userName") String userName,*/
    	String  path = request.getSession().getServletContext().getRealPath("/");
    	String chaResult = employeeManager.chaReport(userId, path, file.getInputStream());
    	String str = "";
    	if(chaResult.equals("")){
    		str = employeeManager.readReport(userId,path,file.getInputStream());  
    	}else{
    		str = chaResult;
    	}
        
    	return str;
    	
    } 
	
	
	
	
}
