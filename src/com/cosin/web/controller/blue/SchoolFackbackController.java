package com.cosin.web.controller.blue;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cosin.utils.DateUtils;
import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SchoolFackback;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUserRole;
import com.cosin.web.service.blue.IContainManager;
import com.cosin.web.service.blue.ISchoolFackbackManager;
import com.cosin.web.service.blue.ISchoolManager;
import com.cosin.web.service.system.ISysManager;


@Scope("prototype")
@Controller
public class SchoolFackbackController {

	@Autowired
	private ISchoolFackbackManager schoolFackbackManager;
	@Autowired
	private ISysManager sysDao;
	@Autowired
	private ISchoolManager schoolManager;
	@Autowired
	private IContainManager containManager;
	/**
	 * 跳转到园长信箱页面
	 * @return
	 */
	@RequestMapping(value="/schoolFackback/schoolFackback", produces = "text/html;charset=UTF-8")
	public ModelAndView fackBack(HttpServletRequest request,HttpServletResponse ronse){
		ModelAndView mav = new ModelAndView();
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		
		List<Object> listPw = userSession.list;
		
		for (int i = 0; i < listPw.size(); i++) {
			
			if("mailbox_chakan".equals(listPw.get(i))){//添加权限
				
				mav.addObject("mailbox_chakan", listPw.get(i));
				
			}
			if("mailbox_del".equals(listPw.get(i))){
				
				mav.addObject("mailbox_del", listPw.get(i));
				
			}
			if("mailbox_daochu".equals(listPw.get(i))){
				
				mav.addObject("mailbox_daochu", listPw.get(i));
				
			}
		}
		mav.setViewName("/schoolFackback/schoolFackback");
		
		return mav;
	}
	/*@RequestMapping(value="/schoolFackback/schoolFackback")
	public String fackBack(){
		return "/schoolFackback/schoolFackback";
	}*/

	/**
	 *查询功能  操作
	 *分页功能  操作
	 *搜索功能  操作
	 * @param request 获取前台页面属性
	 * @param response
	 * @return
	 */
	@RequestMapping(value="schoolFackback/selectSchoolNameList", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectSchoolNameList(HttpServletRequest request,HttpServletResponse response){
	
		String schoolFackbackId = request.getParameter("schoolFackbackId");
		String ischoolName = request.getParameter("schoolName");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		
		String enable ;
		enable = request.getParameter("enable");
		if("已读".equals(enable)){
			enable ="1";
		}
		if("未读".equals(enable)){
			enable ="0";
		}
		Map pageParam = PageUtils.getPageParam(request);
		
		int num = 0;
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		SysUserRole sysUserRole = schoolManager.getSysUserRoleByUserId(userId);
		String roleId = sysUserRole.getSysRole().getRoleId();
		SysSubrole subrole = schoolManager.getSysSubRoleByRoleId(roleId);
		List<SchoolFackback> listsubject = new ArrayList<SchoolFackback>();
		if("总园".equals(subrole.getRoleLevel())&&sysUserRole.getSysRole().getType()==1){
			
			listsubject = schoolFackbackManager.getSchoolFackbackLimit(startDate,endDate,ischoolName, enable , new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
			List total = schoolFackbackManager.getSchoolFackbackCount(startDate,endDate,ischoolName, enable);
			num = total.size();
		}
		else{
			List<School> schools = containManager.getSchools(userId);
			String schoolsId="";
			for (int i = 0; i < schools.size(); i++) {
				School school = schools.get(i);
				if(i==schools.size()-1){
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'";
				}
				else{
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'"+",";
				}
			}
			if(!schoolsId.equals("")){
				
				listsubject = schoolFackbackManager.getSchoolFackbackLimit(schoolsId,startDate,endDate,ischoolName, enable , new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
				List total = schoolFackbackManager.getSchoolFackbackCountBySchoolId(schoolsId,startDate,endDate,ischoolName, enable);
				num = total.size();
			}
		}
		String schoolNam = "";
		List listRes = new ArrayList();
		for (int i = 0; i < listsubject.size(); i++) {
			SchoolFackback vcsubject= listsubject.get(i);
			Map map  = new HashMap();
			map.put("schoolFackbackId",vcsubject.getSchoolFackbackId());
			//园区名
			map.put("schoolName", vcsubject.getSchool().getSchoolName());
			//反馈时间
			map.put("createTime", vcsubject.getCreateDate());
			//学生姓名
			map.put("studentName",vcsubject.getStudent().getStudentName());
			//反馈内容
			map.put("content", vcsubject.getContent());
			//查看状态
//			map.put("enable", vcsubject.getEnable());
			map.put("enable",vcsubject.getEnable());
			listRes.add(map);
		}
		
		Map mapRet = PageUtils.getPageParam(listRes,num);
		return JsonUtils.fromObject(mapRet);
	}
	
	/**
	 * 跳转到详情页面
	 * @param request
	 * @return
	 */
//	@RequestMapping(value = "schoolFackback/detail")
//	public ModelAndView look(HttpServletRequest request) {
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.setViewName("/schoolFackback/detail");
//		return modelAndView;
//	}
	
	
	
	
	
	/**
	 * 删除功能 操作
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/schoolFackback/delSchoolFackback", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String schoolFackbackDel(HttpServletRequest request,HttpServletResponse response){
		
		
		String deleteKeys=request.getParameter("delKeys");
		String[] keyArr = deleteKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null){
				
				schoolFackbackManager.schoolFackbackDelId(key);
			}
				
		}
			Map map = new HashMap();
			map.put("code", 100);
			map.put("msg", "删除成功");
			String resStr = JsonUtils.fromObject(map);
			return resStr;
	}
	
	/**
	 * 当点击详情页面时候 更改查看状态
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/schoolFackback/updateSchoolFackback", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String updatesubject(HttpServletRequest request,HttpServletResponse response) {
		String schoolFackbackId = request.getParameter("schoolFackbackId");
		schoolFackbackManager.updataSchoolFackback(schoolFackbackId);
		
		SchoolFackback schoolFackback = schoolFackbackManager.getSchoolFackbackId(schoolFackbackId);
		Map<Object, Object> map = new HashMap<Object, Object>();
		//反馈内容
		map.put("content", schoolFackback.getContent());
		/**
		 * 反馈人信息
		 */
		//学生姓名
		String studentName = schoolFackback.getStudent().getStudentName();
		//亲戚关系  父亲……
		//String relation = schoolFackback.getStudent().getParents().getRelation();
		String parentId = schoolFackback.getStudent().getParents().getParentId();
		Parentsstudent parentsstudent = schoolFackbackManager.getParentsstudentByParentId(parentId);
		String relation = parentsstudent.getRelativeShip();
		//亲戚姓名 
		String parentsName = schoolFackback.getStudent().getParents().getSysUser().getUserName();
		//电话
		String tel = schoolFackback.getStudent().getParents().getSysUser().getMobile();
		//园区  届数   年级  班级
		String school = schoolFackback.getStudent().getSchool().getSchoolName();
		String intoYear = schoolFackback.getStudent().getClasses().getIntoYear();
		String gra = schoolFackback.getStudent().getClasses().getGrade();
		String grade = null;
		if(gra.equals("1")){
			grade = "托班";
		}else if(gra.equals("2")){
			grade = "小班";
		}else if(gra.equals("3")){
			grade = "中班";
		}else if(gra.equals("4")){
			grade = "大班";
		}else if(gra.equals("5")){
			grade = "学前班";
		}
		
		
		String className = schoolFackback.getStudent().getClasses().getClassesName();
		
		//反馈人信息 传值
		map.put("feedback", "学生" + studentName + "的" + relation + ":" + parentsName + " " + tel);
		map.put("feedback1", school + intoYear + "届" + grade + className);
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
	/**
	 * 导出
	 * @param response
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value="/schoolFackback/daochuSchoolFackback",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public void daochuComment(HttpServletResponse response, HttpServletRequest request) throws IOException{
		
//		UserSession userSession = (UserSession)request.getSession().getAttribute("userSession");
//		
//		String order = request.getParameter("order");//asc/desc正序倒叙
//		String sort = request.getParameter("sort");//videoSum排序字段
//		String SsName = request.getParameter("div");
//		String startDate = request.getParameter("startDate");
//		String endDate = request.getParameter("endDate");
//		int mode = 2;
//		
//		
//		List<SchoolFackback> listLog = schoolFackbackManager.getSchoolFackbackLimit(mode,order,sort,SsName,0, 0,startDate,endDate);
		
		
		String schoolFackbackId = request.getParameter("schoolFackbackId");
		String ischoolName = request.getParameter("schoolName");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		
		String enable ;
		enable = request.getParameter("enable");
		if("已读".equals(enable)){
			enable ="1";
		}
		if("未读".equals(enable)){
			enable ="0";
		}
		Map pageParam = PageUtils.getPageParam(request);
		
		
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		SysUserRole sysUserRole = schoolManager.getSysUserRoleByUserId(userId);
		String roleId = sysUserRole.getSysRole().getRoleId();
		SysSubrole subrole = schoolManager.getSysSubRoleByRoleId(roleId);
		List<SchoolFackback> listsubject = new ArrayList<SchoolFackback>();
		if("总园".equals(subrole.getRoleLevel())&&sysUserRole.getSysRole().getType()==1){
			
			listsubject = schoolFackbackManager.getSchoolFackbackLimit(startDate,endDate,ischoolName, enable , new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		}
		else{
			List<School> schools = containManager.getSchools(userId);
			String schoolsId="";
			for (int i = 0; i < schools.size(); i++) {
				School school = schools.get(i);
				if(i==schools.size()-1){
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'";
				}
				else{
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'"+",";
				}
			}
			if(!schoolsId.equals("")){
				
				listsubject = schoolFackbackManager.getSchoolFackbackLimit(schoolsId,startDate,endDate,ischoolName, enable , new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
			}
		}
		String schoolNam = "";
		List listRes = new ArrayList();
		for (int i = 0; i < listsubject.size(); i++) {
			SchoolFackback vcsubject= listsubject.get(i);
			Map map  = new HashMap();
			map.put("schoolFackbackId",vcsubject.getSchoolFackbackId());
			//园区名
			map.put("schoolName", vcsubject.getSchool().getSchoolName());
			//反馈时间
			map.put("createTime", vcsubject.getCreateDate());
			//学生姓名
			map.put("studentName",vcsubject.getStudent().getStudentName());
			//反馈内容
			map.put("content", vcsubject.getContent());
			//查看状态
//			map.put("enable", vcsubject.getEnable());
			map.put("enable",vcsubject.getEnable());
			listRes.add(map);
		}
		//创建一个新的 excel 
        HSSFWorkbook wb = new HSSFWorkbook();
        //创建 sheet 页
        HSSFSheet sheet = wb.createSheet("园长信箱"); 
        HSSFRow row = sheet.createRow((int) 0);  
        //sheet.setColumnWidth((short) 0, (short) (35.7 * 150));
        //sheet.setColumnWidth((short)1, (short) 600);
       
        //设置水平居中
        HSSFCellStyle style = wb.createCellStyle();  
        
        
        //设置标题居中
        HSSFHeader header = sheet.getHeader();
        header.setCenter("园长信箱");
        String[] excelHeader = new String[]{"园区名","反馈内容","反馈时间","学生姓名"};
        for (int i = 0; i < excelHeader.length; i++) {  
            HSSFCell cell = row.createCell(i);  
            cell.setCellValue(excelHeader[i]);  
            cell.setCellStyle(style);  
            sheet.autoSizeColumn(i);  
        }  
        for (int i = 0; i < listRes.size(); i++) {  
            row = sheet.createRow(i + 1);  
            Map map = (Map)listRes.get(i);  
            row.createCell(0).setCellValue(map.get("schoolName").toString());  
            row.createCell(1).setCellValue(map.get("content").toString());
            row.createCell(2).setCellValue(map.get("createTime").toString());
            row.createCell(3).setCellValue(map.get("studentName").toString());
//            row.createCell(4).setCellValue(map.get("commentNum").toString());
//            row.createCell(5).setCellValue(map.get("shareNum").toString());
//            row.createCell(6).setCellValue(map.get("subscribeNum").toString());
        }  // 
        sheet.autoSizeColumn((short)0);// 列表宽度自定义
        sheet.autoSizeColumn((short)1);// 列表宽度自定义
        sheet.autoSizeColumn((short)2);// 列表宽度自定义
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 居中
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);// 居中
        style.setWrapText(true); 
        response.setContentType("application/vnd.ms-excel");    
        String excelName = "园长信箱";
        response.setHeader("Content-disposition", "attachment;filename="+new String(excelName.getBytes(), "ISO8859-1")+".xls");    
        OutputStream ouputStream = response.getOutputStream();    
        wb.write(ouputStream);    
        ouputStream.flush();    
        ouputStream.close();   
	}

	
	
	
	
}
