package com.cosin.web.controller.blue;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cosin.utils.DateUtils;
import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.Recipe;
import com.cosin.web.entity.RecipeRecord;
import com.cosin.web.entity.RecipeSchool;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUserRole;
import com.cosin.web.service.blue.IContainManager;
import com.cosin.web.service.blue.IRecipeManager;
import com.cosin.web.service.blue.ISchoolManager;


@Scope("prototype")
@Controller
public class RecipeController {

	@Autowired
	private IRecipeManager recipeManager;
	@Autowired
	private ISchoolManager schoolManager;
	@Autowired
	private IContainManager containManager;
	/**
	 * 跳转到每周食谱页面
	 * @return
	 */
	@RequestMapping(value="/recipe/recipe", produces = "text/html;charset=UTF-8")
	public ModelAndView recipe(HttpServletRequest request,HttpServletResponse ronse){
		ModelAndView mav = new ModelAndView();
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String role = userSession.roleId;
		
		List<Object> listPw = userSession.list;
		
		for (int i = 0; i < listPw.size(); i++) {
			
			if("recipe_new".equals(listPw.get(i))){//添加权限
				
				mav.addObject("recipe_new", listPw.get(i));
				
			}
			if("recipe_chakan".equals(listPw.get(i))){
				
				mav.addObject("recipe_chakan", listPw.get(i));
				
			}
			if("recipe_chehui".equals(listPw.get(i))){
				
				mav.addObject("recipe_chehui", listPw.get(i));
				
			}
			if("recipe_del".equals(listPw.get(i))){
				
				mav.addObject("recipe_del", listPw.get(i));
				
			}
		}
		mav.setViewName("/recipe/recipe");
		
		return mav;
	}
	/*@RequestMapping(value="/recipe/recipe")
	public String recipe(){
		return "/recipe/recipe";
	}*/

	/**
	 *查询功能  操作
	 *分页功能  操作
	 *搜索功能  操作
	 * @param request 获取前台页面属性
	 * @param response
	 * @return
	 * @throws ParseException 
	 * @throws NumberFormatException 
	 */
	@RequestMapping(value="/recipe/selectrecipe",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectsubject(HttpServletRequest request,HttpServletResponse response) throws NumberFormatException, ParseException{
		String startDate = request.getParameter("startDate");
		Map pageParam = PageUtils.getPageParam(request);
		
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		SysUserRole sysUserRole = schoolManager.getSysUserRoleByUserId(userId);
		String roleId = sysUserRole.getSysRole().getRoleId();
		SysSubrole subrole = schoolManager.getSysSubRoleByRoleId(roleId);
		List<RecipeRecord> listsubject = new ArrayList<RecipeRecord>();
		
		int num = 0;
		if("总园".equals(subrole.getRoleLevel())&&sysUserRole.getSysRole().getType()==1){
			
			listsubject = recipeManager.getRecipeLimit(startDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
			int total = recipeManager.getRecipeCount(startDate);
			num = total;
		}
		else{
			List<School> schools = containManager.getSchools(userId);
			String schoolsId="";
			for (int i = 0; i < schools.size(); i++) {
				School school = schools.get(i);
				if(i==schools.size()-1){
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'";
				}
				else{
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'"+",";
				}

			}
			if(!schoolsId.equals("")){
				
				List<RecipeSchool> lists = recipeManager.getRecipeSchoolLimit(schoolsId,startDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
				for (int i = 0; i < lists.size(); i++) {
					RecipeRecord r = lists.get(i).getRecipeRecord();
					listsubject.add(r);
					num++;
				}
			}
		}
		
		List list = new ArrayList();
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		for (int i = 0; i < listsubject.size(); i++) {
			RecipeRecord vcsubject=listsubject.get(i);
			Map map  = new HashMap();
			map.put("recipeId",vcsubject.getRecipeRecordId());
			//发布时间
			map.put("createTime", vcsubject.getCreateDate());
			//食谱开始时间
			map.put("beginDate", date.format(vcsubject.getBeginTime()));
			//发布人
			map.put("UserName", vcsubject.getSysUser().getUserName());
			//查看人数
			map.put("LookNum", vcsubject.getLookNum());
			//园区
			String recipeRecordId = vcsubject.getRecipeRecordId();
			String schoolName="";
			if(!subrole.getRoleLevel().equals("总园")){
				List<School> schools = containManager.getSchools(userId);
				for (int j = 0; j < schools.size(); j++) {
					School school = schools.get(j);
					schoolName+= school.getSchoolName()+",";
				}
				
			}
			else{
				List<RecipeSchool> listRoScP = recipeManager.getRecipeSchoolById(recipeRecordId);
				for (int j = 0; j < listRoScP.size(); j++) {
					RecipeSchool recipeSchool = listRoScP.get(j);
					schoolName+= recipeSchool.getSchool().getSchoolName()+",";
				}
			}
			
			map.put("validSchool", schoolName);
			
			list.add(map);
		}
		
		Map mapRet = PageUtils.getPageParam(list,num);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	} 
	
	/**
	 * 草稿箱 跳转
	 * @param request
	 * @return
	 */
	@RequestMapping(value="recipe/recipeData")
    public ModelAndView recipe(HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/recipe/recipeDraft");
	    return modelAndView;
    }
	
	/**
	 * 草稿箱 列表查询
	 * @param request
	 * @param response
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value="/recipe/selectdraft",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectDraft(HttpServletRequest request,HttpServletResponse response) throws ParseException{
		String startDate = request.getParameter("startDate");
		Map pageParam = PageUtils.getPageParam(request);
		
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		SysUserRole sysUserRole = schoolManager.getSysUserRoleByUserId(userId);
		String roleId = sysUserRole.getSysRole().getRoleId();
		SysSubrole subrole = schoolManager.getSysSubRoleByRoleId(roleId);
		List<RecipeRecord> listsubject = new ArrayList<RecipeRecord>();
		
		int num = 0;
		if("总园".equals(subrole.getRoleLevel())&&sysUserRole.getSysRole().getType()==1){
			
			listsubject = recipeManager.getDraftLimit(startDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
			int total = recipeManager.getDraftCount(startDate);
			num = total;
		}
		else{
			List<School> schools = containManager.getSchools(userId);
			String schoolsId="";
			for (int i = 0; i < schools.size(); i++) {
				School school = schools.get(i);
				if(i==schools.size()-1){
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'";
				}
				else{
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'"+",";
				}

			}
			if(!schoolsId.equals("")){
				
				List<RecipeSchool> lists = recipeManager.getRecipeDraftSchoolLimit(schoolsId,startDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
				for (int i = 0; i < lists.size(); i++) {
					RecipeRecord r = lists.get(i).getRecipeRecord();
					listsubject.add(r);
					num++;
				}
			}
		}
		List list = new ArrayList();
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		for (int i = 0; i < listsubject.size(); i++) {
			RecipeRecord vcsubject=listsubject.get(i);
			Map map  = new HashMap();
			map.put("recipeId",vcsubject.getRecipeRecordId());
			//发布时间
			map.put("createTime", vcsubject.getCreateDate());
			//食谱开始时间
			map.put("beginDate", date.format(vcsubject.getBeginTime()));
			//发布人
			map.put("UserName", vcsubject.getSysUser().getUserName());
			
			
			
			list.add(map);
		}
			
			Map mapRet = PageUtils.getPageParam(list,num);
			String resStr = JsonUtils.fromObject(mapRet);
			return resStr;
	} 
	
	
	/**
	 * 每周食谱 查看页面跳转
	 * @param request
	 * @return
	 */
//	@RequestMapping(value="recipe/recipeLook")
//    public ModelAndView look(HttpServletRequest request){
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.setViewName("/recipe/recipeLook");
//	    return modelAndView;
//    }
	
	
	/**
	 * 删除功能 操作
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/recipe/delRecipe", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String delsubject(HttpServletRequest request,HttpServletResponse response){
		
		String deleteKeys=request.getParameter("delKeys");
		String[] keyArr = deleteKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null){
				
				RecipeRecord recipe = recipeManager.getRecipeId(key);
				recipeManager.recipeDelId(key);
			}
				
		}
			Map map = new HashMap();
			map.put("code", 100);
			map.put("msg", "删除成功");
			String resStr = JsonUtils.fromObject(map);
			return resStr;
	}
	
	/**
	 * 保存  食谱
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/recipe/saveRecipe", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveRecipe(HttpServletRequest request,HttpServletResponse response) {
		
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		//发布人
		String user = userSession.userName;
		String userId = userSession.userId;

		String beginDate = request.getParameter("beginDate");
		String breakfast = request.getParameter("breakfastt");
		String light1 = request.getParameter("light11");
		String lunch = request.getParameter("lunchh");
		String light2 = request.getParameter("light22");
		String dinner = request.getParameter("dinnerr");
		
		String recipeId = request.getParameter("recipeId");
		//判断 是否 草稿
		String draf = request.getParameter("draf");
		
		//获取 通知对象  园区（字符串拼接）
		String schoolVar = request.getParameter("schoolVar");
		//添加或存稿
		String mode = request.getParameter("mode");
		recipeManager.saveRecipe(recipeId,userId,beginDate,breakfast,light1,lunch,light2,dinner,mode,schoolVar,draf);
		
		
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功");
		String resStr = JsonUtils.fromObject(map).toString();
		return resStr;
	}
	
	/**
	 * 撤回操作 并移至草稿箱
	 */
	@RequestMapping(value="/recipe/roolBackRecipe", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String roolBackRecipe(HttpServletRequest request,HttpServletResponse response){
		
		String recipeId=request.getParameter("openBack");
		
		recipeManager.roolBackRecipe(recipeId);
		
		
			Map map = new HashMap();
			map.put("code", 100);
			map.put("msg", "操作成功");
			String resStr = JsonUtils.fromObject(map);
			return resStr;
	}
	

	
	/**
	 * 编辑 查看 食谱
	 */
	@RequestMapping(value = "recipe/bjRecipe", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String bjRecipe(HttpServletRequest request) {
		String recipeId = request.getParameter("recipeId");
		RecipeRecord recipeRecord= recipeManager.getRecipeRecordById(recipeId);
		//食谱查询 周一 至 周五
		List<Recipe> recipe = recipeManager.getRecipeById(recipeId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("beginTime", recipeRecord.getBeginTime());
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String bDate = dateFormat.format(recipeRecord.getBeginTime());
		Date date = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, bDate);
		
		List listNew = new ArrayList();
		List list = DateUtils.dateToWeek(date);
		for(int i=0; i<list.size(); i++)
		{
			String str = list.get(i).toString();
			
			if(i==0)
				str += "(周一)";
			if(i==1)
				str += "(周二)";
			if(i==2)
				str += "(周三)";
			if(i==3)
				str += "(周四)";
			if(i==4)
				str += "(周五)";
			
			listNew.add(str);
		}
		map.put("array1", listNew);
		
		
		//周一   一天
		String one = recipe.get(0).getContent();
		String[] oneList = one.split("\\|");
		map.put("one1", oneList[1]);
		map.put("one2", oneList[2]);
		map.put("one3", oneList[3]);
		map.put("one4", oneList[4]);
		map.put("one5", oneList[5]);
		
		//周二 一天
		String two= recipe.get(1).getContent();
		String[] twoList = two.split("\\|");
		map.put("two1", twoList[1]);
		map.put("two2", twoList[2]);
		map.put("two3", twoList[3]);
		map.put("two4", twoList[4]);
		map.put("two5", twoList[5]);
		
		//周三   一天
		String three = recipe.get(2).getContent();
		String[] threeList = three.split("\\|");
		map.put("three1", threeList[1]);
		map.put("three2", threeList[2]);
		map.put("three3", threeList[3]);
		map.put("three4", threeList[4]);
		map.put("three5", threeList[5]);
		
		//周四   一天
		String four = recipe.get(3).getContent();
		String[] fourList = four.split("\\|");
		map.put("four1", fourList[1]);
		map.put("four2", fourList[2]);
		map.put("four3", fourList[3]);
		map.put("four4", fourList[4]);
		map.put("four5", fourList[5]);
		
		//周五   一天
		String five = recipe.get(4).getContent();
		String[] fiveList = five.split("\\|");
		map.put("five1", fiveList[1]);
		map.put("five2", fiveList[2]);
		map.put("five3", fiveList[3]);
		map.put("five4", fiveList[4]);
		map.put("five5", fiveList[5]);
		

		map.put("code", "100");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
	/**
	 * 编辑 查看 园区 被选中
	 */
	@RequestMapping(value = "recipe/bjSchool", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String bjSchool(HttpServletRequest request) {
		String recipeId = request.getParameter("recipeId");
		List<RecipeSchool> recipeSchool = recipeManager.getRecipeSchoolById(recipeId);
		List list = new ArrayList();
		for (int i = 0; i < recipeSchool.size(); i++) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("schoolId", recipeSchool.get(i).getSchool().getSchoolId());	
			map.put("schoolName", recipeSchool.get(i).getSchool().getSchoolName());
			list.add(map);
		}

		String resStr = JsonUtils.fromArrayObject(list);
		return resStr;
	}
	
	
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/recipe/getWeek", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getWeek(HttpServletRequest request,HttpServletResponse response) {
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		//String userId = userSession.userId;
		String year = request.getParameter("year");
		String month = request.getParameter("month");
		String day = request.getParameter("day");
		Date date = DateUtils.parseStrToDate(DateUtils.defaultDatePattern, year+"-"+month+"-"+day+"-");
		//DateUtils.
		
		List listNew = new ArrayList();
		List list = DateUtils.dateToWeek(date);
		for(int i=0; i<list.size(); i++)
		{
			
			String str = list.get(i).toString();
			
			if(i==0)
				str += "(周一)";
			if(i==1)
				str += "(周二)";
			if(i==2)
				str += "(周三)";
			if(i==3)
				str += "(周四)";
			if(i==4)
				str += "(周五)";
			
			listNew.add(str);
		}
//		List list = new ArrayList();
//		list.add("1111");
//		list.add("1111");
//		list.add("1111");
//		list.add("1111");
//		list.add("1111");
		
		Map map = new HashMap();

		map.put("code", 100);
		map.put("array", listNew);
		map.put("msg", "操作成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
	
	
	
	

}
