package com.cosin.web.controller.blue;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.From;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cosin.utils.JPushUtils;
import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.dao.blue.INoticeDao;
import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Classteacher;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Notice;
import com.cosin.web.entity.NoticeSub;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SignNotice;
import com.cosin.web.entity.SignPrice;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUserRole;
import com.cosin.web.service.blue.IContainManager;
import com.cosin.web.service.blue.INoticeManager;
import com.cosin.web.service.blue.ISchoolManager;
import com.cosin.web.service.blue.IStudentManager;

/**
 * 公告管理
 * 
 * @author
 *
 */
@Scope("prototype")
@Controller
public class NoticeController {

	@Autowired
	private INoticeManager noticeManager;
	@Autowired
	private IStudentManager studentManager;
	@Autowired
	private ISchoolManager schoolManager;
	@Autowired
	private IContainManager containManager;
	@Autowired
	private INoticeDao noticeDao;
	/*
	 * 控制跳转 channel页面
	 */
	@RequestMapping(value="/notice/noticeData", produces = "text/html;charset=UTF-8")
	public ModelAndView indexs(HttpServletRequest request,HttpServletResponse ronse){
		ModelAndView mav = new ModelAndView();
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String role = userSession.roleId;
		
		List<Object> listPw = userSession.list;
		
		for (int i = 0; i < listPw.size(); i++) {
			
			if("notice_new".equals(listPw.get(i))){//添加权限
				
				mav.addObject("notice_new", listPw.get(i));
				
			}
			if("notice_caogao".equals(listPw.get(i))){
				
				mav.addObject("notice_caogao", listPw.get(i));
				
			}
			if("notice_chakan".equals(listPw.get(i))){
				
				mav.addObject("notice_chakan", listPw.get(i));
				
			}
			if("notice_chehui".equals(listPw.get(i))){
				
				mav.addObject("notice_chehui", listPw.get(i));
				
			}
			if("notice_del".equals(listPw.get(i))){
				
				mav.addObject("notice_del", listPw.get(i));
				
			}
			if("notice_daochu".equals(listPw.get(i))){
				
				mav.addObject("notice_daochu", listPw.get(i));
				
			}
		}
		mav.setViewName("/notice/notice");
		
		return mav;
	}

	/**
	 * 查询功能 操作 分页功能 操作 搜索功能 操作
	 * 
	 * @param request
	 *            获取前台页面属性
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notice/selectnotice", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectsubject(HttpServletRequest request,
			HttpServletResponse response) {
		String SsName = request.getParameter("SsName");
		Map pageParam = PageUtils.getPageParam(request);
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		SysUserRole sysUserRole = schoolManager.getSysUserRoleByUserId(userId);
		String roleId = sysUserRole.getSysRole().getRoleId();
		SysSubrole subrole = schoolManager.getSysSubRoleByRoleId(roleId);
		
		int num = 0;
		List<Notice> listsubject = new ArrayList<Notice>();
		if("总园".equals(subrole.getRoleLevel())&&sysUserRole.getSysRole().getType()==1){
			
			listsubject = noticeManager.getNoticeLimit(SsName, new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
			int total = noticeManager.getNoticeCount(SsName);
			num = total;
		}
		else{
			
			List<Notice> listsubject1 = noticeManager.getNoticeLimit(SsName, new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
			List<School> schools = containManager.getSchools(userId);
			String classIds="";
			for (int i = 0; i < schools.size(); i++) {
				School school = schools.get(i);
				List<Classes> cls = noticeManager.getClassess(school.getSchoolId());
				for (int j = 0; j < cls.size(); j++) {
					Classes classes = cls.get(j);
					if(j==cls.size()-1){
						classIds = classIds+"'"+classes.getClassesId()+"'";
					}
					else{
						classIds = classIds+"'"+classes.getClassesId()+"'"+",";
					}
				}
			}
			if(!classIds.equals("")){
				List<NoticeSub> list = noticeManager.getNoticeSubLimit(classIds,SsName, new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
				for (int i = 0; i < list.size(); i++) {
					Notice notice = list.get(i).getNotice();
					int find = 0;
					for (int j = 0; j < listsubject1.size(); j++) {
						Notice d = listsubject1.get(j);
						if(d.getNoticeId()==notice.getNoticeId()){
							find = 1;
						}
					}
					if(find==0){
						listsubject.add(notice);
						num++;
					}
				}
			}
			for  ( int  i  =   0 ; i  <  listsubject.size()  -   1 ; i ++ )   { 
				for  ( int  j  =  listsubject.size()  -   1 ; j  >  i; j -- )   { 
					if  (listsubject.get(j).equals(listsubject.get(i)))   { 
						listsubject.remove(j); 
					} 
				} 
			}
		}
		List list = new ArrayList();
		for (int i = 0; i < listsubject.size(); i++) {
			Notice vcsubject = listsubject.get(i);
			Map map = new HashMap();
			map.put("noticeId", vcsubject.getNoticeId());
			map.put("title", vcsubject.getTitle());
			map.put("content", vcsubject.getContent());
			map.put("createDate", vcsubject.getCreateDate());

			// 类型
			//map.put("type", vcsubject.getType());
			int typet =  vcsubject.getType();
			if(typet==1){
				map.put("typett","报名");
			}else if(typet==2){
				map.put("typett","支付");
			}else{
				map.put("typett","仅查看");
			}
			
			// 发布人
			map.put("createName", vcsubject.getSysUser().getUserName());
			// 查看人数
			map.put("lookNum", vcsubject.getLookNum());

			list.add(map);
		}
		
		Map mapRet = PageUtils.getPageParam(list, num);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}

	/**
	 * 草稿箱
	 * 
	 * @param request
	 * @return 
	 */
	@RequestMapping(value = "notice/draftData")
	public ModelAndView draft(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/notice/draft");
		// modelAndView.addObject("data", listMainMenu);
		return modelAndView;
	}

	/**
	 * 草稿箱 列表查询
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notice/selectdraft", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectDraft(HttpServletRequest request,
			HttpServletResponse response) {
		String SsName = request.getParameter("SsName");
		Map pageParam = PageUtils.getPageParam(request);
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		SysUserRole sysUserRole = schoolManager.getSysUserRoleByUserId(userId);
		String roleId = sysUserRole.getSysRole().getRoleId();
		SysSubrole subrole = schoolManager.getSysSubRoleByRoleId(roleId);
		
		int num = 0;
		List<Notice> listsubject = new ArrayList<Notice>();
		if("总园".equals(subrole.getRoleLevel())&&sysUserRole.getSysRole().getType()==1){
			
			listsubject = noticeManager.getDraftLimit(SsName, new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
			int total = noticeManager.getNoticeDraftCount(SsName);
			num = total;
		}
		else{
			List<Notice> listsubject1 = noticeManager.getDraftLimit(SsName, new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
			List<School> schools = containManager.getSchools(userId);
			String classIds="";
			for (int i = 0; i < schools.size(); i++) {
				School school = schools.get(i);
				List<Classes> cls = noticeManager.getClassess(school.getSchoolId());
				for (int j = 0; j < cls.size(); j++) {
					Classes classes = cls.get(j);
					if(j==cls.size()-1){
						classIds = classIds+"'"+classes.getClassesId()+"'";
					}
					else{
						classIds = classIds+"'"+classes.getClassesId()+"'"+",";
					}
				}
				

			}
			if(!classIds.equals("")){
				List<NoticeSub> list = noticeManager.getNoticeSubDraftLimit(classIds,SsName, new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
				for (int i = 0; i < list.size(); i++) {
					Notice notice = list.get(i).getNotice();
					int find = 0;
					for (int j = 0; j < listsubject1.size(); j++) {
						Notice d = listsubject1.get(j);
						if(d.getNoticeId()==notice.getNoticeId()){
							find = 1;
						}
					}
					if(find==0){
						listsubject.add(notice);
						num++;
					}
				}
			}
			for  ( int  i  =   0 ; i  <  listsubject.size()  -   1 ; i ++ )   { 
				for  ( int  j  =  listsubject.size()  -   1 ; j  >  i; j -- )   { 
					if  (listsubject.get(j).equals(listsubject.get(i)))   { 
						listsubject.remove(j); 
					} 
				} 
			}
		}
		
		
		List list = new ArrayList();
		for (int i = 0; i < listsubject.size(); i++) {
			Notice vcsubject = listsubject.get(i);
			Map map = new HashMap();
			map.put("noticeId", vcsubject.getNoticeId());
			// 标题
			map.put("title", vcsubject.getTitle());
			// 最后编辑时间、（这个最后需要更改）
			map.put("createDate", vcsubject.getCreateDate());

			// 操作者
			map.put("createName", vcsubject.getSysUser().getUserName());
			// 类型
			int typet =  vcsubject.getType();
			if(typet==1){
				map.put("typett","报名");
			}else if(typet==2){
				map.put("typett","支付");
			}else{
				map.put("typett","仅查看");
			}
			

			list.add(map);
		}
		
		Map mapRet = PageUtils.getPageParam(list, num);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}


	/**
	 * 删除功能 操作
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notice/delNotice", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String delsubject(HttpServletRequest request,
			HttpServletResponse response) {


		String deleteKeys = request.getParameter("delKeys");
		String[] keyArr = deleteKeys.split(",");
		for (int i = 0; i < keyArr.length; i++) {
			String key = keyArr[i];
			if (!"".equals(key) && key != null) {

				Notice notice = noticeManager.getNoticeId(key);
				noticeManager.noticeDelId(key);
			}

		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "删除成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}

	
	/**
	 * 撤回操作
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notice/rollNotice", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String rollBack(HttpServletRequest request,HttpServletResponse response) {
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		String noticeId = request.getParameter("noticeId");
		Map map = new HashMap();

		
		noticeManager.rollNotice(noticeId, userId);
		
		map.put("code", 100);
		map.put("msg", "操作成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	

	/**
	 *  保存功能
	 *  操作 提交数据 传送到后台 
	 *  添加功能 操作
	 *  下拉框
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/subject/savesubject", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String adddsubject(HttpServletRequest request,HttpServletResponse response) {
		
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		//发布人
		String user = userSession.userName;
		String userId = userSession.userId;
		String noticeId = request.getParameter("noticeId");
		// 标题
		String ntitle = request.getParameter("ntitle");
		//图片路径
		String filePath = request.getParameter("filePath");
		// 正文
		String txtContent = request.getParameter("txtContent");
		
		 String str1 = txtContent.replaceAll("src=\"/FileManagerService/","src=\"http://114.215.132.161:8083/FileManagerService/");
		
		//类型
		String ntype = request.getParameter("ntype");
		//通知对象  所有园区
		String data = request.getParameter("data");
		//判断 是否 草稿
		String draf = request.getParameter("draf");
		//添加或存稿
		String mode = request.getParameter("mode");
		//支付
		String payBill = request.getParameter("priceList");
		
		noticeManager.saveNotice(payBill,noticeId,userId,ntitle,filePath,txtContent,ntype,mode,data,draf);
		
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}


	/*
	 * 操作功能 单个删除
	 */
	@RequestMapping(value = "/notice/deleteNotice", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String del(HttpServletRequest request, HttpServletResponse response) {


		String deleteKeys = request.getParameter("delKeys");

		Notice notice = noticeManager.getNoticeId(deleteKeys);

		noticeManager.noticeDelId(deleteKeys);

		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "删除成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}

	/**
	 * 修改功能 操作
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notice/updateNotice", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String updateNotice(HttpServletRequest request,
			HttpServletResponse response) {
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");;
		String userId = userSession.userId;
		String mode = request.getParameter("mode");
		String noticeId = request.getParameter("noticeId");
		Notice notice = noticeManager.getNoticeId(noticeId);
		Map map = new HashMap();
		map.put("title", notice.getTitle());
		map.put("content", notice.getContent());
		map.put("img", notice.getImg());
		map.put("noticeId", notice.getNoticeId());
		map.put("type", notice.getType());
		
		List<Object> list = new ArrayList<Object>();
		if(notice.getPrice() != null){
			
			String[] priceList = notice.getPrice().split("\\|");
			for (int i = 0; i < priceList.length; i++) {
				Map map2 = new HashMap();
				map2.put("price", priceList[i]);
				list.add(map2);
				map.put("price", list);
			}
		}
		
		map.put("code", 100);
		map.put("msg", "操作成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}

	/**
	 * noticeEditTree
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notice/noticeTree", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String noticeTree(HttpServletRequest request,
			HttpServletResponse response) {
		List<Object> listEvery = new ArrayList<Object>();// 新new
		// 一个list集合,用来放map数据;
		String id = request.getParameter("id");
		
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		SysUserRole sysUserRole = schoolManager.getSysUserRoleByUserId(userId);
		String roleId = sysUserRole.getSysRole().getRoleId();
		SysSubrole subrole = schoolManager.getSysSubRoleByRoleId(roleId);
		String roleLevel = subrole.getRoleLevel();
		if(roleLevel.equals("班级")){
			Map<String, Object> map = new HashMap<String, Object>();
			
			Employee employee = noticeDao.getEmployee(userId);
			List<Object> list = new ArrayList<Object>();
			List<Classes> classesList = noticeDao.getClassessById(employee.getEmployeeId());
			if(classesList.size() > 0){
				map.put("id", "0");
				map.put("text", "所有班级");
			}else{
				map.put("id", "0");
				map.put("text", "(空)");
			}
			if(classesList.size()==0)
			{
				List<Classteacher> listc = noticeDao.getClassteacherList(employee.getEmployeeId());
				
				for (int i = 0; i < listc.size(); i++) {
					Classes classes = listc.get(i).getClasses();
					Map<String, Object> mapEveryData = new HashMap<String, Object>();
					mapEveryData.put("id", classes.getClassesId());
					mapEveryData.put("text", classes.getClassesName());
					list.add(mapEveryData);
				}
				map.put("children", list);
				listEvery.add(map);
			}
			else{
				for (int i = 0; i < classesList.size(); i++) {
					Classes classes = classesList.get(i);
					Map<String, Object> mapEveryData = new HashMap<String, Object>();
					mapEveryData.put("id", classes.getClassesId());
					mapEveryData.put("text", classes.getClassesName());
					list.add(mapEveryData);
				}
				map.put("children", list);
				listEvery.add(map);
			}
			
		}
		else if(roleLevel.equals("年级")){
			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("id", "0");
//			map.put("text", "所有班级");
			List<Object> list = new ArrayList<Object>();
//			List<Classes> classesList = noticeDao.getClassessByGrade(subrole.getSysRole().getGrade());
			Employee employee = noticeDao.getEmployee(userId);
			
			if(employee.getGrade()!=null){
				
				List<Classes> classesList = noticeDao.getClassessByGrade(employee.getGrade(),employee.getSchool().getSchoolId());
				if(classesList.size() > 0){
					map.put("id", "0");
					map.put("text", "所有班级");
				}else{
					map.put("id", "0");
					map.put("text", "(空)");
				}
				for (int i = 0; i < classesList.size(); i++) {
					Classes classes = classesList.get(i);
					Map<String, Object> mapEveryData = new HashMap<String, Object>();
					mapEveryData.put("id", classes.getClassesId());
					mapEveryData.put("text", classes.getClassesName());
					list.add(mapEveryData);
				}
			}
			map.put("children", list);
			listEvery.add(map);
		}
		else if(roleLevel.equals("分园")){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", "0");
			map.put("text", "所有年级");
			List<Object> list = new ArrayList<Object>();
			Employee employee = noticeDao.getEmployee(userId);
			for (int i = 0; i < 5; i++) {
				Map<String, Object> mapEveryData = new HashMap<String, Object>();
				mapEveryData.put("id", i+1);
				if(i==0){
					mapEveryData.put("text", "托班");
					List<Classes> classesList = noticeDao.getClassByGrade(i+1,employee.getSchool().getSchoolId());
					List childrenClass = new ArrayList();
					for (int j = 0; j < classesList.size(); j++) {
						Classes lc = classesList.get(j);
						Map classMap = new HashMap();
						classMap.put("id", lc.getClassesId());
						classMap.put("text", lc.getClassesName());
						childrenClass.add(classMap);
					}
					mapEveryData.put("children", childrenClass);
				}else if(i==1){
					mapEveryData.put("text", "小班");
					List<Classes> classesList = noticeDao.getClassByGrade(i+1,employee.getSchool().getSchoolId());
					List childrenClass = new ArrayList();
					for (int j = 0; j < classesList.size(); j++) {
						Classes lc = classesList.get(j);
						Map classMap = new HashMap();
						classMap.put("id", lc.getClassesId());
						classMap.put("text", lc.getClassesName());
						childrenClass.add(classMap);
					}
					mapEveryData.put("children", childrenClass);
				}else if(i==2){
					mapEveryData.put("text", "中班");
					List<Classes> classesList = noticeDao.getClassByGrade(i+1,employee.getSchool().getSchoolId());
					List childrenClass = new ArrayList();
					for (int j = 0; j < classesList.size(); j++) {
						Classes lc = classesList.get(j);
						Map classMap = new HashMap();
						classMap.put("id", lc.getClassesId());
						classMap.put("text", lc.getClassesName());
						childrenClass.add(classMap);
					}
					mapEveryData.put("children", childrenClass);
				}else if(i==3){
					mapEveryData.put("text", "大班");
					List<Classes> classesList = noticeDao.getClassByGrade(i+1,employee.getSchool().getSchoolId());
					List childrenClass = new ArrayList();
					for (int j = 0; j < classesList.size(); j++) {
						Classes lc = classesList.get(j);
						Map classMap = new HashMap();
						classMap.put("id", lc.getClassesId());
						classMap.put("text", lc.getClassesName());
						childrenClass.add(classMap);
					}
					mapEveryData.put("children", childrenClass);
				}else if(i==4){
					mapEveryData.put("text", "学前班");
					List<Classes> classesList = noticeDao.getClassByGrade(i+1,employee.getSchool().getSchoolId());
					List childrenClass = new ArrayList();
					for (int j = 0; j < classesList.size(); j++) {
						Classes lc = classesList.get(j);
						Map classMap = new HashMap();
						classMap.put("id", lc.getClassesId());
						classMap.put("text", lc.getClassesName());
						childrenClass.add(classMap);
					}
					mapEveryData.put("children", childrenClass);
				}
				list.add(mapEveryData);
				
				for (int j = 0; j < list.size(); j++) {
					Map map1 = (Map) list.get(j);
					List list1 = (List) map1.get("children");
					if(list1.size()==0){
						list.remove(j);
					}
				}
				
			}
			
			map.put("children", list);
			listEvery.add(map);
		}
		else{
			if (id == null){
				id="0";
				List<Object> listNotice = noticeManager.getNoticeTree(id);//
				
				Map<String, Object> map = new HashMap<String, Object>();
				
				map.put("id", "0");
				map.put("text", "所有园区");
				List<Object> list = new ArrayList<Object>();
				for (int i = 0; i < listNotice.size(); i++) {
					
					Object[] obj = (Object[]) listNotice.get(i);
					Map<String, Object> mapEveryData = new HashMap<String, Object>();
					mapEveryData.put("id", obj[0]);
					mapEveryData.put("state", "closed");
					mapEveryData.put("text", obj[1]);
					List listSub = getListClasses(obj[0]);//年级
					if(listSub.size() > 0)
					{
						mapEveryData.put("children", listSub);
					}
					list.add(mapEveryData);
					
				}
				
				map.put("children", list);
				listEvery.add(map);
				
			}
		}
		
		String str = JsonUtils.fromArrayObject(listEvery);
		return str;
	}

	public List<Object> getListClasses(Object id) {//年级查询
		
		List<Object> listEvery = new ArrayList<Object>();
		List<Object> listNotice = noticeManager.getListClas(id);
		for (int i = 0; i < listNotice.size(); i++) {
			String nianJi= "";
			Object[] obj = (Object[]) listNotice.get(i);
			Map<String, Object> mapEveryData = new HashMap<String, Object>();
			mapEveryData.put("id", obj[0]);
			if("1".equals(obj[2])){
				nianJi = "托班";
			}else if("2".equals(obj[2])){
				nianJi = "小班";
			}else if("3".equals(obj[2])){
				nianJi = "中班";
			}else if("4".equals(obj[2])){
				nianJi = "大班";
			}else if("5".equals(obj[2])){
				nianJi = "学前班";		
			}
			mapEveryData.put("text", nianJi);
			mapEveryData.put("state", "closed");
			List listTh = getListCla(obj[2],obj[3]);//班级
			if(listTh.size() > 0)
			{
				mapEveryData.put("children", listTh);
			}
			listEvery.add(mapEveryData);
		}

		return listEvery;
	}
	public List<Object> getListCla(Object id,Object schoolId) {//班级查询
		
		List<Object> listEvery = new ArrayList<Object>();
		List<Classes> listNotice = noticeManager.getClass(id,schoolId);
		for (int i = 0; i < listNotice.size(); i++) {
			Classes classes = listNotice.get(i);
			Map<String, Object> mapEveryData = new HashMap<String, Object>();
			mapEveryData.put("id", classes.getClassesId());
			mapEveryData.put("text", classes.getClassesName());
			listEvery.add(mapEveryData);
		}
		return listEvery;
	}
	
	
	
	//编辑数据获取
	
	@RequestMapping(value = "notice/noticeEditTree", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String noticeEditTree(HttpServletRequest request,HttpServletResponse response) {
		List<Object> listEvery = new ArrayList<Object>();// 新new
		// 一个list集合,用来放map数据;
		String id = request.getParameter("id");
		String noticeId = request.getParameter("noticeId");
		
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		SysUserRole sysUserRole = schoolManager.getSysUserRoleByUserId(userId);
		String roleId = sysUserRole.getSysRole().getRoleId();
		SysSubrole subrole = schoolManager.getSysSubRoleByRoleId(roleId);
		String roleLevel = subrole.getRoleLevel();
		if(roleLevel.equals("班级")){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", "0");
			map.put("text", "所有班级");
			Employee employee = noticeDao.getEmployee(userId);
			List<Object> list = new ArrayList<Object>();
			List<Classes> classesList = noticeDao.getClassessById(employee.getEmployeeId());
			if(classesList.size()==0)
			{
				List<Classteacher> listc = noticeDao.getClassteacherList(employee.getEmployeeId());
				for (int i = 0; i < listc.size(); i++) {
					Classes classes = listc.get(i).getClasses();
					Map<String, Object> mapEveryData = new HashMap<String, Object>();
					mapEveryData.put("id", classes.getClassesId());
					mapEveryData.put("text", classes.getClassesName());
					list.add(mapEveryData);
				}
				map.put("children", list);
				listEvery.add(map);
			}
			else{
				for (int i = 0; i < classesList.size(); i++) {
					Classes classes = classesList.get(i);
					Map<String, Object> mapEveryData = new HashMap<String, Object>();
					mapEveryData.put("id", classes.getClassesId());
					mapEveryData.put("text", classes.getClassesName());
					List<NoticeSub> noticeSubList = noticeDao.getNoticeByNoticeId(noticeId);
					for (int k = 0; k < noticeSubList.size(); k++) {
						NoticeSub noticeSub = noticeSubList.get(k);
						String classIdd = noticeSub.getClasses().getClassesId();
						String sss = classes.getClassesId();
						if(sss.equals(classIdd)){
							mapEveryData.put("checked", "true");
						}
					}
					list.add(mapEveryData);
				}
				map.put("children", list);
				listEvery.add(map);
			}
			
		}
		else if(roleLevel.equals("年级")){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", "0");
			map.put("text", "所有班级");
			List<Object> list = new ArrayList<Object>();
//			List<Classes> classesList = noticeDao.getClassessByGrade(subrole.getSysRole().getGrade());
			Employee employee = noticeDao.getEmployee(userId);
			
			if(employee.getGrade()!=null){
				
				List<Classes> classesList = noticeDao.getClassessByGrade(employee.getGrade(),employee.getSchool().getSchoolId());
				for (int i = 0; i < classesList.size(); i++) {
					Classes classes = classesList.get(i);
					Map<String, Object> mapEveryData = new HashMap<String, Object>();
					mapEveryData.put("id", classes.getClassesId());
					mapEveryData.put("text", classes.getClassesName());
					List<NoticeSub> noticeSubList = noticeDao.getNoticeByNoticeId(noticeId);
					for (int k = 0; k < noticeSubList.size(); k++) {
						NoticeSub noticeSub = noticeSubList.get(k);
						String classIdd = noticeSub.getClasses().getClassesId();
						String sss = classes.getClassesId();
						if(sss.equals(classIdd)){
							mapEveryData.put("checked", "true");
						}
					}
					list.add(mapEveryData);
				}
			}
			map.put("children", list);
			listEvery.add(map);
		}
		else if(roleLevel.equals("分园")){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", "0");
			map.put("text", "所有年级");
			List<Object> list = new ArrayList<Object>();
			Employee employee = noticeDao.getEmployee(userId);
			for (int i = 0; i < 5; i++) {
				Map<String, Object> mapEveryData = new HashMap<String, Object>();
				mapEveryData.put("id", i+1);
				if(i==0){
					mapEveryData.put("text", "托班");
					List<Classes> classesList = noticeDao.getClassByGrade(i+1,employee.getSchool().getSchoolId());
					List childrenClass = new ArrayList();
					
					List<NoticeSub> noticeSubList = noticeDao.getNoticeByNoticeId(noticeId);
					for (int j = 0; j < classesList.size(); j++) {
						Classes lc = classesList.get(j);
						Map classMap = new HashMap();
						for (int k = 0; k < noticeSubList.size(); k++) {
							NoticeSub noticeSub = noticeSubList.get(k);
							String classIdd = noticeSub.getClasses().getClassesId();
							String sss = lc.getClassesId();
							if(sss.equals(classIdd)){
								classMap.put("checked", "true");
							}
						}
						classMap.put("id", lc.getClassesId());
						classMap.put("text", lc.getClassesName());
						childrenClass.add(classMap);
					}
					mapEveryData.put("children", childrenClass);
				}else if(i==1){
					mapEveryData.put("text", "小班");
					List<Classes> classesList = noticeDao.getClassByGrade(i+1,employee.getSchool().getSchoolId());
					List childrenClass = new ArrayList();
					for (int j = 0; j < classesList.size(); j++) {
						Classes lc = classesList.get(j);
						Map classMap = new HashMap();
						List<NoticeSub> noticeSubList = noticeDao.getNoticeByNoticeId(noticeId);
						for (int k = 0; k < noticeSubList.size(); k++) {
							NoticeSub noticeSub = noticeSubList.get(k);
							String classIdd = noticeSub.getClasses().getClassesId();
							String sss = lc.getClassesId();
							if(sss.equals(classIdd)){
								classMap.put("checked", "true");
							}
						}
						classMap.put("id", lc.getClassesId());
						classMap.put("text", lc.getClassesName());
						childrenClass.add(classMap);
					}
					mapEveryData.put("children", childrenClass);
				}else if(i==2){
					mapEveryData.put("text", "中班");
					List<Classes> classesList = noticeDao.getClassByGrade(i+1,employee.getSchool().getSchoolId());
					List childrenClass = new ArrayList();
					for (int j = 0; j < classesList.size(); j++) {
						Classes lc = classesList.get(j);
						Map classMap = new HashMap();
						List<NoticeSub> noticeSubList = noticeDao.getNoticeByNoticeId(noticeId);
						for (int k = 0; k < noticeSubList.size(); k++) {
							NoticeSub noticeSub = noticeSubList.get(k);
							String classIdd = noticeSub.getClasses().getClassesId();
							String sss = lc.getClassesId();
							if(sss.equals(classIdd)){
								classMap.put("checked", "true");
							}
						}
						classMap.put("id", lc.getClassesId());
						classMap.put("text", lc.getClassesName());
						childrenClass.add(classMap);
					}
					mapEveryData.put("children", childrenClass);
				}else if(i==3){
					mapEveryData.put("text", "大班");
					List<Classes> classesList = noticeDao.getClassByGrade(i+1,employee.getSchool().getSchoolId());
					List childrenClass = new ArrayList();
					for (int j = 0; j < classesList.size(); j++) {
						Classes lc = classesList.get(j);
						Map classMap = new HashMap();
						List<NoticeSub> noticeSubList = noticeDao.getNoticeByNoticeId(noticeId);
						for (int k = 0; k < noticeSubList.size(); k++) {
							NoticeSub noticeSub = noticeSubList.get(k);
							String classIdd = noticeSub.getClasses().getClassesId();
							String sss = lc.getClassesId();
							if(sss.equals(classIdd)){
								classMap.put("checked", "true");
							}
						}
						classMap.put("id", lc.getClassesId());
						classMap.put("text", lc.getClassesName());
						childrenClass.add(classMap);
					}
					mapEveryData.put("children", childrenClass);
				}else if(i==4){
					mapEveryData.put("text", "学前班");
					List<Classes> classesList = noticeDao.getClassByGrade(i+1,employee.getSchool().getSchoolId());
					List childrenClass = new ArrayList();
					for (int j = 0; j < classesList.size(); j++) {
						Classes lc = classesList.get(j);
						Map classMap = new HashMap();
						List<NoticeSub> noticeSubList = noticeDao.getNoticeByNoticeId(noticeId);
						for (int k = 0; k < noticeSubList.size(); k++) {
							NoticeSub noticeSub = noticeSubList.get(k);
							String classIdd = noticeSub.getClasses().getClassesId();
							String sss = lc.getClassesId();
							if(sss.equals(classIdd)){
								classMap.put("checked", "true");
							}
						}
						classMap.put("id", lc.getClassesId());
						classMap.put("text", lc.getClassesName());
						childrenClass.add(classMap);
					}
					mapEveryData.put("children", childrenClass);
				}
				list.add(mapEveryData);
				
				for (int j = 0; j < list.size(); j++) {
					Map map1 = (Map) list.get(j);
					List list1 = (List) map1.get("children");
					if(list1.size()==0){
						list.remove(j);
					}
				}
				
			}
			
			map.put("children", list);
			listEvery.add(map);
		}
		else{
			if (id == null){
				id="0";
				List<Object> listNotice = noticeManager.getNoticeTree(id);//
				
				Map<String, Object> map = new HashMap<String, Object>();
				
				map.put("id", "0");
				map.put("text", "所有园区");
				List<Object> list = new ArrayList<Object>();
				for (int i = 0; i < listNotice.size(); i++) {
					
					Object[] obj = (Object[]) listNotice.get(i);
					Map<String, Object> mapEveryData = new HashMap<String, Object>();
					mapEveryData.put("id", obj[0]);
					mapEveryData.put("state", "closed");
					mapEveryData.put("text", obj[1]);
					
					List listSub = getListEditClasses(obj[0],noticeId);//年级
					if(listSub.size() > 0)
					{
						mapEveryData.put("children", listSub);
					}
					list.add(mapEveryData);
					
				}
				
				map.put("children", list);
				listEvery.add(map);
				
			}
		}
		
		String str = JsonUtils.fromArrayObject(listEvery);
		return str;
	}
	
	/*@RequestMapping(value = "/notice/noticeEditTree", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String noticeEditTree(HttpServletRequest request,
			HttpServletResponse response) {
		
		String id = request.getParameter("id");
		String noticeId = request.getParameter("noticeId");
		if (id == null)
			id = "0";
		List<Object> listNotice = noticeManager.getNoticeTree(id);//
		List<Object> listEvery = new ArrayList<Object>();// 新new
															// 一个list集合,用来放map数据;
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("id", "0");
		map.put("text", "所有园区");
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < listNotice.size(); i++) {
			
			Object[] obj = (Object[]) listNotice.get(i);
			Map<String, Object> mapEveryData = new HashMap<String, Object>();
			mapEveryData.put("id", obj[0]);
			mapEveryData.put("state", "closed");
			mapEveryData.put("text", obj[1]);
			List listSub = getListEditClasses(obj[0],noticeId);//年级
			if(listSub.size() > 0)
			{
				mapEveryData.put("children", listSub);
			}
			list.add(mapEveryData);
		}
		
		map.put("children", list);
		listEvery.add(map);
		String str = JsonUtils.fromArrayObject(listEvery);
		return str;
	}*/

	
	public List<Object> getListEditClasses(Object id, String noticeId) {//年级查询
		
		List<Object> listEvery = new ArrayList<Object>();
		List<Object> listNotice = noticeManager.getListClas(id);
		List<NoticeSub> noticeSubList = noticeManager.getNoticeSubByNoticeId(noticeId);

		for (int i = 0; i < listNotice.size(); i++) {
			String nianJi= "";
			Object[] obj = (Object[]) listNotice.get(i);
			Map<String, Object> mapEveryData = new HashMap<String, Object>();
			mapEveryData.put("id", obj[0]);
			if("1".equals(obj[2])){
				nianJi = "托班";
			}else if("2".equals(obj[2])){
				nianJi = "小班";
			}else if("3".equals(obj[2])){
				nianJi = "中班";
			}else if("4".equals(obj[2])){
				nianJi = "大班";
			}else if("5".equals(obj[2])){
				nianJi = "学前班";		
			}
			mapEveryData.put("text", nianJi);
			mapEveryData.put("state", "closed");
			List listTh = getListEditCla(obj[2],obj[3]);//班级
			if(listTh.size() > 0)
			{
				mapEveryData.put("children", listTh);
			}
			listEvery.add(mapEveryData);
			for (int j = 0; j < noticeSubList.size(); j++) {
				NoticeSub noticeSub = noticeSubList.get(j);
				String classesId = noticeSub.getClasses().getClassesId();
				if(classesId.equals(obj[0]));{
					mapEveryData.put("checked", "true");
				}
			}
		}
		
		for  ( int  ii  =   0 ; ii  <  listEvery.size()  -   1 ; ii ++ )   { 
		    for  ( int  jj  =  listEvery.size()  -   1 ; jj  >  ii; jj -- )   { 
		      if  (listEvery.get(jj).equals(listEvery.get(ii)))   { 
		    	  listEvery.remove(jj); 
		      } 
		    } 
		  }
		return listEvery;
	}
	
	public List<Object> getListEditCla(Object id,Object schoolId) {//班级查询
		
		List<Object> listEvery = new ArrayList<Object>();
		List<Classes> listNotice = noticeManager.getClass(id,schoolId);
		for (int i = 0; i < listNotice.size(); i++) {
			Classes classes = listNotice.get(i);
			Map<String, Object> mapEveryData = new HashMap<String, Object>();
			mapEveryData.put("id", classes.getClassesId());
			mapEveryData.put("text", classes.getClassesName());
			
			listEvery.add(mapEveryData);
		}
		return listEvery;
	}
	
	
	/**
	 * 判断对象是否为空
	 * @param object
	 * @return
	 */
	public static boolean isNull(Object object){
        if(null == object){
        return true;
    }
    return false;
	}
	
	
	/**
	 *   查看
	 */
	@RequestMapping(value = "notice/noticeLook", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String noticeLook(HttpServletRequest request,HttpServletResponse response) {
		String noticeId = request.getParameter("noticeId");
		
		Notice notice = noticeManager.getNoticeId(noticeId);
		
		int num = 0;
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		//标题
		map.put("title", notice.getTitle());
		//图片
		map.put("img", notice.getImg());
		//正文
		map.put("content", notice.getContent());
		//发布人
		map.put("createMan", notice.getSysUser().getUserName());
		//发布时间
		map.put("createTime", notice.getCreateDate());
		//通知人数
		//职工数量 普通老师 园长 管理员……
		List<NoticeSub> noticeSubList = noticeManager.getNoticeSubByNoticeId(noticeId);
		List listSchool = new ArrayList();
		for (int i = 0; i < noticeSubList.size(); i++) {
			NoticeSub noticeSub = noticeSubList.get(i);
				
			Classes classesId = noticeSub.getClasses();
			
			boolean flag = isNull(classesId);
			if(flag==false){
				String schoolId =noticeSub.getClasses().getSchool().getSchoolId();
				listSchool.add(schoolId);
				
			}
		}
		for  ( int  i  =   0 ; i  <  listSchool.size()  -   1 ; i ++ )   { 
		    for  ( int  j  =  listSchool.size()  -   1 ; j  >  i; j -- )   { 
		      if  (listSchool.get(j).equals(listSchool.get(i)))   { 
		    	  listSchool.remove(j); 
		      } 
		    } 
		  } 
		
		for (int i = 0; i < listSchool.size(); i++) {
			String schoolId = (String) listSchool.get(i);
			List<Classes> classList = noticeDao.getClassBySchoolId(schoolId);
			for (int j = 0; j < classList.size(); j++) {
				String clasId = classList.get(j).getClassesId();
				List<Classteacher> classteacherList = noticeDao.getClassteacherListByClassId(clasId);
				num += classteacherList.size();
			}
//			List<Employee> employeeList = noticeManager.getEmployeeBySchoolId(schoolId);
//					num+=employeeList.size();
		}
		
		//班主任
		num= num+1;
		
		//家长数量
		for (int i = 0; i < noticeSubList.size(); i++) {
			NoticeSub noticeSub = noticeSubList.get(i);
			Classes classes = noticeSub.getClasses();
			boolean flag = isNull(classes);
			if(flag==false){
				
				String classId = classes.getClassesId();
				List<Student> studentList = noticeManager.getStudentListByClassesId(classId);
				for (int j = 0; j < studentList.size(); j++) {
					Student student = studentList.get(j);
					String studentId = student.getStudentId();
					List<Parentsstudent> parentsstudentList = noticeManager.getParentListByStudentId(studentId);
						num+=parentsstudentList.size();
				}
			}
		}
		map.put("peopleNum", num);
		
		
		//通知对象
		//有效园区 通知到班级Classes
		List<NoticeSub> listRoScP = noticeManager.getNoticeSubBynoticeId(noticeId);
		String className="";
		String schoolName = "";
		for (int j = 0; j < listRoScP.size(); j++) {
			NoticeSub noticeSub = listRoScP.get(j);
			Classes classes = noticeSub.getClasses();
			boolean flag = isNull(classes);
			if(flag==false){
				
				String schoolId = classes.getSchool().getSchoolId();
				String grade = noticeSub.getClasses().getGrade();
				String gradeName = null;
				if(grade.equals("1")){
					gradeName = "托班";
				}else if(grade.equals("2")){
					gradeName = "小班";
				}else if(grade.equals("3")){
					gradeName = "中班";
				}else if(grade.equals("4")){
					gradeName = "大班";
				}else if(grade.equals("5")){
					gradeName = "学前班";
				}
				if(schoolName.equals(classes.getSchool().getSchoolName())){
					className+= gradeName + "-" + noticeSub.getClasses().getClassesName()+";";
				}else{
					
					className+="<br/>" + classes.getSchool().getSchoolName()+": "+ gradeName + "-" + noticeSub.getClasses().getClassesName()+";";
				}
				schoolName = classes.getSchool().getSchoolName();
			}
			
		}
		map.put("validClass", className);
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
	/**
	 * 已报名 
	 */
	@RequestMapping(value = "/notice/allReadyType", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String allReadyType(HttpServletRequest request,HttpServletResponse response) {
		String schoolName = request.getParameter("schoolName");
		String grade = request.getParameter("grade");
		String className = request.getParameter("className");
		String noticeId = request.getParameter("noticeId");
		Map pageParam = PageUtils.getPageParam(request);
		List<NoticeSub> listsubject = noticeManager.getNoticeLimit(noticeId,schoolName,grade,className,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		List<SignNotice> signNoticeList = noticeManager.getSignNoticeByNoticeId(noticeId);
		List list = new ArrayList();
		for (int i = 0; i < listsubject.size(); i++) {
			NoticeSub vcsubject = listsubject.get(i);
			Classes classes = vcsubject.getClasses();
			if(classes != null&&!"".equals(classes)){
				String classId = classes.getClassesId();
				Student student = noticeManager.getStudentByClassesId(classId);
				if(student != null&&!"".equals(student)){
					String studentIdSub = student.getStudentId();
					for (int j = 0; j < signNoticeList.size(); j++) {
						SignNotice signNotice = signNoticeList.get(j);
						String studentIdSign = signNotice.getStudent().getStudentId();
						if(studentIdSign.equals(studentIdSub)){
							Map map = new HashMap();
							map.put("noticeSubId", vcsubject.getNiticesubId());
							//园区名 
							map.put("schoolName", vcsubject.getClasses().getSchool().getSchoolName());
							//年级
							String gra = vcsubject.getClasses().getGrade();
							if(gra.equals("1")){
								map.put("grade","托班");
							}else if(gra.equals("2")){
								map.put("grade","小班");
							}else if(gra.equals("3")){
								map.put("grade","中班");
							}else if(gra.equals("4")){
								map.put("grade","大班");
							}else if(gra.equals("5")){
								map.put("grade","学前班");
							}
							//班级 
							map.put("classesName", vcsubject.getClasses().getClassesName());
							//学生姓名
							map.put("studentName", student.getStudentName());
							List<Parentsstudent> parentsstudentList = studentManager.getParentsstudentByStudentId(studentIdSub);
							//家长姓名
							map.put("parentName", student.getParents().getSysUser().getUserName());
							//亲戚关系 
							map.put("relation", parentsstudentList.get(0).getRelativeShip());//有问题
							//联系电话
							map.put("tel", student.getParents().getSysUser().getMobile());
							//报名时间
							map.put("createDate", signNotice.getCreareDate());
							list.add(map);
						}
					}
				}
			}
		}
		int total = noticeManager.getSignNoticeCount(noticeId,schoolName,grade,className);
		Map mapRet = PageUtils.getPageParam(list, total);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	/**
	 * 未报名
	 */
	@RequestMapping(value = "/notice/notReadyType", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String notReadyType(HttpServletRequest request,HttpServletResponse response) {
		Map pageParam = PageUtils.getPageParam(request);
		String noticeId = request.getParameter("noticeId");
		
		String stuId = null;
		int num = 0;
		
		List<SignNotice> signNoticeList = noticeManager.getSignNoticeByNoticeId(noticeId);
		List<NoticeSub> listsubject = noticeManager.getNoticeSubLimit(noticeId,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		
		Map map1 = new HashMap();
		for(int i=0; i<signNoticeList.size(); i++)
		{
			SignNotice signNotice = signNoticeList.get(i);
			map1.put(signNotice.getStudent().getStudentId(), signNotice.getStudent().getStudentId());
		}
		
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < listsubject.size(); i++) {
			NoticeSub vcsubject = listsubject.get(i);
			Classes classes = vcsubject.getClasses();
			if(classes != null&&!"".equals(classes)){
				String classId = classes.getClassesId();
				List<Student> studentList = noticeManager.getStudentListByClassesId(classId);
				for (int j = 0; j < studentList.size(); j++) {
					Student student = studentList.get(j);
					
					if(student != null&&!"".equals(student)){
						String studentIdSub = student.getStudentId();
						stuId = studentIdSub;
						if(map1.containsKey(studentIdSub))
							continue;
							num++;
							Map map = new HashMap();
							map.put("noticeSubId", vcsubject.getNiticesubId());
							//园区名 
							map.put("schoolName", vcsubject.getClasses().getSchool().getSchoolName());
							//班级 
							map.put("classesName", vcsubject.getClasses().getClassesName());
							//学生姓名
							String studentId = student.getStudentId();
							Parentsstudent parentsstudent = noticeManager.getParentsstudentByStudentId(studentId);
							map.put("studentName", student.getStudentName());
							//家长姓名
							map.put("parentName", parentsstudent.getParents().getSysUser().getUserName());
							//亲戚关系 
							map.put("relation", parentsstudent.getRelativeShip());
							//联系电话
							map.put("tel", parentsstudent.getParents().getSysUser().getMobile());
							//年级
							String gra = vcsubject.getClasses().getGrade();
							if(gra.equals("1")){
								map.put("grade","托班");
							}else if(gra.equals("2")){
								map.put("grade","小班");
							}else if(gra.equals("3")){
								map.put("grade","中班");
							}else if(gra.equals("4")){
								map.put("grade","大班");
							}else if(gra.equals("5")){
								map.put("grade","学前班");
							}
							list.add(map);
					}
				}
			}
		}
		int total = noticeManager.getNoticeSubCount(noticeId);
		Map mapRet = PageUtils.getPageParam(list, num);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	
	/**
	 * 已支付 
	 */
	@RequestMapping(value = "/notice/already_price", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String already_price(HttpServletRequest request,HttpServletResponse response) {
		String schoolName = request.getParameter("schoolName");
		String grade = request.getParameter("grade");
		String className = request.getParameter("className");
		String noticeId = request.getParameter("noticeId");
		Map pageParam = PageUtils.getPageParam(request);
		List<SignPrice> signPriceList = noticeManager.getSignPriceLimit(noticeId,schoolName,grade,className,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		List list = new ArrayList();
		for (int i = 0; i < signPriceList.size(); i++) {
			SignPrice vcsubject = signPriceList.get(i);
			Map map = new HashMap();
			int type = vcsubject.getNotice().getType();
			String chaNoticeId = vcsubject.getNotice().getNoticeId();
				//园区名 
				map.put("schoolName", vcsubject.getStudent().getSchool().getSchoolName());
				//年级
				String gra = vcsubject.getStudent().getClasses().getGrade();
				if(gra.equals("1")){
					map.put("grade","托班");
				}else if(gra.equals("2")){
					map.put("grade","小班");
				}else if(gra.equals("3")){
					map.put("grade","中班");
				}else if(gra.equals("4")){
					map.put("grade","大班");
				}else if(gra.equals("5")){
					map.put("grade","学前班");
				}
				//班级 
				map.put("classesName", vcsubject.getStudent().getClasses().getClassesName());
				//学生姓名
					
				map.put("studentName", vcsubject.getStudent().getStudentName());
				String studentId = vcsubject.getStudent().getStudentId();
				Parentsstudent parentsstudent = noticeManager.getParentsstudentByStudentId(studentId);
				//家长姓名
				map.put("parentName", parentsstudent.getParents().getSysUser().getUserName());
				//亲戚关系 
				map.put("relation", parentsstudent.getRelativeShip());
				//联系电话
				map.put("tel", parentsstudent.getParents().getSysUser().getMobile());
				//支付选项
				map.put("type", vcsubject.getPrice());
				
				//报名时间
				map.put("createDate", vcsubject.getCreateTime());
				list.add(map);
		}
		int total = noticeManager.getSignPriceCount(noticeId,schoolName,grade,className);
		Map mapRet = PageUtils.getPageParam(list, total);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	/**
	 * 未支付
	 */
	@RequestMapping(value = "/notice/already_notP", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String already_notP(HttpServletRequest request,HttpServletResponse response) {
		Map pageParam = PageUtils.getPageParam(request);
		String schoolName = request.getParameter("schoolName");
		String grade = request.getParameter("grade");
		String className = request.getParameter("className");
		String noticeId = request.getParameter("noticeId");
		
		List<SignPrice> signPriceList = noticeManager.getNoSignPriceLimit(noticeId,schoolName,grade,className,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < signPriceList.size(); i++) {
			SignPrice vcsubject = signPriceList.get(i);
			Map<Object,Object> map = new HashMap<Object,Object>();
			
			map.put("schoolName", vcsubject.getStudent().getSchool().getSchoolName());
			//班级 
			map.put("classesName", vcsubject.getStudent().getClasses().getClassesName());
			//学生姓名
			Student student = vcsubject.getStudent();
			if(student!=null){
				String studentId = student.getStudentId();
				Parentsstudent parentsstudent = noticeManager.getParentsstudentByStudentId(studentId);
				map.put("studentName", student.getStudentName());
				//家长姓名
				map.put("parentName", parentsstudent.getParents().getSysUser().getUserName());
				//亲戚关系 
				map.put("relation", parentsstudent.getRelativeShip());
				//联系电话
				map.put("tel", parentsstudent.getParents().getSysUser().getMobile());
			}
			//年级
			String gra = vcsubject.getStudent().getClasses().getGrade();
			if(gra.equals("1")){
				map.put("grade","托班");
			}else if(gra.equals("2")){
				map.put("grade","小班");
			}else if(gra.equals("3")){
				map.put("grade","中班");
			}else if(gra.equals("4")){
				map.put("grade","大班");
			}else if(gra.equals("5")){
				map.put("grade","学前班");
			}
			
			list.add(map);
		}
		int total = noticeManager.getNoPriceCount(noticeId,schoolName,grade,className);
		Map mapRet = PageUtils.getPageParam(list, total);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	
	/**
	 * 二级联动
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "notice/listSchool", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getListSchool(HttpServletResponse response,
			HttpServletRequest request) {
		String noticeId = request.getParameter("noticeId");
		
		List<Object> listRes = new ArrayList<Object>();
		List<NoticeSub> NoticeList = noticeManager.getNoticeByNoticeId(noticeId);
		
		
		for (int i = 0; i < NoticeList.size(); i++) {
			Map map = new HashMap();
			NoticeSub noticeSub = NoticeList.get(i);
			Classes classes = noticeSub.getClasses();
			if(classes!=null){
				map.put("noticeId", noticeSub.getNotice().getNoticeId());
				map.put("schoolId", noticeSub.getClasses().getSchool().getSchoolId());
				map.put("schoolName", noticeSub.getClasses().getSchool().getSchoolName());
				listRes.add(map);
			}
		}
		
		for  ( int  i  =   0 ; i  <  listRes.size()  -   1 ; i ++ )   { 
		    for  ( int  j  =  listRes.size()  -   1 ; j  >  i; j -- )   { 
		      if  (listRes.get(j).equals(listRes.get(i)))   { 
		    	  listRes.remove(j); 
		      } 
		    } 
		  } 
	
		String str = JsonUtils.fromArrayObject(listRes);
		return str;
	}
	
	@RequestMapping(value = "notice/listGrade", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getListGrade(HttpServletResponse response,
			HttpServletRequest request) {
		String schoolId = request.getParameter("schoolId");
		List listRes = noticeManager.getListGradeBySchoolId(schoolId);
		String str = JsonUtils.fromArrayObject(listRes);
		return str;
	}
	
	@RequestMapping(value = "notice/listClassName", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getListClassName(HttpServletResponse response,
			HttpServletRequest request) {
		String gradeName = request.getParameter("gradeName");
		List listRes = noticeManager.getListClassNameByGradeName(gradeName);
		String str = JsonUtils.fromArrayObject(listRes);
		return str;
	}
	
	
	
	/**
	 * 导出  已报名
	 * @param response
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value="/notice/daochuNotice",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public void daochuNotice(HttpServletResponse response, HttpServletRequest request) throws IOException{
		
		String schoolName = request.getParameter("schoolName");
		String grade = request.getParameter("grade");
		String className = request.getParameter("className");
		String noticeId = request.getParameter("noticeId");
		Map pageParam = PageUtils.getPageParam(request);
		List<NoticeSub> listsubject = noticeManager.getNoticeLimit(noticeId,schoolName,grade,className,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		List<SignNotice> signNoticeList = noticeManager.getSignNoticeByNoticeId(noticeId);
		List list = new ArrayList();
		for (int i = 0; i < listsubject.size(); i++) {
			NoticeSub vcsubject = listsubject.get(i);
			Classes classes = vcsubject.getClasses();
			if(classes != null&&!"".equals(classes)){
				String classId = classes.getClassesId();
				Student student = noticeManager.getStudentByClassesId(classId);
				if(student != null&&!"".equals(student)){
					String studentIdSub = student.getStudentId();
					for (int j = 0; j < signNoticeList.size(); j++) {
						SignNotice signNotice = signNoticeList.get(j);
						String studentIdSign = signNotice.getStudent().getStudentId();
						if(studentIdSign.equals(studentIdSub)){
							Map map = new HashMap();
							map.put("noticeSubId", vcsubject.getNiticesubId());
							//园区名 
							map.put("schoolName", vcsubject.getClasses().getSchool().getSchoolName());
							//年级
							String gra = vcsubject.getClasses().getGrade();
							if(gra.equals("1")){
								map.put("grade","托班");
							}else if(gra.equals("2")){
								map.put("grade","小班");
							}else if(gra.equals("3")){
								map.put("grade","中班");
							}else if(gra.equals("4")){
								map.put("grade","大班");
							}else if(gra.equals("5")){
								map.put("grade","学前班");
							}
							//班级 
							map.put("classesName", vcsubject.getClasses().getClassesName());
							//学生姓名
							map.put("studentName", student.getStudentName());
							List<Parentsstudent> parentsstudentList = studentManager.getParentsstudentByStudentId(studentIdSub);
							//家长姓名
							map.put("parentName", student.getParents().getSysUser().getUserName());
							//亲戚关系 
							map.put("relation", parentsstudentList.get(0).getRelativeShip());
							//联系电话
							map.put("tel", student.getParents().getSysUser().getMobile());
							//报名时间
							map.put("createDate", vcsubject.getNotice().getCreateDate());
							list.add(map);
						}
					}
				}
			}
		}
		//创建一个新的 excel 
        HSSFWorkbook wb = new HSSFWorkbook();
        //创建 sheet 页
        HSSFSheet sheet = wb.createSheet("已报名"); 
        HSSFRow row = sheet.createRow((int) 0);  
        //sheet.setColumnWidth((short) 0, (short) (35.7 * 150));
        //sheet.setColumnWidth((short)1, (short) 600);
       
        //设置水平居中
        HSSFCellStyle style = wb.createCellStyle();  
        
        
        //设置标题居中
        HSSFHeader header = sheet.getHeader();
        header.setCenter("已报名");
        String[] excelHeader = new String[]{"园区名","年级","班级","学生姓名","家长姓名","亲戚关系","联系电话","报名时间"};
        for (int i = 0; i < excelHeader.length; i++) {  
            HSSFCell cell = row.createCell(i);  
            cell.setCellValue(excelHeader[i]);  
            cell.setCellStyle(style);  
            sheet.autoSizeColumn(i);  
        }  
        for (int i = 0; i < list.size(); i++) {  
            row = sheet.createRow(i + 1);  
            Map map = (Map)list.get(i);  
            row.createCell(0).setCellValue(map.get("schoolName").toString());  
            row.createCell(1).setCellValue(map.get("grade").toString());
            row.createCell(2).setCellValue(map.get("classesName").toString());
            row.createCell(3).setCellValue(map.get("studentName").toString());
            row.createCell(4).setCellValue(map.get("parentName").toString());
            row.createCell(5).setCellValue(map.get("relation").toString());
            row.createCell(6).setCellValue(map.get("tel").toString());
            row.createCell(7).setCellValue(map.get("createDate").toString());
        }   
        sheet.autoSizeColumn((short)0);// 列表宽度自定义
        sheet.autoSizeColumn((short)1);// 列表宽度自定义
        sheet.autoSizeColumn((short)2);// 列表宽度自定义
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 居中
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);// 居中
        style.setWrapText(true); 
        response.setContentType("application/vnd.ms-excel");    
        String excelName = "已报名";
        response.setHeader("Content-disposition", "attachment;filename="+new String(excelName.getBytes(), "ISO8859-1")+".xls");    
        OutputStream ouputStream = response.getOutputStream();    
        wb.write(ouputStream);    
        ouputStream.flush();    
        ouputStream.close();  
	}
	
	
	/**
	 * 导出 未支付
	 * @param response
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value="/notice/daochu_notP",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public void daochu_notP(HttpServletResponse response, HttpServletRequest request) throws IOException{
		
		Map pageParam = PageUtils.getPageParam(request);
		String schoolName = request.getParameter("schoolName");
		String grade = request.getParameter("grade");
		String className = request.getParameter("className");
		String noticeId = request.getParameter("noticeId");
		
		List<SignPrice> signPriceList = noticeManager.getNoSignPriceLimit(noticeId,schoolName,grade,className,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < signPriceList.size(); i++) {
			SignPrice vcsubject = signPriceList.get(i);
			Map<Object,Object> map = new HashMap<Object,Object>();
			
			map.put("schoolName", vcsubject.getStudent().getSchool().getSchoolName());
			//班级 
			map.put("classesName", vcsubject.getStudent().getClasses().getClassesName());
			//学生姓名
			Student student = vcsubject.getStudent();
			if(student!=null){
				String studentId = student.getStudentId();
				Parentsstudent parentsstudent = noticeManager.getParentsstudentByStudentId(studentId);
				map.put("studentName", student.getStudentName());
				//家长姓名
				map.put("parentName", parentsstudent.getParents().getSysUser().getUserName());
				//亲戚关系 
				map.put("relation", parentsstudent.getRelativeShip());
				//联系电话
				map.put("tel", parentsstudent.getParents().getSysUser().getMobile());
			}
			//年级
			String gra = vcsubject.getStudent().getClasses().getGrade();
			if(gra.equals("1")){
				map.put("grade","托班");
			}else if(gra.equals("2")){
				map.put("grade","小班");
			}else if(gra.equals("3")){
				map.put("grade","中班");
			}else if(gra.equals("4")){
				map.put("grade","大班");
			}else if(gra.equals("5")){
				map.put("grade","学前班");
			}
			
			list.add(map);
		}
		//创建一个新的 excel 
        HSSFWorkbook wb = new HSSFWorkbook();
        //创建 sheet 页
        HSSFSheet sheet = wb.createSheet("未支付"); 
        HSSFRow row = sheet.createRow((int) 0);  
        //sheet.setColumnWidth((short) 0, (short) (35.7 * 150));
        //sheet.setColumnWidth((short)1, (short) 600);
       
        //设置水平居中
        HSSFCellStyle style = wb.createCellStyle();  
        
        
        //设置标题居中
        HSSFHeader header = sheet.getHeader();
        header.setCenter("未支付");
        String[] excelHeader = new String[]{"园区名","年级","班级","学生姓名","家长姓名","亲戚关系","联系电话"};
        for (int i = 0; i < excelHeader.length; i++) {  
            HSSFCell cell = row.createCell(i);  
            cell.setCellValue(excelHeader[i]);  
            cell.setCellStyle(style);  
            sheet.autoSizeColumn(i);  
        }  
        for (int i = 0; i < list.size(); i++) {  
            row = sheet.createRow(i + 1);  
            Map map = (Map)list.get(i);  
            row.createCell(0).setCellValue(map.get("schoolName").toString());  
            row.createCell(1).setCellValue(map.get("grade").toString());
            row.createCell(2).setCellValue(map.get("classesName").toString());
            row.createCell(3).setCellValue(map.get("studentName").toString());
            row.createCell(4).setCellValue(map.get("parentName").toString());
            row.createCell(5).setCellValue(map.get("relation").toString());
            row.createCell(6).setCellValue(map.get("tel").toString());
            
        }   
        sheet.autoSizeColumn((short)0);// 列表宽度自定义
        sheet.autoSizeColumn((short)1);// 列表宽度自定义
        sheet.autoSizeColumn((short)2);// 列表宽度自定义
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 居中
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);// 居中
        style.setWrapText(true); 
        response.setContentType("application/vnd.ms-excel");    
        String excelName = "未支付";
        response.setHeader("Content-disposition", "attachment;filename="+new String(excelName.getBytes(), "ISO8859-1")+".xls");    
        OutputStream ouputStream = response.getOutputStream();    
        wb.write(ouputStream);    
        ouputStream.flush();    
        ouputStream.close();  
	}
	
	/**
	 * 导出 支付
	 * @param response
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value="/notice/daochu_price",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public void daochu_price(HttpServletResponse response, HttpServletRequest request) throws IOException{
		
		String schoolName = request.getParameter("schoolName");
		String grade = request.getParameter("grade");
		String className = request.getParameter("className");
		String noticeId = request.getParameter("noticeId");
		Map pageParam = PageUtils.getPageParam(request);
		List<SignPrice> signPriceList = noticeManager.getSignPriceLimit(noticeId,schoolName,grade,className,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		List list = new ArrayList();
		for (int i = 0; i < signPriceList.size(); i++) {
			SignPrice vcsubject = signPriceList.get(i);
			Map map = new HashMap();
			int type = vcsubject.getNotice().getType();
			String chaNoticeId = vcsubject.getNotice().getNoticeId();
				//园区名 
				map.put("schoolName", vcsubject.getStudent().getSchool().getSchoolName());
				//年级
				String gra = vcsubject.getStudent().getClasses().getGrade();
				if(gra.equals("1")){
					map.put("grade","托班");
				}else if(gra.equals("2")){
					map.put("grade","小班");
				}else if(gra.equals("3")){
					map.put("grade","中班");
				}else if(gra.equals("4")){
					map.put("grade","大班");
				}else if(gra.equals("5")){
					map.put("grade","学前班");
				}
				//班级 
				map.put("classesName", vcsubject.getStudent().getClasses().getClassesName());
				//学生姓名
					
				map.put("studentName", vcsubject.getStudent().getStudentName());
				String studentId = vcsubject.getStudent().getStudentId();
				Parentsstudent parentsstudent = noticeManager.getParentsstudentByStudentId(studentId);
				//家长姓名
				map.put("parentName", parentsstudent.getParents().getSysUser().getUserName());
				//亲戚关系 
				map.put("relation", parentsstudent.getRelativeShip());
				//联系电话
				map.put("tel", parentsstudent.getParents().getSysUser().getMobile());
				//支付选项
				map.put("type", vcsubject.getPrice());
				
				//支付时间
				map.put("createDate", vcsubject.getCreateTime());
				list.add(map);
		}
		//创建一个新的 excel 
        HSSFWorkbook wb = new HSSFWorkbook();
        //创建 sheet 页
        HSSFSheet sheet = wb.createSheet("已支付"); 
        HSSFRow row = sheet.createRow((int) 0);  
        //sheet.setColumnWidth((short) 0, (short) (35.7 * 150));
        //sheet.setColumnWidth((short)1, (short) 600);
       
        //设置水平居中
        HSSFCellStyle style = wb.createCellStyle();  
        
        
        //设置标题居中
        HSSFHeader header = sheet.getHeader();
        header.setCenter("已支付");
        String[] excelHeader = new String[]{"园区名","年级","班级","学生姓名","家长姓名","亲戚关系","联系电话","支付选项","支付时间"};
        for (int i = 0; i < excelHeader.length; i++) {  
            HSSFCell cell = row.createCell(i);  
            cell.setCellValue(excelHeader[i]);  
            cell.setCellStyle(style);  
            sheet.autoSizeColumn(i);  
        }  
        for (int i = 0; i < list.size(); i++) {  
            row = sheet.createRow(i + 1);  
            Map map = (Map)list.get(i);  
            row.createCell(0).setCellValue(map.get("schoolName").toString());  
            row.createCell(1).setCellValue(map.get("grade").toString());
            row.createCell(2).setCellValue(map.get("classesName").toString());
            row.createCell(3).setCellValue(map.get("studentName").toString());
            row.createCell(4).setCellValue(map.get("parentName").toString());
            row.createCell(5).setCellValue(map.get("relation").toString());
            row.createCell(6).setCellValue(map.get("tel").toString());
            row.createCell(6).setCellValue(map.get("type").toString());
            row.createCell(6).setCellValue(map.get("createDate").toString());
            
        }   
        sheet.autoSizeColumn((short)0);// 列表宽度自定义
        sheet.autoSizeColumn((short)1);// 列表宽度自定义
        sheet.autoSizeColumn((short)2);// 列表宽度自定义
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 居中
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);// 居中
        style.setWrapText(true); 
        response.setContentType("application/vnd.ms-excel");    
        String excelName = "已支付";
        response.setHeader("Content-disposition", "attachment;filename="+new String(excelName.getBytes(), "ISO8859-1")+".xls");    
        OutputStream ouputStream = response.getOutputStream();    
        wb.write(ouputStream);    
        ouputStream.flush();    
        ouputStream.close();  
	}


}
