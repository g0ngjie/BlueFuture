package com.cosin.web.controller.blue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.dao.blue.IContainDao;
import com.cosin.web.entity.Activity;
import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUserRole;
import com.cosin.web.service.blue.IContainManager;
import com.cosin.web.service.blue.ISchoolManager;

@Scope("prototype")
@Controller
public class SchoolController {

	
@Autowired
private ISchoolManager schoolManager;
@Autowired
private IContainManager containManager;
@Autowired
private IContainDao containDao;
	
	/**
	 * 跳转到园区管理界面
	 * 
	 */
	@RequestMapping(value="/school/school", produces = "text/html;charset=UTF-8")
	public ModelAndView school(HttpServletRequest request,HttpServletResponse ronse){
		ModelAndView mav = new ModelAndView();
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String role = userSession.roleId;
		
		List<Object> listPw = userSession.list;
		
		for (int i = 0; i < listPw.size(); i++) {
			
			if("park_add".equals(listPw.get(i))){//添加权限
				
				mav.addObject("park_add", listPw.get(i));
				
			}
			if("park_xiugai".equals(listPw.get(i))){
				
				mav.addObject("park_xiugai", listPw.get(i));
				
			}
			if("park_del".equals(listPw.get(i))){
				
				mav.addObject("park_del", listPw.get(i));
				
			}
		}
		mav.setViewName("/school/school");
		
		return mav;
	}


	@RequestMapping(value = "/school/schoolList", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String schoolListData(HttpServletResponse response,
			HttpServletRequest request) {
		String SsName = request.getParameter("SsName");	
		Map pageParam = PageUtils.getPageParam(request);
		
		int num = 0;
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		SysUserRole sysUserRole = schoolManager.getSysUserRoleByUserId(userId);
		String roleId = sysUserRole.getSysRole().getRoleId();
		SysSubrole subrole = schoolManager.getSysSubRoleByRoleId(roleId);
		List<Employee> list = containDao.getEmployees(userId);
		
		List<School> listsubject = new ArrayList<School>();
		if("总园".equals(subrole.getRoleLevel())&&sysUserRole.getSysRole().getType()==1){
			listsubject = schoolManager.getSchoolList(new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
			int totle = schoolManager.getSchoolfenye(SsName);
			num = totle;
		}
		else{
			List<School> schools = containManager.getSchools(userId);
			String schoolsId="";
			for (int i = 0; i < schools.size(); i++) {
				School school = schools.get(i);
				if(i==schools.size()-1){
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'";
				}
				else{
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'"+",";
				}
			}
			if(!schoolsId.equals("")){
				
				listsubject = schoolManager.getSchoolList(schoolsId,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
				List totle = schoolManager.getSchoolfenyeBySchoolId(schoolsId,SsName);
				num = totle.size();
			}
		}
		
		List listRes = new ArrayList();
		for (int i = 0; i < listsubject.size(); i++) {
			School school = listsubject.get(i);
			Map<String, Object> map = new HashMap<String, Object>();
			// 学校名称	
			map.put("schoolName", school.getSchoolName());
			// 省份名称
			map.put("provinceName", school.getSysAreaByProvinceId().getAreaName());
			// 城市
			map.put("city", school.getSysAreaByCityId().getAreaName());
			// 园长
			Employee emplo = school.getEmployee();
			if(emplo != null){
				
				map.put("employeeName", school.getEmployee().getSysUserByUserId().getUserName());
			}else{
				map.put("employeeName", "无");
			}
			// 添加人
			map.put("createUser", school.getSysUser().getUserName());
			// 添加日期
			map.put("createDate", school.getCreateDate());
			// 捕获园区ID
			map.put("schoolId", school.getSchoolId());
			// 添加人
			map.put("userId", school.getSysUser().getUserId());
			// 区域id
			map.put("cityId", school.getSysAreaByProvinceId().getAreaId());
			listRes.add(map);
		}
		
		Map mapRet = PageUtils.getPageParam(listRes, num);
		String resStr = JsonUtils.fromObject(mapRet);// 
		return resStr;
	}

	/**
	 * 编辑School
	 */
	@RequestMapping(value = "school/bjSchool", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String bjSchool(HttpServletRequest request) {
		String schoolId = request.getParameter("schoolId");
		School userSchool = schoolManager.findbjSchool(schoolId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("schoolName", userSchool.getSchoolName());
		map.put("province", userSchool.getSysAreaByProvinceId().getAreaName());
		map.put("provinceId", userSchool.getSysAreaByProvinceId().getAreaId());

		map.put("city", userSchool.getSysAreaByCityId().getAreaName());
		map.put("cityId", userSchool.getSysAreaByCityId().getAreaId());

		Employee emplo = userSchool.getEmployee();
		if(emplo != null){
			if(emplo.getEnable() == 0){
				
				map.put("employeeName", userSchool.getEmployee().getSysUserByUserId().getUserName());
				map.put("employeeNameId", userSchool.getEmployee().getSysUserByUserId().getUserId());
			}
		}else{
			map.put("employeeName", "");
			map.put("employeeNameId", "");
		}

		map.put("schoolId", userSchool.getSchoolId());
		map.put("enable", userSchool.getEnable());
		map.put("code", "100");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}

	/**
	 * 保存园区管理信息
	 */
	@RequestMapping(value = "/school/saveSchool", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveShop(HttpServletRequest request,
			HttpServletResponse response) {
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		// String ip = userSession.ip;
		// String token = userSession.token;
		// String vcName = "园区管理";
		String userId = userSession.userId;

		String schoolName = request.getParameter("schoolName");
		String employeeId = null;
		String cityId = request.getParameter("cityId");
		String provinceId = request.getParameter("provinceId");
		String schoolId = request.getParameter("schoolId");
		String mode = request.getParameter("mode");

		// 查询园区是否有重复
		List<School> list = schoolManager.chaSchool(schoolName);
		if (!mode.equals("edit") && list.size() > 0) {
			return "layer.msg('园区已存在')";
		} else {
			schoolManager.saveSchool(schoolName, employeeId, cityId,
					provinceId, schoolId, mode, userId);
			Map map = new HashMap();
			map.put("code", 100);
			map.put("msg", "保存成功");
			String resStr = JsonUtils.fromObject(map);
			return resStr;
		}

	}

	// public String getEmployeeName(){
	//
	// return areaManager.getEmployee()
	// }

	/**
	 * 删除园区School
	 */
	@RequestMapping(value = "/school/delSchool", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String delSchool(HttpServletResponse response,
			HttpServletRequest request) {

		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		Map map = new HashMap();
		for (int i = 0; i < keyArr.length; i++) {
			String key = keyArr[i];
			if (!"".equals(key) && key != null) {
				School school = schoolManager.getBySchoolId(key);
				List<Classes> classes = schoolManager.getClassesBySchoolId(school.getSchoolId());
				List<Employee> employeeList = schoolManager.getEmployeeBySchoolId(school.getSchoolId());
				if(classes.size() > 0 ||employeeList.size() > 0){
					map.put("code", 101);
					map.put("msg", "该园区不可删除!");
				}else{
					schoolManager.delSchool(key);
					map.put("code", 100);
					map.put("msg", "删除成功");
				}
			}

		}
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	/**
	 * 获取-----省------的数据 省市区三级联动
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "school/listSysAreaData", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getSysArea() {

		List listRes = schoolManager.getSysArea();

		return JsonUtils.fromArrayObject(listRes);
	}

	@RequestMapping(value = "school/listCity", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getSysCity(HttpServletResponse response,
			HttpServletRequest request) {
		String cityId = request.getParameter("cityId");
		List listRes = schoolManager.getSysCity(cityId);

		return JsonUtils.fromArrayObject(listRes);
	}

	@RequestMapping(value = "school/listEmployee", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getEmployee(HttpServletResponse response,HttpServletRequest request) {
		String schoolId = request.getParameter("schoolId");
		List listRes = schoolManager.getEmployee(schoolId);
		
		return JsonUtils.fromArrayObject(listRes);
	}

}
