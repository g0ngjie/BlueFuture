package com.cosin.web.controller.blue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.relation.Role;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.dao.blue.IRoleDao;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;
import com.cosin.web.service.blue.IContainManager;
import com.cosin.web.service.blue.IRoleManager;
import com.cosin.web.service.blue.ISchoolManager;


@Scope("prototype")
@Controller
public class RoleController {

	@Autowired
	private IRoleManager roleManager;
	
	@Autowired
	private ISchoolManager schoolManager;
	@Autowired
	private IContainManager containManager;
	@Autowired
	private IRoleDao roleDao;
	/**
	 * 跳转到角色管理页面
	 * @return
	 */
	@RequestMapping(value="/role/roleData", produces = "text/html;charset=UTF-8")
	public ModelAndView indexs(HttpServletRequest request,HttpServletResponse ronse){
		ModelAndView mav = new ModelAndView();
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String role = userSession.roleId;
		
		SysSubrole subrole = roleManager.getsysSubRoleById(role);
		String roleLevel = subrole.getRoleLevel();
		mav.addObject("roleLe", roleLevel);
		
		List<Object> listPw = userSession.list;
		
		for (int i = 0; i < listPw.size(); i++) {
			
			if("role_add".equals(listPw.get(i))){//添加权限
				
				mav.addObject("role_add", listPw.get(i));
				
			}
			if("role_del".equals(listPw.get(i))){
				
				mav.addObject("role_del", listPw.get(i));
				
			}
			if("role_xiugai".equals(listPw.get(i))){
				
				mav.addObject("role_xiugai", listPw.get(i));
				
			}
		}
		mav.setViewName("/role/role");
		
		return mav;
	}

	/**
	 * 查询功能
	 * @return
	 */
	@RequestMapping(value="/role/selectrole",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectsubject(HttpServletRequest request,HttpServletResponse ronse){
		
		
		Map pageParam = PageUtils.getPageParam(request);
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		SysUserRole sysUserRole = schoolManager.getSysUserRoleByUserId(userId);
		String roleId1 = sysUserRole.getSysRole().getRoleId();
		SysSubrole subrole = schoolManager.getSysSubRoleByRoleId(roleId1);
		List<SysRole> listsubject = new ArrayList<SysRole>();
		
		int num = 0;
		if("总园".equals(subrole.getRoleLevel())&&sysUserRole.getSysRole().getType()==1){
			
			listsubject = roleManager.getRoleLimit(new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
			int total = roleManager.getRoleCount();
			num = total;
		}
		else{
			List<School> schools = containManager.getSchools(userId);
			String schoolsId="";
			for (int i = 0; i < schools.size(); i++) {
				School school = schools.get(i);
				if(i==schools.size()-1){
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'";
				}
				else{
					schoolsId = schoolsId+"'"+school.getSchoolId()+"'"+",";
				}

			}
			if(!schoolsId.equals("")){
				
				listsubject = roleManager.getRoleLimit(schoolsId,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
				List total = roleManager.getRoleCountBySchoolId(schoolsId);
				num = total.size();
			}
		}
		
		List list = new ArrayList();
		for (int i = 0; i < listsubject.size(); i++) {
			SysRole vcsubject=listsubject.get(i);
			if(vcsubject.getRolerName().equals("管理员")){
				num--;
				continue;
			}
			Map map  = new HashMap();
			//获取Id
			map.put("roleId",vcsubject.getRoleId());
			//角色名称
			map.put("roleName", vcsubject.getRolerName());
			//有效园区
			String roleId = vcsubject.getRoleId();
			List<SysRoleSchoolPower> listRoScP = roleManager.getsysRoleSchoolPowerByRoleId(roleId);
			String schoolName="";
			for (int j = 0; j < listRoScP.size(); j++) {
				SysRoleSchoolPower sysRoleSchoolPower = listRoScP.get(j);
				if(sysRoleSchoolPower.getSchool() != null){
					
					schoolName+= sysRoleSchoolPower.getSchool().getSchoolName()+",";
				}
			}
			map.put("validSchool", schoolName);
			//添加人
			map.put("employeeName",vcsubject.getSysUser().getUserName());
			//添加时间
			map.put("createDate",vcsubject.getCreateDate());
			
			//根据id 查询 sysSubrole表里面的 角色级别
			SysSubrole sysSubrole = roleManager.getsysSubRoleById(roleId);
			if(sysSubrole != null){
				//角色级别
				map.put("roleLevel",sysSubrole.getRoleLevel());
			}else{
				//角色级别
				map.put("roleLevel","无");
//				//添加人
//				map.put("employeeName","");
//				//添加时间
//				map.put("createDate","");
			}
			
			list.add(map);
		}
			
			Map mapRet = PageUtils.getPageParam(list,num);
			String resStr = JsonUtils.fromObject(mapRet);
			return resStr;
			
	} 
	/**
	 * 保存操作
	 * 角色
	 */
	@RequestMapping(value="/role/saveRole",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveStudent(HttpServletRequest request,HttpServletResponse response){

		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		
		//获取id
		String roleId = request.getParameter("roleId");
		//获取角色名称
		String roleName= request.getParameter("roleName");
		//获取类型  拼接字符串
		String typeName = request.getParameter("typeNam");
		// 获取有效园区名称 总名称
		String schoolNameData = request.getParameter("schoolVar");
		//获取角色级别
		String rolelevel = request.getParameter("rolelevel");
		//mode
		String mode = request.getParameter("mode");
		
		String subRoleIdd = request.getParameter("subRoleIdd");
		
		String subRoleId = request.getParameter("subRoleId");
		
		String nianji = null;
		
		if(rolelevel.equals("年级")){
			
			nianji = request.getParameter("isNianJ");
		}
		
		
		String recipe = request.getParameter("recipe");
		String recipe_kejian = request.getParameter("recipe_kejian");
		String recipe_new = request.getParameter("recipe_new");
		String recipe_chakan = request.getParameter("recipe_chakan");
		String recipe_chehui = request.getParameter("recipe_chehui");
		String recipe_del = request.getParameter("recipe_del");
		String mailbox = request.getParameter("mailbox");
		String mailbox_kejian = request.getParameter("mailbox_kejian");
		String mailbox_chakan = request.getParameter("mailbox_chakan");
		String mailbox_del = request.getParameter("mailbox_del");
		String mailbox_daochu = request.getParameter("mailbox_daochu");
		String park = request.getParameter("park");
		String park_kejian = request.getParameter("park_kejian");
		String park_add = request.getParameter("park_add");
		String park_xiugai = request.getParameter("park_xiugai");
		String park_del = request.getParameter("park_del");
		String classes = request.getParameter("classes");
		String classes_kejian = request.getParameter("classes_kejian");
		String classes_add = request.getParameter("classes_add");
		String classes_addList = request.getParameter("classes_addList");
		String classes_xiugai = request.getParameter("classes_xiugai");
		String classes_del = request.getParameter("classes_del");
		String classes_up = request.getParameter("classes_up");
		String classes_zhuxiao = request.getParameter("classes_zhuxiao");
		String classes_lishi = request.getParameter("classes_lishi");
		String student = request.getParameter("student");
		String student_kejian = request.getParameter("student_kejian");
		String student_add = request.getParameter("student_add");
		String student_addList = request.getParameter("student_addList");
		String student_xiugai = request.getParameter("student_xiugai");
		String student_del = request.getParameter("student_del");
		String student_daochu = request.getParameter("student_daochu");
		String role = request.getParameter("role");
		String role_kejian = request.getParameter("role_kejian");
		String role_add = request.getParameter("role_add");
		String role_xiugai = request.getParameter("role_xiugai");
		String role_del = request.getParameter("role_del");
		String employee = request.getParameter("employee");
		String employee_kejian = request.getParameter("employee_kejian");
		String employee_add = request.getParameter("employee_add");
		String employee_daoru = request.getParameter("employee_daoru");
		String employee_xiugai = request.getParameter("employee_xiugai");
		String employee_del = request.getParameter("employee_del");
		String employee_enable = request.getParameter("employee_enable");
		String employee_daochu = request.getParameter("employee_daochu");
		
		String gaiLan = request.getParameter("gaiLan");
		String gaiLan_kejian = request.getParameter("gaiLan_kejian");
		String gaiLan_xinxi = request.getParameter("gaiLan_xinxi");
		String gaiLan_guanyu = request.getParameter("gaiLan_guanyu");
		String notice = request.getParameter("notice");
		String notice_kejian = request.getParameter("notice_kejian");
		String notice_new = request.getParameter("notice_new");
		String notice_caogao = request.getParameter("notice_caogao");
		String notice_chakan = request.getParameter("notice_chakan");
		String notice_chehui = request.getParameter("notice_chehui");
		String notice_del = request.getParameter("notice_del");
		String notice_daochu = request.getParameter("notice_daochu");
		String activity = request.getParameter("activity");
		String activity_kejian = request.getParameter("activity_kejian");
		String activity_new = request.getParameter("activity_new");
		String activity_chakan = request.getParameter("activity_chakan");
		String activity_chehui = request.getParameter("activity_chehui");
		String activity_del = request.getParameter("activity_del");
		
		String appCheckIn = request.getParameter("appCheckIn");
		String appCheckIn_kejian = request.getParameter("appCheckIn_kejian");
		String appCheckIn_qingjia = request.getParameter("appCheckIn_qingjia");
		String appCheckIn_jieshou = request.getParameter("appCheckIn_jieshou");
		String appCheckIn_chuli = request.getParameter("appCheckIn_chuli");
		String appCheckIn_chaxunlan = request.getParameter("appCheckIn_chaxunlan");
		
		String appGengduo = request.getParameter("appGengduo");
		String appGengduo_gonggao = request.getParameter("appGengduo_gonggao");
		String appGengduo_huodong = request.getParameter("appGengduo_huodong");
		String appGengduo_shipu = request.getParameter("appGengduo_shipu");
		
		Map map = new HashMap();
		SysRole sysRole = roleManager.chaRolebyRoleName(roleName);
		if(sysRole!= null &&!"".equals(sysRole)&&mode.equals("add")){
			map.put("code", 101);
			map.put("msg", "该角色已存在!");
		}else{
			
		roleManager.saveRole(nianji,appGengduo_shipu,appGengduo_huodong,appGengduo_gonggao,appGengduo,appCheckIn_chaxunlan,appCheckIn_chuli,appCheckIn_jieshou,appCheckIn_qingjia,appCheckIn_kejian,appCheckIn,
				subRoleId,roleId,roleName,typeName,schoolNameData,rolelevel,mode,userId,employee_daochu,employee_enable,employee_del,employee_xiugai,employee_daoru,employee_add,employee_kejian,employee,role_del,role_xiugai,
				role_add,role_kejian,role,student_daochu,student_del,student_xiugai,student_addList,student_add,student_kejian,student,classes_lishi,classes_zhuxiao,classes_up,classes_del,classes_xiugai,classes_addList,classes_add,
				classes_kejian,classes,park_del,park_xiugai,park_add,park_kejian,park,mailbox_daochu,mailbox_del,mailbox_chakan,mailbox_kejian,mailbox,recipe_del,recipe_chehui,recipe_chakan,recipe_new,recipe_kejian,
				recipe,activity_del,activity_chehui,activity_chakan,activity_new,activity_kejian,activity,notice_daochu,notice_del,notice_chehui,notice_chakan,notice_caogao,notice_new,notice_kejian,notice,gaiLan_guanyu,gaiLan_xinxi,
				gaiLan_kejian,gaiLan);
		
		map.put("code", 100);
		map.put("msg", "保存成功");
		}
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
	/**
	 * 修改 页面
	 * 回调函数
	 */
	@RequestMapping(value = "/role/roleEdit", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String roleEdit(HttpServletRequest request, HttpServletResponse response) {
		String roleId = request.getParameter("roleId");
		Map<String, Object> map = new HashMap<String, Object>();
		
		SysRole sysRole = roleManager.getRoleById(roleId);
		//角色名称
		map.put("roleName", sysRole.getRolerName());
		//ID
		map.put("roleId", sysRole.getRoleId());
		map.put("type", sysRole.getType());
		//getsysSubRoleById方法是通过roleId 查询
		List<SysSubrole> sysSubrole = roleManager.getsysSubRoleListById(roleId);
		//级别
		if(sysSubrole.size() > 0){
			
			String roleLe = sysSubrole.get(0).getRoleLevel();
			if("总园".equals(roleLe)){
				map.put("roleLevel", 0);
			}else if("分园".equals(roleLe)){
				
				map.put("roleLevel", 1);
			}else if("年级".equals(roleLe)){
				
				map.put("roleLevel", 2);
//				map.put("chaNJ", sysRole.getGrade());
			}else if("班级".equals(roleLe)){
				
				map.put("roleLevel", 3);
			}
			
			map.put("subRoleNum", sysSubrole.size());
			map.put("subRoleIdd", sysSubrole.get(0).getSubRoleId());
		}
		List listSubRole = new ArrayList();
		if(sysSubrole.size() > 0){
			
			for (int i = 0; i < sysSubrole.size(); i++) {
				Map mapSubRole = new HashMap();
				//类型
				mapSubRole.put("typeName", sysSubrole.get(i).getTypeName());
				//类型 Id
				mapSubRole.put("subRoleId", sysSubrole.get(i).getSubRoleId());
				listSubRole.add(mapSubRole);
			}
		}
		map.put("listSubRole", listSubRole);
		
		List<SysRoleSchoolPower> sysRoleSchoolPower = roleManager.getsysRoleSchoolPowerByRoleId(roleId);
		List schoolPowerList = new ArrayList();
		map.put("SysRoleSchoolPowerNum", sysRoleSchoolPower.size());
		if(sysRoleSchoolPower.size() > 0){
			for (int i = 0; i < sysRoleSchoolPower.size(); i++) {
				Map mapSchoolPower = new HashMap();
				//中间表 获取关联园区 school
				mapSchoolPower.put("schoolName", sysRoleSchoolPower.get(i).getSchool().getSchoolName());
				//每个角色 对应中间表 中的 schoolId
				mapSchoolPower.put("schoolId", sysRoleSchoolPower.get(i).getSchool().getSchoolId());
				schoolPowerList.add(mapSchoolPower);
			}
		}
		map.put("schoolPowerList", schoolPowerList);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	} 
	
	/**
	 * 修改 权限 查询  checkbox
	 */
	@RequestMapping(value = "/role/roleEditSel", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String roleEditSel(HttpServletRequest request, HttpServletResponse response) {
		String roleId = request.getParameter("roleId");
		List<Object> listRole = new ArrayList<Object>();
		List<SysRolePower> rolePowerList = roleManager.getsysRolePowerByRoleId(roleId);
		for (int i = 0; i < rolePowerList.size(); i++) {
			Map<String, Object> map = new HashMap<String, Object>();
			SysRolePower sysRolePower = rolePowerList.get(i);
			String powerCode = sysRolePower.getPowerCode();
			String parentId = sysRolePower.getParentId();
			map.put("powerCode", powerCode);
			listRole.add(map);
		}
		
		String resStr = JsonUtils.fromArrayObject(listRole);
		return resStr;
	} 	
	
	/**
	 * 删除 操作 
	 */
	@RequestMapping(value = "/role/delRole", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String delSchool(HttpServletResponse response,
			HttpServletRequest request) {

		//delKeys就是roleId
		String delKeys = request.getParameter("delKeys");
		List<SysUserRole> listl = roleDao.getSysUserRoleList(delKeys);
		Map map = new HashMap();
		if(listl.size()>0){
			map.put("code", 101);
			map.put("msg", "不可删除");
		}
		else{
			roleManager.delRole(delKeys);
			map.put("code", 100);
			map.put("msg", "删除成功");
		}
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 查询 subId
	 */
	@RequestMapping(value = "/role/chaSubId", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String chaSubId(HttpServletResponse response,HttpServletRequest request) {

		//delKeys就是roleId
		String subRoleId = request.getParameter("subRoleId");
		List<SysUser> sysUserList = roleManager.chaSysUserBySubId(subRoleId);
		Map map = new HashMap();
		if(sysUserList.size() > 0){
			map.put("code", 101);
			String resStr = JsonUtils.fromObject(map);
			return resStr;
		}
		map.put("code", 100);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
}
