package com.cosin.web.controller.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;
import com.cosin.web.service.IMenuManager;

@Scope("prototype")
@Controller
public class MenuController {
	@Autowired
	private IMenuManager menuManager;
	/**
	 * 跳转到主页
	 * @return
	 */
	@RequestMapping(value="framework/index")
    public String index(HttpServletRequest request){
		
        return "framework/index";
    }
	@RequestMapping(value="framework/mainindex")
    public String mainindex(HttpServletRequest request){
		
        return "framework/mainindex";
    }
	/**
	 * 框架顶部
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/framework/top")
    public ModelAndView top(HttpServletRequest request){
		SysUser sysUser = new SysUser();
		String userId = "";
		String userName = "";
		String roleName = "";
		UserSession userSession = (UserSession)request.getSession().getAttribute("userSession");
		if(userSession != null){
		//	token = userSession.token;
		//	sysUser = menuManager.getByTokenUser(token);
			userName= userSession.userName;
			userId = userSession.userId;
			SysUserRole sysUserRole = menuManager.getSysUserRoleByUserId(userId);
			roleName = sysUserRole.getSysRole().getRolerName();
		//	userName = sysUser.getUserName();
		}else{
			
		}
		ModelAndView modelAndView = new ModelAndView();
		List listMainMenu = menuManager.getMainMenuList(0, 10);
		modelAndView.addObject("name", userName);
		modelAndView.addObject("userId", userId);
		modelAndView.addObject("roleName", roleName);
		
		modelAndView.setViewName("framework/top");
		modelAndView.addObject("data", listMainMenu);
	    return modelAndView;
    }
	
	/**
	 * 左框架
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/framework/left")
    public ModelAndView left(HttpServletResponse response, HttpServletRequest request){
		String userId = "";
		String roleId = "";
		String userName = "";
		UserSession userSession = (UserSession)request.getSession().getAttribute("userSession");
		if(userSession != null){
		//	token = userSession.token;
		//	sysUser = menuManager.getByTokenUser(token);
			userName= userSession.userName;
			userId = userSession.userId;
			roleId = userSession.roleId;
		//	userName = sysUser.getUserName();
		}
		
		String menuId = request.getParameter("menuId");
		if(menuId == null || "".equals(menuId))
		{
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.addObject("data", new ArrayList());
			modelAndView.addObject("MainMenuName", "");
			return modelAndView;
		}
		SysMenu mainmenu = menuManager.findMenuById(menuId);
		List subMenu = menuManager.findMenuByParentMenuId(menuId,roleId);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("framework/left");
		modelAndView.addObject("data", subMenu);
		modelAndView.addObject("MainMenuName", mainmenu.getMenuName());
		if(subMenu.size() == 0)
		{
			modelAndView.addObject("isSingleMenu", 1);
		}
		else
		{
			modelAndView.addObject("isSingleMenu", 0);
		}
        return modelAndView;
    }
	/*
	 *工作区 左边的菜单
	 * 
	 * */
	@RequestMapping(value="/framework/main")
	public String main(){
		return "framework/main";
	}
	/*
	 * 菜单管理页面的链接  （跳转）的工作区的列表内容
	 * */
	@RequestMapping(value="/framwork/mainmenulist")
	public String mainmenulist(){
		return "/framework/mainmenulist";
	}
	 
	/**
	 * 查询数据
	 * 获取后台数据库  获取数据库的内容
	 * 映射客户端的数据 menulistdata.do
	 */
	@RequestMapping(value="/framework/menulistdata",produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String getMenuListData(HttpServletResponse response, HttpServletRequest request){
	
		String id = request.getParameter("id");
	    if(id == null)
	    	id = "0";
		List<SysMenu> listMenu = menuManager.getMainMenuList(id);
	    List list = new ArrayList();
	    
		for (int i = 0; i < listMenu.size(); i++) {
			SysMenu menu = listMenu.get(i);
			String menuName = menu.getMenuName();
			String menuKey = menu.getMenuId();
			Map map  = new HashMap();
			map.put("menuName", menuName);
			map.put("menuKey", menuKey);
			map.put("state", "closed");
			map.put("enable", menu.getEnable());
			list.add(map);
		}
		String resStr = JsonUtils.fromArrayObject(list);
		return resStr;
    }
	
	/**
	 * 添加下列菜单
	 * 保存添加主菜单 对话框信息的添加 
	 * 传送数据到后台
	 * @param response
	 * @param request
	 */
	@RequestMapping(value="/framework/savemenu", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String saveMenu(HttpServletResponse response, HttpServletRequest request){
		String name = request.getParameter("name");
		String code = request.getParameter("code");
		String url = request.getParameter("url");
		String icon = request.getParameter("order");
		String enable = request.getParameter("enable");
		String type = request.getParameter("icon");
		String menukey = request.getParameter("menukey");
		String parentMenukey = request.getParameter("parentMenukey");
		String mode = request.getParameter("mode");
		
		menuManager.addMenu(mode,menukey,name, code, url, icon, new Integer(enable),type);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 * 编辑
	 * 获取主菜单详情
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/framework/getedit", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String getEdit(HttpServletResponse response, HttpServletRequest request){
		String menukey=request.getParameter("menukey");
		SysMenu sysmenu=menuManager.findMenuById(menukey);
		Map map = new HashMap();
		map.put("name", sysmenu.getMenuName());
		map.put("code", sysmenu.getMenuCode());
		map.put("enable", sysmenu.getEnable());
		map.put("enIcon", sysmenu.getIcon());
		map.put("menuId", sysmenu.getMenuId());
		map.put("orderNo", sysmenu.getOrderNo());
		map.put("url", sysmenu.getUrl());
		if(menukey.equals("1"))
			map.put("sysMenu", "");
		else
			map.put("sysMenu", sysmenu.getSysMenu().getMenuId());
		
		map.put("msg", "操作成功");
		map.put("code", 100);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
     }
	
	/**
	 * 删除
	 */
	@RequestMapping(value="framework/delmenu", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getdel(HttpServletResponse response, HttpServletRequest request){
		String menukey = request.getParameter("menukey");
		menuManager.delmenu(menukey);
		Map map = new HashMap();
		map.put("code", "100");
		map.put("msg", "删除成功");
		String resStr = JsonUtils.fromObject(map);
	    return resStr;
	}
	/**
	 * 隐藏
	 */
	@RequestMapping(value="framework/hideMenu.do", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String gethide(HttpServletResponse response, HttpServletRequest request){
		String menuKey = request.getParameter("menuId");
		menuManager.hideById(menuKey);
		Map map = new HashMap();
		map.put("code", "100");
		map.put("msg", "隐藏成功");
		String resStr = JsonUtils.fromObject(map);
	    return resStr;
	}
	

}
