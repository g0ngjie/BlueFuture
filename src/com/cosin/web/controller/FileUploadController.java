/*     */ package com.cosin.web.controller;
/*     */ 
/*     */ import java.net.InetAddress;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;

/*     */ import javax.servlet.http.HttpServletRequest;
/*     */ import javax.servlet.http.HttpServletResponse;

/*     */ import org.springframework.context.annotation.Scope;
/*     */ import org.springframework.stereotype.Controller;
/*     */ import org.springframework.web.bind.annotation.RequestMapping;
/*     */ import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
/*     */ import com.cosin.utils.RequestFiledUtils;
/*     */ 
/*     */ @Scope("prototype")
/*     */ @Controller
/*     */ public class FileUploadController
/*     */ {
/*     */  
/*     */   @RequestMapping(value={"/fileupload/uploadImg"}, produces={"text/html;charset=UTF-8"})
/*     */   @ResponseBody
/*     */   public String uploadImg(HttpServletRequest request, HttpServletResponse response)
/*     */   {
/* 153 */     RequestFiledUtils rfu = new RequestFiledUtils(response, request, "img");
/* 154 */     List listFile = rfu.getListFile();
/* 155 */     if (listFile.size() == 0)
/*     */     {
/* 157 */       Map resMap = new HashMap();
/* 158 */       resMap.put("code", Integer.valueOf(101));
/* 159 */       String resStr = JsonUtils.fromObject(resMap);
/* 160 */       return resStr;
/*     */     }
/*     */ 
/* 164 */     Map mapFile = (Map)listFile.get(0);
/* 165 */     Map resMap = new HashMap();
/* 166 */     resMap.put("code", Integer.valueOf(100));
/* 167 */     resMap.put("fullImgUrl", "http://localhost:8083/FileManagerService/img/" + mapFile.get("fileName").toString());
/* 168 */     resMap.put("filePath", mapFile.get("fileName").toString());
/* 169 */     resMap.put("fileSize", mapFile.get("size").toString());
/* 170 */     resMap.put("extName", mapFile.get("extName").toString());
/*     */ 
/* 173 */     String resStr = JsonUtils.fromObject(resMap);
/* 174 */     return resStr;

/*     */   }
/*     */ 

public String  localhostIp(HttpServletRequest request){
    // TODO Auto-generated method stub
	
	String ip = request.getHeader("x-forwarded-off");      

    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {     

        ip = request.getHeader("Proxy-Client-IP");      

    }     

    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {     

        ip = request.getHeader("WL-Proxy-Client-IP");     

    }     

    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {     

        ip = request.getRemoteAddr();      

    } 
   /* InetAddress ia=null;
    try {
        ia=ia.getLocalHost();
         
        String localname=ia.getHostName();
        String localip=ia.getHostAddress();
        System.out.println("本机名称是："+ localname);
        System.out.println("本机的ip是 ："+localip);
        return localip;
    } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }*/
	return ip;
}

///*     */   @RequestMapping(value={"/fileupload/uploadImgForKindEditor"}, produces={"text/html;charset=UTF-8"})
///*     */   @ResponseBody
///*     */   public String uploadImgForKindEditor(HttpServletRequest request, HttpServletResponse response)
///*     */   {
///* 186 */     RequestFiledUtils rfu = new RequestFiledUtils(response, request, "img");
///* 187 */     List listFile = rfu.getListFile();
///* 188 */     if (listFile.size() == 0)
///*     */     {
///* 190 */       Map resMap = new HashMap();
///* 191 */       resMap.put("error", Integer.valueOf(1));
///* 192 */       resMap.put("message", "上传");
///* 193 */       String resStr = JsonUtils.fromObject(resMap);
///* 194 */       return resStr;
///*     */     }
///*     */ 	  String localIp = localhostIp(request);
///* 199 */     String path = request.getContextPath();
///* 200 */     String basePath = "/FileManagerService/";//request.getScheme() + "://" + localIp + ":" + request.getServerPort() + "/FileManagerService" + "/";
///*     */ 
///* 203 */     Map mapFile = (Map)listFile.get(0);
///* 204 */     Map resMap = new HashMap();
///* 205 */     resMap.put("error", Integer.valueOf(0));
///* 206 */     resMap.put("url", basePath + "img/" + mapFile.get("fileName").toString());
///*     */ 
///* 212 */     String resStr = JsonUtils.fromObject(resMap);
///* 213 */     return resStr;
//
///*     */   }


/*     */   @RequestMapping(value={"/fileupload/uploadImgForKindEditor"}, produces={"text/html;charset=UTF-8"})
/*     */   @ResponseBody
/*     */   public String uploadImgForKindEditor(HttpServletRequest request, HttpServletResponse response)
/*     */   {
/* 186 */     RequestFiledUtils rfu = new RequestFiledUtils(response, request, "img");
/* 187 */     List listFile = rfu.getListFile();
/* 188 */     if (listFile.size() == 0)
/*     */     {
/* 190 */       Map resMap = new HashMap();
/* 191 */       resMap.put("error", Integer.valueOf(1));
/* 192 */       resMap.put("message", "上传");
/* 193 */       String resStr = JsonUtils.fromObject(resMap);
/* 194 */       return resStr;
/*     */     }
/*     */ 
/* 199 */     String path = request.getContextPath();
/* 200 */     String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/FileManagerService" + "/";
/*     */ 
/* 203 */     Map mapFile = (Map)listFile.get(0);
/* 204 */     Map resMap = new HashMap();
/* 205 */     resMap.put("error", Integer.valueOf(0));
/* 206 */     resMap.put("url", basePath + "img/" + mapFile.get("fileName").toString());
/*     */ 
/* 212 */     String resStr = JsonUtils.fromObject(resMap);
/* 213 */     return resStr;

/*     */   }	

 }

