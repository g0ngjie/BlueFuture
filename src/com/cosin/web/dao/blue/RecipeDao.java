package com.cosin.web.dao.blue;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.Recipe;
import com.cosin.web.entity.RecipeRecord;
import com.cosin.web.entity.RecipeSchool;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysUser;

@Repository
public class RecipeDao extends BaseDao implements IRecipeDao {

	// 分页
	@Override
	public List getRecipeLimit(String monday,String fri, int start, int limit) {
		String sql="from RecipeRecord as a where a.isDraft=0";
		 if(monday != null && !"".equals(monday)){//届数
			 
				sql += " and a.beginTime >= '" + monday + " 00:00:00'";
			}
		 if(fri != null && !"".equals(fri)) //年级
			{
			 sql += " and a.beginTime <= '" + fri + " 00:00:00'";
			}
		sql+=" and a.isDel=0 order by a.createDate desc";
		return query(sql,start,limit);

	}

	// 数量 获取分页 数据
	@Override
	public int getRecipeCount(String monday,String fri) {
		String hql = "select count(*) from RecipeRecord as a where a.isDraft=0";
		 if(monday != null && !"".equals(monday)){//届数
			 
			 hql += " and a.beginTime >= '" + monday + " 00:00:00'";
			}
		 if(fri != null && !"".equals(fri)) //年级
			{
			 hql += " and a.beginTime <= '" + fri + " 00:00:00'";
			}
		 hql+=" and a.isDel=0 order by a.createDate desc";
		return queryCount(hql);
	}

	// 获取recipeId
	@Override
	public RecipeRecord getRecipeId(String recipeId) {
		String sql = "from RecipeRecord as a  where a.recipeRecordId='" + recipeId
				+ "' order by a.createDate desc";
		List<RecipeRecord> list = query(sql);
		if (list.size() > 0)
			return list.get(0);
		return null;
	}

	// 删除
	@Override
	public void recipeDelId(String recipeId) {
		RecipeRecord recipeRecord=getRecipeId(recipeId);
		recipeRecord.setIsDel(1);
		super.save(recipeRecord);
	}

	
	// 根据名称查询
	@Override
	public SysUser getUser2(String user) {
		String sql = "from SysUser as a where a.userId='" + user
				+ "'and a.isDel=0 and a.type=1";
		List<SysUser> list = query(sql);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	//草稿箱列表查询 
	@Override
	public List<RecipeRecord> getDraftLimit(String monday,String fri, int start, int limit) {
		String sql="from RecipeRecord as a where a.isDraft = 1";
		 if(monday != null && !"".equals(monday)){//届数
			 
				sql += " and a.beginTime >= '" + monday + " 00:00:00'";
			}
		 if(fri != null && !"".equals(fri)) //年级
			{
			 sql += " and a.beginTime <= '" + fri + " 00:00:00'";
			}
		sql+=" and a.isDel=0 order by a.createDate desc";
		return query(sql,start,limit);
	}


	//草稿箱 数量 获取分页 数据
		@Override
		public int getDraftCount(String SsName) {
			String hql = "select count(*) from RecipeRecord as a where a.isDraft = 1";
			if(SsName != null && !"".equals(SsName))
			{
				hql += " and a.title like '%" + SsName + "%'";
			}
			hql+=" and a.isDel=0 order by a.createDate desc";
			return queryCount(hql);
		}
		
		/**
		 * 2016年10月10日下午3:07:23
		 *  龚杰
		 *  注释：草稿箱 数量 获取分页 数据
		 */
		@Override
		public int getDraftCount(String monday, String fri) {
			String hql = "select count(*) from RecipeRecord as a where a.isDraft = 1";
			 if(monday != null && !"".equals(monday)){//届数
				 
				 hql += " and a.beginTime >= '" + monday + " 00:00:00'";
				}
			 if(fri != null && !"".equals(fri)) //年级
				{
				 hql += " and a.beginTime <= '" + fri + " 00:00:00'";
				}
			 hql+=" and a.isDel=0 order by a.createDate asc";
			return queryCount(hql);
		}	
		
		public SysUser getSysUserById(String userId) {
			String sql = "from SysUser as a where a.isDel = 0 and a.userId ='" + userId + "'";
			List<SysUser> list = query(sql);
			if(list.size()>0)
				return list.get(0);
			return null;
		}

		/**
		 * 保存 食谱
		 */
		@Override
		public void saveRecipeRecord(RecipeRecord recipeRecord) {

			super.save(recipeRecord);
			
		}

		/**
		 * 保存 食谱  五天
		 */
		@Override
		public void saveRecipe(Recipe recipe) {
			super.save(recipe);
			
		}

		/**
		 * 根据id 查school
		 */
		@Override
		public School getSchoolById(String key) {
			String sql = "from School as a where a.isDel = 0 and a.schoolId ='" + key + "'";
			List<School> list = query(sql);
			if(list.size()>0)
				return list.get(0);
			return null;
		}

		/**
		 * 保存中间表
		 */
		@Override
		public void saveRecipeSchool(RecipeSchool recipeSchool) {
			super.save(recipeSchool);
			
		}

		/**
		 * 通过id 获取对象
		 */
		@Override
		public List<RecipeSchool> getRecipeSchoolById(String recipeRecordId) {
			String sql = "from RecipeSchool as a where 1 = 1  and a.recipeRecord = '" + recipeRecordId + "'";
			return query(sql);
		}

		/**
		 * 通过recipeId 查询对象
		 */
		@Override
		public RecipeRecord getRecipeRecordById(String recipeId) {
			String sql = "from RecipeRecord as a where a.isDel = 0 and a.recipeRecordId = '" + recipeId + "'";
			List<RecipeRecord> list = query(sql);
			if(list.size() > 0)
				return list.get(0);
			return null;
		}

		/**
		 * 根据 recipeRecordId 查询 一周 的食谱
		 */
		@Override
		public List<Recipe> getRecipeById(String recipeId) {
			String sql ="from Recipe as a where a.recipeRecord = '" + recipeId + "'";
			return query(sql);
		}

		/**
		 * 真删RecipeSchool 中间表
		 */
		@Override
		public void delRecipeSchool(List<RecipeSchool> recipeSchoolList) {
			for (int i = 0; i < recipeSchoolList.size(); i++) {
				RecipeSchool recipeSchool = recipeSchoolList.get(i);
				super.delete(recipeSchool);
			}
			
		}

		/**
		 * 真删 操作
		 */
		@Override
		public void delRecipe(List<Recipe> obj) {
			for (int i = 0; i < obj.size(); i++) {
				Recipe object = obj.get(i);
				super.delete(object);
			}
			
		}

		/**
		 * 2016年10月23日下午4:59:16
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List<RecipeSchool> getRecipeSchoolLimit(String schoolsId,
				String monday, String fri, Integer integer, Integer integer2) {
			// TODO Auto-generated method stub
			String sql="from RecipeSchool as a where a.recipeRecord.isDraft=0";
			 if(monday != null && !"".equals(monday)){//届数
				 
					sql += " and a.recipeRecord.beginTime >= '" + monday + " 00:00:00'";
				}
			 if(fri != null && !"".equals(fri)) //年级
				{
				 sql += " and a.recipeRecord.beginTime <= '" + fri + " 00:00:00'";
				}
			sql+=" and a.recipeRecord.isDel=0 and a.school in ("+ schoolsId +") order by a.recipeRecord.createDate desc";
			return query(sql,integer,integer2);
		}

		/**
		 * 2016年12月8日下午5:26:21
		 *  阳朔
		 *  注释:
		 */
		@Override
		public List<RecipeSchool> getRecipeDraftSchoolLimit(String schoolsId,
				String monday, String fri, Integer integer, Integer integer2) {
			String sql="from RecipeSchool as a where a.recipeRecord.isDraft=1";
			 if(monday != null && !"".equals(monday)){//届数
				 
					sql += " and a.recipeRecord.beginTime >= '" + monday + " 00:00:00'";
				}
			 if(fri != null && !"".equals(fri)) //年级
				{
				 sql += " and a.recipeRecord.beginTime <= '" + fri + " 00:00:00'";
				}
			sql+=" and a.recipeRecord.isDel=0 and a.school in ("+ schoolsId +") order by a.recipeRecord.createDate desc";
			return query(sql,integer,integer2);
		}


	
}
