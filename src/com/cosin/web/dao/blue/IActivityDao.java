package com.cosin.web.dao.blue;

import java.util.List;

import com.cosin.web.entity.Activity;
import com.cosin.web.entity.ActivitySchool;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.School;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

public interface IActivityDao {

	//分页
    public List getActivityLimit(String activityId, int start, int limit);
    //数量
    public int getActivityCount(String SsName);
    //获取noticeId
  	public Activity getActivityId(String activityId);
    /**
     * 保存  精彩活动 
     */
  	public void saveActivity(Activity activity);
  	 //删除
    public void activityDelId(String activityId);
    //查询用户表 user
//    public List getUser();
    //根据id查询
    public SysUser getUser2(String user);
    //获取草稿箱列表
	List<Activity> getDraftLimit(String ssName, Integer start, Integer limit);
	
	/**
	 * 通过userId  获取 user
	 */
	public SysUser getSysUserById(String userId);

	/**
	 * 通过 园区id 查询 对象
	 * @param key
	 * @return
	 */
	public School getSchoolById(String key);
	
	/**
	 * 保存外间表
	 * @param activitySchool
	 */
	public void saveActivitySchool(ActivitySchool activitySchool);
	
	/**
	 * 中间表查询
	 * @param activityId
	 * @return 
	 */
	public List<ActivitySchool> getActivitySchoolByActivityId(String activityId);
	
	/**
	 * 删除中间表
	 * @param activitySchoolList
	 */
	public void delActivitySchoolById(List<ActivitySchool> activitySchoolList);
	/**
	 * 2016年10月8日下午4:02:17
	 *  龚杰
	 *  注释：
	 */
	public List<ActivitySchool> getSchoolByActivityId(String activityId);
	/**
	 * 2016年10月12日下午8:18:15
	 *  阳朔
	 *  注释:
	 */
	public int getActivityDraftCount(String ssName);
	/**
	 * 2016年10月23日下午4:41:01
	 *  阳朔
	 *  注释:
	 */
	public List<ActivitySchool> getActivityLimitBySchools(String ssName,
			String schoolsId, Integer integer, Integer integer2);
	/**
	 * 2016年10月25日下午5:30:42
	 *  阳朔
	 *  注释:根据schoolId 获取 关联的职工Id
	 */
	public List<Student> getStudentBySchoolId(String schoolId);
	/**
	 * 2016年10月25日下午5:33:41
	 *  阳朔
	 *  注释:
	 */
	public List<Parentsstudent> getParentsstudentByStudentId(String studentId);
	/**
	 * 2016年10月25日下午5:49:18
	 *  阳朔
	 *  注释:根据schoolId 获取 关联的职工Id
	 */
	public List<Employee> getEmployeeBySchoolId(String schoolId);
	/**
	 * 2016年10月26日下午3:34:48
	 *  阳朔
	 *  注释:
	 */
	public List<ActivitySchool> getActivityCountBySchoolId(String ssName, String schoolsId);
	/**
	 * 2016年10月30日下午4:29:06
	 *  阳朔
	 *  注释:发送活动 看权限是否可见
	 */
	public SysUserRole getSysUserRoleByEmpUserId(String userId);
	/**
	 * 2016年10月30日下午4:30:15
	 *  阳朔
	 *  注释:发送活动 看权限是否可见
	 */
	public List<SysRolePower> getSysRolePowerByEmp_RoleId(String emp_RoleId);
	/**
	 * 2016年12月8日下午3:14:30
	 *  阳朔
	 *  注释:
	 */
	public List<ActivitySchool> getActivityDraftLimitBySchools(String ssName,
			String schoolsId, Integer integer, Integer integer2);
	/**
	 * 2016年12月8日下午3:14:34
	 *  阳朔
	 *  注释:
	 */
	public List<ActivitySchool> getActivityDraftCountBySchoolId(String ssName,
			String schoolsId);

}
