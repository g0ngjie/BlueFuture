package com.cosin.web.dao.blue;

import java.util.List;

import com.cosin.web.entity.Contain;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysUser;


public interface IContainDao {

	/**
	 * 获取正文
	 */
	Contain getContainList();

	/**
	 * 园区数量
	 * @return
	 */
	int getSchoolNum();

	/**
	 * 班级数量
	 * @return
	 */
	int getClassesNum();

	/**
	 * 班主任数量
	 * @return 
	 */
	int getClassTeacherNum();

	/**
	 * 普通老师数量
	 */
	int getOtherTeacherNum();

	/**
	 * 学生数量
	 * @return
	 */
	int getStudentNum();

	/**
	 * 家长数量
	 * @return
	 */
	int getParentNum();

	/**
	 * 保存
	 */
	void saveContain(Contain contain);

	/**
	 * 删除 内容
	 * @param i
	 */
	void delContain(Contain contain1);

	/**
	 * 先查 后删
	 * @param string
	 * @return 
	 */
	Contain getContain();

	SysUser getSysUserById(String userId);

	void saveSysUser(SysUser sysUser);

	/**
	 * 2016年10月23日下午3:08:12
	 *  阳朔
	 *  注释:
	 */
	List<SysUser> getSysUserByPhone(String mobile);

	/**
	 * 2016年10月23日下午3:10:55
	 *  阳朔
	 *  注释:
	 */
	void saveUser(SysUser sysUser);

	/**
	 * 2016年10月23日下午3:54:53
	 *  阳朔
	 *  注释:
	 */
	List<Employee> getEmployees(String userId);

	/**
	 * 2016年10月23日下午3:59:17
	 *  阳朔
	 *  注释:
	 */
	int getClassesNum(String schoolsId);

	/**
	 * 2016年10月23日下午4:02:48
	 *  阳朔
	 *  注释:
	 */
	int getTeacherBySchool(String schoolsId);

	/**
	 * 2016年10月23日下午4:06:44
	 *  阳朔
	 *  注释:
	 */
	int getStudentsBySchool(String schoolsId);

	/**
	 * 2016年10月23日下午4:09:00
	 *  阳朔
	 *  注释:
	 */
	List<Student> getParentStudents(String schoolsId);

	/**
	 * 2016年10月23日下午4:10:50
	 *  阳朔
	 *  注释:
	 */
	List<Parentsstudent> getParentsstudent(String studentId);




}
