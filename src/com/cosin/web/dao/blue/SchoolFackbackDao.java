package com.cosin.web.dao.blue;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.SchoolFackback;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysUser;


@Repository
public class SchoolFackbackDao extends BaseDao implements ISchoolFackbackDao{

	//分页
	@Override
	public List getSchoolFackbackLimit(String startDate,String endDate,String schoolName,String enable, int start, int limit) {
		String sql="from SchoolFackback as a where a.isDel = 0 ";
		if(startDate != null && !"".equals(startDate))//开始时间
		{
			sql += " and a.createDate >= '" + startDate + " 00:00:00'";
		}
		if(endDate != null && !"".equals(endDate))//结束时间
		{
			sql += " and a.createDate <= '" + endDate + " 23:59:59'";
		}
		if (schoolName != null && !"".equals(schoolName)) {
			sql+= "and a.school.schoolName = '"+ schoolName +"'";
		}
		if (enable != null && !"".equals(enable)) {
			sql+= "and a.enable = '"+ enable +"'";
		}
		sql+="order by a.createDate desc";
		return query(sql,start,limit);
	}
	// 数量 获取分页 数据 
	@Override
	public int getSchoolFackbackCount() {
		String sql="select count(*) from  SchoolFackback as a where a.isDel = 0";
		return queryCount(sql);
	}
	//获取schoolFackbackId
	@Override
	public SchoolFackback getSchoolFackbackId(String schoolFackbackId) {
		String sql = "from SchoolFackback as a  where a.schoolFackbackId='"+schoolFackbackId+"' and a.isDel = 0";
		List<SchoolFackback> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;	
	}
	//删除
	@Override
	public void schoolFackbackDelId(String schoolFackbackId) {
		SchoolFackback schoolFackback=getSchoolFackbackId(schoolFackbackId);
		schoolFackback.setIsDel(1);
		super.save(schoolFackback);
	}
	//根据名称查询
	@Override
	public SysUser getUser2(String user) {
		String sql="from SysUser as a where a.userId='"+user+"'and a.isDel=0 and a.type=1";
		List<SysUser> list=query(sql);
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}
	
	/**
	 * 当点击 详情 同时  改变查看状态
	 */
	@Override
	public void save(String schoolFackbackId) {
		SchoolFackback schoolFackback= getSchoolFackbackId(schoolFackbackId);
		int enableli = schoolFackback.getEnable();
		if(enableli!=1){
			schoolFackback.setEnable(1);
		}
		save(schoolFackback);
	}
	
	/**
	 * 
	 */
	@Override
	public List<SchoolFackback> getSchoolFackbackLimit(int mode, String order,
			String sort, String SsName, int start, int limit, String startDate,
			String endDate) {
		 String sql="from SchoolFackback as a where 1=1";
		 if(SsName != null && !"".equals(SsName)){//平台
				sql += " and a.platform like '%" + SsName + "%'";
			}
		 if(startDate != null && !"".equals(startDate))
			{
				sql += " and a.createDate >= '" + startDate + " 00:00:00'";
			}
		if(endDate != null && !"".equals(endDate))
			{
				sql += " and a.createDate <= '" + endDate + " 23:59:59'";
			}
		 sql+="  order by ";
		 
		 if (sort != null && !"".equals(sort)){//排序字段
			 
			 if("num".equals(sort)){//分享量
				 sql += " a.num "+ order;
			 }
		 }else{
			 sql += " a.createDate desc";
		 }
		 if(mode == 1)
			 return query(sql,start,limit);
		 return query(sql);
	}
	/**
	 * 2016年10月23日下午5:15:20
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SchoolFackback> getSchoolFackbackLimit(String schoolsId,
			String startDate, String endDate, String schoolName,
			String enable, Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		String sql="from SchoolFackback as a where a.isDel = 0 and a.school in ("+ schoolsId +")";
		if(startDate != null && !"".equals(startDate))//开始时间
		{
			sql += " and a.createDate >= '" + startDate + " 00:00:00'";
		}
		if(endDate != null && !"".equals(endDate))//结束时间
		{
			sql += " and a.createDate <= '" + endDate + " 23:59:59'";
		}
		if (schoolName != null && !"".equals(schoolName)) {
			sql+= "and a.school.schoolName = '"+ schoolName +"'";
		}
		if (enable != null && !"".equals(enable)) {
			sql+= "and a.enable = '"+ enable +"'";
		}
		sql+="order by a.createDate desc";
		return query(sql,integer,integer2);
	}
	/**
	 * 2016年10月26日下午4:15:51
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List getSchoolFackbackCount(String startDate, String endDate,
			String ischoolName, String enable) {
		String sql="from SchoolFackback as a where a.isDel = 0 ";
		if(startDate != null && !"".equals(startDate))//开始时间
		{
			sql += " and a.createDate >= '" + startDate + " 00:00:00'";
		}
		if(endDate != null && !"".equals(endDate))//结束时间
		{
			sql += " and a.createDate <= '" + endDate + " 23:59:59'";
		}
		if (ischoolName != null && !"".equals(ischoolName)) {
			sql+= "and a.school.schoolName = '"+ ischoolName +"'";
		}
		if (enable != null && !"".equals(enable)) {
			sql+= "and a.enable = '"+ enable +"'";
		}
		sql+="order by a.createDate desc";
		return query(sql);
	}
	/**
	 * 2016年10月26日下午4:33:30
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List getSchoolFackbackCountBySchoolId(String schoolsId,
			String startDate, String endDate, String schoolName, String enable) {
		String sql="from SchoolFackback as a where a.isDel = 0 and a.school in ("+ schoolsId +")";
		if(startDate != null && !"".equals(startDate))//开始时间
		{
			sql += " and a.createDate >= '" + startDate + " 00:00:00'";
		}
		if(endDate != null && !"".equals(endDate))//结束时间
		{
			sql += " and a.createDate <= '" + endDate + " 23:59:59'";
		}
		if (schoolName != null && !"".equals(schoolName)) {
			sql+= "and a.school.schoolName = '"+ schoolName +"'";
		}
		if (enable != null && !"".equals(enable)) {
			sql+= "and a.enable = '"+ enable +"'";
		}
		sql+="order by a.createDate desc";
		return query(sql);
	}
	/**
	 * 2016年11月1日下午7:14:00
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Parentsstudent getParentsstudentByParentId(String parentId) {
		String sql = "from Parentsstudent as a where a.parents = '" + parentId + "'";
		List<Parentsstudent> list=query(sql);
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}
	
	
	
	
}
