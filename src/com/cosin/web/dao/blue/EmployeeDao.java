package com.cosin.web.dao.blue;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Classteacher;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

@Repository
public class EmployeeDao extends BaseDao implements IEmployeeDao {

	/**
	 * 分页
	 */
	@Override
	public List getEmployeeLimit(String SsName,String SsTel, int start, int limit) {
		String sql = "from Employee as a where a.isDel = 0 and a.sysUserByUserId.isDel=0 and a.school.isDel=0";
		if (SsName != null && !"".equals(SsName)) {
			sql += " and a.sysUserByUserId.userName like '%" + SsName + "%'";
		}
		if (SsTel != null && !"".equals(SsTel)) {
			sql += " and a.sysUserByUserId.mobile = '" + SsTel + "'";
		}
		sql+="order by a.school.createDate desc";
		return query(sql, start, limit);
	}

	/**
	 * 数量 获取分页 数据
	 */
	@Override
	public int getEmployeeCount(String SsName,String SsTel) {
		String sql = "select count(*) from  Employee as a where a.isDel = 0 and a.sysUserByUserId.isDel=0 and a.school.isDel=0";
		if (SsName != null && !"".equals(SsName)) {
			sql += " and a.sysUserByUserId.userName like '%" + SsName + "%'";
		}
		if (SsTel != null && !"".equals(SsTel)) {
			sql += " and a.sysUserByUserId.mobile = '" + SsTel + "'";
		}
		return queryCount(sql);
	}
	/**
	 * 数量 获取分页 数据 修改
	 */
	@Override
	public List getEmployeeCountGai(String SsName,String SsTel) {
		String sql = "from Employee as a where a.isDel = 0 ";
		if (SsName != null && !"".equals(SsName)) {
			sql += " and a.sysUserByUserId.userName like '%" + SsName + "%'";
		}
		if (SsTel != null && !"".equals(SsTel)) {
			sql += " and a.sysUserByUserId.mobile = '" + SsTel + "'";
		}
		return query(sql);
	}

	/**
	 * 获取角色名称
	 */
	@Override
	public List<SysRole> getlistRole() {
		String sql = "from SysRole as a where 1 = 1 and a.isDel = 0";
		return query(sql);
	}
	
	/**
	 * 通过roleId  获取 类型
	 */
	@Override
	public List<SysSubrole> getlistSubRole(String roleId) {
		String sql = "from SysSubrole as a where a.sysRole = '" + roleId + "' and a.isDel = 0";
		return query(sql);
	}

	/**
	 * 通过 userId 获取 sysUser
	 */
	@Override
	public SysUser getSysUserById(String userId) {
		String sql = "from SysUser as a where a.userId = '" + userId + "' and a.isDel = 0";
		List<SysUser> list = query(sql);
		if (list.size() > 0)
			return list.get(0);
		return null;
	}
	
	/**
	 * 通过roleId 查询中间表sysroleschoolpower
	 */
	@Override
	public List<SysRoleSchoolPower> getListsysRoleSchoolPower(String roleId) {
		String sql = "from SysRoleSchoolPower as a where a.sysRole = '" + roleId + "' ";
		
		return query(sql);
	}

	/**
	 * 通过schoolId 获取 school
	 * @return 
	 */
	@Override
	public School getSchoolById(String schoolId) {
		String sql = "from School as a where a.schoolId = '" + schoolId + "'";
		List<School> list = query(sql);
		if (list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 通过roleId 查出sysRole表
	 */
	@Override
	public SysRole getSysRoleById(String roleId) {
		String sql = "from SysRole as a where a.isDel = 0 and a.roleId = '" + roleId + "'";
		List<SysRole> list = query(sql);
		if (list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 通过subRoleId 查出 SysSubRole表
	 */
	@Override
	public SysSubrole getSysSubRoleById(String subRoleId) {
		String sql = "from SysSubrole as a where a.subRoleId = '" + subRoleId + "'";
		List<SysSubrole> list = query(sql);
		if (list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 职工表保存
	 */
	@Override
	public void save(Employee employee) {
		super.save(employee);
	}

	/**
	 * 保存职工到sysUser表
	 */
	@Override
	public void saveSysUser(SysUser sysUser) {
		super.save(sysUser);
	}
	
	/**
	 * 根据Id 查出Employee
	 */
	@Override
	public Employee getEmployeeById(String employeeId) {
		String sql ="from Employee as a where a.isDel = 0 and a.employeeId = '" + employeeId + "'";
		List<Employee> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}
	
	/**
	 * 删除Employee
	 */
	@Override
	public void delEmployeeById(String delKeys) {
		super.save(delKeys);
	}

	/**
	 * 
	 */
	@Override
	public SysSubrole getSysSubRoleByName(String cellStr) {
		String sql = "from SysSubrole as a where a.isDel=0 and a.typeName = '" + cellStr + "'";
		List<SysSubrole> list = query(sql);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 通过 字段 查询
	 */
	@Override
	public School getByName(String cellStr) {
		String sql = "from School as a where a.isDel=0 and a.schoolName = '" + cellStr + "'";
		List<School> list = query(sql);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 角色字段 查询 
	 */
	@Override
	public SysRole getSysRoleByName(String cellStr) {
		String sql = "from SysRole as a where a.isDel=0 and a.rolerName = '" + cellStr + "'";
		List<SysRole> list = query(sql);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 通过 roleId  查询 sysUser
	 */
	@Override
	public SysUser getSysUserByRoleId(String roleId) {
		String sql = "from SysUser as a where a.sysRole = '" + roleId + "'";
		List<SysUser> list = query(sql);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 2016年9月29日下午5:06:07
	 *  龚杰
	 *  注释：
	 */
	@Override
	public void saveObj(Object obj) {
		super.save(obj);
	}

	/**
	 * 2016年9月29日下午5:57:13
	 *  龚杰
	 *  注释：通过userId  查询 role
	 */
	@Override
	public List<SysUserRole> getSysUserRoleByUserId(String useId) {
		String sql = "from SysUserRole as a where a.sysUser = '" + useId + "'";
		return query(sql);
	}

	/**
	 * 2016年9月29日下午6:53:21
	 *  龚杰
	 *  注释：真删除
	 */
	@Override
	public void delObj(Object obj) {
		super.delete(obj);
		
	}

	/**
	 * 2016年10月10日下午6:32:36
	 *  龚杰
	 *  注释：
	 */
	@Override
	public SysSubrole getSysSubRoleByRoleId(String roleId) {
		String sql = "from SysSubrole as a where a.isDel = 0 and a.sysRole = '" + roleId + "'";
		List<SysSubrole> list = query(sql);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 2016年10月11日下午12:00:44
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUserRole getSysUserRoleByRoleId(String roleId) {
		String sql = "from SysUserRole as a where 1 = 1 and a.sysRole = '" + roleId + "'";
		List<SysUserRole> list = query(sql);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 2016年10月14日下午2:30:08
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUser chaSysUserByLogin(String tel) {
		String sql = "from SysUser as a where a.isDel = 0 and a.type = 2 and a.loginName = '" + tel + "'";
		List<SysUser> list = query(sql);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 2016年10月17日上午10:57:35
	 *  阳朔
	 *  注释:查询所有type = 1的 家长端
	 */
	@Override
	public SysUser getsysUserParentByLoginName(String loginName) {
		String sql = "from SysUser as a where a.isDel = 0 and a.type = 1 and a.loginName = '" + loginName + "'";
		List<SysUser> list = query(sql);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 保存
	 */
	@Override
	public void saveUserRole(SysUserRole sysUserRole) {
		super.save(sysUserRole);
		
	}

	/**
	 * 2016年10月20日下午4:44:46
	 *  阳朔
	 *  注释:查询 家长是否为老师 已存在 
	 */
	@Override
	public SysUser getParenUseByLogin(String tel) {
		String sql ="from SysUser as a where a.isDel = 0 and a.type = 1 and a.loginName = '" + tel + "'";
		List<SysUser> list = query(sql);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 2016年10月22日下午3:09:41
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUser getSysUserBySubRoleId(String subRoleId) {
		String sql = "from SysUser as a where a.isDel = 0 and a.sysSubrole = '" + subRoleId + "'";
		List<SysUser> list = query(sql);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 2016年10月22日下午3:24:38
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysRoleSchoolPower getSysRoleSchoolPowerByRoleId(String roleId) {
		String sql = "from SysRoleSchoolPower as a where a.sysRole = '" + roleId + "'";
		List<SysRoleSchoolPower> list = query(sql);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 2016年10月22日下午3:58:56
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<Employee> getEmployeeBySchoolId(String schoolId, String ssName,
			String ssTel, Integer start, Integer limit) {
		String sql = "from Employee as a where a.isDel = 0  and a.sysUserByUserId.isDel=0  and a.school.isDel=0";
		if (schoolId != null && !"".equals(schoolId)) {
			sql += " and a.school = '" + schoolId + "'";
		}
		if (ssName != null && !"".equals(ssName)) {
			sql += " and a.sysUserByUserId.userName like '%" + ssName + "%'";
		}
		if (ssTel != null && !"".equals(ssTel)) {
			sql += " and a.sysUserByUserId.mobile = '" + ssTel + "'";
		}
		sql+="order by a.school.createDate desc";
		return query(sql, start, limit);
	}
	
	/**
	 * 2016年10月22日下午3:58:56
	 *  阳朔
	 *  注释: 分页数量 改
	 */
	@Override
	public List<Employee> getEmployeeBySchoolIdCount(String schoolId, String ssName,
			String ssTel) {
		String sql = "from Employee as a where a.isDel = 0  and a.sysUserByUserId.isDel=0  and a.school.isDel=0";
		if (schoolId != null && !"".equals(schoolId)) {
			sql += " and a.school = '" + schoolId + "'";
		}
		if (ssName != null && !"".equals(ssName)) {
			sql += " and a.sysUserByUserId.userName like '%" + ssName + "%'";
		}
		if (ssTel != null && !"".equals(ssTel)) {
			sql += " and a.sysUserByUserId.mobile = '" + ssTel + "'";
		}
		return query(sql);
	}

	/**
	 * 2016年10月22日下午4:16:14
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Employee getEmployeeByIUserId(String userId) {
		// TODO Auto-generated method stub
		String sql = "from Employee as a where a.isDel = 0 and a.sysUserByUserId='"+ userId +"'";
		return (Employee) query(sql).get(0);
	}

	/**
	 * 2016年10月22日下午4:44:22
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysRoleSchoolPower> getSysRoleSchoolPowerBySchoolId(
			String schoolId) {
		String sql = "from SysRoleSchoolPower as a where a.school = '" + schoolId + "'";
		return query(sql);
	}

	/**
	 * 2016年10月23日下午8:09:39
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void saveSchool(School school) {
		super.save(school);
		
	}

	/**
	 * 2016年10月23日下午9:28:44
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void delSysUserRole(SysUserRole sysUser) {
		// TODO Auto-generated method stub
		super.delete(sysUser);
	}

	/**
	 * 2016年10月23日下午9:30:20
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUserRole getSysUserRoleByUseri(String userId) {
		// TODO Auto-generated method stub
		String sql = "from SysUserRole as a where a.sysUser = '" + userId + "'";
		List<SysUserRole> sysUserRole = query(sql);
		if(sysUserRole.size()>0){
			return sysUserRole.get(0);
		}
		return null;
	}

	/**
	 * 2016年10月26日下午1:49:05
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUserRole chaSysUserRoleByUserId(String userId) {
		String sql = "from SysUserRole as a where a.sysUser = '" + userId + "'";
		List<SysUserRole> sysUserRole = query(sql);
		if(sysUserRole.size()>0){
			return sysUserRole.get(0);
		}
		return null;
	}

	/**
	 * 2016年10月26日下午2:59:23
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getEmployeeCountBySchoolId(String schoolId, String ssName,
			String ssTel) {
		String sql = "select count(*) from  Employee as a where a.isDel = 0";
		if (schoolId != null && !"".equals(schoolId)) {
			sql += " and a.school = '" + schoolId + "'";
		}
		if (ssName != null && !"".equals(ssName)) {
			sql += " and a.sysUserByUserId.userName like '%" + ssName + "%'";
		}
		if (ssTel != null && !"".equals(ssTel)) {
			sql += " and a.sysUserByUserId.mobile = '" + ssTel + "'";
		}
		return queryCount(sql);
	}

	/**
	 * 2016年10月27日下午5:17:52
	 *  阳朔
	 *  注释:
	 */
	@Override
	public School getSchoolByName(String schoolNam) {
		String sql = "from School as a where a.isDel=0 and a.schoolName = '" + schoolNam + "'";
		List<School> sysUserRole = query(sql);
		if(sysUserRole.size()>0){
			return sysUserRole.get(0);
		}
		return null;
	}

	/**
	 * 2016年10月27日下午6:52:20
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Classteacher getClassesTeacherByEmId(String employeeId) {
		String sql ="from Classteacher as a where a.classes.isDel = 0 and a.employee = '" + employeeId + "'";
		List<Classteacher> sysUserRole = query(sql);
		if(sysUserRole.size()>0){
			return sysUserRole.get(0);
		}
		return null;
	}

	/**
	 * 2016年10月27日下午6:55:05
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Classes getClassesByEmId(String employeeId) {
		String sql = "from Classes as a where a.isDel = 0 and a.employee = '" + employeeId + "'";
		List<Classes> sysUserRole = query(sql);
		if(sysUserRole.size()>0){
			return sysUserRole.get(0);
		}
		return null;
	}

	/**
	 * 2016年11月23日上午11:29:17
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysRole> getlistRoleByZY() {
		String sql = "from SysRole as a where a.isDel = 0";
		return query(sql);
	}

	/**
	 * 2016年11月23日下午12:35:40
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<Employee> getEmployeeBySchoolNullLimit(String ssName, String ssTel, Integer start, Integer limit) {
		String sql = "from Employee as a where a.isDel = 0 and a.school = null";
		if (ssName != null && !"".equals(ssName)) {
			sql += " and a.sysUserByUserId.userName like '%" + ssName + "%'";
		}
		if (ssTel != null && !"".equals(ssTel)) {
			sql += " and a.sysUserByUserId.mobile = '" + ssTel + "'";
		}
		return query(sql, start, limit);
	}

	/**
	 * 2016年11月23日下午12:51:20
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List getEmployeeCountZY(String ssName, String ssTel) {
		String sql = "from Employee as a where a.isDel = 0 and a.school = null";
		if (ssName != null && !"".equals(ssName)) {
			sql += " and a.sysUserByUserId.userName like '%" + ssName + "%'";
		}
		if (ssTel != null && !"".equals(ssTel)) {
			sql += " and a.sysUserByUserId.mobile = '" + ssTel + "'";
		}
		return query(sql);
	}


}
