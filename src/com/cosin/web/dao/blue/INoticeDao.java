package com.cosin.web.dao.blue;

import java.util.List;

import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Classteacher;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Notice;
import com.cosin.web.entity.NoticeSub;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.SignNotice;
import com.cosin.web.entity.SignPrice;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

public interface INoticeDao {

	//分页
    public List getNoticeLimit(String noticeId, int start, int limit);
    //数量
    public int getNoticeCount(String SsName);
    //获取noticeId
  	public Notice getNoticeId(String noticeId);
   
  	 //删除
    public void noticeDelId(String noticeId);
    //查询用户表 user
//    public List getUser();
    //根据id查询
    public SysUser getUser2(String user);
    //获取草稿箱 
	public List<Notice> getDraftLimit(String ssName, int start, int limit);
	/**
	 * 
	 * @param id
	 * @return
	 */
	public List<Object> noticeDao(String id);
	/**
	 * 
	 * @param id
	 * @return
	 */
	public List<Classes> getListClasses(Object id);
	//撤回操作
//	public void rollNotice(String noticeId);
	public void rollNotice(Notice notice);
	
	/**
	 * 通过userId 查询user
	 */
	public SysUser getSysUserById(String userId);
	
	 /**
	  * 保存公告
	  */
  	public void saveNotice(Notice otNotice);
  	
	public List<Object> getListClas(Object id);
	//班级
	public List<Classes> getClass(Object id,Object schoolId);
	
	public Classes getByIdClasses(String string);
	
	public void saveObj(Object obj);
	
	/**
	 * 通过noticeId 查询 中间表
	 * 下午3:31:52
	 */
	public List<NoticeSub> getNoticeSubByNoId(String noticeId);
	/**
	 * 删除
	 * 下午3:36:14
	 */
	public void delNoticeSub(List<NoticeSub> listNoticeSub);
	
	/**
	 * 根据noticeId  查询NoticeSub
	 */
	public List<NoticeSub> getNoticeSubByNoticeId(String noticeId);
	
	/**
	 * 根据noticeId  查询 noticesub  中间表  关联班级
	 * @param noticeId
	 * @return
	 */
	public List<NoticeSub> getNoticeSubBynoticeId(String noticeId);
	
	/**
	 * 已报名
	 * @param schoolName
	 * @param grade
	 * @param className
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<NoticeSub> getNoticeLimit(String noticeId,String schoolName, String grade,
			String className, Integer start, Integer limit);
	
	/**
	 * 通过classId  查询  学生
	 * 
	 * @param classesId
	 * @return
	 */
	public Student getStudentByClassesId(String classesId);
	
	/**
	 * 数量
	 * @param schoolName
	 * @param grade
	 * @param className
	 * @return
	 */
	public int getNoticeCount(String noticeId,String schoolName, String grade, String className);
	
	/**
	 * 二级联动
	 * @param schoolId
	 * @return
	 */
	public List<Classes> getListGradeBySchoolId(String schoolId);
	
	public List<Classes> getListClassNameByGradeName(String grade);

	/**
	 * 通过schoolId  查询 employee
	 * @param schoolId
	 * @return
	 */
	public List<Employee> getEmployeeBySchoolId(String schoolId);
	
	/**
	 * 通过 classId  查询 所有学生、
	 * @param classId
	 * @return
	 */
	public List<Student> getStudentListByClassesId(String classId);
	
	/**
	 * 通过studentId  查询家长
	 * @param studentId
	 * @return
	 */
	public List<Parentsstudent> getParentListByStudentId(String studentId);
	
	/**
	 * 未报名
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<NoticeSub> getNoticeSubLimit(String noticeId,Integer start, Integer limit);
	public int getNoticeSubCount(String noticeId);
	/**
	 * 2016年9月30日下午12:19:42
	 *  龚杰
	 *  注释：通过classId  查询 普通老师
	 */
	public List<Classteacher> getClassteacherListByClassId(String clasId);
	/**
	 * 2016年10月7日下午12:32:41
	 *  龚杰
	 *  注释：班级 全查
	 */
	public List<Classes> getAllClasses();
	/**
	 * 2016年10月10日上午9:28:06
	 *  龚杰
	 *  注释：
	 */
	public int getNoticeDraftCount(String ssName);
	/**
	 * 2016年10月17日上午11:46:48
	 *  阳朔
	 *  注释:
	 */
	public List<NoticeSub> getNotice2Limit(String noticeId, Integer integer,
			Integer integer2);
	/**
	 * 2016年10月17日下午1:35:54
	 *  阳朔
	 *  注释:
	 */
	public List<NoticeSub> getNoticeByNoticeId(String noticeId);
	/**
	 * 2016年10月17日下午2:52:15
	 *  阳朔
	 *  注释:查询已报名 新改
	 */
	public List<SignNotice> getSignNoticeLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2);
	/**
	 * 2016年10月17日下午2:58:41
	 *  阳朔
	 *  注释:查询已报名数量 新改
	 */
	public int getSignNoticeCount(String noticeId, String schoolName,
			String grade, String className);
	/**
	 * 2016年10月17日下午3:07:51
	 *  阳朔
	 *  注释:
	 */
	public Parentsstudent getParentsstudentByStudentId(String studentId);
	/**
	 * 2016年10月17日下午3:14:22
	 *  阳朔
	 *  注释:查询未报名
	 */
	public List<SignNotice> getSignNoticeByNoticeId(String noticeId);
	/**
	 * 2016年10月23日下午4:29:01
	 *  阳朔
	 *  注释:
	 */
	public List<Classes> getClassess(String schoolId);
	/**
	 * 2016年10月23日下午4:31:23
	 *  阳朔
	 *  注释:
	 */
	public List<NoticeSub> getNoticeSubLimit(String classIds, String ssName,
			Integer integer, Integer integer2);
	/**
	 * 2016年10月24日下午9:03:45
	 *  阳朔
	 *  注释:
	 */
	public Employee getEmployee(String userId);
	/**
	 * 2016年10月24日下午9:04:59
	 *  阳朔
	 *  注释:
	 */
	public List<Classteacher> getClassteacherList(String employeeId);
	/**
	 * 2016年10月24日下午9:16:50
	 *  阳朔
	 *  注释:
	 */
	public List<Classes> getClassessById(String employeeId);
	/**
	 * 2016年10月24日下午9:41:53
	 *  阳朔
	 *  注释:
	 * @param schoolId 
	 */
	public List<Classes> getClassessByGrade(Integer grade, String schoolId);
	/**
	 * 2016年10月27日下午7:52:02
	 *  阳朔
	 *  注释:已支付
	 */
	public List<SignPrice> getSignPriceLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2);
	/**
	 * 2016年10月27日下午8:00:22
	 *  阳朔
	 *  注释:
	 */
	public int getSignPriceCount(String noticeId, String schoolName,
			String grade, String className);
	/**
	 * 2016年10月27日下午8:07:51
	 *  阳朔
	 *  注释:
	 */
	public List<NoticeSub> getNoticePriceLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2);
	/**
	 * 2016年10月27日下午8:09:36
	 *  阳朔
	 *  注释:支付
	 */
	public int getNoticeSubPriceCount(String noticeId,String schoolName,String grade,String className);
	/**
	 * 2016年10月28日下午4:24:57
	 *  阳朔
	 *  注释:
	 */
	public List<SignPrice> getNoSignPriceLimit(String noticeId,
			String schoolName, String grade, String className, Integer integer,
			Integer integer2);
	/**
	 * 2016年10月28日下午4:30:19
	 *  阳朔
	 *  注释:
	 */
	public int getNoPriceCount(String noticeId, String schoolName,
			String grade, String className);
	/**
	 * 2016年10月30日下午4:37:32
	 *  阳朔
	 *  注释:
	 */
	public SysUserRole getSysUserRoleByEmpUserId(String userId);
	/**
	 * 2016年10月30日下午4:38:41
	 *  阳朔
	 *  注释:
	 */
	public List<SysRolePower> getSysRolePowerByEmp_RoleId(String emp_RoleId);
	/**
	 * 2016年11月1日下午9:06:22
	 *  阳朔
	 *  注释:
	 * @param schoolId 
	 */
	public List<Classes> getClassByGrade(int i, String schoolId);
	/**
	 * 2016年11月9日上午11:06:54
	 *  阳朔
	 *  注释:
	 */
	public List<Classes> getClassBySchoolId(String schoolId);
	/**
	 * 2016年12月8日下午5:55:17
	 *  阳朔
	 *  注释:
	 */
	public List<NoticeSub> getNoticeSubDraftLimit(String classIds,
			String ssName, Integer integer, Integer integer2);
	

}
