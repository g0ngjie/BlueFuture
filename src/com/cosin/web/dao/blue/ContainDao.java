package com.cosin.web.dao.blue;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.Contain;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysUser;


@Repository
public class ContainDao extends BaseDao implements IContainDao{

	/**
	 * 获取正文
	 */
	@Override
	public Contain getContainList() {
		
		String sql = "from Contain as a where 1 = 1";
		List<Contain> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 园区数量
	 */
	@Override
	public int getSchoolNum() {
		String sql = "select count(*) from School as a where a.isDel = 0";
		return queryCount(sql);
	}

	/**
	 * 班级数量
	 */
	@Override
	public int getClassesNum() {
		String sql = "select count(*) from Classes as a where a.isDel = 0";
		return queryCount(sql);
	}

	/**
	 * 班主任 数量
	 * @return 
	 */
	@Override
	public int getClassTeacherNum() {

		String sql = "select count(*) from Employee as a where isDel = 0";
		
		return queryCount(sql);
	}

	/**
	 * 普通老师数量
	 * @return 
	 */
	@Override
	public int getOtherTeacherNum() {

		String sql = "select count(*) from SysUser as a where isDel = 0 and a.sysRole.rolerName = '普通老师'";
		return queryCount(sql);
	}

	/**
	 * 学生数量
	 */
	@Override
	public int getStudentNum() {
		String sql = "select count(*) from Student as a where a.isDel = 0";
		return queryCount(sql);
	}

	/**
	 * 家长数量
	 */
	@Override
	public int getParentNum() {
		String sql = "select count(*) from Parents as a where a.isDel = 0";
		return queryCount(sql);
	}

	/**
	 * 保存
	 */
	@Override
	public void saveContain(Contain contain) {
		super.save(contain);
	}

	/**
	 * 删除内容
	 */
	@Override
	public void delContain(Contain contain1) {
		super.delete(contain1);
	}

	@Override
	public Contain getContain() {
		String sql = "from Contain where 1 = 1 ";
		List<Contain> list = query(sql);
			if(list.size() > 0)
				return list.get(0);
		return null;
	}

	/**
	 * 
	 */
	@Override
	public SysUser getSysUserById(String userId) {
		String sql = "from SysUser as a where a.isDel = 0 and a.userId = '" + userId + "'";
		List<SysUser> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 
	 */
	@Override
	public void saveSysUser(SysUser sysUser) {
		super.save(sysUser);
		
	}

	/**
	 * 2016年10月23日下午3:08:30
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysUser> getSysUserByPhone(String mobile) {
		// TODO Auto-generated method stub
		String sql = "from SysUser as a where a.isDel = 0 and a.loginName = '" + mobile + "'";
		List<SysUser> list = query(sql);
		return list;
	}

	/**
	 * 2016年10月23日下午3:10:59
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void saveUser(SysUser sysUser) {
		// TODO Auto-generated method stub
		super.save(sysUser);
	}

	/**
	 * 2016年10月23日下午3:55:00
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<Employee> getEmployees(String userId) {
		// TODO Auto-generated method stub
		String sql = "from Employee as a where a.isDel = 0 and a.sysUserByUserId = '" + userId + "'";
		List<Employee> list = query(sql);
		return list;
	}

	/**
	 * 2016年10月23日下午3:59:23
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getClassesNum(String schoolsId) {
		// TODO Auto-generated method stub
		String sql = "select count(*) from Classes as a where a.isDel = 0 and a.school in ("+ schoolsId +")";
		return queryCount(sql);
	}

	/**
	 * 2016年10月23日下午4:02:56
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getTeacherBySchool(String schoolsId) {
		// TODO Auto-generated method stub
		String sql = "select count(*) from Employee as a where a.isDel = 0 and a.school in ("+ schoolsId +")";
		return queryCount(sql);
	}

	/**
	 * 2016年10月23日下午4:06:49
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getStudentsBySchool(String schoolsId) {
		// TODO Auto-generated method stub
		String sql = "select count(*) from Student as a where a.isDel = 0 and a.school in ("+ schoolsId +")";
		return queryCount(sql);
	}

	/**
	 * 2016年10月23日下午4:09:05
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<Student> getParentStudents(String schoolsId) {
		// TODO Auto-generated method stub
		String sql = "from Student as a where a.isDel = 0 and a.school in ("+ schoolsId +")";
		return query(sql);
	}

	/**
	 * 2016年10月23日下午4:10:58
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<Parentsstudent> getParentsstudent(String studentId) {
		// TODO Auto-generated method stub
		String sql = "from Parentsstudent as a where a.student ='"+ studentId +"'";
		return query(sql);
	}
}
