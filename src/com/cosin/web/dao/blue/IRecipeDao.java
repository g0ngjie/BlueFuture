package com.cosin.web.dao.blue;

import java.util.List;

import com.cosin.web.entity.Recipe;
import com.cosin.web.entity.RecipeRecord;
import com.cosin.web.entity.RecipeSchool;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysUser;

public interface IRecipeDao {

	// 分页
	public List getRecipeLimit(String monday,String fri, int start, int limit);

	// 数量
	public int getRecipeCount(String monday,String fri);

	// 获取noticeId
	public RecipeRecord getRecipeId(String recipeId);

	// 添加
	// public void saveRecipe(Activity recipe);
	// 删除
	 public void recipeDelId(String recipeId);

	// 根据id查询
	public SysUser getUser2(String user);

	//草稿箱列表查询 
	List<RecipeRecord> getDraftLimit(String monday,String fri, int start, int limit);

	//草稿箱数量
	int getDraftCount(String SsName);

	/**
	 * 
	 * @param userId
	 * @return
	 */
	public SysUser getSysUserById(String userId);

	/**
	 * 保存食谱
	 * @param recipeRecord
	 */
	public void saveRecipeRecord(RecipeRecord recipeRecord);

	/**
	 * 保存 食谱内容 五天
	 * @param recipe
	 */
	public void saveRecipe(Recipe recipe);

	/**
	 * 根据id 查school 园区
	 * @param key
	 * @return
	 */
	public School getSchoolById(String key);

	/**
	 * 保存 中间表
	 * @param recipeSchool
	 */
	public void saveRecipeSchool(RecipeSchool recipeSchool);

	/**
	 * 通过id 获取对象
	 * @param recipeRecordId
	 * @return
	 */
	public List<RecipeSchool> getRecipeSchoolById(String recipeRecordId);

	/**
	 * 通过recipeId 查询 对象
	 * @param recipeId
	 */
	public RecipeRecord getRecipeRecordById(String recipeId);

	/**
	 * 根据 recipeRecordId 查询  食谱 集合 
	 * @param recipeId
	 * @return
	 */
	public List<Recipe> getRecipeById(String recipeId);

	/**
	 * 真删 RecipeSchool 中间表
	 * @param recipeSchoolList
	 */
	public void delRecipeSchool(List<RecipeSchool> recipeSchoolList);

	/**
	 * 真删 操作
	 * @param recipeList
	 */
	public void delRecipe(List<Recipe> recipeList);

	/**
	 * 2016年10月10日下午3:07:09
	 *  龚杰
	 *  注释：
	 */
	public int getDraftCount(String monday, String fri);

	/**
	 * 2016年10月23日下午4:59:10
	 *  阳朔
	 *  注释:
	 */
	public List<RecipeSchool> getRecipeSchoolLimit(String schoolsId,
			String monday, String fri, Integer integer, Integer integer2);

	/**
	 * 2016年12月8日下午5:26:09
	 *  阳朔
	 *  注释:
	 */
	public List<RecipeSchool> getRecipeDraftSchoolLimit(String schoolsId,
			String monday, String fri, Integer integer, Integer integer2);




}
