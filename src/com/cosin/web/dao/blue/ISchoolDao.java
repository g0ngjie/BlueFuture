package com.cosin.web.dao.blue;

import java.util.List;

import com.cosin.web.entity.Classes;
import com.cosin.web.entity.EmpSchool;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysArea;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

public interface ISchoolDao {

	
	//获取园区管理数据
	List<School> getSchool( int start, int limit);

	//获取园区管理数据分页信息
	int getRolefenye(String ssName);

	//编辑园区School
	School getBySchool(String schoolId);

	//根据SchoolId 获取园区数据
	School getBySchoolId(String schoolId);

	//保存园区数据
	void save(School school);


	//查询园区是否有重复dao层
	List<School> chaSchool(String schoolName);

	//删除园区
	void delSchool(String schoolId);

	Employee getEmployeeById(String employeeId);
	//通过id获取城市

	List<SysArea> getSysArea();

	List<SysArea> getSysCity(String cityId);

	List<Employee> getEmployee(String schoolId);

	SysUser findUserByUserId(String userId);

	SysArea getSysAreaById(String cityId);

	SysArea getSysCityById(String cityId);

	/**
	 * 2016年9月30日下午1:33:04
	 *  龚杰
	 *  注释：保存
	 */
	void saveObj(Object obj);

	/**
	 * 2016年10月10日下午12:08:08
	 *  龚杰
	 *  注释：
	 */
	Employee getEmployeeByUserId(String employeeId);

	/**
	 * 2016年10月21日下午4:59:11
	 *  阳朔
	 *  注释:
	 */
	List<SysSubrole> getSysSubroleList();

	/**
	 * 2016年10月21日下午5:02:46
	 *  阳朔
	 *  注释:
	 */
	void saveSysRoleSchoolPower(SysRoleSchoolPower sysRoleSchoolPower);

	/**
	 * 2016年10月22日下午1:37:20
	 *  阳朔
	 *  注释:
	 */
	SysUserRole getSysUserRoleByUserId(String userId);

	/**
	 * 2016年10月22日下午1:40:43
	 *  阳朔
	 *  注释:
	 */
	SysSubrole getSysSubRoleByRoleId(String roleId);

	/**
	 * 2016年10月22日下午1:55:42
	 *  阳朔
	 *  注释:
	 */
	SysRoleSchoolPower getRoleSchoolPowerByRoleId(String roleId);

	/**
	 * 2016年10月24日下午6:09:22
	 *  阳朔
	 *  注释:查询所有角色
	 */
	List<SysRole> getSysRoleAll();

	/**
	 * 2016年10月24日下午8:03:35
	 *  阳朔
	 *  注释:
	 */
	List<School> getSchool(String schoolsId, Integer integer, Integer integer2);

	/**
	 * 2016年10月26日下午4:38:56
	 *  阳朔
	 *  注释:
	 */
	List getSchoolfenyeBySchoolId(String schoolsId, String ssName);

	/**
	 * 2016年11月15日下午12:10:09
	 *  阳朔
	 *  注释:
	 */
	List<Classes> getClassesBySchoolId(String schoolId);

	/**
	 * 2016年11月15日下午12:17:23
	 *  阳朔
	 *  注释:
	 */
	List<Employee> getEmployeeBySchoolId(String schoolId);


	
}
