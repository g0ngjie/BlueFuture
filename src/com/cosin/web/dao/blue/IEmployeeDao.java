package com.cosin.web.dao.blue;

import java.util.List;

import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Classteacher;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

public interface IEmployeeDao {

	/**
	 * 分页
	 */
	public List getEmployeeLimit(String employeeId,String SsTel, int start, int limit);

	/**
	 * 数量
	 */
	public int getEmployeeCount(String SsName,String SsTel);

	/**
	 * 获取角色名称
	 */
	public List<SysRole> getlistRole();

	/**
	 * 通过roleId  获取 类型
	 */
	public List<SysSubrole> getlistSubRole(String roleId);
	
	/**
	 * 通过userId 获取 sysUser
	 */
	public SysUser getSysUserById(String userId);
	
	/**
	 * 根据roleId 查询中间表sysroleschoolpower
	 */
	public List<SysRoleSchoolPower> getListsysRoleSchoolPower(String roleId);

	/**
	 * 通过schoolId 获取 school
	 */
	public School getSchoolById(String schoolId);

	/**
	 * 通过roleId 查出 sysRole表
	 */
	public SysRole getSysRoleById(String roleId);

	/**
	 * 通过subRoleId 查出 sysSubRole表
	 */
	public SysSubrole getSysSubRoleById(String subRoleId);

	/**
	 * 职工表保存
	 */
	public void save(Employee employee);

	/**
	 * 保存 职工到sysUser表
	 */
	public void saveSysUser(SysUser sysUser);

	/**
	 * 删除操作 Employee
	 */
	public void delEmployeeById(String delKeys);

	/**
	 * 根据Id 查出employee
	 */
	Employee getEmployeeById(String employeeId);

	/*
	 * 
	 */
	public SysSubrole getSysSubRoleByName(String cellStr);

	/**
	 * 导入 园区 字段查询
	 * @param cellStr
	 * @return
	 */
	public School getByName(String cellStr);

	/**
	 * 导入  角色 字段查询
	 * @param cellStr
	 * @return
	 */
	public SysRole getSysRoleByName(String cellStr);

	/**
	 * 通过roleId 查询 sysUser
	 * @param roleId
	 * @return
	 */
	public SysUser getSysUserByRoleId(String roleId);

	/**
	 * 2016年9月29日下午5:04:58
	 *  龚杰
	 *  注释：保存
	 */
	public void saveObj(Object obj);

	/**
	 * 2016年9月29日下午5:56:59
	 *  龚杰
	 *  注释：通过userId 查询 role
	 */
	public List<SysUserRole> getSysUserRoleByUserId(String useId);

	/**
	 * 2016年9月29日下午6:53:07
	 *  龚杰
	 *  注释：真删除
	 */
	public void delObj(Object obj);

	/**
	 * 2016年10月10日下午6:32:29
	 *  龚杰
	 *  注释：
	 */
	public SysSubrole getSysSubRoleByRoleId(String roleId);

	/**
	 * 2016年10月11日上午11:59:16
	 *  阳朔
	 *  注释:通过roleId 查询sysUserRole
	 */
	public SysUserRole getSysUserRoleByRoleId(String roleId);

	/**
	 * 2016年10月14日下午2:29:48
	 *  阳朔
	 *  注释:查询 是否存在
	 */
	public SysUser chaSysUserByLogin(String tel);

	/**
	 * 2016年10月17日上午10:57:22
	 *  阳朔
	 *  注释:查询所有type = 1的 家长端
	 */
	public SysUser getsysUserParentByLoginName(String loginName);

	/**
	 * 保存
	 * @param sysUserRole
	 */
	public void saveUserRole(SysUserRole sysUserRole);

	/**
	 * 2016年10月20日下午4:44:25
	 *  阳朔
	 *  注释:查询家长是否为老师 存在
	 */
	public SysUser getParenUseByLogin(String tel);

	/**
	 * 2016年10月22日下午3:09:30
	 *  阳朔
	 *  注释:
	 */
	public SysUser getSysUserBySubRoleId(String subRoleId);

	/**
	 * 2016年10月22日下午3:24:34
	 *  阳朔
	 *  注释:
	 */
	public SysRoleSchoolPower getSysRoleSchoolPowerByRoleId(String roleId);

	/**
	 * 2016年10月22日下午3:58:49
	 *  阳朔
	 *  注释:
	 */
	public List<Employee> getEmployeeBySchoolId(String schoolId, String ssName,
			String ssTel, Integer integer, Integer integer2);

	/**
	 * 2016年10月22日下午4:16:00
	 *  阳朔
	 *  注释:
	 */
	public Employee getEmployeeByIUserId(String userId);

	/**
	 * 2016年10月22日下午4:44:17
	 *  阳朔
	 *  注释:
	 */
	public List<SysRoleSchoolPower> getSysRoleSchoolPowerBySchoolId(
			String schoolId);

	/**
	 * 2016年10月23日下午8:09:33
	 *  阳朔
	 *  注释:
	 */
	public void saveSchool(School school);

	/**
	 * 2016年10月23日下午9:28:37
	 *  阳朔
	 *  注释:
	 */
	public void delSysUserRole(SysUserRole sysUser);

	/**
	 * 2016年10月23日下午9:30:15
	 *  阳朔
	 *  注释:
	 */
	public SysUserRole getSysUserRoleByUseri(String userId);

	/**
	 * 2016年10月26日下午1:48:57
	 *  阳朔
	 *  注释:
	 */
	public SysUserRole chaSysUserRoleByUserId(String userId);

	/**
	 * 2016年10月26日下午2:59:18
	 *  阳朔
	 *  注释:
	 */
	public int getEmployeeCountBySchoolId(String schoolId, String ssName,
			String ssTel);

	/**
	 * 2016年10月27日下午5:17:43
	 *  阳朔
	 *  注释:
	 */
	public School getSchoolByName(String schoolNam);

	/**
	 * 2016年10月27日下午6:52:15
	 *  阳朔
	 *  注释:
	 */
	public Classteacher getClassesTeacherByEmId(String employeeId);

	/**
	 * 2016年10月27日下午6:55:00
	 *  阳朔
	 *  注释:
	 */
	public Classes getClassesByEmId(String employeeId);

	/**
	 * 2016年10月30日下午12:54:04
	 *  阳朔
	 *  注释: 总园级别 数量查询 改动版
	 */
	List getEmployeeCountGai(String SsName, String SsTel);

	/**
	 * 2016年10月30日下午1:05:19
	 *  阳朔
	 *  注释: 分页数量 改 角色级别
	 */
	List<Employee> getEmployeeBySchoolIdCount(String schoolId, String ssName,
			String ssTel);

	/**
	 * 2016年11月23日上午11:28:32
	 *  阳朔
	 *  注释:
	 */
	public List<SysRole> getlistRoleByZY();

	/**
	 * 2016年11月23日下午12:35:33
	 *  阳朔
	 *  注释:
	 * @param integer2 
	 * @param integer 
	 * @param ssTel 
	 * @param ssName 
	 */
	public List<Employee> getEmployeeBySchoolNullLimit(String ssName, String ssTel, Integer integer, Integer integer2);

	/**
	 * 2016年11月23日下午12:51:14
	 *  阳朔
	 *  注释:
	 */
	public List getEmployeeCountZY(String ssName, String ssTel);


	
	
	
	
	
}
