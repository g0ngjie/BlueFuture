package com.cosin.web.dao.blue;

import java.util.List;

import javax.management.relation.Role;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;


@Repository
public class RoleDao extends BaseDao implements IRoleDao{

	//分页
	@Override
	public List<SysRole> getRoleLimit(int start, int limit) {
		String sql="from SysRole as a where a.isDel=0 order by a.createDate desc";
	
		return query(sql,start,limit);
		
	}
	// 数量 获取分页 数据 
	@Override
	public int getRoleCount() {
		String sql="select count(*) from  SysRole as a where a.isDel=0 ";
		
		return queryCount(sql);
		
	}
	/**
	 * 根据userId  查询 sysUser表
	 * @return 
	 */
	@Override
	public SysUser findByUserId(String userId) {
		String sql="from SysUser as a where a.userId='"+userId+"'and a.isDel=0";
		List<SysUser> list=query(sql);
		if(list.size()>0){
			return list.get(0);
		}
		return null;
		
	}
	
	/**
	 * 保存
	 */
	@Override
	public void saveRole(SysRole sysRole) {
		super.save(sysRole);
	}

	/**
	 * 真删除
	 */
	@Override
	public void delRole(String roleId) {
		SysRole sysRole=getRoleById(roleId);
		sysRole.setIsDel(1);
		super.save(sysRole);
//		super.delete(sysRole);
	}
	
	/**
	 * 真删除  中间表  学校 关联
	 */
//	@Override
//	public void delRole(SysRoleSchoolPower sysRoleSchoolPower) {
//		super.delete(sysRoleSchoolPower);
//	}
	/**
	 * 真删  根据关联的 roleId  删除 所有的类型
	 */
	@Override
	public void delSysSubrole(List<SysSubrole> sysSubrole) {
		for (int i = 0; i < sysSubrole.size(); i++) {
			
			super.save(sysSubrole.get(i));
		}
//		super.delete(sysSubrole);
	}
	
	//获取roleId 删除是可以用
	@Override
	public SysRole getRoleById(String roleId) {
		String sql = "from SysRole as a  where a.roleId='"+roleId+"' and a.isDel = 0 order by a.createDate desc"  ;
		List<SysRole> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;	
	}
	
	/**
	 * 保存sysSubRole表
	 */
	@Override
	public void saveSubrole(SysSubrole sysSubrole) {
		super.save(sysSubrole);
	}
	
	/**
	 * 查询中间表
	 */
	@Override
	public List<SysRoleSchoolPower> getsysRoleSchoolPowerByRoleId(String roleId) {
		String sql = "from SysRoleSchoolPower as a  where a.sysRole.roleId='" + roleId + "'";
		return query(sql);
	}
	
	/**
	 * 根据roleId 查询 sysSubrole表里面的 角色级别
	 */
	@Override
	public SysSubrole getsysSubRoleById(String roleId) {
		String sql = "from SysSubrole as a  where a.sysRole='"+roleId+"' and a.isDel = 0 "  ;
		List<SysSubrole> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}
	
	/**
	 * 根据 roleId  查出 类型表 关联的集合
	 */
	@Override
	public List<SysSubrole> getsysSubRoleListById(String roleId) {
		String sql = "from SysSubrole as a  where a.sysRole='"+roleId+"' and a.isDel = 0 "  ;
		return query(sql);
	}
	
	/**
	 * 通过 园区Id 查询 园区
	 */
	@Override
	public School getSchoolById(String key) {
		String sql = "from School as a  where a.schoolId='"+key+"' and a.isDel = 0 "  ;
		List<School> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}
	
	/**
	 * 保存外键 表
	 */
	@Override
	public void saveSysRoleSchoolPower(SysRoleSchoolPower sysRoleSchoolPower) {
		super.save(sysRoleSchoolPower);
	}
	
	/**
	 * 保存子角色表
	 */
	@Override
	public void saveSysSubrole(SysSubrole sysSubrole) {
		super.save(sysSubrole);
	}
	@Override
	public void saveObj(Object obj) {
		
		super.save(obj);
	}
	/* 
	 * 王思明
	 * 2016年9月21日
	 * 上午11:59:08
	 */
	@Override
	public void delObj(Object obj) {
		
		super.delete(obj);
		
	}
	/* 
	 * 王思明
	 * 2016年9月21日
	 * 下午12:00:00
	 */
	@Override
	public List<SysRolePower> getBySysRolePower(String roleId) {
		
		String sql = "from SysRolePower as a where a.sysRole.roleId='"+ roleId +"'";
		return query(sql);
		
	}
	
	/**
	 * 通过roleId  查询 subrole
	 */
	@Override
	public List<SysSubrole> getsysSubRoleByRoleId(String roleId) {
		String sql = "from SysSubrole as a where a.sysRole.roleId='"+ roleId +"'";
		return query(sql);
	}
	/**
	 * 2016年10月9日下午12:03:54
	 *  龚杰
	 *  注释：
	 */
	@Override
	public List<SysRolePower> getsysRolePowerByRoleId(String roleId) {
		String sql = "from SysRolePower as a where 1 = 1 and a.sysRole = '" + roleId + "'";
		return query(sql);
	}
	/**
	 * 2016年10月17日下午5:48:49
	 *  阳朔
	 *  注释:查询 角色名称是否存在
	 */
	@Override
	public SysRole chaRolebyRoleName(String roleName) {
		String sql = "from SysRole as a where a.isDel = 0 and a.rolerName = '" + roleName + "'";
		List<SysRole> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}
	/**
	 * 2016年10月22日上午1:56:42
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysUser> chaSysUserBySubId(String subRoleId) {
		String sql = "from SysUser as a where a.isDel = 0 and a.sysSubrole = '" + subRoleId + "'";
		return query(sql);
	}
	/**
	 * 2016年10月22日上午2:49:29
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void delSub(String subRId) {
		String sql = "from SysSubrole as a where a.isDel = 0 and a.subRoleId = '" + subRId + "'";
		List<SysSubrole> list = query(sql);
		for (int i = 0; i < list.size(); i++) {
			SysSubrole subrole = list.get(i);
			super.delete(subrole);
		}
		
	}
	/**
	 * 2016年10月22日上午3:12:04
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysSubrole getsysSubRoleListBysuId(String subRId) {
		// TODO Auto-generated method stub
		String sql = "from SysSubrole as a where isDel=0 and subRoleId='" + subRId + "'";
		List<SysSubrole> list = query(sql);
		return list.get(0);
	}
	/**
	 * 2016年10月22日上午3:22:40
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void delSysSubrole(SysSubrole subrole) {
		// TODO Auto-generated method stub
		super.delete(subrole);
	}
	/**
	 * 2016年10月23日下午5:49:48
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysRoleSchoolPower> getRoleLimit(String schoolsId, Integer integer,
			Integer integer2) {
		// TODO Auto-generated method stub
		String sql="from SysRoleSchoolPower as a where a.school in ("+ schoolsId +")";
		
		return query(sql,integer,integer2);
	}
	/**
	 * 2016年10月23日下午8:50:35
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysUserRole> getSysUserRoleList(String roleId) {
		// TODO Auto-generated method stub
		String sql="from SysUserRole as a where a.sysRole.isDel = 0 and a.sysRole='"+ roleId +"'";
		return query(sql);
	}
	/**
	 * 2016年10月26日下午4:52:56
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysRoleSchoolPower> getRoleCountBySchoolId(String schoolsId) {
		String sql="from SysRoleSchoolPower as a where a.school in ("+ schoolsId +")";
		
		return query(sql);
	}
	
}
