package com.cosin.web.dao.blue;

import java.util.List;

import javax.management.relation.Role;

import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

public interface IRoleDao {

	//分页
    public List<SysRole> getRoleLimit(int start, int limit);
    //数量
    public int getRoleCount();
    //根据登陆着id 查询 sysUser表
    public SysUser findByUserId(String userId);
    
    /**
     * 保存
     * @param sysRole
     */
    public void saveRole(SysRole sysRole);
    
    //删除
    public void delRole(String roleId);
    
    //获取roleId 删除时可以用
    public SysRole getRoleById(String roleId);
    
	/**
	 * 保存subRole表
	 * @param sysSubrole
	 */
	public void saveSubrole(SysSubrole sysSubrole);
	/**
	 * 查询中间表
	 * @param roleId
	 * @return
	 */
	public List<SysRoleSchoolPower> getsysRoleSchoolPowerByRoleId(String roleId);
	/**
	 * 根据id 查询 sysSubrole表里面的 角色级别
	 */
	public SysSubrole getsysSubRoleById(String roleId);
	/**
	 * 通过 园区Id 查询 园区
	 * @param key
	 * @return
	 */
	public School getSchoolById(String key);
	/**
	 * 保存外键表
	 * @param sysRoleSchoolPower
	 */
	public void saveSysRoleSchoolPower(SysRoleSchoolPower sysRoleSchoolPower);
	/**
	 * 保存 子角色表
	 * @param sysSubrole
	 */
	public void saveSysSubrole(SysSubrole sysSubrole);

	/**
	 * 根据roleId 查出 类型表 集合
	 * @param roleId
	 * @return
	 */
	public List<SysSubrole> getsysSubRoleListById(String roleId);
	/**
	 * 跟据关联的 roleId  删除 所有 类型
	 * @param sysSubrole
	 */
	public void delSysSubrole(List<SysSubrole> sysSubrole);
	
	public void saveObj(Object obj);
	/**
	 * 王思明 
	 * 2016年9月21日
	 * 上午11:58:56
	 */
	public void delObj(Object obj);
	/**
	 * 王思明 
	 * 2016年9月21日
	 * 上午11:59:52
	 */
	public List<SysRolePower> getBySysRolePower(String roleId);
	
	/**
	 * 通过 roleId  删除subrole
	 * @param roleId
	 * @return
	 */
	public List<SysSubrole> getsysSubRoleByRoleId(String roleId);
	/**
	 * 2016年10月9日下午12:03:07
	 *  龚杰
	 *  注释：
	 */
	public List<SysRolePower> getsysRolePowerByRoleId(String roleId);
	/**
	 * 2016年10月17日下午5:47:39
	 *  阳朔
	 *  注释:查询 角色名称是否存在
	 */
	public SysRole chaRolebyRoleName(String roleName);
	/**
	 * 2016年10月22日上午1:56:37
	 *  阳朔
	 *  注释:
	 */
	public List<SysUser> chaSysUserBySubId(String subRoleId);
	/**
	 * 2016年10月22日上午2:49:23
	 *  阳朔
	 *  注释:
	 */
	public void delSub(String subRId);
	/**
	 * 2016年10月22日上午3:11:41
	 *  阳朔
	 *  注释:
	 */
	public SysSubrole getsysSubRoleListBysuId(String subRId);
	/**
	 * 2016年10月22日上午3:22:33
	 *  阳朔
	 *  注释:
	 */
	public void delSysSubrole(SysSubrole subrole);
	/**
	 * 2016年10月23日下午5:49:43
	 *  阳朔
	 *  注释:
	 */
	public List<SysRoleSchoolPower> getRoleLimit(String schoolsId, Integer integer,
			Integer integer2);
	/**
	 * 2016年10月23日下午8:50:28
	 *  阳朔
	 *  注释:
	 */
	public List<SysUserRole> getSysUserRoleList(String roleId);
	/**
	 * 2016年10月26日下午4:52:49
	 *  阳朔
	 *  注释:
	 */
	public List<SysRoleSchoolPower> getRoleCountBySchoolId(String schoolsId);

}
