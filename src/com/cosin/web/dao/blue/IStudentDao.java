package com.cosin.web.dao.blue;

import java.util.List;

import com.cosin.web.entity.Classes;
import com.cosin.web.entity.Classteacher;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.Parents;
import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.School;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;

public interface IStudentDao {

	//分页
    public List<Student> getStudentLimit(String schoolName,String SsName,String intoYear,String gradeName,String classesName, int start, int limit);
    //数量
    public int getStudentCount(String schoolName,String SsName,String intoYear,String gradeName,String classesName);
    //获取noticeId
  	public Student getStudentId(String studentId);
    //添加 
//  	public void saveStudent(Activity student);
  	 //删除
    public void studentDelId(String studentId);
    //查询用户表 user
//    public List getUser();
    //根据id查询
    public SysUser getUser2(String user);
    
    /**
     * 添加 学生 student
     */
    public void saveStudent(Student student);
    
    /**
     * 查询班级列表
     * @return
     */
	public List<School> getSelectSchool();
	/**
	 * 下拉弹窗 查询 班级Classes
	 * @return
	 */
	public List<Classes> getselectClasses();
	/**
	 * 下拉弹窗添加选项里面  
	 * 获取园区 id
	 * @param schoolId
	 * @return 
	 */
	public School getSchoolById(String schoolId);
	/**
	 * 下拉弹窗  添加 选项里面
	 * 获取 班级 id
	 * @param classesId
	 * @return
	 */
	public Classes getClassesById(String classesId);
	/*
	 * 班级全查
	 */
	public Classes selectClasses();
	/**
	 * 保存 班级 
	 * @param classes1
	 */
	public void save(Classes classes1);
	/**
	 * 保存 家长 到sysuser表
	 * @param sysuser
	 */
	public void save(SysUser sysuser);
	/**
	 * 保存 关系 亲戚 表
	 * @param parents
	 */
	public void save(Parents parents);
	/**
	 * 保存 中间表 关联  parentId  studentId
	 * @param parentsstudent
	 */
	public void save(Parentsstudent parentsstudent);
	/**
	 * 创建人 Id  登陆者
	 * @param userId
	 * @return
	 */
	public SysUser findUserByUserId(String userId);
	/**
	 * 修改选项里面 查询 学生表 
	 * @param studentId
	 * @return 
	 */
	public Student findStudentById(String studentId);
	public SysUser getParentById(String parentId);
	public Parents getRelationById(String relationId);
	
	/**
	 * 通过userId  查询 user
	 * @param userId
	 * @return
	 */
	public SysUser getSysUserById(String userId);
	
	/**
	 * 班级 全查
	 * @return
	 */
	public List<Classes> getClasses();
	
	/**
	 * 根据届数 查年纪
	 * @param intoYear
	 * @return
	 */
	public List<Classes> getListGradeByIntoyear(String schoolId,String intoYear);
	
	/**
	 * 通过schoolId  获取 classes 集合对象
	 * @param schoolId
	 * @return
	 */
	public List<Classes> getClassesBySchoolId(String schoolId);
	
	/**
	 * 根据grade 查询 className
	 * @param grade
	 * @return
	 */
	public List<Classes> getListClassNameByGrade(String schoolId,String intoYear,String grade);
	
	/**
	 * 通过 className 获取 class 对象
	 * @param classesName
	 * @return
	 */
	public Classes getClassesByClassName(String classesName);
	
	/**
	 * 通过studentId 获取 中间表 
	 * @param studentId
	 * @return
	 */
	public List<Parentsstudent> getParentsstudentByStudentId(String studentId);
	
	/**
	 * 删除中间表
	 * @param parentsstudent
	 */
	public void delParentsstudent(List<Parentsstudent> parentsstudent);
	
	/**
	 * 根据园区名查询
	 * @param cellStr
	 * @return
	 */
	public School getSchoolByName(String cellStr);
	
	
	/**
	 * 根据 多条查询 classes
	 * @param string
	 * @param string2
	 * @param string3
	 * @return
	 */
	public Classes getClassesByDataName(String schoolId,String string, String string2,
			String string3);
	
	/**
	 * 通过 多条数据 查询班级
	 * @param schoolId
	 * @param intoYear
	 * @param gradeName
	 * @param classesName
	 * @return
	 */
	public Classes getClassesByData(String schoolId, String intoYear,
			String gradeName, String classesName);
	
	/**
	 * 园区 全查
	 * @return
	 */
	public List<School> getListSchool();
	/**
	 * 2016年10月14日下午2:23:41
	 *  阳朔
	 *  注释:查询 是否存在
	 */
	public SysUser getSysUserParentByTel(String tel);
	/**
	 * 2016年10月14日下午4:11:40
	 *  阳朔
	 *  注释:
	 */
	public List<Parentsstudent> getParentsstudentByParentId(String parentId);
	/**
	 * 2016年10月14日下午6:11:09
	 *  阳朔
	 *  注释:通过studentName,classesId,schoolId 查student 是否重复
	 */
	public Student chaStudentByData(String studentName, String classesId,
			String schoolId);
	public void saveParentsstudent(Parentsstudent parentsstudent);
	/**
	 * 2016年10月20日下午4:15:59
	 *  阳朔
	 *  注释:
	 */
	public Parents getParentByUserId(String userId);
	/**
	 * 2016年10月20日下午4:34:18
	 *  阳朔
	 *  注释:查询 家长 是否为老师
	 */
	public SysUser getTeachUserByLogin(String tel);
	/**
	 * 2016年10月22日上午12:24:58
	 *  阳朔
	 *  注释:查询 loginname 老师 是否重复
	 */
	public SysUser chaTeacherUserByLoginName(String tel);
	/**
	 * 2016年10月22日上午12:33:58
	 *  阳朔
	 *  注释:
	 */
	public Parentsstudent getParentsstudentByParenId(String parenId);
	/**
	 * 2016年10月23日下午5:33:44
	 *  阳朔
	 *  注释:
	 */
	public List<Student> getStudentLimit(String classIds, String schoolName,
			String ssName, String intoYear, String gradeName,
			String classesName, Integer integer, Integer integer2);
	/**
	 * 2016年10月26日下午4:48:39
	 *  阳朔
	 *  注释:
	 */
	public List getStudentCountByClassId(String classIds, String schoolName,
			String ssName, String intoYear, String gradeName, String classesName);
	/**
	 * 2016年11月10日下午5:14:05
	 *  阳朔
	 *  注释:
	 */
	public Employee getEmployeeByUserId(String userId);
	/**
	 * 2016年11月10日下午5:16:38
	 *  阳朔
	 *  注释:
	 */
	public List<Classes> getClassess(String string, String schoolId);
	/**
	 * 2016年11月10日下午6:01:26
	 *  阳朔
	 *  注释:
	 */
	public Classes getClassesByEmpId(String emId);
	/**
	 * 2016年11月10日下午6:08:24
	 *  阳朔
	 *  注释:
	 */
	public List<Classteacher> getClassTeacherByEmId(String emId);
	/**
	 * 2016年11月10日下午6:28:41
	 *  阳朔
	 *  注释:
	 */
	public List<Classes> getClassessByData(String intoyear, String gra,
			String claName, String schoolId);
	/**
	 * 2016年11月17日上午10:03:31
	 *  阳朔
	 *  注释:
	 */
	public void delParentsstudent(Parentsstudent parentsstudent);
	/**
	 * 2016年11月17日下午4:55:13
	 *  阳朔
	 *  注释:
	 */
	public Student chaStudentByCode(String studentCode);
	/**
	 * 2016年11月23日下午1:21:10
	 *  阳朔
	 *  注释:
	 */
	public SysSubrole getSysSubroleRoleId(String roleId);
	/**
	 * 2016年12月8日下午6:41:31
	 *  阳朔
	 *  注释:
	 */
	public SysUser getSysUserTeaByTel(String phone);
	

}
