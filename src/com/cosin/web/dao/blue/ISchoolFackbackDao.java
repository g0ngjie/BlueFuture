package com.cosin.web.dao.blue;

import java.util.List;

import com.cosin.web.entity.Parentsstudent;
import com.cosin.web.entity.SchoolFackback;
import com.cosin.web.entity.Student;
import com.cosin.web.entity.SysUser;

public interface ISchoolFackbackDao {

	//分页
    public List getSchoolFackbackLimit(String startDate,String endDate,String schoolName,String enable, int start, int limit);
    //数量
    public int getSchoolFackbackCount();
    //获取noticeId
  	public SchoolFackback getSchoolFackbackId(String schoolFackbackId);
    //添加 
//  	public void saveSchoolFackback(Activity schoolFackback);
  	 //删除
    public void schoolFackbackDelId(String schoolFackbackId);
    //查询用户表 user
//    public List getUser();
    //根据id查询
    public SysUser getUser2(String user);
    /**
     * 当点击详情是  改变查看状态
     * @param schoolFackback
     */
	public void save(String schoolFackbackId);
	/*
	 * 
	 */
	public List<SchoolFackback> getSchoolFackbackLimit(int mode, String order,
			String sort, String ssName, int start, int limit, String startDate,
			String endDate);
	/**
	 * 2016年10月23日下午5:15:14
	 *  阳朔
	 *  注释:
	 */
	public List<SchoolFackback> getSchoolFackbackLimit(String schoolsId,
			String startDate, String endDate, String ischoolName,
			String enable, Integer integer, Integer integer2);
	/**
	 * 2016年10月26日下午4:15:47
	 *  阳朔
	 *  注释:
	 */
	public List getSchoolFackbackCount(String startDate, String endDate,
			String ischoolName, String enable);
	/**
	 * 2016年10月26日下午4:33:26
	 *  阳朔
	 *  注释:
	 */
	public List getSchoolFackbackCountBySchoolId(String schoolsId,
			String startDate, String endDate, String ischoolName, String enable);
	/**
	 * 2016年11月1日下午7:13:53
	 *  阳朔
	 *  注释:
	 */
	public Parentsstudent getParentsstudentByParentId(String parentId);
	
    
    
}
