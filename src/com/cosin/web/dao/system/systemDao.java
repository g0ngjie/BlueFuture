/**
 * 
 */
package com.cosin.web.dao.system;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

/**
 * @author 王思明
 * 2016年7月19日
 * 下午2:06:10
 */
@Repository  //将Dao类声明为Bean
public class systemDao extends BaseDao implements ISystemDao {

	/*
	 * 后台用户登录验证
	 * 王思明
	 * 2016年7月19日
	 * 下午3:19:30
	 */
	@Override
	public SysUser getByEmailNamePwd(String loginName, String pwd) {
		String hql = "from SysUser as a where a.isDel = 0 and a.pwd='" + pwd + "' and a.loginName='" + loginName + "'";
		List<SysUser> list = query(hql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/* 
	 * 保存对象方法
	 * 王思明
	 * 2016年7月19日
	 * 下午3:21:33
	 */
	@Override
	public void saveObject(Object obj) {
		
		
		super.save(obj);
	}

	@Override
	public SysUserRole getByIdSysRole(String userId) {
		
		String sql = "from SysUserRole as a where a.sysUser.userId='"+ userId +"'";
		List<SysUserRole> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
		
	}

	/* 
	 * 王思明
	 * 2016年9月26日
	 * 下午4:57:42
	 */
	@Override
	public SysRolePower getBySysRolePower(String roleId) {
		
		String sql = "from SysRolePower as a where a.sysRole.roleId='"+ roleId +"' and a.parentId is null"; 
		
		List<SysRolePower> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
		
	}

	/* 
	 * 王思明
	 * 2016年9月26日
	 * 下午5:54:38
	 */
	@Override
	public List<SysRolePower> getSysRolePower(String roleId) {
		
		String sql = "from SysRolePower as a where a.sysRole.roleId='"+ roleId +"' and a.parentId != null";
		
		return query(sql);
	}

	/**
	 * 2016年10月8日下午5:39:56
	 *  龚杰
	 *  注释：
	 */
	@Override
	public List<School> getAllSchool() {
		String sql = "from School as a where a.isDel = 0";
		return query(sql);
	}

	/**
	 * 2016年10月8日下午5:47:32
	 *  龚杰
	 *  注释：通过userId 查询角色
	 */
	@Override
	public List<SysUserRole> getRoleByUserId(String createUserId) {
		String sql = "from SysUserRole as a where 1 = 1 and a.sysUser = '" + createUserId + "'";
		
		return query(sql);
	}

	/**
	 * 2016年10月8日下午5:52:18
	 *  龚杰
	 *  注释：角色对应的园区
	 */
	@Override
	public List<SysRoleSchoolPower> getSysRoleSchoolPowerListByRoleId(
			String roleId) {
		String sql = "from SysRoleSchoolPower as a where 1 = 1 and a.sysRole = '" + roleId + "'";
		return query(sql);
	}

	/**
	 * 2016年10月11日下午3:51:58
	 *  阳朔
	 *  注释:
	 */
	@Override
	public 	List<Employee> getEmployeeByUserId(String createUserId) {
		String sql = "from Employee as a where a.isDel = 0 and a.sysUserByUserId = '" + createUserId + "'";
		return query(sql);
	}

	/**
	 * 2016年12月9日下午12:05:49
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Employee getEmployeeById(String userId) {
		// TODO Auto-generated method stub
		String sql = "from Employee as a where a.isDel = 0 and a.sysUserByUserId = '" + userId + "'";
		List<Employee> list =  query(sql);
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}
	

	
	
}
