package com.cosin.web.dao.system;

import java.util.List;

import com.cosin.web.entity.SysLog;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

/**
 * 分享统计模块
 * caishaojun
 * @author 
 */
public interface ISysDao {
	//分页
    public List getLogLimit(String SsName,String logId, int start, int limit,String startDate,String endDate,String fun,String operate);
    //获取分页数据 分页数量
    public int getLogCount(String SsName, String startDate,String endDate,String fun,String operate);
	//获取 ID
	public SysLog getLogId(String logId);
	//导出功能
	public List<SysLog> getLogLimitDaoChu(String SsName,String startDate,String endDate,String fun,String operate);
	//分页导出
//	public  List<VcVideo>  getByIdVcVideo(String mediaId);
	//查询userRole
	public SysUserRole getUr(String userId);
	
	
	
	
	//查询sysUser
    public SysUser getSysUserId(String id);
    //保存
  	public void saveAdd(Object object,String token,String vcName,String ip);
  	//查看
  	public void look(Object object,String token,String vcName,String ip);
  	//删除
	public void del(Object object,String token,String vcName,String ip);
	//启用
	public void using(Object object,String token,String vcName,String ip);
	//禁用
	public void offing(Object object,String token,String vcName,String ip);
	//修改
	public void updata(String token,String vcName,String ip);
	//操作 关键词管理  设置视频/媒体
	public void videoAndMedia(Object object, String token,String vcName,String ip);
	//热词管理   上移
	public void up(Object object, String token,String vcName,String ip);
	//热词管理   下移
	public void down(Object object, String token,String vcName,String ip);
	//导出
	public void daochu( String token,String vcName,String ip);
	//新建推送
	public void push( Object object,String token,String vcName,String ip);
	//驳回
	public void reject( Object object,String token,String vcName,String ip,String fun);
	
	
}
