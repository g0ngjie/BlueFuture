/**
 * 
 */
package com.cosin.web.dao.system;

import java.util.List;

import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

/**
 * @author 王思明
 * 2016年7月19日
 * 下午2:03:48
 */
public interface ISystemDao {

	/**
	 * 验证用户登录信息
	 * @param loginName
	 * @param pwd
	 * @return
	 */
	public SysUser getByEmailNamePwd(String loginName, String pwd);

	/**
	 * 保存方法
	 * 王思明 
	 * 2016年7月19日
	 * 下午3:21:16
	 */
	public void saveObject(Object obj);
	
	/**
	 * @return
	 */
	public SysUserRole getByIdSysRole(String userId);

	/**
	 * 王思明 
	 * 2016年9月26日
	 * 下午4:57:30
	 */
	public SysRolePower getBySysRolePower(String roleId);

	/**
	 * 王思明 
	 * 2016年9月26日
	 * 下午5:53:44
	 */
	public List<SysRolePower> getSysRolePower(String roleId);

	/**
	 * 2016年10月8日下午5:39:39
	 *  龚杰
	 *  注释：操作者级别 查询园区
	 */
	public List<School> getAllSchool();

	/**
	 * 2016年10月8日下午5:47:16
	 *  龚杰
	 *  注释：通过userId  查询role角色
	 */
	public List<SysUserRole> getRoleByUserId(String createUserId);

	/**
	 * 2016年10月8日下午5:52:07
	 *  龚杰
	 *  注释：角色对应的园区
	 */
	public List<SysRoleSchoolPower> getSysRoleSchoolPowerListByRoleId(
			String roleId);

	/**
	 * 2016年10月11日下午3:51:50
	 *  阳朔
	 *  注释:
	 */
	public List<Employee> getEmployeeByUserId(String createUserId);

	/**
	 * 2016年12月9日下午12:05:39
	 *  阳朔
	 *  注释:
	 */
	public Employee getEmployeeById(String userId);

	
}
