package com.cosin.web.dao.system;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Formatter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysPowerItem;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

/**
 * 菜单Dao
 * @author zhb 2016-03-30
 */
@Repository
public class MenuDao extends BaseDao implements IMenuDao{
	
	/**
	 * 获取一级菜单
	 * @return
	 */
	public List getMainMenuList(int start, int limit)
	{
		String hql = "from SysMenu as a where a.sysMenu=null order by a.orderNo desc";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}
		
	/**
	 * 根据主键获取菜单
	 * @param id
	 * @return
	 */
	public SysMenu findMenuById(String id)
	{
		Formatter fmt = new Formatter();
		String sql = fmt.format("from SysMenu as a where a.menuId='%s'", id).toString();
		List list = query(sql);
		if(list.size() > 0)
			return (SysMenu)list.get(0);
		return null;
	}

	/**
	 * 获取下级菜单
	 * @param id
	 * @return
	 */
	public List findMenuByParentMenuId(String id)
	{
		String sql = "from SysMenu as a where a.sysMenu.menuId='" + id +"' and a.enable=1 order by a.createDate,a.orderNo desc";
		List list = query(sql);
		return list;
	}
	
	//查询  dao
	@Override
	public List getSysMenu(String menuId) {
		String sql = "";
		if("0".equals(menuId))
			sql="from SysMenu as a where a.sysMenu.menuId is null  order by a.orderNo desc";
		else
			sql="from SysMenu as a where a.sysMenu.menuId = '" + menuId +"'  order by a.orderNo desc"; 
		List list =query(sql);
		return list;
	}

	@Override
	public void saveSysMenu(String menuName, String menuId) {
		// TODO Auto-generated method stub
	}

	@Override
	public void saveAddSysMenu(String menuName, String menuId) {
		// TODO Auto-generated method stub
	}

	@Override
	public void delSysMenu(String sysMenu) {
		
		super.delete(sysMenu);
	}
	
	@Override
	public void saveSysMenu(SysMenu sysMenus) {
		super.save(sysMenus); 
	}

	@Override
	public void delSysMenu(SysMenu sysMenu) {
		// TODO Auto-generated method stub
		super.delete(sysMenu);
	}

	/* 
	 * 根据token获取用户信息（用户名--用来在右上角显示）
	 * 王思明
	 * 2016年7月19日
	 * 下午5:38:34
	 */
	@Override
	public SysUser getByTokenUser(Object token) {
		String sql = "from SysUser as a where a.token='"+ token +"'";
		List<SysUser> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	@Override
	public List<SysRolePower> getByIdSysRolePower(String roleId) {
		
		String sql = "from SysRolePower as a where a.sysRole.roleId='"+ roleId +"' and a.parentId is null";
		
		return query(sql);
		
	}
	
	//
	@Override
	public SysPowerItem getBySysPowerItemId(String powerCode) {
		
		String sql = "from SysPowerItem as a where a.powerCode='"+ powerCode +"'";
		List<SysPowerItem> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
		
	}

	/* 
	 * 王思明
	 * 2016年9月26日
	 * 下午4:41:38
	 */
	@Override
	public List<SysMenu> getByMenuId(String menuId) {
		
		String sql = "from SysMenu as a where a.sysMenu.menuId='"+ menuId +"' and a.enable=1 order by a.orderNo desc";
		//List<SysMenu> list = query(sql);
		return query(sql);
	
	}

	/* 
	 * 王思明
	 * 2016年9月26日
	 * 下午5:15:47
	 */
	@Override
	public SysPowerItem getSysPowerItemByMenuId(String menuId) {
		
		String sql = "from SysPowerItem as a where a.sysMenu.menuId='"+ menuId +"'";
		List<SysPowerItem> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/* 
	 * 王思明
	 * 2016年9月26日
	 * 下午5:30:42
	 */
	@Override
	public SysRolePower getSysRolePowerByPower(String roleId, String powerCode) {
		
		String sql = "from SysRolePower as a where a.sysRole.roleId='"+ roleId +"' and a.powerCode='"+ powerCode +"'";
		
		List<SysRolePower> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
		
	}

	/**
	 * 2016年9月30日下午3:35:41
	 *  龚杰
	 *  注释：登陆者 角色
	 */
	@Override
	public SysUserRole getSysUserRoleByUserId(String userId) {
		String sql = "from SysUserRole as a where a.sysUser = '" + userId + "'";
		List<SysUserRole> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

}
