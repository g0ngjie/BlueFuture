package com.cosin.web.dao.system;

import java.util.List;

import org.springframework.stereotype.Controller;

import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysPowerItem;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

public interface IMenuDao {
	public List getMainMenuList(int start, int limit);
	
	public SysMenu findMenuById(String id);
	
	public List findMenuByParentMenuId(String id);
	
	//查询  dao接口
	public List getSysMenu(String menuId);
	
	public void saveSysMenu(String menuName,String menuId); 
	
	public void saveAddSysMenu(String menuName,String menuId);
	
	public void delSysMenu(String sysMenu);
	//保存  dao接口
	public void saveSysMenu(SysMenu sysMenus);

	public void delSysMenu(SysMenu sysMenu);

	/**
	 * 根据token获取用户信息（用户名--用来在右上角显示）
	 * 王思明 
	 * 2016年7月19日
	 * 下午5:38:23
	 */
	public SysUser getByTokenUser(Object token);
	
	//
	public List<SysRolePower> getByIdSysRolePower(String roleId);
	//获取左边菜单
	public SysPowerItem getBySysPowerItemId(String powerCode);

	/**
	 * 王思明 
	 * 2016年9月26日
	 * 下午4:41:30
	 */
	public List<SysMenu> getByMenuId(String menuId);

	/**
	 * 王思明 
	 * 2016年9月26日
	 * 下午5:15:33
	 */
	public SysPowerItem getSysPowerItemByMenuId(String menuId);

	/**
	 * 王思明 
	 * 2016年9月26日
	 * 下午5:30:27
	 */
	public SysRolePower getSysRolePowerByPower(String roleId, String powerCode);

	/**
	 * 2016年9月30日下午3:35:28
	 *  龚杰
	 *  注释：登陆者 角色
	 */
	public SysUserRole getSysUserRoleByUserId(String userId);

	
	
}
