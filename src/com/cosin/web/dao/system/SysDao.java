package com.cosin.web.dao.system;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.SysLog;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;
@Repository
public class SysDao extends BaseDao implements ISysDao {

	private static final Object Object = null;
	//分页
	@Override
	public List getLogLimit(String SsName, String logId, int start, int limit,
			String startDate, String endDate,String fun,String operate) {
		 String sql="from SysLog as a where 1=1";
		 if(SsName != null && !"".equals(SsName)){//平台
			 
				sql += " and a.sysUser.userName like '%" + SsName + "%'";
			}
		 if(startDate != null && !"".equals(startDate))
			{
				sql += " and a.createDate >= '" + startDate + " 00:00:00'";
			}
		if(endDate != null && !"".equals(endDate))
			{
				sql += " and a.createDate <= '" + endDate + " 23:59:59'";
			}
		if(fun != null && !"".equals(fun))
		{
			sql += " and a.funName like '%" + fun + "%'";
		}
		if(operate != null && !"".equals(operate))
		{
			sql += " and a.action like '%" + operate + "%'";
		}
		 sql+="  order by a.createDate desc";
		 return query(sql,start,limit);
	}
	//获取分页数据 分页数量
	@Override
	public int getLogCount(String SsName, String startDate, String endDate,String fun,String operate) {
		String sql="select count(*) from  SysLog as a where 1=1";
		if(SsName != null && !"".equals(SsName)){
			sql += " and a.sysUser.userName like '%" + SsName + "%'";
		}
	 if(startDate != null && !"".equals(startDate))
		{
			sql += " and a.createDate >= '" + startDate + " 00:00:00'";
		}
	if(endDate != null && !"".equals(endDate))
		{
			sql += " and a.createDate <= '" + endDate + " 23:59:59'";
		}
	if(fun != null && !"".equals(fun))
	{
		sql += " and a.funName like '%" + fun + "%'";
	}
	if(operate != null && !"".equals(operate))
	{
		sql += " and a.action like '%" + operate + "%'";
	}
	 sql+="  order by a.createDate desc";
		return queryCount(sql);
	}
	//获取 ID
	@Override
	public SysLog getLogId(String logId) {
		String sql = "from SysLog  as a where a.logId='"+logId+"'";
		List<SysLog> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}
	
	//导出功能
	@Override
	public List<SysLog> getLogLimitDaoChu(String SsName, String startDate,
			String endDate,String fun,String operate) {
		String sql="from SysLog as a where 1=1";
		 if(SsName != null && !"".equals(SsName))
		 	{
				sql += " and a.sysUser.userName like '%" + SsName + "%'";
			}
		 if(startDate != null && !"".equals(startDate))
			{
				sql += " and a.createDate >= '" + startDate + " 00:00:00'";
			}
		if(endDate != null && !"".equals(endDate))
			{
				sql += " and a.createDate <= '" + endDate + " 23:59:59'";
			}
		if(fun != null && !"".equals(fun))
		{
			sql += " and a.funName like '%" + fun + "%'";
		}
		if(operate != null && !"".equals(operate))
		{
			sql += " and a.action like '%" + operate + "%'";
		}
		 sql+="  order by a.createDate desc";
		 List list = query(sql);
		return query(sql);
	}
	//分页导出
//	@Override
//	public List<VcVideo> getByIdVcVideo(String mediaId) {
//		String hql = "select count(*) from VcVideo as a where a.videoId='" + mediaId + "'";
//		List<VcVideo> list = query(hql);
//		return list;
//	}
	//添加
	@Override
	public void saveAdd(Object object,String token,String vcName,String ip) {
		SysLog sysLog = new SysLog();
		sysLog.setFunName(vcName);
		sysLog.setAction("添加");
		sysLog.setContent("创建了"+vcName+"\""+object+"\"");
		sysLog.setCreateDate(new Timestamp(new Date().getTime()));
		SysUser sysUser = getSysUserId(token);
		sysLog.setSysUser(sysUser);
		super.save(sysLog);
	}
	//查询sysUser
	@Override
	public SysUser getSysUserId(String id) {
		String hql = "from SysUser as a where a.token ='"+ id+"'"  ;
		List<SysUser> list = query(hql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}
	//查看
	@Override
	public void look(Object object, String token,String vcName,String ip) {
		SysLog sysLog = new SysLog();
		sysLog.setFunName(vcName);
		sysLog.setAction("查看");
		sysLog.setContent("查看了"+vcName+"\""+object+"\"");
		sysLog.setCreateDate(new Timestamp(new Date().getTime()));
		SysUser sysUser = getSysUserId(token);
		sysLog.setSysUser(sysUser);
		super.save(sysLog);
	}
	//删除
	@Override
	public void del(Object object, String token,String vcName,String ip) {
		SysLog sysLog = new SysLog();
		sysLog.setFunName(vcName);
		sysLog.setAction("删除");
		sysLog.setContent("删除了"+vcName+"\""+object+"\"");
		sysLog.setCreateDate(new Timestamp(new Date().getTime()));
		SysUser sysUser = getSysUserId(token);
		sysLog.setSysUser(sysUser);
		super.save(sysLog);
	}
	//启用
	@Override
	public void using(Object object, String token,String vcName,String ip) {
		SysLog sysLog = new SysLog();
		sysLog.setFunName(vcName);
		sysLog.setAction("启用");
		sysLog.setContent("启用了"+vcName+"\""+object+"\"");
		sysLog.setCreateDate(new Timestamp(new Date().getTime()));
		SysUser sysUser = getSysUserId(token);
		sysLog.setSysUser(sysUser);
		super.save(sysLog);
	}
	//禁用
	@Override
	public void offing(Object object, String token,String vcName,String ip) {
		SysLog sysLog = new SysLog();
		sysLog.setFunName(vcName);
		sysLog.setAction("禁用");
		sysLog.setContent("禁用了"+vcName+"\""+object+"\"");
		sysLog.setCreateDate(new Timestamp(new Date().getTime()));
		SysUser sysUser = getSysUserId(token);
		sysLog.setSysUser(sysUser);
		super.save(sysLog);
	}
	
	//修改
		@Override
		public void updata(String token,String vcName,String ip) {
			SysLog sysLog = new SysLog();
			sysLog.setFunName(vcName);
			sysLog.setAction("修改");
			
			sysLog.setContent("修改了"+vcName+"的数据");
			
			sysLog.setCreateDate(new Timestamp(new Date().getTime()));
			SysUser sysUser = getSysUserId(token);
			sysLog.setSysUser(sysUser);
			super.save(sysLog);
		}
	
	//操作 设置关联视频/媒体
	@Override
	public void videoAndMedia(Object object, String token,String vcName,String ip) {
		SysLog sysLog = new SysLog();
		sysLog.setFunName(vcName);
		sysLog.setAction("设置关联视频/媒体");
		sysLog.setContent("操作了关联视频/媒体的数据");
		sysLog.setCreateDate(new Timestamp(new Date().getTime()));
		SysUser sysUser = getSysUserId(token);
		sysLog.setSysUser(sysUser);
		super.save(sysLog);
	}
	
	//热词管理   上移
	@Override
	public void up(Object object, String token,String vcName,String ip) {
		SysLog sysLog = new SysLog();
		sysLog.setFunName(vcName);
		sysLog.setAction("上移");
		sysLog.setContent("上移了"+vcName+"\""+object+"\"");
		sysLog.setCreateDate(new Timestamp(new Date().getTime()));
		SysUser sysUser = getSysUserId(token);
		sysLog.setSysUser(sysUser);
		super.save(sysLog);
	}
	//热词管理   下移
		@Override
		public void down(Object object, String token,String vcName,String ip) {
			SysLog sysLog = new SysLog();
			sysLog.setFunName(vcName);
			
			sysLog.setAction("下移");
			sysLog.setContent("下移了"+vcName+"\""+object+"\"");
			sysLog.setCreateDate(new Timestamp(new Date().getTime()));
			SysUser sysUser = getSysUserId(token);
			sysLog.setSysUser(sysUser);
			super.save(sysLog);
		}
	//导出
	@Override
	public void daochu( String token,String vcName,String ip) {
		SysLog sysLog = new SysLog();
		
		sysLog.setFunName(vcName);
		sysLog.setAction("导出");
		sysLog.setContent("点击了导出功能模块");
		
		sysLog.setCreateDate(new Timestamp(new Date().getTime()));
		SysUser sysUser = getSysUserId(token);
		sysLog.setSysUser(sysUser);
		super.save(sysLog);
	}
	
	//新建推送
	@Override
	public void push( Object object,String token,String vcName,String ip) {
		SysLog sysLog = new SysLog();
		sysLog.setFunName(vcName);
		sysLog.setAction("新建推送");
		sysLog.setContent("创建了"+vcName+"\""+object+"\"");
		
		sysLog.setCreateDate(new Timestamp(new Date().getTime()));
		SysUser sysUser = getSysUserId(token);
		sysLog.setSysUser(sysUser);
		super.save(sysLog);
	}
	//
	@Override
	public void reject( Object object,String token,String vcName,String ip,String fun) {
		SysLog sysLog = new SysLog();
		sysLog.setFunName(vcName);
		sysLog.setAction(fun);
		sysLog.setContent(fun+"了"+vcName+"\""+object+"\"");
		
		sysLog.setCreateDate(new Timestamp(new Date().getTime()));
		SysUser sysUser = getSysUserId(token);
		sysLog.setSysUser(sysUser);
		super.save(sysLog);
	}
	
	
	//查询 userRole
	@Override
	public SysUserRole getUr(String userId) {
		String hql = "from SysUserRole as a where a.sysUser.userId ='"+ userId+"'"  ;
		List<SysUserRole> list = query(hql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}
	
	
	
}
