package com.cosin.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class DateUtils {

	public static String DEF_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static String defaultDatePattern = "yyyy-MM-dd";
	
	public static String parseTimstaToStr(String format, Timestamp str)
	{
		String tsStr = "";
		DateFormat sdf = new SimpleDateFormat(format); 
		try {   
	            //方法一   
			 	tsStr = sdf.format(str);   
	           /* //方法二   
	            tsStr = ts.toString();   */
	    } catch (Exception e) {   
	            e.printStackTrace();   
	    }
		return tsStr;
		
	}
	
	/**
	 * 字符串转日期
	 * @param format
	 * @param str
	 * @return
	 */
	public static Date parseStrToDate(String format, String str)
	{
		SimpleDateFormat sdf =   new SimpleDateFormat(format);
		try {
			return sdf.parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	/**
	 * 解析日期到字符串
	 * @param format
	 * @param date
	 * @return
	 */
	public static String parseDateToStr(String format, Date date)
	{
		SimpleDateFormat sdf =   new SimpleDateFormat(format);
		String str = sdf.format(date);
		return str;
	}
	
	/**
	 * Date转换TimeStamp
	 * @param date
	 * @return
	 */
	public static Timestamp dateToTs(Date date)
	{
		return new java.sql.Timestamp(date.getTime());
	}
	/**
	 * 获取系统的当前一天的时间
	 * 王思明 
	 * 2016年7月13日
	 */
	public static Date getNextDay(Date date) {  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(date);  
        calendar.add(Calendar.DAY_OF_MONTH, 0);  
        date = calendar.getTime();  
        return date;  
    }
	/**
	 * 获取当前时间的往后第一天
	 * 王思明 
	 * 2016年7月13日
	 */
    public static Date getNextTimeDay(Date date) {  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(date);  
        calendar.add(Calendar.DAY_OF_MONTH, +1);  
        date = calendar.getTime();  
        return date;  
    }  
	/**
	 * 获取当前时间的往前第一天
	 * 王思明 
	 * 2016年7月13日
	 */
    public static Date getLastDay(Date date) {  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(date);  
        calendar.add(Calendar.DAY_OF_MONTH, -1);  
        date = calendar.getTime();  
        return date;  
    }  
    /**
	 * 获取当前时间的往前第二天
	 * 王思明 
	 * 2016年7月13日
	 */
    public static Date getLastSubDay(Date date) {  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(date);  
        calendar.add(Calendar.DAY_OF_MONTH, -2);  
        date = calendar.getTime();  
        return date;  
    }  
    
    /**
     * timestamp转date
     * 2016年7月28日
     * 上午7:37:49
     * wangsiming
     */
    public static String timestampToStr(Timestamp ts) {  
    	String tsStr = "";
    	DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	try {
    		//方法一
    		tsStr = sdf.format(ts);
    		return tsStr;
    		//方法二
    		//tsStr = ts.toString();
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return null;
    }
	
    
    public static String getWeekBegin(String strDate) {
		Date date = DateUtils.parseStrToDate(DEF_FORMAT, strDate + " 00:00:00");
		Calendar cd=Calendar.getInstance();
		cd.setTime(date);
		
		int mondayPlus;
		new Date().getTime();
		//Calendar cd = new Calendar()
		//Calendar cd = Calendar.getInstance();
		// 获得今天是一周的第几天，星期日是第一天，星期二是第二天......
		int dayOfWeek = cd.get(Calendar.DAY_OF_WEEK) - 1; // 因为按中国礼拜一作为第一天所以这里减1
		if (dayOfWeek == 1) {
		mondayPlus = 0;
		} else {
		mondayPlus = 1 - dayOfWeek;
		}
		GregorianCalendar currentDate = new GregorianCalendar();
		currentDate.add(GregorianCalendar.DATE, mondayPlus);
		Date monday = currentDate.getTime();
		DateFormat df = DateFormat.getDateInstance();
		String preMonday = df.format(monday);
		return preMonday + " 00:00:00";
	} 	
    
    public static int compare_date(String DATE1, String DATE2) {
	       
	       
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dt1 = df.parse(DATE1);
            Date dt2 = df.parse(DATE2);
            if (dt1.getTime() > dt2.getTime()) {
                System.out.println("dt1 在dt2前");
                return 1;
            } else if (dt1.getTime() < dt2.getTime()) {
                System.out.println("dt1在dt2后");
                return -1;
            } else {
                return 0;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return 0;
    }
    
    /**
     * java生成随机数字和字母组合
     * @param length[生成随机数的长度]
     * @return
     */
    public static String getCharAndNumr(int length) {
        String val = "";
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            // 输出字母还是数字
            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num"; 
            // 字符串
            if ("char".equalsIgnoreCase(charOrNum)) {
                // 取得大写字母还是小写字母
                int choice = random.nextInt(2) % 2 == 0 ? 97 : 97; 
                val += (char) (choice + random.nextInt(26));
            } else if ("num".equalsIgnoreCase(charOrNum)) { // 数字
                val += String.valueOf(random.nextInt(10));
            }
        }
        return val;
    }

    /**
     * java生成随机数字
     * @param length[生成随机数的长度]
     * @return
     */
    public static String getRodamNumr(int length) {
        String val = "";
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            // 输出字母还是数字
            String charOrNum = random.nextInt(2) % 2 == 0 ? "num" : "num"; 
            // 字符串
            if ("char".equalsIgnoreCase(charOrNum)) {
                // 取得大写字母还是小写字母
                int choice = random.nextInt(2) % 2 == 0 ? 97 : 97; 
                val += (char) (choice + random.nextInt(26));
            } else if ("num".equalsIgnoreCase(charOrNum)) { // 数字
                val += String.valueOf(random.nextInt(10));
            }
        }
        return val;
    }
    
    
    /**
	 * 根据日期获得所在周的日期 
	 * @param mdate
	 * @return
	 */
	public static List<String> dateToWeek(Date mdate) {
		int b = mdate.getDay();
		Date fdate;
		List<String> list = new ArrayList<String>();
		Long fTime = mdate.getTime() - b * 24 * 3600000;
		for (int a = 1; a <= 5; a++) {
			fdate = new Date();
			fdate.setTime(fTime + (a * 24 * 3600000));
			SimpleDateFormat sFormat = new SimpleDateFormat(DateUtils.defaultDatePattern);
		    String sdate = sFormat.format(fdate);
			list.add(a-1, sdate);
		}
		return list;
	}
	public static String getOutTradeNo() {
		SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss", Locale.getDefault());
		Date date = new Date();
		String key = format.format(date);
		Random r = new Random();
		key = key + r.nextInt();
		key = key.substring(0, 15);
		return key;
	}
}
