package com.cosin.utils;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

public class FileUploadUtils {
	
	    
	/*//上传视频、音频、视频统一方法
	public static Map getParams(HttpServletRequest request, HttpServletResponse response)
	{
		Map map = new HashMap();
		String path = request.getSession().getServletContext().getRealPath("/") + "\\temp\\";
		File tempPath = new File(path);  
        if (!tempPath.exists()) {  
            tempPath.mkdir();  
        }  
        
        path = request.getSession().getServletContext().getRealPath("/") + "\\img\\";
        String uploadPath = path;//"E:\\project\\j2ee\\Falvfagui\\WebRoot\\img";
        
        //DiskFileItemFactory：创建 FileItem 对象的工厂，在这个工厂类中可以配置内存缓冲区大小和存放临时文件的目录。  
        DiskFileItemFactory factory = new DiskFileItemFactory();  
        // maximum size that will be stored in memory  
        factory.setSizeThreshold(4096);  
        // the location for saving data that is larger than getSizeThreshold()  
        factory.setRepository(tempPath);  
          
        //ServletFileUpload：负责处理上传的文件数据，并将每部分的数据封装成一到 FileItem 对象中。  
        //在接收上传文件数据时，会将内容保存到内存缓存区中，如果文件内容超过了 DiskFileItemFactory 指定的缓冲区的大小，  
        //那么文件将被保存到磁盘上，存储为 DiskFileItemFactory 指定目录中的临时文件。  
        //等文件数据都接收完毕后，ServletUpload再从文件中将数据写入到上传文件目录下的文件中  
          
          
        ServletFileUpload upload = new ServletFileUpload(factory);  
        // maximum size before a FileUploadException will be thrown  
        upload.setSizeMax(1000000 * 20);  
        
        List fileItems;
		try {
			fileItems = upload.parseRequest(request);
			String itemNo = "";  
	        //Iterator iter = fileItems.iterator()取其迭代器  
	        //iter.hasNext()检查序列中是否还有元素  
	        int i = 0;
	        
	        List listFileItem = new ArrayList();
	        for (Iterator iter = fileItems.iterator(); iter.hasNext();) {  
	            //获得序列中的下一个元素  
	            FileItem item = (FileItem) iter.next();  
	            
	          
	            //判断是文件还是文本信息  
	            //是普通的表单输入域  
	            if(item.isFormField()) {  
	            	String filedName = item.getFieldName();
	            	String value = item.getString();
//		                    if ("itemNo".equals(item.getFieldName())) {  
//		                        itemNo = item.getString();  
//		                    }  
	            	map.put(filedName, value);
	            }  
	            else
	            {
	            	listFileItem.add(item);
	            }
	        }
	        
	        map.put("listFile", listFileItem);
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
        
        
        return map;
	}*/
	
	//上传视频、音频、视频统一方法
	public static Map saveUploadFileForSpringMvc(HttpServletRequest request, HttpServletResponse response)
	{
		List listFile = new ArrayList();
		Map mapRet = new HashMap();
		String imgs = "";
        String videos = "";
        String audios = "";
        String files = "";
        
        //String path = request.getSession().getServletContext().getRealPath("/") + "\\..\\FileManagerService\\img\\";
        String path = request.getSession().getServletContext().getRealPath("/") + File.separator + ".." + File.separator + "FileManagerService" + File.separator + "img" + File.separator;
        File f = new File(path);
        if(!f.exists())
        	f.mkdirs();
        String uploadPath = path;//"E:\\project\\j2ee\\Falvfagui\\WebRoot\\img";
        
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());  
        //判断 request 是否有文件上传,即多部分请求  
        if(multipartResolver.isMultipart(request)){  
        	 //转换成多部分request    
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest)request;  
            //取得request中的所有文件名  
            Iterator<String> iter = multiRequest.getFileNames();  
            int i=0;
            while(iter.hasNext()){  
            	String fileType = "";
            	 //记录上传过程起始时的时间，用来计算上传时间  
                int pre = (int) System.currentTimeMillis();  
                //取得上传文件  
                CommonsMultipartFile file = (CommonsMultipartFile)multiRequest.getFile(iter.next());  
                if(file != null){  
                    //取得当前上传文件的文件名称  
                    String fieldName = file.getFileItem().getFieldName();//file.getOriginalFilename();  
                    //如果名称不为“”,说明该文件存在，否则说明该文件不存在  
                    if(fieldName.trim() !=""){  
                    	
                    	String arr[] = file.getOriginalFilename().split("\\.");
  
                        //定义上传路径  
                        //String path = "H:/" + fileName;  
                        
                        String newFileName = new Date().getTime() + "_" + (i+1) + (arr.length>0?"." + arr[arr.length-1]:"");
                        
                        if(fieldName.startsWith("Img"))
                        {
                        	if(imgs.length() != 0)
                        	{
                        		imgs += ",";
                        	}
                        	imgs += newFileName;
                        	fileType = "image";
                        }
                        else if(fieldName.startsWith("Video"))
                        {
                        	if(videos.length() != 0)
                        	{
                        		videos += ",";
                        	}
                        	videos += newFileName;
                        	fileType = "video";
                        }
                        else if(fieldName.startsWith("Audio"))
                        {
                        	if(audios.length() != 0)
                        	{
                        		audios += ",";
                        	}
                        	audios += newFileName;
                        	fileType = "audio";
                        }
                        else if(fieldName.startsWith("File"))
                        {
                        	if(files.length() != 0)
                        	{
                        		files += ",";
                        	}
                        	files += newFileName;
                        	fileType = "file";
                        }
                        
                        File localFile = new File(path + File.separator + newFileName);  
                        try {
							file.transferTo(localFile);
						} catch (IllegalStateException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}  
                        
                        Map mapFileInfo = new HashMap();
                        mapFileInfo.put("fileName", newFileName);
                        mapFileInfo.put("size", file.getSize());
                        mapFileInfo.put("extName", arr[arr.length-1]);
                        mapFileInfo.put("fileType", fileType);
                        listFile.add(mapFileInfo);
                        i++;
                    }  
                    
                   
                }  
                //记录上传该文件后的时间  
                int finaltime = (int) System.currentTimeMillis();  
                System.out.println(finaltime - pre);  

            }
        }
        
        mapRet.put("filesArray", listFile);
        mapRet.put("files", files);
        mapRet.put("imgs", imgs);
        mapRet.put("videos", videos);
        mapRet.put("audios", audios);
        return mapRet;
	}
	
	/*
	//上传视频、音频、视频统一方法
		public static Map saveUploadFileForSpringMvc(HttpServletRequest request, HttpServletResponse response)
		{
			List listFile = new ArrayList();
			Map mapRet = new HashMap();
			String imgs = "";
	        String videos = "";
	        String audios = "";
	        String files = "";
	        
	        //String path = request.getSession().getServletContext().getRealPath("/") + "\\..\\FileManagerService\\img\\";\
	        String path = request.getSession().getServletContext().getRealPath("/") + File.separator + ".." + File.separator + "FileManagerService" + File.separator + "img" + File.separator;
	        File f = new File(path);
	        if(!f.exists())
	        	f.mkdirs();
	        String uploadPath = path;//"E:\\project\\j2ee\\Falvfagui\\WebRoot\\img";
	        
			CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());  
	        //判断 request 是否有文件上传,即多部分请求  
	        if(multipartResolver.isMultipart(request)){  
	        	 //转换成多部分request    
	            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest)request;  
	            //取得request中的所有文件名  
	            Iterator<String> iter = multiRequest.getFileNames();  
	            int i=0;
	            while(iter.hasNext()){  
	            	String fileType = "";
	            	 //记录上传过程起始时的时间，用来计算上传时间  
	                int pre = (int) System.currentTimeMillis();  
	                //取得上传文件  
	                CommonsMultipartFile file = (CommonsMultipartFile)multiRequest.getFile(iter.next());  
	                if(file != null){  
	                    //取得当前上传文件的文件名称  
	                    String fieldName = file.getFileItem().getFieldName();//file.getOriginalFilename();  
	                    //如果名称不为“”,说明该文件存在，否则说明该文件不存在  
	                    if(fieldName.trim() !=""){  
	                    	
	                    	String arr[] = file.getOriginalFilename().split("\\.");
	  
	                        //定义上传路径  
	                        //String path = "H:/" + fileName;  
	                        
	                        String newFileName = new Date().getTime() + "_" + (i+1) + (arr.length>0?"." + arr[arr.length-1]:"");
	                        
	                        if(fieldName.startsWith("Img"))
	                        {
	                        	if(imgs.length() != 0)
	                        	{
	                        		imgs += ",";
	                        	}
	                        	imgs += newFileName;
	                        	fileType = "image";
	                        }
	                        else if(fieldName.startsWith("Video"))
	                        {
	                        	if(videos.length() != 0)
	                        	{
	                        		videos += ",";
	                        	}
	                        	videos += newFileName;
	                        	fileType = "video";
	                        }
	                        else if(fieldName.startsWith("Audio"))
	                        {
	                        	if(audios.length() != 0)
	                        	{
	                        		audios += ",";
	                        	}
	                        	audios += newFileName;
	                        	fileType = "audio";
	                        }
	                        else if(fieldName.startsWith("File"))
	                        {
	                        	if(files.length() != 0)
	                        	{
	                        		files += ",";
	                        	}
	                        	files += newFileName;
	                        	fileType = "file";
	                        }
	                        
	                        File localFile = new File(path + File.separator + newFileName);  
	                        try {
								file.transferTo(localFile);
							} catch (IllegalStateException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}  
	                        
	                        Map mapFileInfo = new HashMap();
	                        mapFileInfo.put("fileName", newFileName);
	                        mapFileInfo.put("size", file.getSize());
	                        mapFileInfo.put("extName", arr[arr.length-1]);
	                        mapFileInfo.put("fileType", fileType);
	                        listFile.add(mapFileInfo);
	                        i++;
	                    }  
	                    
	                   
	                }  
	                //记录上传该文件后的时间  
	                int finaltime = (int) System.currentTimeMillis();  
	                System.out.println(finaltime - pre);  

	            }
	        }
	        
	        mapRet.put("filesArray", listFile);
	        mapRet.put("files", files);
	        mapRet.put("imgs", imgs);
	        mapRet.put("videos", videos);
	        mapRet.put("audios", audios);
	        return mapRet;
		}
	*/
	/*//上传视频、音频、视频统一方法
	public static Map saveUploadFile(HttpServletRequest request, HttpServletResponse response)
	{
		List listFile = new ArrayList();
		Map mapRet = new HashMap();
		String imgs = "";
        String videos = "";
        String audios = "";
        String files = "";
		
        String path = request.getSession().getServletContext().getRealPath("/") + "\\temp\\";
		File tempPath = new File(path);  
        if (!tempPath.exists()) {  
            tempPath.mkdir();  
        }  
        
        path = request.getSession().getServletContext().getRealPath("/") + "\\..\\WqService\\img\\";
        String uploadPath = path;//"E:\\project\\j2ee\\Falvfagui\\WebRoot\\img";
        
		 //DiskFileItemFactory：创建 FileItem 对象的工厂，在这个工厂类中可以配置内存缓冲区大小和存放临时文件的目录。  
        DiskFileItemFactory factory = new DiskFileItemFactory();  
        // maximum size that will be stored in memory  
        factory.setSizeThreshold(4096);  
        // the location for saving data that is larger than getSizeThreshold()  
        factory.setRepository(tempPath);  
          

        //ServletFileUpload：负责处理上传的文件数据，并将每部分的数据封装成一到 FileItem 对象中。  
        //在接收上传文件数据时，会将内容保存到内存缓存区中，如果文件内容超过了 DiskFileItemFactory 指定的缓冲区的大小，  
        //那么文件将被保存到磁盘上，存储为 DiskFileItemFactory 指定目录中的临时文件。  
        //等文件数据都接收完毕后，ServletUpload再从文件中将数据写入到上传文件目录下的文件中  
          
          
        ServletFileUpload upload = new ServletFileUpload(factory);  
        upload.setHeaderEncoding("UTF-8");
        // maximum size before a FileUploadException will be thrown  
        upload.setSizeMax(1000000 * 20);  
	
        *//*******************************解析表单传递过来的数据，返回List集合数据-类型:FileItem***********//*  
        try {  
            List fileItems = upload.parseRequest(request);   
            String itemNo = "";  
            //Iterator iter = fileItems.iterator()取其迭代器  
            //iter.hasNext()检查序列中是否还有元素  
            int i = 0;
            
            for (Iterator iter = fileItems.iterator(); iter.hasNext();) {  
                //获得序列中的下一个元素  
                FileItem item = (FileItem) iter.next();  
                long size = item.getSize(); 
                if(size == 0)
                	continue;
              
                //判断是文件还是文本信息  
                //是普通的表单输入域  
                if(item.isFormField()) {  
//	                    if ("itemNo".equals(item.getFieldName())) {  
//	                        itemNo = item.getString();  
//	                    }  
                	mapRet.put(item.getFieldName(), item.getString("utf-8"));
                }  
                //是否为input="type"输入域  
                if (!item.isFormField()) { 
                	String fileType = "";
                	Map mapFileInfo = new HashMap();
                	
                	String arr[] = item.getName().split("\\.");
                    
                    String fieldName = item.getFieldName();
                    String newFileName = new Date().getTime() + "_" + (i+1) + (arr.length>0?"." + arr[arr.length-1]:"");
                    i++;
                    
                    if(fieldName.startsWith("Img"))
                    {
                    	if(imgs.length() != 0)
                    	{
                    		imgs += ",";
                    	}
                    	imgs += newFileName;
                    	fileType = "image";
                    }
                    else if(fieldName.startsWith("Video"))
                    {
                    	if(videos.length() != 0)
                    	{
                    		videos += ",";
                    	}
                    	videos += newFileName;
                    	fileType = "video";
                    }
                    else if(fieldName.startsWith("Audio"))
                    {
                    	if(audios.length() != 0)
                    	{
                    		audios += ",";
                    	}
                    	audios += newFileName;
                    	fileType = "audio";
                    }
                    else if(fieldName.startsWith("File"))
                    {
                    	if(files.length() != 0)
                    	{
                    		files += ",";
                    	}
                    	files += newFileName;
                    	fileType = "file";
                    }
                	
                    //上传文件的名称和完整路径  
                    String fileName = item.getName();  
                    
                    size = item.getSize();  
                    //判断是否选择了文件  
                    if ((fileName == null || fileName.equals("")) && size == 0) {  
                        continue;  
                    }  
                    //截取字符串 如：C:\WINDOWS\Debug\PASSWD.LOG  
                    fileName = newFileName;//fileName.substring(fileName.lastIndexOf("\\") + 1, fileName.length());  
                     
                    try
                    {
                     // 保存文件在服务器的物理磁盘中：第一个参数是：完整路径（不包括文件名）第二个参数是：文件名称     
                    //item.write(file);  
                    //修改文件名和物料名一致，且强行修改了文件扩展名为gif  
                    //item.write(new File(uploadPath, itemNo + ".gif"));  
                    //将文件保存到目录下，不修改文件名  
                    item.write(new File(uploadPath, fileName));  
 
                    //将图片文件名写入打数据库                    
                    //itemManager.uploadItemImage(itemNo, fileName);  
                    
                    File file = new File(uploadPath, fileName);
                    String fn=file.getName();
                    String prefix = fn.substring(fn.lastIndexOf(".")+1);
                   
                    
                    mapFileInfo.put("fileName", fileName);
                    mapFileInfo.put("size", size);
                    mapFileInfo.put("extName", prefix);
                    mapFileInfo.put("fileType", fileType);
                    listFile.add(mapFileInfo);
                    }
                    catch(Exception e)
                    {
                    	e.printStackTrace();  
                    }
                }  
            }  
            //response.sendRedirect(request.getContextPath() + "/servlet/item/SearchItemServlet");  
        } catch (Exception e) {  
            e.printStackTrace();  
            //throw new ApplicationException("上传失败！");  
        }   
        mapRet.put("filesArray", listFile);
        mapRet.put("files", files);
        mapRet.put("imgs", imgs);
        mapRet.put("videos", videos);
        mapRet.put("audios", audios);
        return mapRet;
	}
	
	
	
	public static Map uploadFile(Map param)
	{
		HttpServletRequest request = (HttpServletRequest)param.get("request");//  put("request", request);
		String path = request.getSession().getServletContext().getRealPath("/") + "\\img\\";
        String uploadPath = path;//"E:\\project\\j2ee\\Falvfagui\\WebRoot\\img";
		int i = 0;
		List fileItems = (List)param.get("listFile");
		if(fileItems==null)
		{
			Map mapRet = new HashMap();
	        mapRet.put("imgs", "");
	        mapRet.put("videos", "");
	        mapRet.put("audios", "");
	        return mapRet;
		}
		String imgs = "";
        String videos = "";
        String audios = "";
		for (Iterator iter = fileItems.iterator(); iter.hasNext();) {  
            //获得序列中的下一个元素  
            FileItem item = (FileItem) iter.next(); 
            if (!item.isFormField()) {         
            	String arr[] = item.getName().split("\\.");
                
                String fieldName = item.getFieldName();
                String newFileName = new Date().getTime() + "_" + (i+1) + (arr.length>0?"." + arr[arr.length-1]:"");
                i++;
                
                if(fieldName.startsWith("Img"))
                {
                	if(imgs.length() != 0)
                	{
                		imgs += ",";
                	}
                	imgs += newFileName;
                }
                else if(fieldName.startsWith("Video"))
                {
                	if(videos.length() != 0)
                	{
                		videos += ",";
                	}
                	videos += newFileName;
                }
                else if(fieldName.startsWith("Audio"))
                {
                	if(audios.length() != 0)
                	{
                		audios += ",";
                	}
                	audios += newFileName;
                }
            	
                //上传文件的名称和完整路径  
                String fileName = item.getName();  
                  
                long size = item.getSize();  
                //判断是否选择了文件  
                if ((fileName == null || fileName.equals("")) && size == 0) {  
                    continue;  
                }  
                //截取字符串 如：C:\WINDOWS\Debug\PASSWD.LOG  
                fileName = newFileName;//fileName.substring(fileName.lastIndexOf("\\") + 1, fileName.length());  
                  
                 // 保存文件在服务器的物理磁盘中：第一个参数是：完整路径（不包括文件名）第二个参数是：文件名称     
                //item.write(file);  
                //修改文件名和物料名一致，且强行修改了文件扩展名为gif  
                //item.write(new File(uploadPath, itemNo + ".gif"));  
                //将文件保存到目录下，不修改文件名  
                try {
					item.write(new File(uploadPath, fileName));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  

                //将图片文件名写入打数据库                    
                //itemManager.uploadItemImage(itemNo, fileName);  
                  
            } 
		}
		Map mapRet = new HashMap();
        mapRet.put("imgs", imgs);
        mapRet.put("videos", videos);
        mapRet.put("audios", audios);
        return mapRet;
	}*/
}
