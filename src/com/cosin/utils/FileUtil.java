/*    */ package com.cosin.utils;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.io.FileInputStream;
/*    */ import java.io.FileOutputStream;
/*    */ import java.io.InputStream;
/*    */ import java.io.PrintStream;
/*    */ 
/*    */ public class FileUtil
/*    */ {
/*    */   public static void copyFile(String oldPath, String newPath)
/*    */   {
/*    */     try
/*    */     {
/* 17 */       int bytesum = 0;
/* 18 */       int byteread = 0;
/* 19 */       File oldfile = new File(oldPath);
/* 20 */       if (oldfile.exists()) {
/* 21 */         InputStream inStream = new FileInputStream(oldPath);
/* 22 */         FileOutputStream fs = new FileOutputStream(newPath);
/* 23 */         byte[] buffer = new byte[1444];
/*    */ 
/* 25 */         while ((byteread = inStream.read(buffer)) != -1) {
/* 26 */           bytesum += byteread;
/* 27 */           System.out.println(bytesum);
/* 28 */           fs.write(buffer, 0, byteread);
/*    */         }
/* 30 */         inStream.close();
/*    */       }
/*    */     }
/*    */     catch (Exception e) {
/* 34 */       System.out.println("复制单个文件操作出错");
/* 35 */       e.printStackTrace();
/*    */     }
/*    */   }
/*    */ }

/* Location:           E:\QQ\GraduateService\WEB-INF\classes\
 * Qualified Name:     com.cosin.utils.FileUtil
 * JD-Core Version:    0.5.3
 */