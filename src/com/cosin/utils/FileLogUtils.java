/*    */ package com.cosin.utils;
/*    */ 
/*    */ import java.io.BufferedWriter;
/*    */ import java.io.File;
/*    */ import java.io.FileWriter;
/*    */ import java.io.IOException;
/*    */ 
/*    */ public class FileLogUtils
/*    */ {
/*    */   public static void write(String filename, String content)
/*    */   {
/* 11 */     String pathName = filename;
/*    */     try {
/* 13 */       File file = new File(pathName);
/* 14 */       if (!(file.exists()))
/* 15 */         file.createNewFile();
/* 16 */       BufferedWriter br = new BufferedWriter(new FileWriter(file, true));
/* 17 */       br.write(content);
/* 18 */       br.flush();
/* 19 */       br.close();
/*    */     }
/*    */     catch (IOException e) {
/* 22 */       e.printStackTrace();
/*    */     }
/*    */   }
/*    */ }

/* Location:           E:\QQ\GraduateService\WEB-INF\classes\
 * Qualified Name:     com.cosin.utils.FileLogUtils
 * JD-Core Version:    0.5.3
 */