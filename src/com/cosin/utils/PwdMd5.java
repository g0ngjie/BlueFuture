package com.cosin.utils;

import java.security.MessageDigest;

import org.apache.commons.id.Hex;

/**
 * Md5加密
 * @author kun
 *
 */
public class PwdMd5 {
	/**
	 * 32位加密
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static String pwdMD5(String str) throws Exception {  
		MessageDigest md5 = null ;  
	    try {  
	        md5 = MessageDigest.getInstance("MD5" );  
	    }
	    catch (Exception e) {  
	        e.printStackTrace();  
	        return   "" ;  
	    }  
	    char[] charArray = str.toCharArray();  
	    byte[] byteArray =  new byte[charArray.length];  
	    for ( int  i =  0 ; i < charArray.length; i++) {  
	        byteArray[i] = (byte)charArray[i];  
	    }  
	    byte[] md5Bytes = md5.digest(byteArray);  
	    StringBuffer hexValue = new  StringBuffer();  
	    for (int i = 0 ; i < md5Bytes.length; i++) {  
	        int val = ((int)md5Bytes[i])&0xff ;  
	        if (val < 16) {  
	            hexValue.append("0");  
	        }  
	        hexValue.append(Integer.toHexString(val));  
	    }  
	    return  hexValue.toString();  
	}
	/**
	 * 生层32随机数
	 * @return
	 */
	public static String getCode() {
		  return new String(Hex.encodeHex(org.apache.commons.id.uuid.UUID
		    .randomUUID().getRawBytes()));
	}
	
	
}
