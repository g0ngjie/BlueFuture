package com.cosin.utils;

import java.sql.Timestamp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * json工具类   2016-02-19
 * @author zhb
 * @version 1.0
 */
public class JsonUtils {
	public static String fromObject(Object obj)
	{
		JsonConfig config = new JsonConfig();
        config.registerJsonValueProcessor(Timestamp.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		JSONObject jsonObject = new JSONObject();
		return JSONObject.fromObject(obj, config).toString();	
	}
	
	public static String fromObject(Object obj, String[] excludes)
	{
		JsonConfig config = new JsonConfig();
		config.setExcludes(excludes);
        config.registerJsonValueProcessor(Timestamp.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		JSONObject jsonObject = new JSONObject();
		return JSONObject.fromObject(obj, config).toString();	
	}
	
	public static String fromArrayObject(Object obj)
	{
		JsonConfig config = new JsonConfig();
        config.registerJsonValueProcessor(Timestamp.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
        JSONArray jsonObject = new JSONArray();
		return JSONArray.fromObject(obj, config).toString();
	}
	
	public static String fromArrayObject(Object obj, String[] excludes)
	{
		JsonConfig config = new JsonConfig();
		config.setExcludes(excludes);
        config.registerJsonValueProcessor(Timestamp.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
        JSONArray jsonObject = new JSONArray();
		return JSONArray.fromObject(obj, config).toString();
	}
	
	public static String simpleResults(List list)
	{
		Map map = new HashMap();
		map.put("code", 100);
		map.put("results", list);
		map.put("count", list.size());
		JsonConfig config = new JsonConfig();
        config.registerJsonValueProcessor(Timestamp.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		String str = JSONObject.fromObject(map, config).toString();
		return str;
	}
	
	/**
     * 将json格式的字符串解析成Map对象 <li>
     * json格式：{"name":"admin","retries":"3fff","testname"
     * :"ddd","testretries":"fffffffff"}
     */
	public static Map toHashMap(String  jsonStr)
    {
		JSONObject json = JSONObject.fromObject(jsonStr);
		Map map = new HashMap();
	    for(Object k : json.keySet()){
	      Object v = json.get(k); 
	      //如果内层还是数组的话，继续解析
	        map.put(k.toString(), v);
	     }
	    return map;
    }

}
