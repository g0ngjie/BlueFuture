/*    */ package com.cosin.utils;
/*    */ 
/*    */ import java.util.List;
/*    */ import java.util.Map;
/*    */ import javax.servlet.http.HttpServletRequest;
/*    */ import javax.servlet.http.HttpServletResponse;
/*    */ 
/*    */ public class RequestFiledUtils
/*    */ {
/*    */   private HttpServletRequest request;
/* 14 */   private Map map = null;
/*    */ 
/*    */   public RequestFiledUtils(HttpServletResponse response, HttpServletRequest request, String dir) {
/* 17 */     this.request = request;
/*    */     try
/*    */     {
/* 21 */       this.map = FileUploadUtils.saveUploadFileForSpringMvc(request, response);
/*    */     }
/*    */     catch (Exception e)
/*    */     {
/* 27 */       this.map = null;
/*    */     }
/*    */   }
/*    */ 
/*    */   public List getListFile()
/*    */   {
/* 33 */     return ((List)this.map.get("filesArray"));
/*    */   }
/*    */ 
/*    */   public String getParam(String paramName)
/*    */   {
/* 38 */     if (this.map != null)
/*    */     {
/* 40 */       return ((String)this.map.get(paramName));
/*    */     }
/*    */ 
/* 44 */     return this.request.getParameter(paramName);
/*    */   }
/*    */ 
/*    */   public String getFileParam(String paramName)
/*    */   {
/* 50 */     String val = null;
/* 51 */     if (this.map != null)
/*    */     {
/* 53 */       val = (String)this.map.get(paramName);
/*    */     }
/*    */     else
/*    */     {
/* 57 */       val = this.request.getParameter(paramName);
/*    */     }
/*    */ 
/* 60 */     if (val == null) {
/* 61 */       val = "";
/*    */     }
/* 63 */     return val;
/*    */   }
/*    */ }

/* Location:           E:\QQ\GraduateService\WEB-INF\classes\
 * Qualified Name:     com.cosin.utils.RequestFiledUtils
 * JD-Core Version:    0.5.3
 */