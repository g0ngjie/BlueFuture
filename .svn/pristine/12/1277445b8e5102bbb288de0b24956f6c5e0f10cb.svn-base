package com.cosin.web.service.blue;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.blue.IEmployeeDao;
import com.cosin.web.entity.Employee;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysSubrole;
import com.cosin.web.entity.SysUser;

@Service
@Transactional
public class EmployeeManager implements IEmployeeManager {

	@Autowired
	private IEmployeeDao employeeDao;

	/**
	 * 分页
	 */
	@Override
	public List getEmployeeLimit(String SsName, int start, int limit) {
		return employeeDao.getEmployeeLimit(SsName, start, limit);
	}

	/**
	 * 数量
	 */
	@Override
	public int getEmployeeCount(String SsName) {
		return employeeDao.getEmployeeCount(SsName);
	}

	/**
	 * 获取角色名称
	 */
	@Override
	public List<SysRole> getlistRole() {
		List listRes = new ArrayList();
		List<SysRole> listSysRole = employeeDao.getlistRole();
		for (int i = 0; i < listSysRole.size(); i++) {
			SysRole sysRole = listSysRole.get(i);
			Map map = new HashMap();
			map.put("roleName", sysRole.getRolerName());
			map.put("roleId", sysRole.getRoleId());
			listRes.add(map);
		}
		return listRes;
	}

	/**
	 * 通过 roleId 获取 类型
	 */
	@Override
	public List<SysSubrole> getlistSubRole(String roleId) {
		List listRes = new ArrayList();
		List<SysSubrole> listSysSubrole = employeeDao.getlistSubRole(roleId);
		for (int i = 0; i < listSysSubrole.size(); i++) {
			SysSubrole sysSubrole = listSysSubrole.get(i);
			Map map = new HashMap();
			map.put("typeName", sysSubrole.getTypeName());
			map.put("subRoleId", sysSubrole.getSubRoleId());
			listRes.add(map);
		}
		return listRes;
	}

	/**
	 * 获取school 园区   操作者级别
	 * 思路：控制层
	 */
	@Override
	public List getlistSchool(String createUserId) {
		List listRes = new ArrayList();
		SysUser sysUser = employeeDao.getSysUserById(createUserId);
		//通过createUserId 获取 role角色 
		String roleId = sysUser.getSysRole().getRoleId();
		//通过roleId 查询中间表sysRoleSchoolPower
		List<SysRoleSchoolPower> listsysRoleSchoolPower = employeeDao.getListsysRoleSchoolPower(roleId);
		for (int i = 0; i < listsysRoleSchoolPower.size(); i++) {
			SysRoleSchoolPower sysRoleSchoolPower = listsysRoleSchoolPower.get(i);
			Map map = new HashMap();
			map.put("schoolName", sysRoleSchoolPower.getSchool().getSchoolName());
			map.put("schoolId", sysRoleSchoolPower.getSchool().getSchoolId());
			listRes.add(map);
		}
		return listRes;
	}

	/**
	 * 保存职工
	 */
	@Override
	public void saveSchool(String userId, String employeeId, String schoolId,
			String roleId, String subRoleId, String sysUserName, String sex,
			String tel, String mode) {

		if (mode.equals("edit")) {
			
			Employee employee = employeeDao.getEmployeeById(employeeId);
			employee.setCreateDate(new Timestamp(new Date().getTime()));
			//保存园区
			School school = employeeDao.getSchoolById(schoolId);
			employee.setSchool(school);
			//查出sysRole表
			SysRole sysRole = employeeDao.getSysRoleById(roleId);
			//查出sysSubRole表
			SysSubrole sysSubrole = employeeDao.getSysSubRoleById(subRoleId);
			SysUser sysUser = employee.getSysUserByUserId();
			
			if(sex.equals("男")){
				sysUser.setSex(1);
			}else if(sex.equals("女")){
				sysUser.setSex(0);
			}
			sysUser.setUserName(sysUserName);
			sysUser.setMobile(tel);
			sysUser.setSysRole(sysRole);
			sysUser.setSysSubrole(sysSubrole);
			employeeDao.saveSysUser(sysUser);
			employee.setSysUserByUserId(sysUser);
			
			employeeDao.save(employee);
			
		}else{
			
			Employee employee = new Employee();
			employee.setIsDel(0);
			employee.setCreateDate(new Timestamp(new Date().getTime()));
			employee.setEnable(0);
			//保存园区
			School school = employeeDao.getSchoolById(schoolId);
			employee.setSchool(school);
			//查出sysRole表
			SysRole sysRole = employeeDao.getSysRoleById(roleId);
			//查出sysSubRole表
			SysSubrole sysSubrole = employeeDao.getSysSubRoleById(subRoleId);
			//创建sysUser表
			SysUser sysUser = new SysUser();
			sysUser.setIsDel(0);
			sysUser.setCreateDate(new Timestamp(new Date().getTime()));
			sysUser.setType(1);
			sysUser.setEnable(0);
			//保存角色
			sysUser.setSysRole(sysRole);
			//保存类型
			sysUser.setSysSubrole(sysSubrole);
			//保存性别 0女 1男
			int sexx =new Integer (sex);
			sysUser.setSex(sexx);
			sysUser.setMobile(tel);
			sysUser.setUserName(sysUserName);
			//保存 添加的user 到sysUser表
			employeeDao.saveSysUser(sysUser);
			employee.setSysUserByUserId(sysUser);
			//保存employee表中的 createId 创建者
			SysUser createUser = employeeDao.getSysUserById(userId);
			employee.setSysUserByCreateUserId(createUser);
			
			employeeDao.save(employee);
			
			
		}
		
	}

	/**
	 * 删除操作
	 */
	@Override
	public void delEmployee(String delKeys) {
		
		
		Employee employee = employeeDao.getEmployeeById(delKeys);
		employee.setIsDel(1);
		//删除关联的sysUser表
		SysUser sysUser = employee.getSysUserByUserId();
		sysUser.setIsDel(1);
		employeeDao.saveSysUser(sysUser);
		
		employeeDao.save(employee);
		
	}

	/* 
	 * 根据EmployeeId 查出对象Employee
	 * 下午4:46:52
	 */
	@Override
	public Employee getEmployeeById(String employeeId) {
		return employeeDao.getEmployeeById(employeeId);
	}

	
	
	
	
	
}
