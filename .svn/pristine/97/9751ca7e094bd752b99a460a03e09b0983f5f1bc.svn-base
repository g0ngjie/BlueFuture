package com.cosin.web.controller.blue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.Recipe;
import com.cosin.web.entity.RecipeRecord;
import com.cosin.web.entity.RecipeSchool;
import com.cosin.web.entity.School;
import com.cosin.web.service.blue.IRecipeManager;


@Scope("prototype")
@Controller
public class RecipeController {

	@Autowired
	private IRecipeManager recipeManager;

	/**
	 * 跳转到每周食谱页面
	 * @return
	 */
	@RequestMapping(value="/recipe/recipe")
	public String recipe(){
		return "/recipe/recipe";
	}

	/**
	 *查询功能  操作
	 *分页功能  操作
	 *搜索功能  操作
	 * @param request 获取前台页面属性
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/recipe/selectrecipe",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectsubject(HttpServletRequest request,HttpServletResponse response){
		String SsName = request.getParameter("SsName");
		Map pageParam = PageUtils.getPageParam(request);
		List<RecipeRecord> listsubject = recipeManager.getRecipeLimit(SsName,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		List list = new ArrayList();
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		for (int i = 0; i < listsubject.size(); i++) {
			RecipeRecord vcsubject=listsubject.get(i);
			Map map  = new HashMap();
			map.put("recipeId",vcsubject.getRecipeRecordId());
			//发布时间
			map.put("createTime", vcsubject.getCreateDate());
			//食谱开始时间
			map.put("beginDate", date.format(vcsubject.getBeginTime()));
			//发布人
			map.put("UserName", vcsubject.getSysUser().getUserName());
			//查看人数
			map.put("LookNum", vcsubject.getLookNum());
			//园区
			String recipeRecordId = vcsubject.getRecipeRecordId();
			List<RecipeSchool> listRoScP = recipeManager.getRecipeSchoolById(recipeRecordId);
			String schoolName="";
			for (int j = 0; j < listRoScP.size(); j++) {
				RecipeSchool recipeSchool = listRoScP.get(j);
				schoolName+= recipeSchool.getSchool().getSchoolName()+",";
			}
			map.put("validSchool", schoolName);
			
			
			list.add(map);
		}
		int total = recipeManager.getRecipeCount(SsName);
		Map mapRet = PageUtils.getPageParam(list,total);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	} 
	
	/**
	 * 草稿箱 跳转
	 * @param request
	 * @return
	 */
	@RequestMapping(value="recipe/recipeData")
    public ModelAndView recipe(HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/recipe/recipeDraft");
	    return modelAndView;
    }
	
	/**
	 * 草稿箱 列表查询
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/recipe/selectdraft",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectDraft(HttpServletRequest request,HttpServletResponse response){
		String SsName = request.getParameter("SsName");	
		Map pageParam = PageUtils.getPageParam(request);
		List<RecipeRecord> listsubject = recipeManager.getDraftLimit(SsName,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		List list = new ArrayList();
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		for (int i = 0; i < listsubject.size(); i++) {
			RecipeRecord vcsubject=listsubject.get(i);
			Map map  = new HashMap();
			map.put("recipeId",vcsubject.getRecipeRecordId());
			//发布时间
			map.put("createTime", vcsubject.getCreateDate());
			//食谱开始时间
			map.put("beginDate", date.format(vcsubject.getBeginTime()));
			//发布人
			map.put("UserName", vcsubject.getSysUser().getUserName());
			
			
			
			list.add(map);
		}
			int total = recipeManager.getRecipeCount(SsName);
			Map mapRet = PageUtils.getPageParam(list,total);
			String resStr = JsonUtils.fromObject(mapRet);
			return resStr;
	} 
	
	
	/**
	 * 每周食谱 查看页面跳转
	 * @param request
	 * @return
	 */
//	@RequestMapping(value="recipe/recipeLook")
//    public ModelAndView look(HttpServletRequest request){
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.setViewName("/recipe/recipeLook");
//	    return modelAndView;
//    }
	
	
	/**
	 * 删除功能 操作
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/recipe/delRecipe", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String delsubject(HttpServletRequest request,HttpServletResponse response){
		
		String deleteKeys=request.getParameter("delKeys");
		String[] keyArr = deleteKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null){
				
				RecipeRecord recipe = recipeManager.getRecipeId(key);
				recipeManager.recipeDelId(key);
			}
				
		}
			Map map = new HashMap();
			map.put("code", 100);
			map.put("msg", "删除成功");
			String resStr = JsonUtils.fromObject(map);
			return resStr;
	}
	
	/**
	 * 添加  食谱
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/recipe/saveRecipe", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveRecipe(HttpServletRequest request,HttpServletResponse response) {
		
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		//发布人
		String user = userSession.userName;
		String userId = userSession.userId;

		String beginDate = request.getParameter("beginDate");
		String breakfast = request.getParameter("breakfastt");
		String light1 = request.getParameter("light11");
		String lunch = request.getParameter("lunchh");
		String light2 = request.getParameter("light22");
		String dinner = request.getParameter("dinnerr");
		
		String recipeId = request.getParameter("recipeId");
		
		//获取 通知对象  园区（字符串拼接）
		String schoolVar = request.getParameter("schoolVar");
		//添加或存稿
		String mode = request.getParameter("mode");
		recipeManager.saveRecipe(recipeId,userId,beginDate,breakfast,light1,lunch,light2,dinner,mode,schoolVar);
		
		
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功");
		String resStr = JsonUtils.fromObject(map).toString();
		return resStr;
	}
	
	/**
	 * 撤回操作 并移至草稿箱
	 */
	@RequestMapping(value="/recipe/roolBackRecipe", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String roolBackRecipe(HttpServletRequest request,HttpServletResponse response){
		
		String recipeId=request.getParameter("openBack");
		
		recipeManager.roolBackRecipe(recipeId);
		
		
			Map map = new HashMap();
			map.put("code", 100);
			map.put("msg", "操作成功");
			String resStr = JsonUtils.fromObject(map);
			return resStr;
	}
	

	
	/**
	 * 编辑 查看 食谱
	 */
	@RequestMapping(value = "recipe/bjRecipe", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String bjRecipe(HttpServletRequest request) {
		String recipeId = request.getParameter("recipeId");
		RecipeRecord recipeRecord= recipeManager.getRecipeRecordById(recipeId);
		//食谱查询 周一 至 周五
		List<Recipe> recipe = recipeManager.getRecipeById(recipeId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("beginTime", recipeRecord.getBeginTime());
		List list = new ArrayList();
		
		//周一   一天
		String one = recipe.get(0).getContent();
		String[] oneList = one.split("\\|");
		map.put("one1", oneList[1]);
		map.put("one2", oneList[2]);
		map.put("one3", oneList[3]);
		map.put("one4", oneList[4]);
		map.put("one5", oneList[5]);
		
		//周二 一天
		String two= recipe.get(1).getContent();
		String[] twoList = two.split("\\|");
		map.put("two1", twoList[1]);
		map.put("two2", twoList[2]);
		map.put("two3", twoList[3]);
		map.put("two4", twoList[4]);
		map.put("two5", twoList[5]);
		
		//周三   一天
		String three = recipe.get(2).getContent();
		String[] threeList = three.split("\\|");
		map.put("three1", threeList[1]);
		map.put("three2", threeList[2]);
		map.put("three3", threeList[3]);
		map.put("three4", threeList[4]);
		map.put("three5", threeList[5]);
		
		//周四   一天
		String four = recipe.get(3).getContent();
		String[] fourList = four.split("\\|");
		map.put("four1", fourList[1]);
		map.put("four2", fourList[2]);
		map.put("four3", fourList[3]);
		map.put("four4", fourList[4]);
		map.put("four5", fourList[5]);
		
		//周五   一天
		String five = recipe.get(4).getContent();
		String[] fiveList = five.split("\\|");
		map.put("five1", fiveList[1]);
		map.put("five2", fiveList[2]);
		map.put("five3", fiveList[3]);
		map.put("five4", fiveList[4]);
		map.put("five5", fiveList[5]);
		

		map.put("code", "100");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
	/**
	 * 编辑 查看 园区 被选中
	 */
	@RequestMapping(value = "recipe/bjSchool", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String bjSchool(HttpServletRequest request) {
		String recipeId = request.getParameter("recipeId");
		List<RecipeSchool> recipeSchool = recipeManager.getRecipeSchoolById(recipeId);
		List list = new ArrayList();
		for (int i = 0; i < recipeSchool.size(); i++) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("schoolId", recipeSchool.get(i).getSchool().getSchoolId());	
			map.put("schoolName", recipeSchool.get(i).getSchool().getSchoolName());
			list.add(map);
		}

		String resStr = JsonUtils.fromArrayObject(list);
		return resStr;
	}
	
	
	
	
	
	
	
	
	
	

}
