package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * Classes entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "classes", catalog = "edudb")
public class Classes implements java.io.Serializable {

	// Fields

	private String classesId;
	private Employee employee;
	private School school;
	private SysUser sysUser;
	private String classesName;
	private String intoYear;
	private Timestamp createDate;
	private Integer enable;
	private Integer isDel;
	private String grade;
	private Set<Classteacher> classteachers = new HashSet<Classteacher>(0);
	private Set<NoticeSub> noticeSubs = new HashSet<NoticeSub>(0);
	private Set<HistoryClass> historyClasses = new HashSet<HistoryClass>(0);
	private Set<GradeClass> gradeClasses = new HashSet<GradeClass>(0);
	private Set<Attendance> attendances = new HashSet<Attendance>(0);
	private Set<StudentLeave> studentLeaves = new HashSet<StudentLeave>(0);
	private Set<Student> students = new HashSet<Student>(0);

	// Constructors

	/** default constructor */
	public Classes() {
	}

	/** full constructor */
	public Classes(Employee employee, School school, SysUser sysUser,
			String classesName, String intoYear, Timestamp createDate,
			Integer enable, Integer isDel, String grade,
			Set<Classteacher> classteachers, Set<NoticeSub> noticeSubs,
			Set<HistoryClass> historyClasses, Set<GradeClass> gradeClasses,
			Set<Attendance> attendances, Set<StudentLeave> studentLeaves,
			Set<Student> students) {
		this.employee = employee;
		this.school = school;
		this.sysUser = sysUser;
		this.classesName = classesName;
		this.intoYear = intoYear;
		this.createDate = createDate;
		this.enable = enable;
		this.isDel = isDel;
		this.grade = grade;
		this.classteachers = classteachers;
		this.noticeSubs = noticeSubs;
		this.historyClasses = historyClasses;
		this.gradeClasses = gradeClasses;
		this.attendances = attendances;
		this.studentLeaves = studentLeaves;
		this.students = students;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "classesId", unique = true, nullable = false, length = 32)
	public String getClassesId() {
		return this.classesId;
	}

	public void setClassesId(String classesId) {
		this.classesId = classesId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employeeId")
	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "schoolId")
	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createUserId")
	public SysUser getSysUser() {
		return this.sysUser;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	@Column(name = "classesName", length = 64)
	public String getClassesName() {
		return this.classesName;
	}

	public void setClassesName(String classesName) {
		this.classesName = classesName;
	}

	@Column(name = "intoYear", length = 32)
	public String getIntoYear() {
		return this.intoYear;
	}

	public void setIntoYear(String intoYear) {
		this.intoYear = intoYear;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name = "enable")
	public Integer getEnable() {
		return this.enable;
	}

	public void setEnable(Integer enable) {
		this.enable = enable;
	}

	@Column(name = "isDel")
	public Integer getIsDel() {
		return this.isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	@Column(name = "grade", length = 32)
	public String getGrade() {
		return this.grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "classes")
	public Set<Classteacher> getClassteachers() {
		return this.classteachers;
	}

	public void setClassteachers(Set<Classteacher> classteachers) {
		this.classteachers = classteachers;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "classes")
	public Set<NoticeSub> getNoticeSubs() {
		return this.noticeSubs;
	}

	public void setNoticeSubs(Set<NoticeSub> noticeSubs) {
		this.noticeSubs = noticeSubs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "classes")
	public Set<HistoryClass> getHistoryClasses() {
		return this.historyClasses;
	}

	public void setHistoryClasses(Set<HistoryClass> historyClasses) {
		this.historyClasses = historyClasses;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "classes")
	public Set<GradeClass> getGradeClasses() {
		return this.gradeClasses;
	}

	public void setGradeClasses(Set<GradeClass> gradeClasses) {
		this.gradeClasses = gradeClasses;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "classes")
	public Set<Attendance> getAttendances() {
		return this.attendances;
	}

	public void setAttendances(Set<Attendance> attendances) {
		this.attendances = attendances;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "classes")
	public Set<StudentLeave> getStudentLeaves() {
		return this.studentLeaves;
	}

	public void setStudentLeaves(Set<StudentLeave> studentLeaves) {
		this.studentLeaves = studentLeaves;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "classes")
	public Set<Student> getStudents() {
		return this.students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

}