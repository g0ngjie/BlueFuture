package com.cosin.web.service.blue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.utils.DateUtils;
import com.cosin.web.dao.blue.IRecipeDao;
import com.cosin.web.entity.Recipe;
import com.cosin.web.entity.RecipeRecord;
import com.cosin.web.entity.RecipeSchool;
import com.cosin.web.entity.School;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRoleSchoolPower;
import com.cosin.web.entity.SysUser;

@Service
@Transactional
public class RecipeManager implements IRecipeManager {

	@Autowired
	private IRecipeDao recipeDao;

    /** 
     * 根据日期获得所在周的日期  
     * @param mdate 
     * @return 
     */  
    @SuppressWarnings("deprecation")  
    public static List<Date> dateToWeek(Date mdate) {  
        int b = mdate.getDay();  
        Date fdate;  
        List<Date> list = new ArrayList<Date>();  
        Long fTime = mdate.getTime() - b * 24 * 3600000;  
        for (int a = 1; a <= 7; a++) {  
            fdate = new Date();  
            fdate.setTime(fTime + (a * 24 * 3600000));  
            list.add(a-1, fdate);  
        }  
        return list;  
    }
	//分页
		@Override
		public List getRecipeLimit(String startDate, int start, int limit) throws ParseException {
			 // 定义输出日期格式  
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        // 比如今天是2012-12-25  
	        if(startDate!=null){
	        	List<Date> days = dateToWeek(sdf.parse(startDate));  
	        	String monday = sdf.format(days.get(0));
	        	String fri = sdf.format(days.get(4));
	        	return recipeDao.getRecipeLimit(monday,fri, start, limit);
	        }
			return recipeDao.getRecipeLimit(null,null, start, limit);
	        
		}
		//数量
		@Override
		public int getRecipeCount(String startDate) throws ParseException {
			 // 定义输出日期格式  
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        // 比如今天是2012-12-25  
	        if(startDate!=null){
	        	List<Date> days = dateToWeek(sdf.parse(startDate));  
		        String monday = sdf.format(days.get(0));
		        String fri = sdf.format(days.get(4));
				return recipeDao.getRecipeCount(monday,fri);
	        }
	        return recipeDao.getRecipeCount(null,null);
		}
		//获取SubjectId
		@Override
		public RecipeRecord getRecipeId(String recipeId) {
			return recipeDao.getRecipeId(recipeId);
		}
		//添加
		@Override
		public void saveRecipe(RecipeRecord recipe) {
			
		}
		
		
		//草稿箱列表查询 
		@Override
		public List<RecipeRecord> getDraftLimit(String startDate, Integer start,
				Integer limit) throws ParseException {
			 // 定义输出日期格式  
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        // 比如今天是2012-12-25  
	        if(startDate!=null){
	        	List<Date> days = dateToWeek(sdf.parse(startDate));  
	        	String monday = sdf.format(days.get(0));
	        	String fri = sdf.format(days.get(4));
	        	return recipeDao.getDraftLimit(monday,fri, start, limit);
	        }
			return recipeDao.getDraftLimit(null,null, start, limit);
		}
		
		
		//删除
		@Override
		public void recipeDelId(String recipeId) {
			recipeDao.recipeDelId(recipeId);
		}
		
		
		/**
		 * 保存 食谱
		 */
		@Override
		public void saveRecipe(String recipeId, String userId,
				String beginDate, String breakfast, String light1,
				String lunch, String light2, String dinner, String mode,String schoolVar, String draf) {

			
			if("edit".equals(mode)){
				//周一
				breakfast =  breakfast.replaceAll(",", "|");
				//周二
				light1 =  light1.replaceAll(",", "|");
				//周三
				lunch =  lunch.replaceAll(",", "|");
				//周四
				light2 =  light2.replaceAll(",", "|");
				//周五
				dinner =  dinner.replaceAll(",", "|");
				
				String strFirstDate = DateUtils.getWeekBegin(beginDate);				

				RecipeRecord recipeRecord = recipeDao.getRecipeId(recipeId);
				recipeRecord.setBeginTime(DateUtils.dateToTs(DateUtils.parseStrToDate(DateUtils.DEF_FORMAT, beginDate + " 00:00:00")));
				recipeRecord.setCreateDate(DateUtils.dateToTs(new Date()));
				recipeRecord.setIsDel(0);
				if("saveDra".equals(draf)){
					recipeRecord.setIsDraft(1);
				}else{
					recipeRecord.setIsDraft(0);
				}
				recipeRecord.setLookNum(0);
				SysUser sysUser = recipeDao.getSysUserById(userId);
				recipeRecord.setSysUser(sysUser);
				
				Date firstDate = DateUtils.parseStrToDate(DateUtils.DEF_FORMAT, strFirstDate + " 00:00:00");
				
				List<Recipe> recipeList = recipeDao.getRecipeById(recipeId);
				recipeDao.delRecipe(recipeList);
				
				for(int i=0; i<5; i++)
				{
					long dd = firstDate.getTime() + i*24*60*60*1000;
					Recipe recipe = new Recipe();
					if(i == 0 && breakfast!="" && breakfast != null)
						recipe.setContent(breakfast);
					if(i == 1 && light1!="" && light1 != null)
						recipe.setContent(light1);
					if(i == 2 && lunch!="" && lunch != null)
						recipe.setContent(lunch);
					if(i == 3 && light2!="" && light2 != null)
						recipe.setContent(light2);
					if(i == 4 && dinner!="" && dinner != null)
						recipe.setContent(dinner);
					recipe.setCreateDate(DateUtils.dateToTs(new Date(dd)));
					recipe.setRecipeRecord(recipeRecord);
					recipeDao.saveRecipe(recipe);
				}
				
					

				recipeDao.saveRecipeRecord(recipeRecord);
				
				//真删  RecipeSchool中间表
				List<RecipeSchool> recipeSchoolList = recipeDao.getRecipeSchoolById(recipeId);
				recipeDao.delRecipeSchool(recipeSchoolList);
					
				//RecipeSchool 
				if(schoolVar!=null){
					
					String[] keyArr = schoolVar.split(",");
					for (int i = 0; i < keyArr.length; i++) {
						String key = keyArr[i];
						if (!"".equals(key) && key != null) {
							RecipeSchool recipeSchool = new RecipeSchool();
							recipeSchool.setRecipeRecord(recipeRecord);
							//添加园区 外键
							School school = recipeDao.getSchoolById(key);
							recipeSchool.setSchool(school);
							//保存 外键表
							recipeDao.saveRecipeSchool(recipeSchool);
						}
					}
				}				
				
			}else{
				
				
				//周一
				breakfast =  breakfast.replaceAll(",", "|");
				//周二
				light1 =  light1.replaceAll(",", "|");
				//周三
				lunch =  lunch.replaceAll(",", "|");
				//周四
				light2 =  light2.replaceAll(",", "|");
				//周五
				dinner =  dinner.replaceAll(",", "|");
				
				String strFirstDate = DateUtils.getWeekBegin(beginDate);
				
				
				RecipeRecord recipeRecord = new RecipeRecord();
				recipeRecord.setBeginTime(DateUtils.dateToTs(DateUtils.parseStrToDate(DateUtils.DEF_FORMAT, beginDate + " 00:00:00")));
				recipeRecord.setCreateDate(DateUtils.dateToTs(new Date()));
				recipeRecord.setIsDel(0);
				if("saveDra".equals(draf)){
					recipeRecord.setIsDraft(1);
				}else{
					recipeRecord.setIsDraft(0);
				}
				recipeRecord.setLookNum(0);
				SysUser sysUser = recipeDao.getSysUserById(userId);
				recipeRecord.setSysUser(sysUser);
				
				Date firstDate = DateUtils.parseStrToDate(DateUtils.DEF_FORMAT, strFirstDate + " 00:00:00");
				for(int i=0; i<5; i++)
				{
					//
					long dd = firstDate.getTime() + i*24*60*60*1000;
					Recipe recipe = new Recipe();
					if(i == 0 && breakfast!="" && breakfast != null)
						recipe.setContent(breakfast);
					if(i == 1 && light1!="" && light1 != null)
						recipe.setContent(light1);
					if(i == 2 && lunch!="" && lunch != null)
						recipe.setContent(lunch);
					if(i == 3 && light2!="" && light2 != null)
						recipe.setContent(light2);
					if(i == 4 && dinner!="" && dinner != null)
						recipe.setContent(dinner);
					recipe.setCreateDate(DateUtils.dateToTs(new Date(dd)));
					recipe.setRecipeRecord(recipeRecord);
					recipeDao.saveRecipe(recipe);
				}
				
					

				recipeDao.saveRecipeRecord(recipeRecord);
				
				if(schoolVar!=null){
					
					String[] keyArr = schoolVar.split(",");
					for (int i = 0; i < keyArr.length; i++) {
						String key = keyArr[i];
						if (!"".equals(key) && key != null) {
							RecipeSchool recipeSchool = new RecipeSchool();
							recipeSchool.setRecipeRecord(recipeRecord);
							//添加园区 外键
							School school = recipeDao.getSchoolById(key);
							recipeSchool.setSchool(school);
							//保存 外键表
							recipeDao.saveRecipeSchool(recipeSchool);
						}
					}
				}
				
				
			}
			
			
			
			
			
		}
		
		/**
		 * 通过recipeRecordId获取对象
		 */
		@Override
		public List<RecipeSchool> getRecipeSchoolById(
				String recipeRecordId) {
			
			return recipeDao.getRecipeSchoolById(recipeRecordId);
		}
		
		/**
		 * 撤回操作 存至草稿箱
		 */
		@Override
		public void roolBackRecipe(String recipeId) {

			RecipeRecord recipeRecord = recipeDao.getRecipeRecordById(recipeId);
			recipeRecord.setIsDraft(1);
			recipeDao.saveRecipeRecord(recipeRecord);
		}
		
		/**
		 * 通过recipeRecordId 查询 recipeRecord
		 */
		@Override
		public RecipeRecord getRecipeRecordById(String recipeId) {
			return recipeDao.getRecipeRecordById(recipeId);
		}
		
		/**
		 * 通过recipeRecordId 查询食谱  Recipe  周一至 周五
		 */
		@Override
		public List<Recipe> getRecipeById(String recipeId) {

			return recipeDao.getRecipeById(recipeId);
		}
		/**
		 * 2016年10月10日下午3:04:49
		 *  龚杰
		 *  注释：
		 * @throws ParseException 
		 */
		@Override
		public int getDraftCount(String startDate) throws ParseException {
			 // 定义输出日期格式  
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        // 比如今天是2012-12-25  
	        if(startDate!=null){
	        	List<Date> days = dateToWeek(sdf.parse(startDate));  
		        String monday = sdf.format(days.get(0));
		        String fri = sdf.format(days.get(4));
				return recipeDao.getDraftCount(monday,fri);
	        }
	        return recipeDao.getDraftCount(null,null);
		}
		
		
		
		
		
		
		
		
		
		
		
}