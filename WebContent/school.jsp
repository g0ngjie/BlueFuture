<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
    <title>园区管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link href="resources/css/jquery-ui-themes.css" type="text/css" rel="stylesheet"/>
    <link href="resources/css/axure_rp_page.css" type="text/css" rel="stylesheet"/>
    <link href="data/styles.css" type="text/css" rel="stylesheet"/>
    <link href="files/园区管理/styles.css" type="text/css" rel="stylesheet"/>
    <script src="resources/scripts/jquery-1.7.1.min.js"></script>
    <script src="resources/scripts/jquery-ui-1.8.10.custom.min.js"></script>
    <script src="resources/scripts/axure/axQuery.js"></script>
    <script src="resources/scripts/axure/globals.js"></script>
    <script src="resources/scripts/axutils.js"></script>
    <script src="resources/scripts/axure/annotation.js"></script>
    <script src="resources/scripts/axure/axQuery.std.js"></script>
    <script src="resources/scripts/axure/doc.js"></script>
    <script src="data/document.js"></script>
    <script src="resources/scripts/messagecenter.js"></script>
    <script src="resources/scripts/axure/events.js"></script>
    <script src="resources/scripts/axure/recording.js"></script>
    <script src="resources/scripts/axure/action.js"></script>
    <script src="resources/scripts/axure/expr.js"></script>
    <script src="resources/scripts/axure/geometry.js"></script>
    <script src="resources/scripts/axure/flyout.js"></script>
    <script src="resources/scripts/axure/ie.js"></script>
    <script src="resources/scripts/axure/model.js"></script>
    <script src="resources/scripts/axure/repeater.js"></script>
    <script src="resources/scripts/axure/sto.js"></script>
    <script src="resources/scripts/axure/utils.temp.js"></script>
    <script src="resources/scripts/axure/variables.js"></script>
    <script src="resources/scripts/axure/drag.js"></script>
    <script src="resources/scripts/axure/move.js"></script>
    <script src="resources/scripts/axure/visibility.js"></script>
    <script src="resources/scripts/axure/style.js"></script>
    <script src="resources/scripts/axure/adaptive.js"></script>
    <script src="resources/scripts/axure/tree.js"></script>
    <script src="resources/scripts/axure/init.temp.js"></script>
    <script src="files/园区管理/data.js"></script>
    <script src="resources/scripts/axure/legacy.js"></script>
    <script src="resources/scripts/axure/viewer.js"></script>
    <script src="resources/scripts/axure/math.js"></script>
    <script type="text/javascript">
      $axure.utils.getTransparentGifPath = function() { return 'resources/images/transparent.gif'; };
      $axure.utils.getOtherPath = function() { return 'resources/Other.html'; };
      $axure.utils.getReloadPath = function() { return 'resources/reload.html'; };
    </script>
  </head>
  <body>
    <div id="base" class="">

      <!-- Unnamed (底栏) -->

      <!-- Unnamed (矩形) -->
      <div id="u3253" class="ax_default box_1">
        <div id="u3253_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3254" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Bold', '应用字体';">&nbsp;&nbsp; 蓝色未来管理系统</span></p>
        </div>
      </div>

      <!-- Unnamed (树状菜单) -->
      <div id="u3255" class="ax_default tree_node treeroot">
        <div id="u3255_children" class="u3255_children">

          <!-- Unnamed (树节点) -->
          <div id="u3256" class="ax_default tree_node treenode">
            <!-- Unnamed (矩形) -->
            <div id="u3257" class="" selectiongroup="u3252u3255_tree_group">
              <div id="u3257_div" class=""></div>
              <!-- Unnamed () -->
              <div id="u3258" class="text" style="visibility: visible;">
                <p><span style="font-family:'应用字体 Regular', '应用字体';">概览</span></p>
              </div>
            </div>
          </div>

          <!-- Unnamed (树节点) -->
          <div id="u3259" class="ax_default tree_node treenode">
            <!-- Unnamed (矩形) -->
            <div id="u3260" class="" selectiongroup="u3252u3255_tree_group">
              <div id="u3260_div" class=""></div>
              <!-- Unnamed () -->
              <div id="u3261" class="text" style="visibility: visible;">
                <p><span style="font-family:'应用字体 Regular', '应用字体';">通知公告</span></p>
              </div>
            </div>
          </div>

          <!-- Unnamed (树节点) -->
          <div id="u3262" class="ax_default tree_node treenode">
            <!-- Unnamed (矩形) -->
            <div id="u3263" class="" selectiongroup="u3252u3255_tree_group">
              <div id="u3263_div" class=""></div>
              <!-- Unnamed () -->
              <div id="u3264" class="text" style="visibility: visible;">
                <p><span style="font-family:'应用字体 Regular', '应用字体';">精彩活动</span></p>
              </div>
            </div>
          </div>

          <!-- Unnamed (树节点) -->
          <div id="u3265" class="ax_default tree_node treenode">
            <!-- Unnamed (矩形) -->
            <div id="u3266" class="" selectiongroup="u3252u3255_tree_group">
              <div id="u3266_div" class=""></div>
              <!-- Unnamed () -->
              <div id="u3267" class="text" style="visibility: visible;">
                <p><span style="font-family:'应用字体 Regular', '应用字体';">每周食谱</span></p>
              </div>
            </div>
          </div>

          <!-- Unnamed (树节点) -->
          <div id="u3268" class="ax_default tree_node treenode">
            <!-- Unnamed (矩形) -->
            <div id="u3269" class="" selectiongroup="u3252u3255_tree_group">
              <div id="u3269_div" class=""></div>
              <!-- Unnamed () -->
              <div id="u3270" class="text" style="visibility: visible;">
                <p><span style="font-family:'应用字体 Regular', '应用字体';">园长信箱</span></p>
              </div>
            </div>
          </div>

          <!-- Unnamed (树节点) -->
          <div id="u3271" class="ax_default tree_node treenode">
            <!-- Unnamed (矩形) -->
            <div id="u3272" class="" selectiongroup="u3252u3255_tree_group">
              <div id="u3272_div" class=""></div>
              <!-- Unnamed () -->
              <div id="u3273" class="text" style="visibility: visible;">
                <p><span style="font-family:'应用字体 Regular', '应用字体';">园区管理</span></p>
              </div>
            </div>
          </div>

          <!-- Unnamed (树节点) -->
          <div id="u3274" class="ax_default tree_node treenode">
            <!-- Unnamed (矩形) -->
            <div id="u3275" class="" selectiongroup="u3252u3255_tree_group">
              <div id="u3275_div" class=""></div>
              <!-- Unnamed () -->
              <div id="u3276" class="text" style="visibility: visible;">
                <p><span style="font-family:'应用字体 Regular', '应用字体';">班级管理</span></p>
              </div>
            </div>
          </div>

          <!-- Unnamed (树节点) -->
          <div id="u3277" class="ax_default tree_node treenode">
            <!-- Unnamed (矩形) -->
            <div id="u3278" class="" selectiongroup="u3252u3255_tree_group">
              <div id="u3278_div" class=""></div>
              <!-- Unnamed () -->
              <div id="u3279" class="text" style="visibility: visible;">
                <p><span style="font-family:'应用字体 Regular', '应用字体';">学生管理</span></p>
              </div>
            </div>
          </div>

          <!-- Unnamed (树节点) -->
          <div id="u3280" class="ax_default tree_node treenode">

            <!-- Unnamed (图片) -->
            <div id="u3281" class="ax_default _图片">
              <img id="u3281_img" class="img " src="images/修改密码/u82_selected.png"/>
              <!-- Unnamed () -->
              <div id="u3282" class="text" style="display: none; visibility: hidden">
                <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
              </div>
            </div>
            <!-- Unnamed (矩形) -->
            <div id="u3283" class="" selectiongroup="u3252u3255_tree_group">
              <div id="u3283_div" class=""></div>
              <!-- Unnamed () -->
              <div id="u3284" class="text" style="visibility: visible;">
                <p><span style="font-family:'应用字体 Regular', '应用字体';">系统管理</span></p>
              </div>
            </div>
            <div id="u3280_children" class="u3280_children">

              <!-- Unnamed (树节点) -->
              <div id="u3285" class="ax_default tree_node treenode">
                <!-- Unnamed (矩形) -->
                <div id="u3286" class="" selectiongroup="u3252u3255_tree_group">
                  <div id="u3286_div" class=""></div>
                  <!-- Unnamed () -->
                  <div id="u3287" class="text" style="visibility: visible;">
                    <p><span style="font-family:'应用字体 Regular', '应用字体';">角色管理</span></p>
                  </div>
                </div>
              </div>

              <!-- Unnamed (树节点) -->
              <div id="u3288" class="ax_default tree_node treenode">
                <!-- Unnamed (矩形) -->
                <div id="u3289" class="" selectiongroup="u3252u3255_tree_group">
                  <div id="u3289_div" class=""></div>
                  <!-- Unnamed () -->
                  <div id="u3290" class="text" style="visibility: visible;">
                    <p><span style="font-family:'应用字体 Regular', '应用字体';">职工管理</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Unnamed (垂直线) -->
      <div id="u3291" class="ax_default line">
        <img id="u3291_img" class="img " src="images/修改密码/u92.png"/>
        <!-- Unnamed () -->
        <div id="u3292" class="text" style="display: none; visibility: hidden">
          <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3293" class="ax_default label">
        <div id="u3293_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3294" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">登录者：总园长-李明</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) [footnote] -->
      <div id="u3293_ann" class="annotation"></div>

      <!-- Unnamed (矩形) -->
      <div id="u3295" class="ax_default link_button">
        <div id="u3295_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3296" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">退出</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3297" class="ax_default link_button">
        <div id="u3297_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3298" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">修改密码</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3299" class="ax_default label">
        <div id="u3299_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3300" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">》园区管理</span></p>
        </div>
      </div>

      <!-- Unnamed (表格) -->
      <div id="u3301" class="ax_default">

        <!-- Unnamed (单元格) -->
        <div id="u3302" class="ax_default _单元格">
          <img id="u3302_img" class="img " src="images/通知公告/u302.png"/>
          <!-- Unnamed () -->
          <div id="u3303" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">编号</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) [footnote] -->
        <div id="u3302_ann" class="annotation"></div>

        <!-- Unnamed (单元格) -->
        <div id="u3304" class="ax_default _单元格">
          <img id="u3304_img" class="img " src="images/查看通知公告-支付/u1018.png"/>
          <!-- Unnamed () -->
          <div id="u3305" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">园区名</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3306" class="ax_default _单元格">
          <img id="u3306_img" class="img " src="images/园区管理/u3306.png"/>
          <!-- Unnamed () -->
          <div id="u3307" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">省份</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3308" class="ax_default _单元格">
          <img id="u3308_img" class="img " src="images/园区管理/u3306.png"/>
          <!-- Unnamed () -->
          <div id="u3309" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">城市</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3310" class="ax_default _单元格">
          <img id="u3310_img" class="img " src="images/园区管理/u3306.png"/>
          <!-- Unnamed () -->
          <div id="u3311" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">园长</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3312" class="ax_default _单元格">
          <img id="u3312_img" class="img " src="images/园区管理/u3312.png"/>
          <!-- Unnamed () -->
          <div id="u3313" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">添加人</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3314" class="ax_default _单元格">
          <img id="u3314_img" class="img " src="images/园区管理/u3314.png"/>
          <!-- Unnamed () -->
          <div id="u3315" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">添加时间</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3316" class="ax_default _单元格">
          <img id="u3316_img" class="img " src="images/园区管理/u3316.png"/>
          <!-- Unnamed () -->
          <div id="u3317" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">操作</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3318" class="ax_default _单元格">
          <img id="u3318_img" class="img " src="images/通知公告/u302.png"/>
          <!-- Unnamed () -->
          <div id="u3319" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">3</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3320" class="ax_default _单元格">
          <img id="u3320_img" class="img " src="images/查看通知公告-支付/u1018.png"/>
          <!-- Unnamed () -->
          <div id="u3321" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">XXX幼儿园</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3322" class="ax_default _单元格">
          <img id="u3322_img" class="img " src="images/园区管理/u3306.png"/>
          <!-- Unnamed () -->
          <div id="u3323" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">广东</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3324" class="ax_default _单元格">
          <img id="u3324_img" class="img " src="images/园区管理/u3306.png"/>
          <!-- Unnamed () -->
          <div id="u3325" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">广州</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3326" class="ax_default _单元格">
          <img id="u3326_img" class="img " src="images/园区管理/u3306.png"/>
          <!-- Unnamed () -->
          <div id="u3327" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">王明</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3328" class="ax_default _单元格">
          <img id="u3328_img" class="img " src="images/园区管理/u3312.png"/>
          <!-- Unnamed () -->
          <div id="u3329" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">XX</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3330" class="ax_default _单元格">
          <img id="u3330_img" class="img " src="images/园区管理/u3314.png"/>
          <!-- Unnamed () -->
          <div id="u3331" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">2016.09.01&nbsp;&nbsp; 17:20</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3332" class="ax_default _单元格">
          <img id="u3332_img" class="img " src="images/园区管理/u3316.png"/>
          <!-- Unnamed () -->
          <div id="u3333" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3334" class="ax_default _单元格">
          <img id="u3334_img" class="img " src="images/通知公告/u302.png"/>
          <!-- Unnamed () -->
          <div id="u3335" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">2</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3336" class="ax_default _单元格">
          <img id="u3336_img" class="img " src="images/查看通知公告-支付/u1018.png"/>
          <!-- Unnamed () -->
          <div id="u3337" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3338" class="ax_default _单元格">
          <img id="u3338_img" class="img " src="images/园区管理/u3306.png"/>
          <!-- Unnamed () -->
          <div id="u3339" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3340" class="ax_default _单元格">
          <img id="u3340_img" class="img " src="images/园区管理/u3306.png"/>
          <!-- Unnamed () -->
          <div id="u3341" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3342" class="ax_default _单元格">
          <img id="u3342_img" class="img " src="images/园区管理/u3306.png"/>
          <!-- Unnamed () -->
          <div id="u3343" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3344" class="ax_default _单元格">
          <img id="u3344_img" class="img " src="images/园区管理/u3312.png"/>
          <!-- Unnamed () -->
          <div id="u3345" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3346" class="ax_default _单元格">
          <img id="u3346_img" class="img " src="images/园区管理/u3314.png"/>
          <!-- Unnamed () -->
          <div id="u3347" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3348" class="ax_default _单元格">
          <img id="u3348_img" class="img " src="images/园区管理/u3316.png"/>
          <!-- Unnamed () -->
          <div id="u3349" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3350" class="ax_default _单元格">
          <img id="u3350_img" class="img " src="images/通知公告/u344.png"/>
          <!-- Unnamed () -->
          <div id="u3351" class="text" style="visibility: visible;">
            <p><span style="font-family:'应用字体 Regular', '应用字体';">1</span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3352" class="ax_default _单元格">
          <img id="u3352_img" class="img " src="images/查看通知公告-支付/u1078.png"/>
          <!-- Unnamed () -->
          <div id="u3353" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3354" class="ax_default _单元格">
          <img id="u3354_img" class="img " src="images/园区管理/u3354.png"/>
          <!-- Unnamed () -->
          <div id="u3355" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3356" class="ax_default _单元格">
          <img id="u3356_img" class="img " src="images/园区管理/u3354.png"/>
          <!-- Unnamed () -->
          <div id="u3357" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3358" class="ax_default _单元格">
          <img id="u3358_img" class="img " src="images/园区管理/u3354.png"/>
          <!-- Unnamed () -->
          <div id="u3359" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3360" class="ax_default _单元格">
          <img id="u3360_img" class="img " src="images/园区管理/u3360.png"/>
          <!-- Unnamed () -->
          <div id="u3361" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3362" class="ax_default _单元格">
          <img id="u3362_img" class="img " src="images/园区管理/u3362.png"/>
          <!-- Unnamed () -->
          <div id="u3363" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>

        <!-- Unnamed (单元格) -->
        <div id="u3364" class="ax_default _单元格">
          <img id="u3364_img" class="img " src="images/园区管理/u3364.png"/>
          <!-- Unnamed () -->
          <div id="u3365" class="text" style="display: none; visibility: hidden">
            <p><span style="font-family:'应用字体 Regular', '应用字体';"></span></p>
          </div>
        </div>
      </div>

      <!-- Unnamed (表格) [footnote] -->
      <div id="u3301_ann" class="annotation"></div>

      <!-- Unnamed (矩形) -->
      <div id="u3366" class="ax_default link_button">
        <div id="u3366_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3367" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">修改</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3368" class="ax_default link_button">
        <div id="u3368_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3369" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">删除</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) [footnote] -->
      <div id="u3368_ann" class="annotation"></div>

      <!-- Unnamed (矩形) -->
      <div id="u3370" class="ax_default link_button">
        <div id="u3370_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3371" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">修改</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3372" class="ax_default link_button">
        <div id="u3372_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3373" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">删除</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3374" class="ax_default link_button">
        <div id="u3374_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3375" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">修改</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3376" class="ax_default link_button">
        <div id="u3376_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3377" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">删除</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3378" class="ax_default button">
        <div id="u3378_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3379" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">添加</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3380" class="ax_default label">
        <div id="u3380_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3381" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">1&nbsp; 2&nbsp; 3 ··· 9</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3382" class="ax_default label">
        <div id="u3382_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3383" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">到第</span></p>
        </div>
      </div>

      <!-- Unnamed (文本框) -->
      <div id="u3384" class="ax_default _文本框">
        <input id="u3384_input" type="text" value="" class="text_sketch"/>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3385" class="ax_default label">
        <div id="u3385_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3386" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">页</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3387" class="ax_default link_button">
        <div id="u3387_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3388" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">确定</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3389" class="ax_default link_button">
        <div id="u3389_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3390" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">下一页</span></p>
        </div>
      </div>

      <!-- Unnamed (矩形) -->
      <div id="u3391" class="ax_default link_button">
        <div id="u3391_div" class=""></div>
        <!-- Unnamed () -->
        <div id="u3392" class="text" style="visibility: visible;">
          <p><span style="font-family:'应用字体 Regular', '应用字体';">上一页</span></p>
        </div>
      </div>
    </div>
  </body>
</html>

