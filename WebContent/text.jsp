<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

    <head>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  
        <title></title>  
     <script type="text/javascript"  
    src="js/jquery-183.js"></script>  
        <script type="text/javascript" src="${pageContext.request.contextPath }/lhgdialog4/lhgdialog.min.js?skin=qq2011"></script>  
        <script type="text/javascript" src="${pageContext.request.contextPath }/kindeditor4/kindeditor.js"></script>  
        <script type="text/javascript" src="${pageContext.request.contextPath }/kindeditor4/lang/zh_CN.js"></script>  
        <script type="text/javascript">  
            var editor = null;  
            KindEditor.ready(function(k){  
                editor = k.create('#kindeditor4',{  
                    width: '800',  
                    height: '370',  
                    resizeType: 0,  
                    cssPath : ['kindeditor4/plugins/code/prettify.css'],  
                    filterMode: true,  
                    allowFileManager: true,  
                    uploadJson: '${pageContext.request.contextPath }/kindeditor/upload.do',  
                    fileManagerJson : '${pageContext.request.contextPath }/kindeditor/manager.do'  
                });  
                editor.html($("#content").html());  
            });  
              
             
        </script>  
    </head>  
    <body>  
        <div id="LKNMDiv">  
            <table border="0" width="100%">  
                <tr>  
                    <td width="40"align="left" height="28"> 标题：</td>  
                    <td><input type="text" style="width:100%;border:1px solid #bbb;" value="lhgDialog是一个功能强大且兼容面广的对话框组件，它拥有精致的界面与友好的接口"/></td>  
                </tr>  
                <tr>  
                    <td width="40"align="center">正<br/><br/><br/><br/><br/><br/>文</td>  
                    <td>  
                        <textarea id="kindeditor4" cols="0" rows="0" style="visibility:hidden"></textarea>  
                    </td>  
                </tr>  
                <tr>  
                    <td width="40"align="left" height="28"> 作者：</td>  
                    <td><input type="text" style="width:30%;border:1px solid #bbb;" value="Elkan(elkan1788@gmail.com)" readonly="true"/></td>  
                </tr>  
            </table>  
        </div>     
    </body>  
</html> 