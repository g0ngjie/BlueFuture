<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script>
var div1 = 0;

$(document).ready(function(){
var IsCheckFlag = true;
$("#grid").datagrid({
	onDblClickRow: function (rowIndex, rowData) {
	//alert(11)
	
	},
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
});

});

function formatEnable(value,row,index){
	if(value == '1')
		return "<a style='color:#2A60F4'>启用</a>";
	return "<a style='color:#FF0000'>禁用</a>";
	
}

/* 操作 */
function formatOper(value,row,index){
	var majorId = row.majorId;
	return "<a href='javascript:edit(\"" + row['majorId'] + "\");'>编辑</a>"
	+ " | <a href='javascript:removeSingle(\"" + row['majorId'] + "\");'>删除</a>";
} 
/* 保存  */
function save()
{	
	$('#formAddRole').form('submit', {
		url : '<%=basePath%>major/saveMajor.do',
		onSubmit : function() {
			return $(this).form('validate');
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.messager.show({
					title:'提示',
					msg:'保存成功',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				$.messager.show({
					title:'提示',
					msg:data.msg,
					timeout: 1000,  
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
		}
	}); 
}
/* 编辑 */
function edit(majorId){
	$.post("<%=basePath%>major/editMajor.do",{majorId:majorId},function(result){
		data = eval("(" + result + ")");
		$('#mode').val("edit");
		$('#majorName').val(data.majorName);
		$('#subject1').val(data.subject1);
		$('#subjectId1').val(data.subjectId1);
		$('#subject2').val(data.subject2);
		$('#subjectId2').val(data.subjectId2);
		$('#subject3').val(data.subject3);
		$('#subjectId3').val(data.subjectId3);
		$('#subject4').val(data.subject4);
		$('#subjectId4').val(data.subjectId4);
		$('#orderNo').val(data.orderNo);
		$('#majorId').val(majorId);
		document.getElementById("adddlg-buttons").style.display="";
		$('#adddlg').panel({title: "修改专业"});
		$('#adddlg').dialog('open');
	}); 
}

/* 添加按钮 */
function add()
{
    document.getElementById("majorName").value = "";
    document.getElementById("orderNo").value = "";
    $('#mode').val("add");	
    $('#adddlg').panel({title: "添加专业"});
	$('#adddlg').dialog('open');
}
/* 删除按钮 */
function remove()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].majorId;
		}
	}
	
	$.post("<%=basePath%>/major/deltemajor.do",{majorId:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'删除成功',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'请选中您要删除的数据',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

/* 删除操作 */  
function removeSingle(majorId){  
   $.messager.confirm('确认','确认删除?',function(row){  
       if(row){  
	       	$.get("<%=basePath%>/major/deltemajor.do?majorId="+majorId,function(data,status){
	       	$('#grid').datagrid('reload');
		        $.messager.show({
		        		
							title:'提示',
							msg:'删除成功',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
				});
	        });
       }  
   }) 
 }

/* 启用按钮 */
function enable()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].majorId;
		}
	}
	
	$.post("<%=basePath%>/subject/saveEnable.do",{enableKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'已启用',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'请选中您要启用的数据',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}
/* 禁用按钮 */
function disable()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].majorId;
		}
	}
	
	$.post("<%=basePath%>/subject/saveDisable.do",{disableKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'已禁用',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'请选中您要禁用的数据',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

function selectSubject(){
	var row = $('#subject').datagrid('getSelected');
	if (row){
		if(div1==1){
			$('#subject1').val(row.subjectName);
			$('#subjectId1').val(row.subjectId);
		}
		if(div1==2){
			$('#subject2').val(row.subjectName);
			$('#subjectId2').val(row.subjectId);
		}
		if(div1==3){
			$('#subject3').val(row.subjectName);
			$('#subjectId3').val(row.subjectId);
		}
		if(div1==4){
			$('#subject4').val(row.subjectName);
			$('#subjectId4').val(row.subjectId);
		}
	}
	$('#selectdlg').dialog('close');
}
 
</script>
</head>
<body class="easyui-layout">
<div id="tb" style="height:auto;">   
	<div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()" >添加</a>
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="remove()" >删除</a>
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="enable()" >启用</a>
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="disable()" >禁用</a>
	 </div> 
	 
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%" title="<img alt='详情页广告管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>所有专业</div>"

			url="<%=basePath%>/major/majorListData.do" toolbar="#tb"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="name" width="20%" align="center">专业名称</th>
					<th field="orderNo" width="20%" align="center">序号</th>
					<th field="enable" width="20%" align="center" formatter="formatEnable">状态</th>
					<th field="oper" width="20%" align="center" formatter="formatOper" align="center">操作</th>
				</tr>
			</thead>
		</table>
<div id="adddlg" class="easyui-dialog" style="width: 360px; height: 400px; padding: 10px 20px;" closed="true"  buttons="#adddlg-buttons" draggable="true" >
	<form id="formAddRole" name="formAddRole" method="post">
		   <div class="fitem" style="margin-top:10px;margin-left:30px" >
			   <label style="display:inline-block;width:70px;color:#666;font-size:15px">专业名称:</label>
			   <input name="majorName" id="majorName" style="border:1px #95B8E7 solid;height:23px;width:160px"/>
		   </div>
		   <div style="width:80%;height:21px;margin-top:12px;margin-left:20px">
	        	<!-- <label style="display:inline-block;width:80px;color:#666;font-size:15px;">选择科目:</label> -->
	            <a class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="javascript:$('#selectdlg').dialog('open');javascript:div1=1">选择科目</a>  
	            <input name="subject1" id="subject1"  style="margin-top: 5px;margin-left:5px;border:none;width: 130px;height: 23px;value="" onclick="javascript:$('#selectdlg').dialog('open');javascript:div1=1"/>
			</div>
			<div style="width:80%;height:21px;margin-top:12px;margin-left:20px">
	        	<!-- <label style="display:inline-block;width:80px;color:#666;font-size:15px;">选择科目:</label> -->
	            <a class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="javascript:$('#selectdlg').dialog('open');javascript:div1=2">选择科目</a>  
	            <input name="subject2" id="subject2"  style="margin-top: 5px;margin-left:5px;border:none;width: 130px;height: 23px;value=""/>
			</div>
			<div style="width:80%;height:21px;margin-top:12px;margin-left:20px">
	        	<!-- <label style="display:inline-block;width:80px;color:#666;font-size:15px;">选择科目:</label> -->
	            <a class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="javascript:$('#selectdlg').dialog('open');javascript:div1=3">选择科目</a>  
	            <input name="subject3" id="subject3"  style="margin-top: 5px;margin-left:5px;border:none;width: 130px;height: 23px;value=""/>
			</div>
			<div style="width:80%;height:21px;margin-top:12px;margin-left:20px">
	        	<!-- <label style="display:inline-block;width:80px;color:#666;font-size:15px;">选择科目:</label> -->
	            <a class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="javascript:$('#selectdlg').dialog('open');javascript:div1=4">选择科目</a>  
	            <input name="subject4" id="subject4"  style="margin-top: 5px;margin-left:5px;border:none;width: 130px;height: 23px;value=""/>
			</div>
		   <div class="fitem" style="margin-top:25px;margin-left:30px" >
			   <label style="display:inline-block;width:70px;color:#666;font-size:15px">编码号:</label>
			   <input onkeyup="this.value=this.value.replace(/\D/g,'')"  onafterpaste="this.value=this.value.replace(/\D/g,'')" name="orderNo" id="orderNo" style="border:1px #95B8E7 solid;height:23px;width:160px"/>
		   </div>
		   
		   
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="majorId" id="majorId" value=""/>
		   <input type="hidden" name="subjectId1" id="subjectId1" value="">
		   <input type="hidden" name="subjectId2" id="subjectId2" value="">
		   <input type="hidden" name="subjectId3" id="subjectId3" value="">
		   <input type="hidden" name="subjectId4" id="subjectId4" value="">
	 </form>
</div>
<div id="adddlg-buttons" style="height: 30px;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="save()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 

<div id="selectdlg" class="easyui-dialog" style="width: 600px; height: 430px; padding: 10px 20px;display: none;" closed="true" buttons="#selectdlg-buttons" title="选择科目" >
	<form id="formAddSubject" name="formAddSubject" method="post">
			
		   <div style="float:left;height:30px;line-height: 30px;"><label style="display:inline-block;color:#666;font-size:15px">科目名称:</label>
			   <input name="ssName" id="ssName" style="border:1px #95B8E7 solid;font-size:15px;width:120px"/>
		   </div>
		   <div style="float:left;margin-left:10px;height:30px;line-height: 30px;"><label style="display:inline-block;color:#666;font-size:15px">科目编码:</label>
			   <input name="ssOrderNo" id="ssOrderNo" style="border:1px #95B8E7 solid;font-size:15px;width:120px"/>
		   </div>
		   <div onclick="sousuoSubject()" style=" float: left;border:1px solid #000;margin-left: 10px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 40px;height:23px;margin-top: 3px;border-radius: 5px;">搜索</div>
	     <div onclick="reSetOwner()" style=" float: left;border:1px solid #000;margin-left: 5px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 40px;height:23px;margin-top: 3px;border-radius: 5px;">重置</div>
	<div style="height:300px;"> 
	  <table id="subject" class="easyui-datagrid" style="width:100%;height:100%;display: none;" 
		url="<%=basePath%>/subject/subjectListData.do" toolbar="" 
		rownumbers="true" pagination="true" singleSelect="true" loadMsg="正在努力为您加载数据">
		<thead>
			<tr>
				<th field="cg" checkbox="true" id="ow" name="ow"></th>
				<th field="subjectName" width="30%" align="center">科目名称</th>
				<th field="orderNo" width="30%" align="center">科目编码</th>
				<th field="enable" width="24%" align="center" formatter="formatEnable">状态</th>
			</tr>
		</thead>
	  </table>
	</div>	   
		   	 <input type="hidden" name="mode" id="mode" value=""/>
		   	 <input type="hidden" name="subjectId" id="ownerId" value=""/>
		   	 
	 </form>
</div>

<div id="selectdlg-buttons" style="height: 30px;display: none">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="selectSubject()" iconcls="icon-save">确定</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#selectdlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 

</body>
</html>
