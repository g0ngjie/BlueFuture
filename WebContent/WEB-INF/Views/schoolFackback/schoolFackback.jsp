<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/css/system/notice.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script src="<%=basePath%>/Public/js/pro_common.js" type="text/javascript"></script>
<script>
$(document).ready(function(){


		<% int sum = 0; %>
	
	<c:if test="${mailbox_daochu == 'mailbox_daochu'}">
	
		document.getElementById("daoChu").style.display="block";
		
		<% sum ++; %>;
		
	</c:if>
	<% if(sum == 0) { %>
		
		$("#grid").datagrid({
			toolbar:''
		});
		
	<% } %>


var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
     
});

});

/* 操作 */
function formatOper(value,row,index){

			<%int i = 0;%>
    	var str = "";
		<c:if test="${ mailbox_chakan == 'mailbox_chakan'}">
			str += "<a href='javascript:openDetail(\"" + row['schoolFackbackId'] + "\");'>详情</a>&nbsp;";
			<%i ++;%>
		</c:if>
		<c:if test="${ mailbox_del == 'mailbox_del'}">
			<%if (i != 0) {%>
				str += " | &nbsp;"
			<%}%>
			str += "<a href='javascript:openDel(\"" + row['schoolFackbackId'] + "\");'>删除</a>&nbsp;";
		</c:if>
		if(str == ""){
	    		str = "<a style='color:#666;'>无此权限</a>";
	    }
		return str;


	/*return "<a href='javascript:openDetail(\"" + row['schoolFackbackId'] + "\");'>详情</a>"
	+ " | <a href='javascript:openDel(\"" + row['schoolFackbackId'] + "\");'>删除</a>";*/
} 

function formatEnable(value){
	if(value == '1')
		return "<a style='color:#2A60F4'>已读</a>";
	return "<a style='color:#2A60F4'>未读</a>";
	
}

/* 编辑角色 */
function openEdite(schoolFackbackId)
{	
	$.post("<%=basePath%>/notice/updateNotice.do",{schoolFackbackId:schoolFackbackId},function(result){
		data = eval("(" + result + ")");
		$('#schoolFackbackId').val(data.schoolFackbackId);
		$('#Name').val(data.title);
		$('#user').combobox('setValue',data.user);
		$('#text').val(data.content);
		$('#mode').val("edit");
		$('#adddlg').panel({title: "编辑用户"});
		$('#adddlg').dialog('open');
	}); 
	
}

//跳转到详情页面
function openDetail(schoolFackbackId){
		
		$.post("<%=basePath%>/schoolFackback/updateSchoolFackback.do",{schoolFackbackId:schoolFackbackId},function(result){
		data = eval("(" + result + ")");
		
		$('#content').html(data.content);
		$('#feedback').html(data.feedback);
		$('#feedback1').html(data.feedback1);
	
		$('#adddlg').panel({title: "园长信箱详情"});
		$('#adddlg').dialog('open');
	});
}

/* 单删 */
function openDel(schoolFackbackId)
{	$.messager.confirm('确认','确认删除?',function(row){

	if(row){
	$.post("<%=basePath%>/schoolFackback/delSchoolFackback.do",{delKeys:schoolFackbackId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
}
})};

/* 删除按钮 */
function openDelete(){	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].schoolFackbackId;
		}
	}
	$.post("<%=basePath%>/notice/delNotice.do",{delKeys:str},function(result){
			layer.closeAll('loading');
			data = eval("(" + result + ")");
			//alert(result)
			if(data.code == 100){
				$('#grid').datagrid('reload');
				$.messager.show({
								title:'提示',
								msg:'<div class="msgs">删除成功</div>',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
			}
		}); 
		}else{
			$.messager.show({
								title:'提示',
								msg:'<div class="msgs">请选择您要删除的数据</div>',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
		}
	
}



//搜索
/* function sousuo()
{
	
	var startDate = $('#startDate').datebox('getValue'); 
	var endDate = $('#endDate').datebox('getValue');
	$('#grid').datagrid('load', {startDate:startDate,endDate:endDate});
	
} */
function schoolNameLi(){

	var schoolName = $('#schoolName').val();
		
	$('#grid').datagrid('load', {schoolName:schoolName});
	
	
	
};

function schoolEnable(){
	var schoolName = $('#schoolName').val();
	var enable = $('#enableNum').combobox('getValue');
	$('#grid').datagrid('load', {schoolName:schoolName,enable:enable});
}

//时间段查询
function schoolEnableTime(){
	var schoolName = $('#schoolName').val();
	var enable = $('#enableNum').combobox('getValue');
	var startDate = $('#startDate').datebox('getValue'); 
	var endDate = $('#endDate').datebox('getValue');
	var dat = startDate <= endDate;
	if(dat == true){
		$('#grid').datagrid('load', {schoolName:schoolName,enable:enable,startDate:startDate,endDate:endDate});
	}else{
		alert("开始日期不能比结束日期大！");
	}
			
}

/* 导出 */
function daochu()
{		
		var startDate = $('#startDate').datebox('getValue'); 
		var endDate = $('#endDate').datebox('getValue');
		window.parent.location="<%=basePath%>/schoolFackback/daochuSchoolFackback.do?startDate="+startDate+"&endDate="+endDate;
}
/* 排序赋值 */
function orderColumn(sort,order){ 
        sortEx = sort;
        orderEx = order;
}
	
</script>
<style type="text/css">
#dv1 span{
	margin-top: 20px;
}	
#dv1 div{
	float: left;
}	
</style>
</head>
<body class="easyui-layout">
	<!--------------------------------- 功能按钮 ----------------------------------->
<div id="tb" style="height:auto;display: none;">   
		
<!--------------------------------- 搜索框 ----------------------------------->
     	<div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		  		 	
		  		 	<!-- 搜索 园区列表 -->
		  		 	<div style="display:inline-block;width:200px; color:#000;font-size:15px;margin-left: 20px;margin-top: 5px; float: left;" class="fitem">
							<label style="float:left; display:inline-block;width:70px;color:#000;font-size:12px;margin-top:2px; margin-left: 0px;">选择园区名</label>
							<input id="schoolName" name="schoolName" class="easyui-combobox"
								style="width:110px;float:left; vertical-align:middle;font-size:15px; color:#000;margin-top: -6px"; data-options="
							valueField:'schoolId',
							textField:'schoolName',
							url:'<%=basePath%>/system/listSchool.do',
							onSelect: function(rec){
							$('#schoolName').val(rec.schoolName);
							schoolNameLi();
							}" />
					</div>
					
						<!-- 搜索 查看状态 -->
		  		 	<div style="float: left;margin-top: 5px;width: 130px; " class="fitem">
							<label style=" display:inline-block;width:55px;color:#000;font-size:12px;margin-top:2px;  ">查看状态</label>
							<select id="enableNum" name="enableNum" class="easyui-combobox" data-options="onSelect: function(rec){schoolEnable();}"
							style="width:60px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px">   
								<option></option>
							    <option value="1">已读</option>   
							    <option value="0">未读</option>   
							</select> 
					</div>
		  		 	
		  		 	
		   <div style="float: left; height: 30px;line-height: 30px;">
		   
		   <label>&nbsp;&nbsp;从: </label>
	   		<input name="startDate" id="startDate" class="easyui-datebox"  style="margin-top: 0px;border:1px #C5C5C5 solid;width: 110px;height: 23px" data-options="formatter:myformatter,parser:myparser"/> ~ 
	   		<label>&nbsp;&nbsp;至: </label>
	   		<input name="endDate" id="endDate" class="easyui-datebox"  style="margin-top: 0px;border:1px #C5C5C5 solid;width: 110px;height: 23px" data-options="formatter:myformatter,parser:myparser"
	   		 />
		   </div>
	   	
		     <div onclick="schoolEnableTime()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 5px;border-radius: 5px;"
		     class="easyui-linkbutton" iconCls="icon-search">搜索</div>	   
		   
	   		<div style="width:100px;margin-top:3px; margin-left:20px; text-align: left;height:35px;float: left; "> 
	    	    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-redo" plain="true" onclick="daochu()" id="daoChu" style="display: none; float: left;" >导出表格</a>
			 </div> 
		   
		   
		   
	
	 </div>
	 
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后台用户管理' style='width:17px;height:17px;float:left' src='<%=basePath%>/img/titleImg.png'>
		<div style='height:20px;line-height:22px;margin-left:10px;margin-top:-4;float:left'>园长信箱</div>"
			url="<%=basePath%>/schoolFackback/selectSchoolNameList.do" toolbar="#tb"  data-options="onSortColumn:orderColumn"
			rownumbers="true" pageSize="20" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="schoolName" width="16%" align="center">园区</th>
					<th field="content" width="16%" align="center">反馈内容</th>
					<th field="createTime" width="16%" align="center">反馈时间</th>
					<th field="studentName" width="16%" align="center">学生姓名</th>
					<th field="enable" width="16%" align="center" formatter="formatEnable">查看状态</th>
					<th field="oper" width="19%" align="center" formatter="formatOper">操作</th>
				</tr>
			</thead>
		</table>
		
<!------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 420px; height: 500px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" >
			
		<div id="dv1">
			<div style="margin-bottom: 20px;"><span style=" margin-left: 20px;font-size: 15px; font-weight: 4px;">反馈内容</span></div>
			
			<div  style="border: 1px solid #EBF3FF; width: 370 ;height: 150px;"><div id="content" style="margin-left: 25px;padding:3px; font-size: 13px; width: 93%; height: 100% " ></div></div>
			<div><span style="margin-left: 20px;font-size: 15px; font-weight: 4px;">反馈人信息</span></div>
			<div style="width: 100%; height: 1px;"></div>
			<div><span style="margin-left: 25px;font-size: 12px;" id="feedback"></span></div>
			<div><span style="margin-left: 25px;font-size: 12px;" id="feedback1"></span></div>
		</div>
	
	
</div>

</body>
<script type="text/javascript">
 $(function(){
 var uu = 0;
    $('#user').combobox({
     	url:'<%=basePath%>/user/userName.do',
     	valueField: 'userId',
		textField: 'userName',
		editable:false ,
		onLoadSuccess: function(result){
			if(uu==0){
				var data = $(this).combobox('getData');
				data.insert(0, {'userId':'0','userName':'全部'});
				uu++;
				$("#user").combobox("loadData", data);
			}
		}
	});
	
}); 
</script>
</html>
