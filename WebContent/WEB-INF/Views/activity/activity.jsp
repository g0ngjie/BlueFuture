<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/css/system/notice.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<link href="<%=basePath%>Public/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<%=basePath%>Public/uploadify/jquery.uploadify-3.1.js"></script>
<script type="text/javascript" src="<%=basePath%>Public/uploadify/swfobject.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js" type="text/javascript"></script>

<script src="<%=basePath%>/Public/editor/kindeditor.js"></script>
<script src="<%=basePath%>/Public/editor/lang/zh_CN.js"></script>
<script src="<%=basePath%>/Public/editor/easyui_kindeditor.js"></script>

<style>
	.uploadify-queue{ display: none;}
</style>

<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script>
$(document).ready(function(){
	var default_left; 
	var default_top;

	$('#adddlg').dialog({   
		title:'新增发布',   
		modal: true,   
		onOpen:function(){    //dialog原始left   
		default_left=$('#adddlg').panel('options').left;       //dialog原始top   
	 	default_top=$('#adddlg').panel('options').top;     
	 },   
	 onMove:function(left,top){ //鼠标拖动时事件    
	 		var body_width=document.body.offsetWidth;//body的宽度   
	  		var body_height=document.body.offsetHeight;//body的高度   
	   	var dd_width= $('#adddlg').panel('options').width;//dialog的宽度  
	     	var dd_height= $('#adddlg').panel('options').height;//dialog的高度       
	       if(left<1||left>(body_width-dd_width)||top<1||top>(body_height-dd_height)){      
	      		 $('#details_dd').dialog('move',{    
	         		left:default_left,     
	         		 top:default_top    
	           });      
	       } 
	      }
	  }); 

	<% int sum = 0; %>
	
	<c:if test="${activity_new == 'activity_new'}">
	
		document.getElementById("addFabu").style.display="block";
		
		<% sum ++; %>;
		
	</c:if>
	<% if(sum == 0) { %>
		
		$("#grid").datagrid({
			toolbar:''
		});
		
	<% } %>



var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
     
});

});

/* 操作 */
function formatOper(value,row,index){
	
	<%int i = 0;%>
    	var str = "";
		<c:if test="${ activity_chakan == 'activity_chakan'}">
			str += "<a href='javascript:openLook(\"" + row['activityId'] + "\");'>查看</a>&nbsp;";
			<%i ++;%>
		</c:if>
		<c:if test="${ activity_chehui == 'activity_chehui'}">
			<%if (i != 0) {%>
				str += " | &nbsp;"
			<%}%>
			str += "<a href='javascript:openBack(\"" + row['activityId'] + "\");'>撤回</a>&nbsp;";
		</c:if>
		<c:if test="${ activity_del == 'activity_del'}">
			<%if (i != 0) {%>
				str += " | &nbsp;"
			<%}%>
			str += "<a href='javascript:openDel(\"" + row['activityId'] + "\");'>删除</a>&nbsp;";
		</c:if>
		if(str == ""){
	    		str = "<a style='color:#666;'>无此权限</a>";
	    }
		return str;

	/*return "<a href='javascript:openLook(\"" + row['activityId'] + "\");'>查看</a>"
	+ " | <a href='javascript:openBack(\"" + row['activityId'] + "\");'>撤回</a>"
	+ " | <a href='javascript:openDel(\"" + row['activityId'] + "\");'>删除</a>";*/
} 
/* 添加 保存 角色 */
function saveAddNotice()
{	


		var arr = document.getElementsByName('schoolName'); 
		var keyId="";
		for(i=1;i<arr.length;i++){
			if(arr[i].checked)
			{
				//alert(arr[i].value);
				keyId+=","+arr[i].value;
			}
		}
		$('#schoolData').val(keyId);
		
		
		//判定
		var ntitle = $('#ntitle').val();
		if(ntitle == "" || ntitle == null){
				layer.msg("请输入标题")
				return null;
			}
		var filePat = $('#filePath').val();
		if(filePat == "" || filePat == null){
				layer.msg("请选择封面图")
				return null;
			}	
		var txtContent = $('#txtContent').val();
		if(txtContent == "" || txtContent == null){
				layer.msg("请输入正文")
				return null;
			}	
		
		
		
		/* var Name = $('#Name').val();
		if(Name == "" || Name == null){
				layer.msg("标题不能为空")
				return null;
			} */
		/* var user = $('#user').combobox("getValue");
		if(user == "" || user == null){
				layer.msg("用户不能为空")
				return null;
			} */
		/* var text = $('#text').val();
		if(text == "" || text == null){
				layer.msg("正文不能为空")
				return null;
			} */
			layer.load(1, {shade: [0.8, '#393D49']})
		$('#formAddNotice').form('submit', {
			url : '<%=basePath%>/activity/saveActivity.do',
			onSubmit : function() {
				return $(this).form('validate');
				var index = layer.load(1, {
					  shade: [0.1,'#fff'] //0.1透明度的白色背景
				})
				
			},
			success : function(result) {
				data = eval("(" + result + ")");
				if(data.code == 100){
					layer.closeAll('loading');
					$('#adddlg').dialog('close');
					$('#grid').datagrid('reload');
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">保存成功</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
					});
				}
				else
				{
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">data.msg</div>',
						timeout: 500,  
						showType:'fade',
						style:{right:'', bottom:''}
						});
				}
			}
		}); 
	
}


/* 编辑角色 */
function openEdite(activityId)
{	
	$.post("<%=basePath%>/notice/updateNotice.do",{activityId:activityId},function(result){
		data = eval("(" + result + ")");
		$('#activityId').val(data.activityId);
		//$('#Name').val(data.title);
		//$('#user').combobox('setValue',data.user);
		//$('#text').val(data.content);
		$('#mode').val("edit");
		$('#adddlg').panel({title: "编辑用户"});
		$('#adddlg').dialog('open');
	}); 
	
}

/* 单删 */
function openDel(activityId)
{	$.messager.confirm('确认','确认删除?',function(row){

	if(row){
	$.post("<%=basePath%>/activity/delActivity.do",{delKeys:activityId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
}
})};
	
/* 添加按钮 */
function add()
{
	
	
	$('#ntitle').textbox('setValue','');
	$('#imgCn').attr("src", "<%=basePath%>/Public/images/null.png");
	//$('#txtContent').html('dfdfsd ');
	editor.html('');
    //document.getElementById("Name").value = "";
   // $('#user').combobox('setValue','');
    //document.getElementById("text").value = "";
    $('#mode').val("add");
    //$('#adddlg').panel({title: "新增发布"});
	$('#adddlg').dialog('open');
	
	
	$.post("<%=basePath%>/school/schoolList.do",{page:1,rows:100},function(result){

		data = eval("(" + result + ")");
	
		var str = "";
		str += "<label class='tile' style='display:inline-block;width:100px; font-size:13px;margin-left: 40px;margin-top: 10px;'>" + "全选" + "</label>";
		str += "<input type='checkbox' name='schoolName' value='" + "0" + "' onchange='selAllOnChange(this)'>";
		
		for(var i=0; i<data.rows.length; i++)
		{
			var obj = data.rows[i];
			str += "<label class='tile' style='display:inline-block;width:100px; font-size:13px;margin-left: 40px;margin-top: 10px;'>" + obj.schoolName + "</label>";
			str += "<input type='checkbox' name='schoolName' value='" + obj.schoolId + "' onchange='selOnChange(this)' >";
		}
		$("#divSchool").html(str);
		
	}); 
	
	
}


function selAllOnChange(obj)
{
	var arr = document.getElementsByName('schoolName');
	//alert(obj.checked);
	if(obj.checked)
	{
		$("#zongyuan").show();
		$("#zongyuan_label").show();
		
		for(i=0;i<arr.length;i++){
			arr[i].checked = true;
		}
		
		$("#zongyuan").attr("checked","checked");
		
		$("#fenyuan").css("margin-left", "20px");
	}
	else
	{
		$("#zongyuan").hide();
		$("#zongyuan_label").hide();
		
		for(i=0;i<arr.length;i++){
			arr[i].checked = false;
		}
		
		$("#zongyuan").removeAttr("checked");
		$("#fenyuan").css("margin-left", "40px");
	}
}


function selOnChange()
{
	var hasNoCheck = false;
	var arr = document.getElementsByName('schoolName');
	for(i=1;i<arr.length;i++){
		if(!arr[i].checked)
		{
			$("#zongyuan").hide();
			$("#zongyuan_label").hide();
			arr[0].checked = false;
			hasNoCheck = true;
			$("#fenyuan").css("margin-left", "40px");
			break;
		}
	}
	
	if(!hasNoCheck)
	{
		$("#zongyuan").show();
		$("#zongyuan_label").show();
		arr[0].checked = true;
		$("#zongyuan").attr("checked","checked");
		$("#fenyuan").css("margin-left", "20px");
	}
	
}

//撤回 
function openBack(activityId){
$.messager.confirm('确定','确定要撤回到草稿箱吗?',function(row){

	if(row){
			$.post("<%=basePath%>/activity/rollActivity.do",{activityId:activityId},function(result){
				data = eval("(" + result + ")");
				if(data.code == 100){
					$('#grid').datagrid('reload');
					$.messager.show({
									title:'提示',
									msg:'<div class="msgs">撤回成功</div>',
									timeout: 1000, 
									showType:'fade',
									style:{right:'', bottom:''}
					});
			}
	}); 
}

})};


/* 草稿箱 */
function openDraft(){	
	window.location.href="<%=basePath%>activity/draftData.do";
}


//跳转到查看页面
function openLook(activityId){
	$.post("<%=basePath%>/activity/activityLook.do",{activityId:activityId},function(result){
		data = eval("(" + result + ")");
		$('#title').html(data.title);
		
		$('#imgg').attr("src","<%=basePath2%>"+data.img);
		$('#content').html(data.content);
		$('#school').html(data.validSchool);
	
		$('#adddl').panel({title: "查看"});
		$('#adddl').dialog('open');
	});	


}

//存草稿
function saveDraft(){
		$('#draf').val("saveDra");
		saveAddNotice()

};

//搜索
function sousuo()
{
	var SsName = $('#SsName').val();
	$('#grid').datagrid('load', {SsName:SsName});
	
}

/* 	function check_all(){
arr = document.getElementsByName('schoolName');
for(i=0;i<arr.length;i++){
arr[i].checked = true;
}
} 

	function check_false(){
arr = document.getElementsByName('schoolName');
for(i=0;i<arr.length;i++){
arr[i].checked = false;
}
}  */
	
	//编辑器
	var editor;
	KindEditor.ready(function (K) {
	    editor = K.create('textarea[name="txtContent"]', {
	        allowFileManager: true,
	        resizeType: 1,
	        width: "70%",
	        height: 260,
	        allowPreviewEmoticons: false,
	        uploadJson : '<%=basePath3%>/BlueFuture/fileupload/uploadImgForKindEditor.do',
	   	    imageUploadJson:'<%=basePath3%>/BlueFuture/fileupload/uploadImgForKindEditor.do',
	        afterBlur: function(){this.sync();},
	        items: [
	            'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
	            'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
	            'insertunorderedlist', '|', 'image','lineheight'/*'emoticons', 'image', 'link'*/]
	    });
	});
	
	
</script>
<style type="text/css">
.ke-container.ke-container-default{

  width:400px!important;
}
</style>
</head>
<body class="easyui-layout">
<!----------------------------- 功能按钮 --------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:110%; margin-top:5px;margin-left:7px;text-align: left;height:35px;"> 
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()"id="addFabu" style="display: none; float: left;">新增发布</a>
        
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="openDraft()">草稿箱</a>
	 </div> 
	 <!--------------------------------- 搜索框 ----------------------------------->
	 <div style="margin-left:10px;  text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;" >
		   <div style="float: left;margin-top:5px; height: 30px; " >
	   		<input name="SsName" id="SsName" placeholder="&nbsp;&nbsp;输入标题关键字"   style="margin-top: 0px; border:1px #C5C5C5 solid;width: 200px;height: 23px"/>
		   </div>
	     <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center; background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 5px;border-radius: 5px;"
	     class="easyui-linkbutton" iconCls="icon-search" >搜索</div>	   
	 </div>
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后台用户管理' style='width:17px;height:17px;float:left' src='<%=basePath%>/img/titleImg.png'>
		<div style='height:20px;line-height:22px;margin-left:10px;margin-top:-4;float:left'>精彩活动</div>"
			url="<%=basePath%>/activity/selectactivity.do" toolbar="#tb" 
			rownumbers="true" pageSize="20" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="validSchool" width="16%" align="center">园区</th>
					<th field="title" width="16%" align="center">标题</th>
					<th field="createDate" width="16%" align="center">发布时间</th>
					<th field="createName" width="16%" align="center">发布人</th>
					<th field="lookNum" width="16%" align="center">查看人数</th>
					<th field="oper" width="19%" align="center" formatter="formatOper">操作</th>
				</tr>
			</thead>
		</table>
		
<!------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width:850px; height: 500px; padding: 10px 20px;display: none;" closed="true" modal="true" resizable="true" buttons="#adddlg-buttons" >
	<form id="formAddNotice" name="formAddNotice" method="post">
		   
		   <!-- 占位  -->
		   <div style="width:50px; height: 10px; "></div>
		     <!-- 标题 easyUI -->
			   <div class="fitem" >
				   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">标题&nbsp&nbsp;:</label>
				   <input name="ntitle" id="ntitle" class="easyui-textbox" style=" width:350px;height:25px; vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; type="text" />
			   </div>
			  
			   <div class="fitem" style="width: 580px;height:300px;">
				<div style="margin-left: 40px;height:10px;width: 90px;line-height: 70px;float: left;font-size:15px;color:#000;">封面图 ：</div>
				<div style="width: 400px;height: 280px;float: left;margin-left: -5px;">
						<img id="imgCn" alt="" src="<%=basePath%>/Public/images/null.png" style="width:350px;height:230px">
						
						<input id="file_upload_cn" name="file_upload_cn" type="file" >
				</div>

			 <div class="fitem" style="float: left;">
				  	<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">正文：</label>
				  		<div style="margin-left: 120px; margin-top: 10px;width: 100%">
				  			<div><span style="color: red">图片上传尺寸宽度不要超过400</span></div>
					   		<textarea name="txtContent" id="txtContent" style="visibility:hidden"></textarea>
				  		</div>
				   </div>
			   </div>
			   	
			   
					
				    <div class="fitem" style="float: left;">
					     <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 20px;">所有园区:</label>
					    
					    <div class="fitem" id="divSchool" style="margin-top: 20px">
	
			 		    </div>
				    </div>
				    
				    
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="activityId" id="activityId" value=""/>
		   <input type="hidden" name="userid" id="userid" value="">
		   <input type="hidden" name="filePath" id="filePath" value="">
		   <input type="hidden" name="schoolData" id="schoolData" value="">
		   <input type="hidden" name="draf" id="draf" value="">
		   
		   
	 </form>
</div>


<div id="adddl" class="easyui-dialog" style="width:600px; height: 500px; padding: 10px 20px;display: none;" closed="true">


		<div id="dv1" style="width: 100%">
			<div style="width: 100%; "><span style="height: 30px;margin-top: 20px; font-weight:5px; float:left; font-size: 17px;margin-left: 30px;">标题 :</span><span style="height: 30px;margin-top: 23px;  margin-left: 30px;float:left; font-size: 13px;" id="title"></span></div>
			<div style="width: 100%; height: 2px; "></div>
			<div style="margin-top: 75px; margin-left: 70px; "><img id="imgg" src="" style="width:350px;height:230px"></div>
					
					<div style="width: 100%;height: 30px; ">
						<span style="height: 30px;margin-top: 20px; font-weight:5px; float:left; font-size: 17px; margin-left: 30px;">正文 :</span>
					</div>	
				<div style="border: 2px solid #EBF3FF; width: 350px ; margin-top: 20px; margin-left: 70px;">
					<div style="padding:5px; font-size: 13px; width: 93%;" id="content"></div>
				</div>
			<div><span style="height: 30px;margin-top: 20px; font-weight:5px; float:left; font-size: 17px; margin-left: 30px;">通知对象 :</span></div>
			<div><span style="margin-left: 55px;font-size: 12px;margin-top: 60px;width:500px" id="school"></span></div>
			<div style="width: 100%;height: 30px;"></div>
		</div>  
		
		
</div>




<!------------------------------ 确认取消按钮 ----------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:240px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddNotice()" iconcls="icon-save">发布</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save"  onclick="saveDraft()">存草稿</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>

<script type="text/javascript">
$('#file_upload_cn').uploadify({
    	'queueID'  : 'uploadify-queue',
        'swf'      : '<%=basePath%>Public/uploadify/uploadify.swf',
        'uploader' : '<%=basePath3%>/FileManagerService/fileupload/uploadImg.do', 
        'buttonText' : '点击上传',
        'height'   : 20,
        'width':346,
        'fileTypeExts': '*.gif; *.jpg; *.png',
        'multi': false,
        'onUploadStart':function(file){
        	layer.msg("<b id='test'>上传进度 0%</b>",{time: -1,shade: [0.4, '#393D49']});
        },
        'onUploadProgress': function(file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal){
       		var process = Math.floor(bytesUploaded/bytesTotal*100);
			$('#test').html("上传进度  " + process + "%");
        },
        'onUploadSuccess': function (file, data, response) { 
        	layer.closeAll();     
        	var resObj = eval("(" + data + ")"); 
        	if(resObj.code == 100)
        	{
        		var str = resObj.filePath;
        		var ff = str.substring(4);
        		$('#imgCn').attr("src", '<%=basePath2%>'+resObj.filePath);
        		/* alert(resObj.fullImgUrl);
        		alert(resObj.filePath);
        		alert(resObj.fileSize);
        		alert(resObj.extName); */
        		//alert(resObj.fileName);
        		$('#filePath').val(resObj.filePath);
        		$('#fileName').val(resObj.filePath);
        		
        	}
        	else
        	{
        		//上传失败，失败原因
        		alert(resObj.msg);
        	}
        }     
    });


 $(function(){
 var uu = 0;
    $('#user').combobox({
     	url:'<%=basePath%>/user/userName.do',
     	valueField: 'userId',
		textField: 'userName',
		editable:false ,
		onLoadSuccess: function(result){
			if(uu==0){
				var data = $(this).combobox('getData');
				data.insert(0, {'userId':'0','userName':'全部'});
				uu++;
				$("#user").combobox("loadData", data);
			}
		}
	});
	
}); 

</script>
<style>
.datagrid-btable .datagrid-cell{padding:6px 4px;overflow: hidden;text-overflow:ellipsis;white-space: nowrap;}  
	    formatter: function(value,row,index){  
       return '<span title='+value+'>'+value+'</span>'  
    }  
</style>
</html>
