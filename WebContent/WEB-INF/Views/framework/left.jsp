<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>无标题文档</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){	
	//导航切换
	$(".menuson .header").click(function(){
		var $parent = $(this).parent();
		$(".menuson>li.active").not($parent).removeClass("active open").find('.sub-menus').hide();
		
		$parent.addClass("active");
		if(!!$(this).next('.sub-menus').size()){
			if($parent.hasClass("open")){
				$parent.removeClass("open").find('.sub-menus').hide();
			}else{
				$parent.addClass("open").find('.sub-menus').show();	
			}
			
			
		}
	});
	
	// 三级菜单点击
	$('.sub-menus li').click(function(e) {
        $(".sub-menus li.active").removeClass("active")
		$(this).addClass("active");
    });
	
	$('.title').click(function(){
		var $ul = $(this).next('ul');
		$('dd').find('.menuson').slideUp();
		if($ul.is(':visible')){
			$(this).next('.menuson').slideUp();
		}else{
			$(this).next('.menuson').slideDown();
		}
	});
})	
</script>


</head>

<body style="background:#F5FFFA">
	<% int m=0; %>
	<% int k=0; %>
    <dl class="leftmenu">
	
	<ul class="menuson">
		<c:forEach items="${data}" var="vo">
			<% if(k == 0){ %>
			 	<li class="active" id="li_<%=k+1%>">
					<cite></cite>
					<a href="#" onclick="selMenu('li_<%=k+1%>');javascript:window.parent.main.location='<%=basePath%>${vo.url}'">${vo.menuName}</a><i></i>
				
					<% if(k==0 && m==0){ %>
					<script>
						window.parent.main.location = "<%=basePath%>${vo.url}";
					</script>
					<% } %>
				</li>
			<% }else{ %>
				<li id="li_<%=k+1%>">
				<cite></cite>
				<a href="#" onclick="selMenu('li_<%=k+1%>');javascript:window.parent.main.location='<%=basePath%>${vo.url}'">${vo.menuName}</a><i></i>
			</li>
			<% } %>
			<% k++; %>
		</c:forEach>
	</ul>
    </dl>
</body>

<Script>
function selMenu(aid)
{
	$(".menuson li").removeClass("active");
	//alert(aid);
	//alert(document.getElementById(aid));
	$("#" + aid).attr("class", "active");
	//alert(obj);
}
</script>
</html>

