<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>蓝色未来管理后台</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
<link rel="stylesheet" type="text/css" href="styles.css">
-->
<style>
	.menu
	{
		float:left;
		width:90px;
		height:24px;
		text-align:center;
		color:white;
		font-size:12px;
		line-height:24px;
	}

	.menu_sel
	{
		float:left;
		width:90px;
		height:24px;
		background:black;
		text-align:center;
		color:white;
		font-size:12px;
		line-height:24px;
	}
</style>
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
</head>
  
<body style="background:url(<%=basePath%>Public/images/topbg.gif) repeat-x;">

<div class="topleft">
	<a href="" target="_parent" ><img style="width:292px;height:80px;margin: 0 0 22 0;" src="<%=basePath%>img/lanlan.png" title="系统首页" /></a>
</div>


<ul class="nav">
	<% int k=0; %>
	<c:forEach items="${data}" var="vo"> 
	<!--{$vo.id}--{$vo.data}<br/>-->
	
		<%-- <% if(k != 0) {%>
		<li><a href="javascript:openSubMenu(${vo.menuId}, 'a_<%=(k+1)%>')" id="a_<%=(k+1)%>" class="selected"><img src="<%=basePath%>Public/images/${vo.icon}" title="${vo.menuName}" /><h2>${vo.menuName}</h2></a></li>
		<%} %> --%>
	<%-- 	<% if(k == 0) {%>
			<li><a href="javascript:openSubMenu(${vo.menuId}, 'a_<%=(k+1)%>')" id="a_<%=(k+1)%>" class="selected"><img src="<%=basePath%>Public/images/${vo.icon}" title="${vo.menuName}" /><h2>${vo.menuName}</h2></a></li>
		<%} %> --%>
		<!--
		<li><a href="imgtable.html" target="rightFrame"><img src="<%=basePath%>Public/images/icon02.png" title="模型管理" /><h2>模型管理</h2></a></li>
		<li><a href="imglist.html"  target="rightFrame"><img src="<%=basePath%>Public/images/icon03.png" title="模块设计" /><h2>模块设计</h2></a></li>
		<li><a href="tools.html"  target="rightFrame"><img src="<%=basePath%>Public/images/icon04.png" title="常用工具" /><h2>常用工具</h2></a></li>
		<li><a href="computer.html" target="rightFrame"><img src="<%=basePath%>Public/images/icon05.png" title="文件管理" /><h2>文件管理</h2></a></li>
		<li><a href="tab.html"  target="rightFrame"><img src="<%=basePath%>Public/images/icon06.png" title="系统设置" /><h2>系统设置</h2></a></li>-->
	<% k++; %>
	</c:forEach>
</ul>
<div style="background: #4A6CA7;width:150px;height:30px;text-align:center; 
	float:right;border-radius:10px;margin-right:10px;margin-top: 20px;line-height: 30px;color: #FFFFFF; ">
	<a style="color:#FFFFFF;" href="#" onclick="tuichu()">退出</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style="color:#FFFFFF" href="javascript:void(0)" onclick="divShow()">修改密码</a></div>
<div style="background: #4A6CA7;width:170px;height:30px;text-align:center; 
	float:right;border-radius:10px;margin-right:10px;margin-top: 20px;line-height: 30px;color: #FFFFFF; ">登陆者: ${roleName}——${name} </div>

<div style="width:100%;height:64px;display:none">
	<div>
		<div style="width:25%;height:64px;float:left">
			<div style="padding-left:0px;padding-top:12px;width:20px;float:left">
				<!--<img src="/Public/images/logo.png" style="width:40px;height:40px">-->
			</div>
			<div style="padding-left:10px;padding-top:22px;float:left;width:230px">
				<span style="font-size:16px;color:white;font-weight:bold;">蓝色未来管理后台</span>
			</div>
		</div>
	</div>
	<div style="width:50%;height:64px;float:left;">
		<div style="width:100%;height:40px;">
			
		</div>
		<div style="width:100%;height:24px;">
			<c:forEach items="${data}" var="vo"> 
				<% if(k==0){ k++;%>
					<div name="div1"style="cursor:pointer" onclick="openSubMenu(${vo.menuId}, this)" class="menu_sel">
					${vo.menuName}
					</div>
				<% }else{ %>
					<div name="div1"style="cursor:pointer" onclick="openSubMenu(${vo.menuId}, this)" class="menu">
					${vo.menuName}
					</div>
				<% } %>
			</c:forEach>
		</div>
	</div>
	<div>
		<div style="width:15%;height:64px;float:left">
			<div style="padding-top:22px;float:left;width:230px">
				<span style="width:30px;font-size:16px;color:white;font-weight:bold;cursor:pointer" onclick="out()">注销</span>
				<span style="width:200px;font-size:20px;color:white;font-weight:bold;cursor:pointer" onclick="alert(1)"></span>
			</div>
		</div>
	</div>
</div>

		<div id="adddlg" class="easyui-dialog" style="width: 420px; height: 500px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" >
			<form id="formAddNotice" name="formAddNotice" method="post">
					
				
			 </form>
		</div>


</body>

<script>

/*  */
function divShow(){
	
	window.top.fuClick();
	
}



function tuichu(){
	//window.top.location = '<%=basePath%>system/login.do';
	
	window.top.location.href = '<%=basePath%>system/outLogin.do';
}


$(document).ready(function()
{	
	
	<% k=0; %>
	<c:forEach items="${data}" var="vo">
		<% if(k==0){ k++;%>
		openSubMenu(${vo.menuId}, 'a_1');
		<% } %>
	</c:forEach>

	//var divArr = $('[name=div1]');
	//var divArr = document.getElementsByName("div1");
	
});
function out(){
	if (confirm("是否注销？")){
		window.top.location = '<%=basePath%>framework/out.do';
	}
}

function openSubMenu(mainMenuKey, aid)
{
	$(".nav li a.selected").removeClass("selected");
	$("#" + aid).attr("class", "selected");
	window.parent.leftFrame.location="<%=basePath%>framework/left.do?menuId=" + mainMenuKey;
}
</script>
</html>


<!--
  <body>
	<c:forEach items="${listEmployee}" var="industry">  
	<div>
      ${industry.name},
	
	</c:forEach>
	<img src="<%=basePath%>/img/launch.jpg"height="100" width="200" border="0"/>
	
  </body>
</html>-->
