<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/css/system/notice.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script src="<%=basePath%>/Public/js/pro_common.js" type="text/javascript"></script>
<script>
var lindata="";
$(document).ready(function(){
	
	
	var default_left; 
	var default_top;

	$('#adddlg').dialog({   
		title:'编辑食谱',   
		modal: true,   
		onOpen:function(){    //dialog原始left   
		default_left=$('#adddlg').panel('options').left;       //dialog原始top   
	 	default_top=$('#adddlg').panel('options').top;     
	 },   
	 onMove:function(left,top){ //鼠标拖动时事件    
	 		var body_width=document.body.offsetWidth;//body的宽度   
	  		var body_height=document.body.offsetHeight;//body的高度   
	   	var dd_width= $('#adddlg').panel('options').width;//dialog的宽度  
	     	var dd_height= $('#adddlg').panel('options').height;//dialog的高度       
	       if(left<1||left>(body_width-dd_width)||top<1||top>(body_height-dd_height)){      
	      		 $('#details_dd').dialog('move',{    
	         		left:default_left,     
	         		 top:default_top    
	           });      
	       } 
	      }
	  }); 
	

	$.post("<%=basePath%>/school/schoolList.do",{page:1,rows:100},function(result){

		data = eval("(" + result + ")");
		lindata=data.rows;
		var str = "";
		str += "<label class='tile' style='display:inline-block;width:100px; font-size:13px;margin-left: 40px;margin-top: 10px;'>" + "全选" + "</label>";
		str += "<input type='checkbox'id='schoolName' name='schoolName' value='" + "0" + "' onchange='selAllOnChange(this)'>";
		
		for(var i=0; i<data.rows.length; i++)
		{
			var obj = data.rows[i];
			str += "<label class='tile' style='display:inline-block;width:100px; font-size:13px;margin-left: 40px;margin-top: 10px;'>" + obj.schoolName + "</label>";
			str += "<input type='checkbox' id='schoolName_'"+ obj.schoolId +" name='schoolName' value='" + obj.schoolId + "' onchange='selOnChange(this)' >";
		}
		$("#divSchool").html(str);
		
		
	}); 	
	
	
	
	
	var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
     
});

var IsCheckFlagg = true;
$("#eatEdit").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlagg = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlagg) {
             IsCheckFlagg = true;
             $("#eatEdit").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlagg) {
             IsCheckFlagg = true;
             $("#eatEdit").datagrid("selectRow", rowIndex);
         }
     }
     
});


});

/* 操作 */
function formatOper(value,row,index){
	return "<a href='javascript:openEdit(\"" + row['recipeId'] + "\");'>编辑</a>"
	+ " | <a href='javascript:openDel(\"" + row['recipeId'] + "\");'>删除</a>";
} 
/* 添加 保存 角色 */
function saveAddNotice()
{	
	//获取早餐    的name值 传入后台
	var arr = document.getElementsByName('breakfast'); 
	//var aa = $('#breakfast').parent();
	//alert(arr.length);
	var typeNa="";
	//alert(arr.length);
	for(i=0;i<5;i++){
		//	alert(arr[i].value);
			typeNa+=","+arr[i].value;
			//alert(typeNa);
	}
	$('#breakfastt').val(typeNa);
	
	//alert(typeNa)
	
	//获取早餐后点心    的name值 传入后台
	var arr1 = document.getElementsByName('light1'); 
	var typeNa1="";
	//alert(arr.length);
	for(i=0;i<5;i++){
		//	alert(arr[i].value);
			typeNa1+=","+arr1[i].value;
	}
	
	$('#light11').val(typeNa1);
	
	
	
	//获取午餐    的name值 传入后台
	var arr2 = document.getElementsByName('lunch'); 
	var typeNa2="";
	//alert(arr.length);
	for(i=0;i<5;i++){
		//	alert(arr[i].value);
			typeNa2+=","+arr2[i].value;
	}
	
	$('#lunchh').val(typeNa2);
	
	
	
	//获取午餐后点心    的name值 传入后台
	var arr3 = document.getElementsByName('light2'); 
	var typeNa3="";
	//alert(arr.length);
	for(i=0;i<5;i++){
		//	alert(arr[i].value);
			typeNa3+=","+arr3[i].value;
	}
	
	$('#light22').val(typeNa3);
	
	
	
	//获取晚餐    的name值 传入后台
	var arr4 = document.getElementsByName('dinner'); 
	var typeNa4="";
	//alert(arr.length);
	for(i=0;i<5;i++){
		//	alert(arr[i].value);
			typeNa4+=","+arr4[i].value;
	}
	
	$('#dinnerr').val(typeNa4);
	
	
	//做限定设置
	var bdt = $('#beginDate').datebox("getValue");
	if(bdt == "" || bdt == null){
			layer.msg("请选择指定的一周")
			return null;
		}
	
	//var week = $('#breakfastt').val() + $('#light11').val() + $('#lunchh').val() + $('#light22').val() + $('#dinnerr').val();
	/* alert(week.length)
	alert(week) */
	//if(week == "" || week == null || week.length < 26){
	//		layer.msg("食谱不能为空")
	//		return null;
	//	}
	
	var breakfastt = $('#breakfastt').val()
	//alert(breakfastt.length)
	if(breakfastt == "" || breakfastt == null || breakfastt.length < 10){
			layer.msg("食谱不能为空,如果无请以文字表示")
			return null;
		}
	
	var light11 = $('#light11').val()
	if(light11 == "" || light11 == null || light11.length < 10){
			layer.msg("食谱不能为空,如果无请以文字表示")
			return null;
		}
		
	var lunchh = $('#lunchh').val()
	if(lunchh == "" || lunchh == null || lunchh.length < 10){
			layer.msg("食谱不能为空,如果无请以文字表示")
			return null;
		}
		
	var light22 = $('#light22').val()
	if(light22 == "" || light22 == null || light22.length < 10){
			layer.msg("食谱不能为空,如果无请以文字表示")
			return null;
		}
		
	var dinnerr = $('#dinnerr').val()
	if(dinnerr == "" || dinnerr == null || dinnerr.length < 10){
			layer.msg("食谱不能为空,如果无请以文字表示")
			return null;
		}
	
	
	//点击 保存的时候 像后台传入 多选框的 园区名
		var arr = document.getElementsByName('schoolName'); 
		var keyId="";
		for(i=1;i<arr.length;i++){
			if(arr[i].checked)
			{
				//alert(arr[i].value);
				keyId+=","+arr[i].value;
			}
		}
		$('#schoolVar').val(keyId);

		/* var Name = $('#Name').val();
		if(Name == "" || Name == null){
				layer.msg("标题不能为空")
				return null;
			} */
		/* var user = $('#user').combobox("getValue");
		if(user == "" || user == null){
				layer.msg("用户不能为空")
				return null;
			} */
		/* var text = $('#text').val();
		if(text == "" || text == null){
				layer.msg("正文不能为空")
				return null;
			} */
		$('#formAddNotice').form('submit', {
			url : '<%=basePath%>/recipe/saveRecipe.do',
			onSubmit : function() {
				return $(this).form('validate');
				var index = layer.load(1, {
					  shade: [0.1,'#fff'] //0.1透明度的白色背景
				})
				
			},
			success : function(result) {
				data = eval("(" + result + ")");
				if(data.code == 100){
					layer.closeAll('loading');
					$('#adddlg').dialog('close');
					$('#grid').datagrid('reload');
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">保存成功</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
					});
				}
				else
				{
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">data.msg</div>',
						timeout: 500,  
						showType:'fade',
						style:{right:'', bottom:''}
						});
				}
			}
		}); 
	
}

function checkSchool(recipeId){
		$.post("<%=basePath%>/recipe/bjSchool.do",{recipeId:recipeId},function(result){
			 
		data = eval("(" + result + ")");
		for(var i=0; i< data.length; i++){
				$("input:checkbox[value="+ data[i].schoolId +"]").attr('checked','true'); 
			
		}

	}); 
}

function openEd(recipeId){

	$.post("<%=basePath%>/recipe/bjRecipe.do",{recipeId:recipeId},function(result){
		data = eval("(" + result + ")");
		$('#recipeId').val(recipeId);
		
		$('#beginDate').datebox('setValue',data.beginTime);
		$('#breakfast0').val(data.one1);
		$('#breakfast1').val(data.one2);
		$('#breakfast2').val(data.one3);
		$('#breakfast3').val(data.one4);
		$('#breakfast4').val(data.one5);
		
		$('#light10').val(data.two1);
		$('#light111').val(data.two2);
		$('#light12').val(data.two3);
		$('#light13').val(data.two4);
		$('#light14').val(data.two5);
		
		$('#lunch0').val(data.three1);
		$('#lunch1').val(data.three2);
		$('#lunch2').val(data.three3);
		$('#lunch3').val(data.three4);
		$('#lunch4').val(data.three5);
		
		$('#light20').val(data.four1);
		$('#light21').val(data.four2);
		$('#light222').val(data.four3);
		$('#light23').val(data.four4);
		$('#light24').val(data.four5);
		
		$('#dinner0').val(data.five1);
		$('#dinner1').val(data.five2);
		$('#dinner2').val(data.five3);
		$('#dinner3').val(data.five4);
		$('#dinner4').val(data.five5);
		
		for(var i=0; i<data.array1.length; i++)
		{
			$('#s' + (i+1)).text(data.array1[i]);
		}
		
	}); 
}

/* 编辑角色 */
function openEdit(recipeId)
{	
	$.post("<%=basePath%>/school/schoolList.do",{page:1,rows:100},function(result){
		data = eval("(" + result + ")");
		lindata=data.rows;
		var str = "";
		str += "<label class='tile' style='display:inline-block;width:100px; font-size:13px;margin-left: 40px;margin-top: 10px;'>" + "全选" + "</label>";
		str += "<input type='checkbox'id='schoolName' name='schoolName' value='" + "0" + "' onchange='selAllOnChange(this)'>";
		
		for(var i=0; i<data.rows.length; i++)
		{
			var obj = data.rows[i];
			str += "<label class='tile' style='display:inline-block;width:100px; font-size:13px;margin-left: 40px;margin-top: 10px;'>" + obj.schoolName + "</label>";
			str += "<input type='checkbox' id='schoolName_'"+ obj.schoolId +" name='schoolName' value='" + obj.schoolId + "' onchange='selOnChange(this)' >";
		}
		$("#divSchool").html(str);
		
		openEd(recipeId);
		checkSchool(recipeId);
		
	}); 
		
		
		$('#mode').val("edit");
		//$('#adddlg').panel({title: "编辑食谱"});
		$('#adddlg').dialog('open');
	
			
		//alert(lindata.length)
}

/* 单删 */
function openDel(recipeId)
{	$.messager.confirm('确认','确认删除?',function(row){

	if(row){
	$.post("<%=basePath%>/recipe/delRecipe.do",{delKeys:recipeId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
}
})};

function selAllOnChange(obj)
{
	var arr = document.getElementsByName('schoolName');
	//alert(obj.checked);
	if(obj.checked)
	{
		for(i=0;i<arr.length;i++){
			arr[i].checked = true;
		}
	}
	else
	{
		for(i=0;i<arr.length;i++){
			arr[i].checked = false;
		}
	}
}


function selOnChange()
{
	var hasNoCheck = false;
	var arr = document.getElementsByName('schoolName');
	for(i=1;i<arr.length;i++){
		if(!arr[i].checked)
		{
			arr[0].checked = false;
			hasNoCheck = true;
			break;
		}
	}
}


/* 添加按钮 */
function backRecipe()
{
   window.location.href="<%=basePath%>recipe/recipe.do";
}

//存草稿
function saveDraft(){
		$('#draf').val("saveDra");
		saveAddNotice()

};

//时间段查询
function soutime(){
	var startDate = $('#startDate').datebox('getValue'); 
	$('#grid').datagrid('load', {startDate:startDate});		
}
	
</script>
<style type="text/css">
.datagrid-body textarea{
	width: 151px;height: 75px;;
}
.datagrid-btable tr{
height:70px; 
}

</style>
</head>
<body class="easyui-layout">
<!----------------------------- 功能按钮 --------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:110%; margin-top:5px;margin-left:7px; text-align: left;height:35px;"> 
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="backRecipe()" >每周食谱</a>>
        
        																					<!--原来的删除 -->
        <a href="javascript:void(0)" class="easyui-linkbutton"  plain="true"" >草稿箱</a>

	 </div> 

	<!--------------------------------- 搜索框 ----------------------------------->
	 <div style="margin-left:10px;text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;" >
		   <div style="float: left;margin-top:5px;height: 30px; " >
		    <span style="float: left;margin-left:5px; margin-top: 3px;margin-right: 10px;color:#404040" >选择食谱日期</span><input name="startDate" id="startDate" class="easyui-datebox" style="float: left; margin-top: 0px;border:1px #C5C5C5 solid;width: 200px;height: 23px" data-options="formatter:myformatter,parser:myparser"/>
		   </div>
					<div onclick="soutime()" style=" float: left;border:1px solid #000;cursor:pointer;width: 100px;height:23px;margin-left:10px; margin-top: 5px;border-radius: 5px;"
			   		  class="easyui-linkbutton" iconCls="icon-search" >搜索
			   		 </div>	 
				</div>	   
	 </div>
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后台用户管理' style='width:17px;height:17px;float:left' src='<%=basePath%>/img/titleImg.png'>
		<div style='height:20px;line-height:22px;margin-left:10px;margin-top:-4;float:left'>草稿箱</div>"
			url="<%=basePath%>/recipe/selectdraft.do" toolbar="#tb"
			rownumbers="true" pageSize="20" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="beginDate" width="24%" align="center">食谱开始日期</th>
					<th field="createTime" width="24%" align="center">最后编辑时间</th>
					<th field="UserName" width="24%" align="center">操作者</th>
					<th field="oper" width="27%" align="center" formatter="formatOper">操作</th>
				</tr>
			</thead>
		</table>
		
<!------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 950px; height: 650px; padding: 10px 20px;display: none;" closed="true" resizable="true"  buttons="#adddlg-buttons" >
	<form id="formAddNotice" name="formAddNotice" method="post">
		   <div class="fitem" style="margin-left: 40px;margin-top: 20px;" >
		   							
				<span style="font-size: 15px;float:left; font-weight: bold;">选择开始时间:</span>&nbsp;&nbsp;<input name="beginDate" id="beginDate" class="easyui-datebox" id="tbstartDate" style="margin-top: 0px;border:1px #C5C5C5 solid;width: 200px;height: 23px"
				 data-options="formatter:myformatter,parser:myparser"/>
 			   </div>
			<div class="fitem" style="margin-left:40px; margin-top: 40px;">   	
					
				<table id="eatEdit" class="easyui-datagrid" singleSelect="false"  style="width: 800px; height: 470px;">   
				    <thead>   
				        <tr> 
				        	<th field="d"></th>  
				            <th field="d2" style="width: 19%;"><span id="s1">周一</span></th>   
				            <th field="de" style="width: 19%;"><span id="s2">周二</span></th>   
				            <th field="df" style="width: 20%;"><span id="s3">周三</span></th>
				            <th field="dg" style="width: 20%;"><span id="s4">周四</span></th>   
				            <th field="dh" style="width: 20%;"><span id="s5">周五</span></th>   
				        </tr>   
				    </thead>   
				    <tbody>   
				        <tr>   
				            <td>早餐</td>
				            <td><textarea id="breakfast0" name="breakfast"></textarea></td>
				            <td><textarea id="light10" name="light1"></textarea></td>
				            <td><textarea id="lunch0" name="lunch"></textarea></td>
				            <td><textarea id="light20" name="light2"></textarea></td>
				            <td><textarea id="dinner0" name="dinner"></textarea></td>   
				        <tr>   
				            <td>点心</td>
				            <td><textarea id="breakfast1" name="breakfast"></textarea></td>
				            <td><textarea id="light111" name="light1"></textarea></td>
				            <td><textarea id="lunch1" name="lunch"></textarea></td>
				            <td><textarea id="light21" name="light2"></textarea></td>
				            <td><textarea id="dinner1" name="dinner"></textarea></td>    
				        </tr>
				        <tr>   
				            <td>午餐</td>
				            <td><textarea id="breakfast2" name="breakfast"></textarea></td>
				            <td><textarea id="light12" name="light1"></textarea></td>
				            <td><textarea id="lunch2" name="lunch"></textarea></td>
				            <td><textarea id="light222" name="light2"></textarea></td>
				            <td><textarea id="dinner2" name="dinner"></textarea></td>    
				        </tr>
				        <tr>   
				            <td>点心</td>
				            <td><textarea id="breakfast3" name="breakfast"></textarea></td>
				            <td><textarea id="light13" name="light1"></textarea></td>
				            <td><textarea id="lunch3" name="lunch"></textarea></td>
				            <td><textarea id="light23" name="light2"></textarea></td>
				            <td><textarea id="dinner3" name="dinner"></textarea></td>    
				        </tr>
				        <tr>   
				            <td>晚餐</td>
				            <td><textarea id="breakfast4" name="breakfast"></textarea></td>
				            <td><textarea id="light14" name="light1"></textarea></td>
				            <td><textarea id="lunch4" name="lunch"></textarea></td>
				            <td><textarea id="light24" name="light2"></textarea></td>
				            <td><textarea id="dinner4" name="dinner"></textarea></td>     
				        </tr>   
				    </tbody>   
				</table> 
		   </div>
		   
		   		<div class="fitem" style="float: left; margin-left: 40px; margin-top: 40px;" >
					     <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;font-weight:bold" margin-left: 40px;margin-top: 20px;">通知对象:</label>
					    
					    <div class="fitem" id="divSchool" style="margin-top: 20px">
	
			 		    </div>
			    </div>
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="recipeId" id="recipeId" value=""/>
		   <input type="hidden" name="userid" id="userid" value="">
		   <input type="hidden" name="breakfastt" id="breakfastt" value="">
		   <input type="hidden" name="light11" id="light11" value="">
		   <input type="hidden" name="lunchh" id="lunchh" value="">
		   <input type="hidden" name="light22" id="light22" value="">
		   <input type="hidden" name="dinnerr" id="dinnerr" value="">
		   <input type="hidden" name="schoolVar" id="schoolVar" value="">
		   <input type="hidden" name="draf" id="draf" value="">
		   
	 </form>
</div>

<!------------------------------ 确认取消按钮 ----------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:200px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddNotice()" iconcls="icon-save">发布</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save"  onclick="saveDraft()">存草稿</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>
<script type="text/javascript">
 $(function(){
 var uu = 0;
    $('#user').combobox({
     	url:'<%=basePath%>/user/userName.do',
     	valueField: 'userId',
		textField: 'userName',
		editable:false ,
		onLoadSuccess: function(result){
			if(uu==0){
				var data = $(this).combobox('getData');
				data.insert(0, {'userId':'0','userName':'全部'});
				uu++;
				$("#user").combobox("loadData", data);
			}
		}
	});
	
}); 
</script>
<style>
.datagrid-btable .datagrid-cell{padding:6px 4px;overflow: hidden;text-overflow:ellipsis;white-space: nowrap;}  
	    formatter: function(value,row,index){  
       return '<span title='+value+'>'+value+'</span>'  
    }  
</style>
</html>
