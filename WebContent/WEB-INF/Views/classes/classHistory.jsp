<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/css/system/notice.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script>
$(document).ready(function(){
//回车点击事件--搜索功能
	$(document).keydown(function(event){
		if(event.keyCode==13){
			//$("#login").click();
			sousuo();
		}
	}) ;

var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
         xianshi(rowIndex,field);
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
     
});

});

/* 操作 */
function formatOper(value,row,index){
	return "<a href='javascript:openLook(\"" + row['historyClassId'] + "\");'>查看</a>"
	+ " | <a href='javascript:openDel(\"" + row['historyClassId'] + "\");'>删除</a>";
} 
/* 添加 保存 角色 */
function saveAddNotice()
{	

		var Name = $('#Name').val();
		if(Name == "" || Name == null){
				layer.msg("标题不能为空")
				return null;
			}
		/* var user = $('#user').combobox("getValue");
		if(user == "" || user == null){
				layer.msg("用户不能为空")
				return null;
			} */
		var text = $('#text').val();
		if(text == "" || text == null){
				layer.msg("正文不能为空")
				return null;
			}
		$('#formAddNotice').form('submit', {
			url : '<%=basePath%>/notice/saveNotice.do',
			onSubmit : function() {
				return $(this).form('validate');
				var index = layer.load(1, {
					  shade: [0.1,'#fff'] //0.1透明度的白色背景
				})
				
			},
			success : function(result) {
				data = eval("(" + result + ")");
				if(data.code == 100){
					layer.closeAll('loading');
					$('#adddlg').dialog('close');
					$('#grid').datagrid('reload');
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">保存成功</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
					});
				}
				else
				{
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">'+ data.msg +'</div>',
						timeout: 500,  
						showType:'fade',
						style:{right:'', bottom:''}
						});
				}
			}
		}); 
	
}
/* 编辑角色 */
function openEdite(historyClassId)
{	
	$.post("<%=basePath%>/notice/updateNotice.do",{historyClassId:historyClassId},function(result){
		data = eval("(" + result + ")");
		$('#historyClassId').val(data.historyClassId);
		$('#Name').val(data.title);
		$('#user').combobox('setValue',data.user);
		$('#text').val(data.content);
		$('#mode').val("edit");
		$('#adddlg').panel({title: "编辑用户"});
		$('#adddlg').dialog('open');
	}); 
	
}

/* 单删 */
function openDel(historyClassId)
{	$.messager.confirm('确认','确认删除?',function(row){

	if(row){
	$.post("<%=basePath%>/classes/delHistory.do",{delKeys:historyClassId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
}
})};

//跳转 回到班级管理 主页
function backHistory()
{
  window.location.href="<%=basePath%>classes/classes.do";
}
//跳转到历史班级查看页面
function openLook(historyClassId){
		$.post("<%=basePath%>/classes/selectLook.do",{historyClassId:historyClassId},function(result){
		data = eval("(" + result + ")");
		$('#schoolName').html("    园区名：" + data.schoolName);
		$('#intoyear').html("    届数：" + data.intoYear);
		$('#grade').html("    年级：" + data.grade);
		$('#classesName').html("    班级名：" + data.className);
		$('#classteacher').html("    班主任：" + data.classTeacher);
		
		var otherTeaher = data.otherTeacherAndTel.replace(",","<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		
		
		$('#otherTeacherAndTel').html("    其他老师 : " + otherTeaher);
		
	}); 
	


		$('#adddlg').panel({title: "查看"});
		$('#adddlg').dialog('open');
}


//搜索
function sousuo()
{
	var SsName = $('#SsName').val();
	$('#grid').datagrid('load', {SsName:SsName});
	
}
<%-- function shuxing(value,row){
	//classes/selectLook  historyClassId
	$.post("<%=basePath%>/classes/selectLook.do",{historyClassId:row.historyClassId},function(result){
		data = eval("(" + result + ")");
		//alert(12)
		//alert(data.otherTeacherAndTel)
		$('#oth').val(data.otherTeacherAndTel);
		//alert(value)
	});	
		var ss =	$('#oth').val();
		alert(ss)
		return "<span title='" + ss + "'>" + value + "</span>"; 
} --%>

function xianshi(rowIndex,field){
$.post("<%=basePath%>/classes/selectHistory.do",function(result){
		data = eval("(" + result + ")");
			if(field=="otherTeacher"){
				layer.msg(data.rows[rowIndex].oth)
			}
		});	

}
</script>
</head>
<body class="easyui-layout">
<!----------------------------- 功能按钮 --------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:110%; margin-top:5px;margin-left:7px;text-align: left;height:35px;"> 
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="backHistory()" >班级管理</a>>
        
        <a href="javascript:void(0)" class="easyui-linkbutton"  plain="true"  >历史班级</a>

	 </div> 
	
<!--------------------------------- 搜索框 ----------------------------------->
	 <div style="margin-left:10px;text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		   <div style="float: left;margin-top:5px; height: 30px; ">
	   		<input name="SsName" id="SsName" placeholder="&nbsp;&nbsp;输入要查询的园区名" style="margin-top: 0px;border:1px #C5C5C5 solid;width: 200px;height: 23px"/>
		   </div>
	     <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer; background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 5px;border-radius: 5px;"
	     class="easyui-linkbutton" iconCls="icon-search">搜索</div>	   
	 </div>
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后台用户管理' style='width:17px;height:17px;float:left' src='<%=basePath%>/img/titleImg.png'>
		<div style='height:20px;line-height:22px;margin-left:10px;margin-top:-4;float:left'>历史班级</div>"
			url="<%=basePath%>/classes/selectHistory.do" toolbar="#tb"
			rownumbers="true" pageSize="20" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据" >
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="schoolName" width="12%" align="center">园区名</th>
					<th field="intoyear" width="12%" align="center">届数</th>
					<th field="grade" width="12%" align="center">年级</th>
					<th field="classesName" width="12%" align="center">班级名</th>
					<th field="classteacher" width="12%" align="center">班主任</th>
					<th field="otherTeacher" width="12%" align="center" >老师</th>
					<th field="status" width="12%" align="center">状态</th>
					<th field="oper" width="15%" align="center" formatter="formatOper">操作</th>
				</tr>
			</thead>
		</table>
		<input type="hidden" name="oth" id="oth" value="">
<!------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 420px; height: 500px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" >
	
			<ul>
				<li><span style="font-size: 13px; width: 100%; height: 15%; line-height: 20px; margin-top: 5px;" id="schoolName"></span></li>
				<li><span style="font-size: 13px; width: 100%; height: 15%; line-height: 20px" id="intoyear"></span></li>
				<li><span style="font-size: 13px; width: 100%; height: 15%; line-height: 20px" id="grade"></span></li>
				<li><span style="font-size: 13px; width: 100%; height: 15%; line-height: 20px" id="classesName"></span></li>
				<li><span style="font-size: 13px; width: 100%; height: 15%; line-height: 20px" id="classteacher"></span></li>
				<li><span style="font-size: 13px; width: 100%; height: 15%; line-height: 20px" id="otherTeacherAndTel"></span></li>
			</ul>
</div>

</body>
<script type="text/javascript">
 $(function(){
 var uu = 0;
    $('#user').combobox({
     	url:'<%=basePath%>/user/userName.do',
     	valueField: 'userId',
		textField: 'userName',
		editable:false ,
		onLoadSuccess: function(result){
			if(uu==0){
				var data = $(this).combobox('getData');
				data.insert(0, {'userId':'0','userName':'全部'});
				uu++;
				$("#user").combobox("loadData", data);
			}
		}
	});
	
}); 
</script>
</html>
