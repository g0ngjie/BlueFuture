﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>蓝色未来管理后台</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="<%=basePath%>/Public/js/topTotle.js"></script>
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<link href="<%=basePath%>../Public/css/main/login.css" rel="stylesheet" type="text/css" />
<script src="js/cloud.js" type="text/javascript"></script>
<script language="javascript">

var gUserId = 0;

$(document).ready(function(){
});
 
$(function(){
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
	$(window).resize(function(){  
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
    });
});  

/* 登录验证 */
function login(){
	var loginUser = $('#loginUser').val();
	var loginPwd = $('#loginPwd').val();
	var radom = $('#captcha').val();
	if(loginUser == "")
	{
		alert("请输入手机号");
		return;
	}
	if(loginPwd == "")
	{
		alert("请输入密码");
		return;
	}
	if(radom == "")
	{
		alert("请输入验证码");
		return;
	}
	$.post("<%=basePath%>system/loginYZ.do",{loginName:loginUser,pwd:loginPwd,radom:radom},function(result){
			data = eval("(" + result + ")");
			var userId = data.userId;
			if(data.code == 100)
			{
				var loginPwd = $('#loginPwd').val();
				if(loginPwd == "000000")
				{
					gUserId = userId;
					$('#beginPassword').textbox('setValue','');
					$('#endPassword').textbox('setValue','');
					$('#doublePassword').textbox('setValue','');
					
					$('#adddlg').panel({title: "修改密码"});
					$('#adddlg').dialog('open');
				}
				else
				{
			   		var url = encodeURI("<%=basePath%>framework/index.do?userId="+ userId);
					window.location.href= url;
				}
			}
			else{
				layer.msg(data.msg);
			}
		}
	);
}

function savePassWord()
{
	var endPassword = $('#endPassword').val();
	var doublePassword = $('#doublePassword').val();
	if(endPassword == "")
	{
		alert("请输入新密码");
		return;
	}
	if(doublePassword != endPassword)
	{
		alert("两次密码不一致");
		return;
	}
	//gUserId
	$.post("<%=basePath%>system/passwordEdit.do",{beginPassword:'000000',endPassword:endPassword,doublePassword:doublePassword},function(result){
		data = eval("(" + result + ")");
		//alert(data.code);
		if(data.code == 100){
			//window.location.href = '<%=basePath%>framework/index.jsp';
			var url = encodeURI("<%=basePath%>framework/index.do?userId="+ gUserId);
			window.location.href= url;
		}
	});
	<%-- var url = encodeURI("<%=basePath%>system/passwordEdit.do?userId="+ gUserId);
	window.location.href= url; --%>
}

var Num = "";
var wait=60;
var telV = "";
function huoqu(obj){
	telV = $('#tel').val();
	if(telV == "")
	{
		alert("请输入手机号");
		return;
	}
	for(var i=0;i<6;i++)
	{
		Num+=Math.floor(Math.random()*10);
	} 
	//alert(Num);
	$.post("<%=basePath%>system/huoqu.do",{mobile:telV,code:Num},function(result){
		data = eval("(" + result + ")");
		if(data.code == 100){
			time(obj);
		}
		if(data.code == 101){
			alert("账号不存在");
		}
	});
}
function time(o) {  
     if (wait == 0) {  
         o.removeAttribute("disabled");            
         o.value="获取验证码";  
         wait = 60;  
     } else {  
         o.setAttribute("disabled", true);  
         o.value="重新发送(" + wait + ")";  
         wait--;  
         setTimeout(function() {  
             time(o)  
         },  
         1000)  
     }  
}  

function forget(){
	$('#addd').dialog('open');
	$('#addd').panel({title: "忘记密码"});
	$('#tel').textbox('setValue','');
	$('#yanzheng').val('');
	
	$('#addd').dialog('open');
}

function xiayibu(){
	
	var yanzheng = $('#yanzheng').val();
	var telV = $('#tel').val();
	if(telV == "")
	{
		alert("请输入手机号");
		return;
	}
	if(yanzheng == "")
	{
		alert("请输入验证码");
		return;
	}
	
	$('#addd').dialog('close');
	$('#accc').dialog('open');
	$('#accc').panel({title: "修改密码"});
	
	$('#forgetNew').textbox('setValue','');
	$('#forgetNewDou').textbox('setValue','');
	
	
}
function saveNew(){
	var forgetNew = $('#forgetNew').val();
	var forgetNewDou = $('#forgetNewDou').val();
	if(forgetNew == "")
	{
		alert("请输入密码");
		return;
	}
	if(forgetNew != forgetNewDou)
	{
		alert("两次密码输入不一致");
		return;
	}
	$.post("<%=basePath%>system/forgetPassWord.do",{mobile:telV,newPassWord:forgetNew},function(result){
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#accc').dialog('close');
			layer.msg("修改成功");
		}
	});
}
</script>
<style type="text/css">
@CHARSET "UTF-8";
/* 登录页也样式调整 */
#login{width:220px;height:35px; font-size:14px; background:#38ACF7;border-radius:3px; font-weight:bold; color:#fff;cursor:pointer; line-height:35px;margin-top: 0px;}
#loginPwd{width:260px; height:48px;margin-left:6px; border:1px solid #81CFFC;line-height:48px; padding-left:44px; font-size:14px;font-weight:bold; background-color: #A7E3FF;border-radius:3px; }
#loginUser{width:260px; height:48px; border:1px solid #81CFFC; line-height:48px; padding-left:44px; font-size:14px; font-weight:bold; background-color: #A7E3FF;border-radius:3px; }
#captcha{width:150px; height:48px; border:1px solid #81CFFC;line-height:48px; padding-left:44px; font-size:14px;font-weight:bold; background-color: #A7E3FF;border-radius:3px; }
#loginbox{
	margin-top: 20%;
	width:420px;
	height:240px;
	margin-left: 30%;
	background: #D0F1FF;
	padding: 20px 0px 0px 20px;
	border-radius:5px;
}
body{  background:url(<%=basePath%>Public/images/lanseMain2.jpg) repeat;background-size:110%;  overflow:hidden;}
.loginbox_div{margin-top: 10px;}
#loginbox_div_3{margin-left: 80px;}
.loginbox_div_lable{font-size: 16px;}
</style>
</head>
<body >
<div class="loginbody" >
	 <div class="loginbox" id="loginbox" >
		 	 
		 	<div class="loginbox_div" >
		 		<label class="loginbox_div_lable">手机号：</label>
		 		<input id="loginUser" name="loginUser" type="text" placeholder="请输入手机号" />
		 	</div>
		 	<div class="loginbox_div">
		 		<label class="loginbox_div_lable">密码：</label>&nbsp;&nbsp;&nbsp;
		 		<input id="loginPwd" name="loginPwd" type="password" placeholder="请输入密码"  />
		 	</div>
		 	
		 	<div class="loginbox_div">
		 		<label class="loginbox_div_lable">验证码：</label>
		 		<input id="captcha" name="captcha" type="text" placeholder="验证码" class="text" maxlength="10" />
		 		<div style="margin-left: 273px;margin-top: -50px;">
			 		 <img width="100px" height="48px" id="captchaImage" src="<%=basePath%>/system/captcha.do" style="cursor:pointer"/>
		 		</div>
		 	</div>
		 	
		 	<div class="loginbox_div" id="loginbox_div_3" style="margin-left: 68px;">
		 		<input id="login" name="login" type="button" value="登录"  onclick="login()" style="float: left;"/>
		 		<input id="forget" name="forget" type="button" value="忘记密码"  onclick="forget()" style="float: left;margin-top: 17px;margin-left: 30px;cursor:pointer;"/>
		 	</div>
		 	 
	 </div>
</div>


<div id="adddlg" class="easyui-dialog" style="width: 360px; height: 200px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" >
	<form id="formAddNotice" name="formAddNotice" method="post">
   <!-- 新密码  输入  -->
   <div class="fitem" >
	   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">新密码</label>
	   <input name="endPassword" id="endPassword" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; validType="length[6,18]" type="password" />
   </div>
   <!-- 确认密码  输入  -->
   <div class="fitem" >
	   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">确认密码</label>
	   <input name="doublePassword" id="doublePassword" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;" validType="length[6,18]" type="password" />
   </div>
   </form>
</div> 		

<!-- 忘记密码 -->
<div id="addd" class="easyui-dialog" style="width: 360px; height: 200px; padding: 10px 20px;display: none;" closed="true"  buttons="#" >
	<form id="formAdd" name="formAdd" method="post">
   <!-- 输入手机号  -->
   <div class="fitem" >
	   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 10px;margin-top: 10px;">输入手机号</label>
	   <input name="tel" id="tel" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;in-top: -6px"; validType="length[6,18]" type="text" />
   </div>
   
   			<div  style="margin-left: 0px;">
		 		<input  type="button" value="获取验证码"  onclick="huoqu(this)" style="float: left;width: 80px;height: 30px;margin-top: 10px;margin-left: 10px;cursor:pointer"/>
		 		<input type="text" id="yanzheng" style="width:100px; height:21px; margin-top: 15px; border: 1px solid #95B8E7;margin-left: 81px;"/>
		 	</div>
		 	<div  style="margin-left: 20px;margin-top: 20px;">
   				<input  type="button" value="下一步"  onclick="xiayibu()" style="float: left;width: 263px;height: 30px;margin-left: -11px;"/>
   			</div>
   </form>
</div> 
<!-- 下一步 -->
<div id="accc" class="easyui-dialog" style="width: 360px; height: 200px; padding: 10px 20px;display: none;" closed="true"  >
	<form id="formAddNotice" name="formAddNotice" method="post">
   <!-- 新密码  输入  -->
   <div class="fitem" >
	   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">新密码</label>
	   <input id="forgetNew" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; validType="length[6,18]" type="password" />
   </div>
   <!-- 确认密码  输入  -->
   <div class="fitem" >
	   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">确认密码</label>
	   <input id="forgetNewDou"  class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;" validType="length[6,18]" type="password" />
   </div>
   <div  style="margin-left: 51px;margin-top: 20px;">
   				<input  type="button" value="确定"  onclick="saveNew()" style="float: left;width: 263px;height: 30px;margin-left: -11px;"/>
   	</div>
   </form>
</div> 
<!------------------------------ 确认取消按钮 ----------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="savePassWord()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 


</body>
<script language="javascript">
$(document).ready(function(){
	//回车点击事件
	$('#loginUser').val('');
	$(document).keydown(function(event){
		if(event.keyCode==13){
			$("#login").click();
		}
	});
	
var browser=navigator.appName
 
var b_version=navigator.appVersion
 
var version=b_version.split(";");
 
var trim_Version=version[1].replace(/[ ]/g,"");

 
if(browser=="Microsoft Internet Explorer" && trim_Version=="MSIE7.0"){
 	$('#loginbox').css('display','none');
    alert('当前浏览器版本过低！请升级浏览器或者切换成兼容默认登陆;');
}else if(browser=="Microsoft Internet Explorer" && trim_Version=="MSIE6.0"){
 
    $('#loginbox').css('display','none');
    alert('当前浏览器版本过低！请升级浏览器或者切换成兼容默认登陆;');
 
}
});

// 更换验证码
$('#captchaImage').click(function() 
{
	//alert(1);
    $('#captchaImage').attr("src", "<%=basePath%>/system/captcha.do?timestamp=" + (new Date()).valueOf());
 });
</script>
</html>
