<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/css/system/syslog.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script src="<%=basePath%>/Public/js/pro_common.js" type="text/javascript"></script>
<script>
$(document).ready(function(){

		var IsCheckFlag = true;
		$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
});
	
	$(function(){
		$('input').bind({
			//获取焦点事件
			focus:function(){
				if (this.value == this.defaultValue){
					this.value="";
				}
			},
			//失去焦点事件
			blur:function(){
				if (this.value == ""){
					$(this).validatebox({ 
						//required:true
					});
					this.value = this.defaultValue;
				}
			}
		});
	})
	
/* 导出 */
function daochu(){		

		var SsName = $('#SsName').val();
		var startDate =  $("#startDate").datebox("getValue");
		var endDate = $("#endDate").datebox("getValue");
		var  fun = $("#fun").val();
	    var  operate = $("#operate").val();
		var ss = '<%=basePath%>';
		window.parent.location=ss+"logdaochu/sysLogDaoChu.do?div="+SsName+"&startDate="+startDate+"&endDate="+endDate+"&fun="+fun+"&operate="+operate;
	}
// 搜索 页面
function selectSearch(){
	var SsName = $('#SsName').val();
	var startDate =  $("#startDate").datebox("getValue");
	var endDate = $("#endDate").datebox("getValue");
	var  fun = $("#fun").val();
	var  operate = $("#operate").val();
	$('#grid').datagrid('load', {startDate:startDate,SsName:SsName,endDate:endDate,fun:fun,operate:operate});
	
}	

$(function () {
	        $('input:text').focus();
	        var $inp = $('input:text');
	        $inp.bind('keydown', function (e) {
	            var key = e.which;
	            if (key == 13) {
	              //  e.preventDefault();
	                var nxtIdx = $inp.index(this) + 1;
	                $(":input:text:eq(" + nxtIdx + ")").focus();
	                selectSearch();
	            }
	        });
	    });
</script>

</head>
<body class="easyui-layout">
	<div id="tb" style="padding:5px;height:auto;display: none;"> 
<!---搜索  ----------->
	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:50px; border-top: 0px solid #D9D9D9;">
		  <div>
			   <label>操作时间: </label>
			 	    <input class="easyui-datebox" id="startDate" name="startDate" style="margin-top: 0px;border:1px #C5C5C5 solid;width: 182px;height: 23px" 
			 	    data-options="formatter:myformatter,parser:myparser" type="text" placeholder="单行输入"> 
			 	   <a>~</a>
			   		<input class="easyui-datebox" id="endDate" name="endDate" style="margin-top: 0px;border:1px #C5C5C5 solid;width: 182px;height: 23px" 
			 	    data-options="formatter:myformatter,parser:myparser" type="text" placeholder="单行输入"> 
			 
		 </div>	
		   <div style="float: left;height: 30px;line-height: 30px;margin-top: 4px;">
		  	   <label>操作人: </label>
		 	   <input name="SsName" id="SsName"  type="text" placeholder="单行输入" 
		 	   style="margin-bottom:2px;border:1px solid  #95B8E7 ;width: 100px;height: 24px" />
		  </div>
		   <div style="float: left;height: 30px;line-height: 30px;margin-top: 4px;margin-left: 5px;">
		  	   <label>操作模块: </label>
		 	   <input name="fun" id="fun"  type="text" placeholder="单行输入" 
		 	   style="margin-bottom:2px;border:1px solid  #95B8E7 ;width: 100px;height: 24px" />
		  </div>
		  <div style="float: left;height: 30px;line-height: 30px;margin-top: 4px;margin-left: 5px;">
		  	   <label>操作类型: </label>
		 	   <input name="operate" id="operate"  type="text" placeholder="单行输入" 
		 	   style="margin-bottom:2px;border:1px solid  #95B8E7 ;width: 100px;height: 24px" />
		 	   <div onclick="selectSearch();" class="easyui-linkbutton" iconCls="icon-search"
			  		style=" border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:25px;background-color: #FFF;color:#000;width: 75px;height:23px;border-radius: 5px;">搜索
			  </div>
		  </div>
	<!-- 导出 -->
		   <div style="float: left;height: 30px;line-height: 30px;margin-top: 9px;margin-left: 5px;">    
		       <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-redo"
		          onclick="daochu()" 
		          style=" border:1px solid #000;background-color: #FFF;color:#000;width: 75px;height:23px;">导出IP</a>
		   </div> 
		    
	  </div> 
   
</div>
<!-- ----页面显示信息------------------------>
<div style="width:100%;height:100%;" id="div">
	<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" 
	title="<img  style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>频道统计</div>"
		url="<%=basePath %>/sys/sysSelect.do" toolbar="#tb" 
		rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据"> 
		<thead>
			<tr>
				<th field="ck" checkbox="true" id="select"></th>
				<th field="createDate" width="20%" align="center">时间</th>
				<th field="nameUser" width="19%" align="center" >角色</th>
				<th field="userId" width="18%" align="center">操作人</th>
				<th field="funName" width="19%" align="center" >功能模块</th>
				<th field="content" width="19%" align="center">内容</th>
			</tr>
		</thead>
	</table>
</div>
</body>
	
</html>
