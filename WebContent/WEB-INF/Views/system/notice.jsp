<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/css/system/notice.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script>
$(document).ready(function(){

var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
     
});

});

/* 操作 */
function formatOper(value,row,index){
	return "<a href='javascript:openEdite(\"" + row['noticeId'] + "\");'>编辑</a>"
	+ " | <a href='javascript:openDel(\"" + row['noticeId'] + "\");'>删除</a>";
} 
/* 添加 保存 角色 */
function saveAddNotice()
{	

		var Name = $('#Name').val();
		if(Name == "" || Name == null){
				layer.msg("标题不能为空")
				return null;
			}
		/* var user = $('#user').combobox("getValue");
		if(user == "" || user == null){
				layer.msg("用户不能为空")
				return null;
			} */
		var text = $('#text').val();
		if(text == "" || text == null){
				layer.msg("正文不能为空")
				return null;
			}
		$('#formAddNotice').form('submit', {
			url : '<%=basePath%>/notice/saveNotice.do',
			onSubmit : function() {
				return $(this).form('validate');
				var index = layer.load(1, {
					  shade: [0.1,'#fff'] //0.1透明度的白色背景
				})
				
			},
			success : function(result) {
				data = eval("(" + result + ")");
				if(data.code == 100){
					layer.closeAll('loading');
					$('#adddlg').dialog('close');
					$('#grid').datagrid('reload');
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">保存成功</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
					});
				}
				else
				{
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">data.msg</div>',
						timeout: 500,  
						showType:'fade',
						style:{right:'', bottom:''}
						});
				}
			}
		}); 
	
}
/* 编辑角色 */
function openEdite(noticeId)
{	
	$.post("<%=basePath%>/notice/updateNotice.do",{noticeId:noticeId},function(result){
		data = eval("(" + result + ")");
		$('#noticeId').val(data.noticeId);
		$('#Name').val(data.title);
		$('#user').combobox('setValue',data.user);
		$('#text').val(data.content);
		$('#mode').val("edit");
		$('#adddlg').panel({title: "编辑用户"});
		$('#adddlg').dialog('open');
	}); 
	
}

/* 单删 */
function openDel(noticeId)
{	
	$.post("<%=basePath%>/notice/deleteNotice.do",{delKeys:noticeId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
}

/* 添加按钮 */
function add()
{
    document.getElementById("Name").value = "";
    $('#user').combobox('setValue','');
    document.getElementById("text").value = "";
    $('#mode').val("add");
    $('#adddlg').panel({title: "添加"});
	$('#adddlg').dialog('open');
}

/* 删除按钮 */
function openDelete(){	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].noticeId;
		}
	}
	$.post("<%=basePath%>/notice/delNotice.do",{delKeys:str},function(result){
			layer.closeAll('loading');
			data = eval("(" + result + ")");
			//alert(result)
			if(data.code == 100){
				$('#grid').datagrid('reload');
				$.messager.show({
								title:'提示',
								msg:'<div class="msgs">删除成功</div>',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
			}
		}); 
		}else{
			$.messager.show({
								title:'提示',
								msg:'<div class="msgs">请选择您要删除的数据</div>',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
		}
	
}
	
</script>
</head>
<body class="easyui-layout">
<!----------------------------- 功能按钮 --------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()" >添加</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="openDelete()" >删除</a>

	 </div> 
	
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后台用户管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>公告管理</div>"
			url="<%=basePath%>/notice/selectnotice.do" toolbar="#tb"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="title" width="20%" align="center">标题</th>
					<th field="content" width="38%" align="center">内容</th>
					<th field="createDate" width="20%" align="center">日期</th>
					<th field="oper" width="17%" align="center" formatter="formatOper">操作</th>
				</tr>
			</thead>
		</table>
		
<!------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 420px; height: 500px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" >
	<form id="formAddNotice" name="formAddNotice" method="post">
		   <div class="fitem" >
			   <label class="tile">标题：</label>
			   <input name="Name" id="Name" class="name" style="width: 270px;" type="text" />
		   </div>
		   <div class="fitem">
			   <label>用户：</label>
			   <input id="user" name="user" class="easyui-combobox" placeholder="请选择" 
			  
	            style="vertical-align:middle;font-size:15px;color:#666;margin-top: -6px;wsidth: 270px;" type="text" />
		   	</div>	
		<!-- 	<select id="customCombobox">  </select> -->
		   	<div class="fitem">
			   <label>正文：</label>
			   <textarea name="text" id="text" class="text" placeholder="多行输入"></textarea>
		   </div>
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="noticeId" id="noticeId" value=""/>
		   <input type="hidden" name="userid" id="userid" value="">
	 </form>
</div>

<!------------------------------ 确认取消按钮 ----------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddNotice()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>
<script type="text/javascript">
 $(function(){
 var uu = 0;
    $('#user').combobox({
     	url:'<%=basePath%>/user/userName.do',
     	valueField: 'userId',
		textField: 'userName',
		editable:false ,
		onLoadSuccess: function(result){
			if(uu==0){
				var data = $(this).combobox('getData');
				data.insert(0, {'userId':'0','userName':'全部'});
				uu++;
				$("#user").combobox("loadData", data);
			}
		}
	});
	
}); 
</script>
</html>
