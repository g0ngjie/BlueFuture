<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/css/system/notice.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script>
$(document).ready(function(){

var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
     
});

});

/* 操作 */
function formatOper(value,row,index){
	return "  <a href='javascript:openDel(\"" + row['feedbackId'] + "\");'>删除</a>";
} 

/* 单删 */
function openDel(feedbackId)
{	
	$.post("<%=basePath%>/notice/deleteFeedbackNotice.do",{delKeys:feedbackId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
}


/* 删除按钮 */
function openDelete(){	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].feedbackId;
		}
	}
	$.post("<%=basePath%>/notice/delFeedbackNotice.do",{delKeys:str},function(result){
			layer.closeAll('loading');
			data = eval("(" + result + ")");
			//alert(result)
			if(data.code == 100){
				$('#grid').datagrid('reload');
				$.messager.show({
								title:'提示',
								msg:'<div class="msgs">删除成功</div>',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
			}
		}); 
		}else{
			$.messager.show({
								title:'提示',
								msg:'<div class="msgs">请选择您要删除的数据</div>',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
		}
	
}
	
</script>
</head>
<body class="easyui-layout">
<!----------------------------- 功能按钮 --------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="openDelete()" >删除</a>

	 </div> 
	
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后台用户管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>意见反馈</div>"
			url="<%=basePath%>/notice/feedbackNotice.do" toolbar="#tb"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="content" width="20%" align="center">联系方式</th>
					<th field="enable" width="38%" align="center">内容</th>
					<th field="createDate" width="20%" align="center">创建时间</th>
					<th field="oper" width="17%" align="center" formatter="formatOper">操作</th>
				</tr>
			</thead>
		</table>
		
</div>

</body>

</html>
