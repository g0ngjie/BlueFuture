<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/css/system/notice.css">


<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>

<script>
$(document).ready(function(){

	<c:if test="${roleLe != '总园'}">
		$('#roleZongY').css('display','none');
	</c:if>

	<% int sum = 0; %>
	
	<c:if test="${role_add == 'role_add'}">
	
		document.getElementById("addRole").style.display="block";
		
		<% sum ++; %>;
		
	</c:if>
	<% if(sum == 0) { %>
		
		$("#grid").datagrid({
			toolbar:''
		});
		
	<% } %>

	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	});
	
});


/* 操作 */
function formatOper(value,row,index){

		<%int i = 0;%>
    	var str = "";
		<c:if test="${ role_xiugai == 'role_xiugai'}">
			str += "<a href='javascript:openEdite(\"" + row['roleId'] + "\");'>修改</a>&nbsp;";
			<%i ++;%>
		</c:if>
		<c:if test="${ role_del == 'role_del'}">
			<%if (i != 0) {%>
				str += " | &nbsp;"
			<%}%>
			str += "<a href='javascript:openDel(\"" + row['roleId'] + "\");'>删除</a>&nbsp;";
		</c:if>
		if(str == ""){
	    		str = "<a style='color:#666;'>无此权限</a>";
	    }
		return str;

} 

/* 添加 保存 角色 */
function saveAddNotice()
{		

		//点击 保存的时候 像后台传入 多选框的 园区名
		var arr = document.getElementsByName('schoolName'); 
		var keyId="";
		for(i=0;i<arr.length;i++){
			if(arr[i].checked)
			{
		 		//alert(arr[i].value)
				//alert(arr[i].value);
				keyId+=","+arr[i].value;
			}
		}
		$('#schoolVar').val(keyId);

		//获取typeName 动态添加的类型 的name值 传入后台
		var arr = document.getElementsByName('typeName'); 
		var typeNa="";
		for(i=1;i<arr.length;i++){
				typeNa+=","+arr[i].value;
		}
		
		$('#typeNam').val(typeNa);
		
			var Name = $('#roleName').val();
			
			if(Name.length > 10){
				layer.msg("请输入正确的角色")
				return null;
			}
			if(Name == "" || Name == null){
					layer.msg("角色不能为空")
					return null;
				}
				
				
			var schoolVar = $('#schoolVar').val();
			if(schoolVar == "" || schoolVar == null){
					layer.msg("请选择有效园区")
					return null;
				}	
				
		var level=$('input:radio[name="rolelevel"]:checked').val();
		//alert(level);
			if(level == "" || level == null){
					layer.msg("请选择角色级别")
					return null;
				}	
			/* if(level == "年级"){
				
				var NJ=$('input:radio[name="isNianJ"]:checked').length;	
				//alert(NJ)
				if(NJ == 0){
					layer.msg("请选择所属年级")
					return null;
				}
			} */
			
			
		$('#formAddNotice').form('submit', {
			url : '<%=basePath%>/role/saveRole.do',
			onSubmit : function() {
				return $(this).form('validate');
				var index = layer.load(1, {
					  shade: [0.1,'#fff'] //0.1透明度的白色背景
				})
				
			},
			success : function(result) {
				data = eval("(" + result + ")");
				if(data.code == 100){
					layer.closeAll('loading');
					$('#adddlg').dialog('close');
					$('#grid').datagrid('reload');
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">保存成功</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
					});
				}
				else
				{
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">'+data.msg+'</div>',
						timeout: 500,  
						showType:'fade',
						style:{right:'', bottom:''}
						});
				}
			}
		}); 
	
}

function getOpenEdite(roleId)
{
	var row1 =$('#divRoleTypeRowTemp');
	$('#divRoleType').empty();
	
	$('#divRoleType').append(row1);
	
	$.post("<%=basePath%>/role/roleEdit.do",{roleId:roleId},function(result){
		data = eval("(" + result + ")");
		$('#roleId').val(data.roleId);
		$('#roleName').textbox('setValue',data.roleName);
		if(data.roleName =="分园长"||data.roleName =="班主任"||data.roleName =="普通老师"){
			$('#roleName').textbox('readonly',true); 
		}else{
			$('#roleName').textbox('readonly',false); 
		}
		$('#validSchool').val(data.schoolName);
		$('#typeName').val(data.typeName).checked;
		
		$('#subRoleIdd').val(data.subRoleIdd);
		
		if(data.listSubRole[0].typeName != ""&& data.listSubRole[0].typeName != null){
			var subId = "";
			for(var i=0; i<data.listSubRole.length; i++)
			{
				var row = data.listSubRole[i];
				var subRoleId = row.subRoleId;
				var typeName = row.typeName;
				var divObj = addType();
				var roleidObj = divObj.find('#typeName');
				roleidObj.val(typeName);
				divObj.val(subRoleId);
				subId+=subRoleId+",";
				
			}
			$('#subRoleId').val(subId);
		}
		else{
			var subId = "";
			for(var i=1; i<data.listSubRole.length; i++)
			{
				var row = data.listSubRole[i];
				var subRoleId = row.subRoleId;
				var typeName = row.typeName;
				var divObj = addType();
				var roleidObj = divObj.find('#typeName');
				
				roleidObj.val(typeName);
				divObj.val(subRoleId);
				subId+=subRoleId+",";
			}
			$('#subRoleId').val(subId);
		}

		for(var i=0; i< data.schoolPowerList.length; i++){
			
			var schoolId = data.schoolPowerList[i].schoolId;
			$('#schoolId' + schoolId).attr("checked", "true");
		}
		
		var roleLevel = data.roleLevel;
		var typ = data.type;
		//alert(roleLevel) roleZongY
		//alert(typ);
		if(typ == 1){
			$('#allSchool').attr("checked", true);
		}else if(typ == 0){
			$('#allSchool').attr("checked", false);
		}
		if(roleLevel==0){
			$('#allSchool').attr("checked", "true");
			$("#zongyuan_label").show();
			$("#zongyuan").show();
			$("#zongyuan").attr("checked","checked");
			$('#showNinaJ').css('display','none');
		}else if(roleLevel==1){
			$("#fenyuan").attr("checked","checked");
			$('#showNinaJ').css('display','none');
		}else if(roleLevel==2){
			$("#nianji").attr("checked","checked");
			/* $('#showNinaJ').css('display','block');
			if(data.chaNJ =="1"){
				$("#tuoban1").attr("checked","checked")
			}else if(data.chaNJ =="2"){
				$("#xiaoban2").attr("checked","checked")
			}else if(data.chaNJ =="3"){
				$("#zhongban3").attr("checked","checked")
			}else if(data.chaNJ =="4"){
				$("#daban4").attr("checked","checked")
			}else if(data.chaNJ =="5"){
				$("#xueqian5").attr("checked","checked")
			}else{
				$('#showNinaJ').css('display','none');
			} */
			
		}else if(roleLevel==3){
			$("#banji").attr("checked","checked");
			$('#showNinaJ').css('display','none');
		}
		
		$('#mode').val("edit");
		$('#adddlg').panel({title: "编辑用户"});
		$('#adddlg').dialog('open');
	}); 
}
/* 查询 设置 权限 */
function openEditSel(roleId){
	
	fukong();
	
	$.post("<%=basePath%>/role/roleEditSel.do",{roleId:roleId},function(result){
		var obj = eval("(" + result + ")");
		for(var key in obj){ //第一层循环取到各个list
			var list = obj[key];
			var powerCode = list.powerCode;
			$("#"+powerCode).attr("checked", "checked");
		} 
	}); 
}

/* 编辑角色 */
function openEdite(roleId)
{	
	$.post("<%=basePath%>/school/schoolList.do",{page:1,rows:100},function(result){

		data = eval("(" + result + ")");
		
		var str = "";
		str += "<div style='display:inline-block;width:auto; font-size:13px;margin-left: 50px;'><label class='tile' >" + "所有园区" + "</label>";
		str += "<input type='checkbox' id='allSchool' name='schoolName'  value='0' onchange='selAllOnChange(this)'></div>";
		
		for(var i=0; i<data.rows.length; i++)
		{
			var obj = data.rows[i];
			str += "<div style='display:inline-block;width:auto; font-size:13px;margin-left: 40px;'><label class='tile'>" + obj.schoolName + "</label>";
			str += "<input type='checkbox' name='schoolName' style='margin-left: 10px;' id='schoolId" + obj.schoolId + "' value='" + obj.schoolId + "' onchange='selOnChange(this)' ></div>";
		}
		$("#divSchool").html(str);
		getOpenEdite(roleId);
		openEditSel(roleId)
	}); 
	
}

/* 单删 */
function openDel(roleId)
{	$.messager.confirm('确认','确认删除?',function(row){

	if(row){
	$.post("<%=basePath%>/role/delRole.do",{delKeys:roleId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 101){
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">该角色正在使用不可删除</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}else if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
}
})};

/*赋空值*/
function fukong(){
	
	$("#gaiLan").attr("checked",false);
	$("#gaiLan_kejian").attr("checked",false);
	$("#gaiLan_xinxi").attr("checked",false);
	$("#gaiLan_guanyu").attr("checked",false);

	$("#notice").attr("checked",false);
	$("#notice_kejian").attr("checked",false);
	$("#notice_new").attr("checked",false);
	$("#notice_caogao").attr("checked",false);
	$("#notice_chakan").attr("checked",false);
	$("#notice_chehui").attr("checked",false);
	$("#notice_del").attr("checked",false);
	$("#notice_daochu").attr("checked",false);
	
	$("#activity").attr("checked",false);
	$("#activity_kejian").attr("checked",false);
	$("#activity_new").attr("checked",false);
	$("#activity_chakan").attr("checked",false);
	$("#activity_chehui").attr("checked",false);
	$("#activity_del").attr("checked",false);
	
	$("#recipe").attr("checked",false);
	$("#recipe_kejian").attr("checked",false);
	$("#recipe_new").attr("checked",false);
	$("#recipe_chakan").attr("checked",false);
	$("#recipe_chehui").attr("checked",false);
	$("#recipe_del").attr("checked",false);
	
	$("#mailbox").attr("checked",false);
	$("#mailbox_kejian").attr("checked",false);
	$("#mailbox_chakan").attr("checked",false);
	$("#mailbox_del").attr("checked",false);
	$("#mailbox_daochu").attr("checked",false);
	
	$("#park").attr("checked",false);
	$("#park_kejian").attr("checked",false);
	$("#park_add").attr("checked",false);
	$("#park_xiugai").attr("checked",false);
	$("#park_del").attr("checked",false);
	
	$("#classes").attr("checked",false);
	$("#classes_kejian").attr("checked",false);
	$("#classes_add").attr("checked",false);
	$("#classes_addList").attr("checked",false);
	$("#classes_xiugai").attr("checked",false);
	$("#classes_del").attr("checked",false);
	$("#classes_up").attr("checked",false);
	$("#classes_zhuxiao").attr("checked",false);
	$("#classes_lishi").attr("checked",false);
	
	$("#student").attr("checked",false);
	$("#student_kejian").attr("checked",false);
	$("#student_add").attr("checked",false);
	$("#student_addList").attr("checked",false);
	$("#student_xiugai").attr("checked",false);
	$("#student_del").attr("checked",false);
	$("#student_daochu").attr("checked",false);
	
	$("#role").attr("checked",false);
	$("#role_kejian").attr("checked",false);
	$("#role_add").attr("checked",false);
	$("#role_xiugai").attr("checked",false);
	$("#role_del").attr("checked",false);

	$("#employee").attr("checked",false);
	$("#employee_kejian").attr("checked",false);
	$("#employee_add").attr("checked",false);
	$("#employee_daoru").attr("checked",false);
	$("#employee_xiugai").attr("checked",false);
	$("#employee_del").attr("checked",false);
	$("#employee_enable").attr("checked",false);
	$("#employee_daochu").attr("checked",false);	
	
	$("#appCheckIn").attr("checked",false);
	$("#appCheckIn_kejian").attr("checked",false);
	$("#appCheckIn_qingjia").attr("checked",false);
	$("#appCheckIn_jieshou").attr("checked",false);
	$("#appCheckIn_chuli").attr("checked",false);
	$("#appCheckIn_chaxunlan").attr("checked",false);
	
	$("#appGengduo").attr("checked",false);
	$("#appGengduo_gonggao").attr("checked",false);
	$("#appGengduo_huodong").attr("checked",false);
	$("#appGengduo_shipu").attr("checked",false);
	
}


/* 添加按钮 */
function add()
{	
	$('#roleName').textbox('readonly',false); 
	var row =$('#divRoleTypeRowTemp');//克隆
	$('#divRoleType').empty();
	$('#divRoleType').append(row);
	$("#fenyuan").attr("checked","checked");
	fukong();
	$('#roleName').textbox('setValue','');
	
    $('#mode').val("add");
    $('#adddlg').panel({title: "角色添加"});
	$('#adddlg').dialog('open');
	$.post("<%=basePath%>/school/schoolList.do",{page:1,rows:100},function(result){

		data = eval("(" + result + ")");
	
		var str = "";
		str += "<div style='display:inline-block;width:auto; font-size:13px;margin-left: 50px;'><label class='tile' >" + "所有园区" + "</label>";
		str += "<input type='checkbox' name='schoolName'  value='0' onchange='selAllOnChange(this)'></div>";
		
		for(var i=0; i<data.rows.length; i++)
		{
			var obj = data.rows[i];
			str += "<div style='display:inline-block;width:auto; font-size:13px;margin-left: 40px;'><label class='tile'>" + obj.schoolName + "</label>";
			str += "<input type='checkbox' name='schoolName' style='margin-left: 10px;' value='" + obj.schoolId + "' onchange='selOnChange(this)' ></div>";
		}
		$("#divSchool").html(str);
		
		
	}); 
}


function selOnChange()
{
	var hasNoCheck = false;
	var arr = document.getElementsByName('schoolName');
	for(i=1;i<arr.length;i++){
		if(!arr[i].checked)
		{
			$("#zongyuan").hide();
			$("#zongyuan_label").hide();
			arr[0].checked = false;
			hasNoCheck = true;
			$("#fenyuan").css("margin-left", "40px");
			$("#fenyuan").attr("checked","checked");
			break;
		}
	}
	
	if(!hasNoCheck)
	{
		$("#zongyuan").show();
		$("#zongyuan_label").show();
		arr[0].checked = true;
		$("#zongyuan").attr("checked","checked");
		$("#fenyuan").css("margin-left", "20px");
	}
	
}

function selAllOnChange(obj)
{
	var arr = document.getElementsByName('schoolName');
	if(obj.checked)
	{
		$('#showNinaJ').css('display','none');
		$("#zongyuan").show();
		$("#zongyuan_label").show();
		
		for(i=0;i<arr.length;i++){
			arr[i].checked = true;
		}
		
		$("#zongyuan").attr("checked","checked");
		
		$("#fenyuan").css("margin-left", "20px");
	}
	else
	{
		$("#zongyuan").hide();
		$("#zongyuan").attr("checked",false);
		
		$("#zongyuan_label").hide();
		
		for(i=0;i<arr.length;i++){
			arr[i].checked = false;
		}
		
		$("#zongyuan").removeAttr("checked");
		$("#fenyuan").css("margin-left", "40px");
		$("#fenyuan").attr("checked","checked");
	}
}



function check_all(){
	arr = document.getElementsByName('schoolName');
	check_val = [];
	for(i=0;i<arr.length;i++){
		arr[i].checked = true;
		check_val.push(arr[i].value);
	}
} 
	
	
function check_false(){
	arr = document.getElementsByName('schoolName');
	for(i=0;i<arr.length;i++){
		arr[i].checked = false;
	}
} 

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//删除类型
function delType(obj)
{
	var objId = $(obj).parent().parent().val();
	$.post("<%=basePath%>/role/chaSubId.do",{subRoleId:objId},function(result){
		data = eval("(" + result + ")");
		
			if(data.code == 101){
				layer.msg("改子角色已被使用,不可删除!")
			}else{
				var ids = $('#subRoleId').val();
				$(obj).parent().parent().remove();
				var words= new Array();   
  				words = ids.split(",");    
				var newIds = "";
				for(var i=0;i<words.length;i++){
					var t = words[i];
					if(t!=objId){
						newIds=newIds+t+",";
					}
				}
				$('#subRoleId').val(newIds);
			}
		});
	
}	
//添加类型
function addType()
{	

	var Name = $('#roleName').val().trim();
	if(Name == ""){
		layer.msg("请先输入角色名称")
		return null;
	}else{
	
		var row =$('#divRoleTypeRowTemp').clone();//克隆
		row.show();
		$('#divRoleType').append(row);
		return row;
	}
}

//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》概览
function selAll_gaiLan(obj){
	if(obj.checked){
		$("#gaiLan_kejian").attr("checked","checked");
		$("#gaiLan_xinxi").attr("checked","checked");
		$("#gaiLan_guanyu").attr("checked","checked");
	}
	if(!obj.checked){
		$("#gaiLan_kejian").attr("checked",false);
		$("#gaiLan_xinxi").attr("checked",false);
		$("#gaiLan_guanyu").attr("checked",false);
	}
}
function sel_kejian(obj){
	if(obj.checked){
		$("#gaiLan").attr("checked","checked");
	}
	if(!obj.checked){
		$("#gaiLan").attr("checked",false);
	}
}
function sel_xinxi(obj){
	if(obj.checked){
		$("#gaiLan").attr("checked","checked");
		$("#gaiLan_kejian").attr("checked","checked");
	}
}
function sel_guanyu(obj){
	if(obj.checked){
		$("#gaiLan").attr("checked","checked");
		$("#gaiLan_kejian").attr("checked","checked");
	}
}
//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》通知公告
function selAll_notice(obj){
	if(obj.checked){
		$("#notice_kejian").attr("checked","checked");
		$("#notice_new").attr("checked","checked");
		$("#notice_caogao").attr("checked","checked");
		$("#notice_chakan").attr("checked","checked");
		$("#notice_chehui").attr("checked","checked");
		$("#notice_del").attr("checked","checked");
		$("#notice_daochu").attr("checked","checked");
	}
	if(!obj.checked){
		$("#notice_kejian").attr("checked",false);
		$("#notice_new").attr("checked",false);
		$("#notice_caogao").attr("checked",false);
		$("#notice_chakan").attr("checked",false);
		$("#notice_chehui").attr("checked",false);
		$("#notice_del").attr("checked",false);
		$("#notice_daochu").attr("checked",false);
	}
}
function noticekejian(obj){
	if(obj.checked){
		$("#notice").attr("checked","checked");
	}
	if(!obj.checked){
		$("#notice").attr("checked",false);
	}
}
function noticenew(obj){
	if(obj.checked){
		$("#notice").attr("checked","checked");
		$("#notice_kejian").attr("checked","checked");
	}
}
function noticecaogao(obj){
	if(obj.checked){
		$("#notice").attr("checked","checked");
		$("#notice_kejian").attr("checked","checked");
	}
}
function noticechakan(obj){
	if(obj.checked){
		$("#notice").attr("checked","checked");
		$("#notice_kejian").attr("checked","checked");
	}
}
function noticechehui(obj){
	if(obj.checked){
		$("#notice").attr("checked","checked");
		$("#notice_kejian").attr("checked","checked");
	}
}
function noticedel(obj){
	if(obj.checked){
		$("#notice").attr("checked","checked");
		$("#notice_kejian").attr("checked","checked");
	}
}
function noticedaochu(obj){
	if(obj.checked){
		$("#notice").attr("checked","checked");
		$("#notice_kejian").attr("checked","checked");
	}
}
//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》精彩活动
function selAll_activity(obj){
	if(obj.checked){
		$("#activity_kejian").attr("checked","checked");
		$("#activity_new").attr("checked","checked");
		$("#activity_chakan").attr("checked","checked");
		$("#activity_chehui").attr("checked","checked");
		$("#activity_del").attr("checked","checked");
	}
	if(!obj.checked){
		$("#activity_kejian").attr("checked",false);
		$("#activity_new").attr("checked",false);
		$("#activity_chakan").attr("checked",false);
		$("#activity_chehui").attr("checked",false);
		$("#activity_del").attr("checked",false);
	}
}
function activitykejian(obj){
	if(obj.checked){
		$("#activity").attr("checked","checked");
	}
	if(!obj.checked){
		$("#activity").attr("checked",false);
	}
}
function activitynew(obj){
	if(obj.checked){
		$("#activity").attr("checked","checked");
		$("#activity_kejian").attr("checked","checked");
	}
}
function activitychakan(obj){
	if(obj.checked){
		$("#activity").attr("checked","checked");
		$("#activity_kejian").attr("checked","checked");
	}
}	
function activitychehui(obj){
	if(obj.checked){
		$("#activity").attr("checked","checked");
		$("#activity_kejian").attr("checked","checked");
	}
}	
function activitydel(obj){
	if(obj.checked){
		$("#activity").attr("checked","checked");
		$("#activity_kejian").attr("checked","checked");
	}
}		
//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》每周食谱
function selAll_recipe(obj){
	if(obj.checked){
		$("#recipe_kejian").attr("checked","checked");
		$("#recipe_new").attr("checked","checked");
		$("#recipe_chakan").attr("checked","checked");
		$("#recipe_chehui").attr("checked","checked");
		$("#recipe_del").attr("checked","checked");
	}
	if(!obj.checked){
		$("#recipe_kejian").attr("checked",false);
		$("#recipe_new").attr("checked",false);
		$("#recipe_chakan").attr("checked",false);
		$("#recipe_chehui").attr("checked",false);
		$("#recipe_del").attr("checked",false);
	}
}
function recipekejian(obj){
	if(obj.checked){
		$("#recipe").attr("checked","checked");
	}
	if(!obj.checked){
		$("#recipe").attr("checked",false);
	}
}
function recipenew(obj){
	if(obj.checked){
		$("#recipe").attr("checked","checked");
		$("#recipe_kejian").attr("checked","checked");
	}
}
function recipechakan(obj){
	if(obj.checked){
		$("#recipe").attr("checked","checked");
		$("#recipe_kejian").attr("checked","checked");
	}
}
function recipechehui(obj){
	if(obj.checked){
		$("#recipe").attr("checked","checked");
		$("#recipe_kejian").attr("checked","checked");
	}
}
function recipedel(obj){
	if(obj.checked){
		$("#recipe").attr("checked","checked");
		$("#recipe_kejian").attr("checked","checked");
	}
}
//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》院长信箱
function selAll_mailbox(obj){
	if(obj.checked){
		$("#mailbox_kejian").attr("checked","checked");
		$("#mailbox_chakan").attr("checked","checked");
		$("#mailbox_del").attr("checked","checked");
		$("#mailbox_daochu").attr("checked","checked");
	}
	if(!obj.checked){
		$("#mailbox_kejian").attr("checked",false);
		$("#mailbox_chakan").attr("checked",false);
		$("#mailbox_del").attr("checked",false);
		$("#mailbox_daochu").attr("checked",false);
	}
}
function mailboxkejian(obj){
	if(obj.checked){
		$("#mailbox").attr("checked","checked");
	}
	if(!obj.checked){
		$("#mailbox").attr("checked",false);
	}
}
function mailboxchakan(obj){
	if(obj.checked){
		$("#mailbox").attr("checked","checked");
		$("#mailbox_kejian").attr("checked","checked");
	}
}
function mailboxdel(obj){
	if(obj.checked){
		$("#mailbox").attr("checked","checked");
		$("#mailbox_kejian").attr("checked","checked");
	}
}
function mailboxdaochu(obj){
	if(obj.checked){
		$("#mailbox").attr("checked","checked");
		$("#mailbox_kejian").attr("checked","checked");
	}
}
//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》园区管理
function selAll_park(obj){
	if(obj.checked){
		$("#park_kejian").attr("checked","checked");
		$("#park_add").attr("checked","checked");
		$("#park_xiugai").attr("checked","checked");
		$("#park_del").attr("checked","checked");
	}
	if(!obj.checked){
		$("#park_kejian").attr("checked",false);
		$("#park_add").attr("checked",false);
		$("#park_xiugai").attr("checked",false);
		$("#park_del").attr("checked",false);
	}
}
function parkkejian(obj){
	if(obj.checked){
		$("#park").attr("checked","checked");
	}
	if(!obj.checked){
		$("#park").attr("checked",false);
	}
}
function parkadd(obj){
	if(obj.checked){
		$("#park").attr("checked","checked");
		$("#park_kejian").attr("checked","checked");
	}
}
function parkxiugai(obj){
	if(obj.checked){
		$("#park").attr("checked","checked");
		$("#park_kejian").attr("checked","checked");
	}
}
function parkdel(obj){
	if(obj.checked){
		$("#park").attr("checked","checked");
		$("#park_kejian").attr("checked","checked");
	}
}
//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》班级管理
function selAll_classes(obj){
	if(obj.checked){
		$("#classes_kejian").attr("checked","checked");
		$("#classes_add").attr("checked","checked");
		$("#classes_addList").attr("checked","checked");
		$("#classes_xiugai").attr("checked","checked");
		$("#classes_del").attr("checked","checked");
		$("#classes_up").attr("checked","checked");
		$("#classes_zhuxiao").attr("checked","checked");
		$("#classes_lishi").attr("checked","checked");
	}
	if(!obj.checked){
		$("#classes_kejian").attr("checked",false);
		$("#classes_add").attr("checked",false);
		$("#classes_addList").attr("checked",false);
		$("#classes_xiugai").attr("checked",false);
		$("#classes_del").attr("checked",false);
		$("#classes_up").attr("checked",false);
		$("#classes_zhuxiao").attr("checked",false);
		$("#classes_lishi").attr("checked",false);
	}
}
function classeskejian(obj){
	if(obj.checked){
		$("#classes").attr("checked","checked");
	}
	if(!obj.checked){
		$("#classes").attr("checked",false);
	}
}
function classesadd(obj){
	if(obj.checked){
		$("#classes").attr("checked","checked");
		$("#classes_kejian").attr("checked","checked");
	}
}
function classesaddList(obj){
	if(obj.checked){
		$("#classes").attr("checked","checked");
		$("#classes_kejian").attr("checked","checked");
	}
}
function classesxiugai(obj){
	if(obj.checked){
		$("#classes").attr("checked","checked");
		$("#classes_kejian").attr("checked","checked");
	}
}
function classesdel(obj){
	if(obj.checked){
		$("#classes").attr("checked","checked");
		$("#classes_kejian").attr("checked","checked");
	}
}
function classesup(obj){
	if(obj.checked){
		$("#classes").attr("checked","checked");
		$("#classes_kejian").attr("checked","checked");
	}
}
function classeszhuxiao(obj){
	if(obj.checked){
		$("#classes").attr("checked","checked");
		$("#classes_kejian").attr("checked","checked");
	}
}
function classeslishi(obj){
	if(obj.checked){
		$("#classes").attr("checked","checked");
		$("#classes_kejian").attr("checked","checked");
	}
}
//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》学生管理
function selAll_student(obj){
	if(obj.checked){
		$("#student_kejian").attr("checked","checked");
		$("#student_add").attr("checked","checked");
		$("#student_addList").attr("checked","checked");
		$("#student_xiugai").attr("checked","checked");
		$("#student_del").attr("checked","checked");
		$("#student_daochu").attr("checked","checked");
	}
	if(!obj.checked){
		$("#student_kejian").attr("checked",false);
		$("#student_add").attr("checked",false);
		$("#student_addList").attr("checked",false);
		$("#student_xiugai").attr("checked",false);
		$("#student_del").attr("checked",false);
		$("#student_daochu").attr("checked",false);
	}
}
function studentkejian(obj){
	if(obj.checked){
		$("#student").attr("checked","checked");
	}
	if(!obj.checked){
		$("#student").attr("checked",false);
	}
}
function studentadd(obj){
	if(obj.checked){
		$("#student").attr("checked","checked");
		$("#student_kejian").attr("checked","checked");
	}
}
function studentaddList(obj){
	if(obj.checked){
		$("#student").attr("checked","checked");
		$("#student_kejian").attr("checked","checked");
	}
}
function studentxiugai(obj){
	if(obj.checked){
		$("#student").attr("checked","checked");
		$("#student_kejian").attr("checked","checked");
	}
}
function studentdel(obj){
	if(obj.checked){
		$("#student").attr("checked","checked");
		$("#student_kejian").attr("checked","checked");
	}
}
function studentdaochu(obj){
	if(obj.checked){
		$("#student").attr("checked","checked");
		$("#student_kejian").attr("checked","checked");
	}
}
//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》角色管理
function selAll_role(obj){
	if(obj.checked){
		$("#role_kejian").attr("checked","checked");
		$("#role_add").attr("checked","checked");
		$("#role_xiugai").attr("checked","checked");
		$("#role_del").attr("checked","checked");
	}
	if(!obj.checked){
		$("#role_kejian").attr("checked",false);
		$("#role_add").attr("checked",false);
		$("#role_xiugai").attr("checked",false);
		$("#role_del").attr("checked",false);
	}
}
function rolekejian(obj){
	if(obj.checked){
		$("#role").attr("checked","checked");
	}
	if(!obj.checked){
		$("#role").attr("checked",false);
	}
}
function roleadd(obj){
	if(obj.checked){
		$("#role").attr("checked","checked");
		$("#role_kejian").attr("checked","checked");
	}
}
function rolexiugai(obj){
	if(obj.checked){
		$("#role").attr("checked","checked");
		$("#role_kejian").attr("checked","checked");
	}
}
function roledel(obj){
	if(obj.checked){
		$("#role").attr("checked","checked");
		$("#role_kejian").attr("checked","checked");
	}
}
//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》职工管理
function selAll_employee(obj){
	if(obj.checked){
		$("#employee_kejian").attr("checked","checked");
		$("#employee_add").attr("checked","checked");
		$("#employee_daoru").attr("checked","checked");
		$("#employee_xiugai").attr("checked","checked");
		$("#employee_del").attr("checked","checked");
		$("#employee_enable").attr("checked","checked");
		$("#employee_daochu").attr("checked","checked");
	}
	if(!obj.checked){
		$("#employee_kejian").attr("checked",false);
		$("#employee_add").attr("checked",false);
		$("#employee_daoru").attr("checked",false);
		$("#employee_xiugai").attr("checked",false);
		$("#employee_del").attr("checked",false);
		$("#employee_enable").attr("checked",false);
		$("#employee_daochu").attr("checked",false);
	}
}
function employeekejian(obj){
	if(obj.checked){
		$("#employee").attr("checked","checked");
	}
	if(!obj.checked){
		$("#employee").attr("checked",false);
	}
}
function employeeadd(obj){
	if(obj.checked){
		$("#employee").attr("checked","checked");
		$("#employee_kejian").attr("checked","checked");
	}
}
function employeedaoru(obj){
	if(obj.checked){
		$("#employee").attr("checked","checked");
		$("#employee_kejian").attr("checked","checked");
	}
}
function employeexiugai(obj){
	if(obj.checked){
		$("#employee").attr("checked","checked");
		$("#employee_kejian").attr("checked","checked");
	}
}
function employeedel(obj){
	if(obj.checked){
		$("#employee").attr("checked","checked");
		$("#employee_kejian").attr("checked","checked");
	}
}
function employeeenable(obj){
	if(obj.checked){
		$("#employee").attr("checked","checked");
		$("#employee_kejian").attr("checked","checked");
	}
}
function employeedaochu(obj){
	if(obj.checked){
		$("#employee").attr("checked","checked");
		$("#employee_kejian").attr("checked","checked");
	}
}
//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》APP端考勤
function selAll_appCheckIn(obj){
	if(obj.checked){
		$("#appCheckIn_kejian").attr("checked","checked");
		$("#appCheckIn_qingjia").attr("checked","checked");
		$("#appCheckIn_jieshou").attr("checked","checked");
		$("#appCheckIn_chuli").attr("checked","checked");
		$("#appCheckIn_chaxunlan").attr("checked","checked");
	}
	if(!obj.checked){
		$("#appCheckIn_kejian").attr("checked",false);
		$("#appCheckIn_qingjia").attr("checked",false);
		$("#appCheckIn_jieshou").attr("checked",false);
		$("#appCheckIn_chuli").attr("checked",false);
		$("#appCheckIn_chaxunlan").attr("checked",false);
	}
}
function appCheckInkejian(obj){
	if(obj.checked){
		$("#appCheckIn").attr("checked","checked");
	}
	if(!obj.checked){
		$("#appCheckIn").attr("checked",false);
	}
}
function appCheckInqingjia(obj){
	if(obj.checked){
		$("#appCheckIn").attr("checked","checked");
		$("#appCheckIn_kejian").attr("checked","checked");
	}
}
function appCheckInjieshou(obj){
	if(obj.checked){
		$("#appCheckIn").attr("checked","checked");
		$("#appCheckIn_kejian").attr("checked","checked");
	}
}
function appCheckInchuli(obj){
	if(obj.checked){
		$("#appCheckIn").attr("checked","checked");
		$("#appCheckIn_kejian").attr("checked","checked");
	}
}
function appCheckInchaxunlan(obj){
	if(obj.checked){
		$("#appCheckIn").attr("checked","checked");
		$("#appCheckIn_kejian").attr("checked","checked");
	}
}
//》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》》APP端 更多
function selAll_appGengduo(obj){
	if(obj.checked){
		$("#appGengduo_gonggao").attr("checked","checked");
		$("#appGengduo_huodong").attr("checked","checked");
		$("#appGengduo_shipu").attr("checked","checked");
	}
	if(!obj.checked){
		$("#appGengduo_gonggao").attr("checked",false);
		$("#appGengduo_huodong").attr("checked",false);
		$("#appGengduo_shipu").attr("checked",false);
	}
}
function appGengduogonggao(obj){
	if(obj.checked){
		$("#appGengduo").attr("checked","checked");
	}
}
function appGengduohuodong(obj){
	if(obj.checked){
		$("#appGengduo").attr("checked","checked");
	}
}
function appGengduoshipu(obj){
	if(obj.checked){
		$("#appGengduo").attr("checked","checked");
	}
}
/* function isNianji(){
	$('#showNinaJ').css('display','block');
}
function noNianji(){
	$('#showNinaJ').css('display','none');
} */	
</script>
</head>
<body class="easyui-layout">
<!----------------------------- 功能按钮 --------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:100%; margin-top:5px;margin-left:7px;text-align: left;height:35px;"> 
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()" id="addRole" style="display: none; float: left;" >添加角色</a>
	</div> 
</div>
		<table id="grid" class="easyui-datagrid" style=" width:100%;height:100%;display: none;" title="<img alt='后台用户管理' style='width:17px;height:17px;float:left' src='<%=basePath%>/img/titleImg.png'>
		<div style='height:20px;line-height:22px;margin-left:10px;margin-top:-4;float:left'>角色管理</div>"
			url="<%=basePath%>/role/selectrole.do" toolbar="#tb" nowrap="false"
			rownumbers="true" pageSize="20" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="roleName" width="16%" align="center">角色名称</th>
					<th field="validSchool" width="16%" align="center">有效园区</th>
					<th field="roleLevel" width="16%" align="center">角色级别</th>
					<th field="employeeName" width="16%" align="center">添加人</th>
					<th field="createDate" width="16%" align="center">添加时间</th>
					<th field="oper" width="17%" align="center" formatter="formatOper" align="center">操作</th>
				</tr>
			</thead>
		</table>
		
<!------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 800px; height: 580px; padding: 10px 5px;display: none;overflow-x: hidden; " closed="true"  buttons="#adddlg-buttons" >
	<form id="formAddNotice" name="formAddNotice" method="post">
		  <!-- 角色名称 -->
		  <span style="display:inline-block; width:80px;color:#000;font-size:15px;margin-left: 25px;margin-top: 5px;font-weight:bold;">基础信息：</span>
		   <div class="fitem" >
		   	<div style="height:20px">
			   <div style="float:left"><label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 5px; ">角色名称：</label></div>
			   <div style="float:left"><input name="roleName" id="roleName" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; type="text" /></div>
			   <div style="float:left" onclick="addType()"><span class="easyui-linkbutton" iconCls="icon-add"  style="width:25px; height:25px; margin-left:3px; font-size:20px"></span></div>
			</div>
		   </div>
		   <!-- 角色类型 -->
		   <div id="divRoleType">
			   <div id="divRoleTypeRowTemp" class="fitem" style="display:none">
			   	   <div style="height:20px">
				   		<div style="float:left"><label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 0px;">类型：</label></div>
				   		<div style="float:left"><input name="typeName"  id="typeName"  style=" width:181px;vertical-align:middle;font-size:15px;margin-top: 3px; border: 1px #95B8E7 solid;" type="text" /></div>
				   		<div style="float:left" onclick="delType(this)"><span class="easyui-linkbutton" iconCls="icon-remove"  style="width:25px; height:25px; margin-left:3px; font-size:20px"></span></div>
				    </div>
			   </div>
		   </div>
		   <span style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 25px;margin-top: 0px;font-weight:bold;">有效园区：</span>
			<!-- 角色名称 -->
		   <div class="fitem" id="divSchool" style="height:auto;width:100%;">

		   </div>
		   		
		<span style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 25px;margin-top: 0px;font-weight:bold">角色级别：</span>
 		
<div class="fitem" >
		<label id="zongyuan_label" class="tile" style="display:inline-block;width:35px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;display:none">总园</label>
	<input id="zongyuan" type="radio" name='rolelevel' value='总园' style="display:none" >
	<label class="tile" style="display:inline-block;width:35px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">分园</label>
	<input id="fenyuan" type="radio" name='rolelevel' value='分园' >
	<label class="tile" style="display:inline-block;width:35px;color:#000;font-size:15px;margin-left: 20px;margin-top: 10px;">年级</label>
	<input id="nianji" type="radio" name='rolelevel' value='年级' >
	<label class="tile" style="display:inline-block;width:35px;color:#000;font-size:15px;margin-left: 20px;margin-top: 10px;">班级</label>
	<input id="banji" type="radio" name='rolelevel' value='班级' >
 </div>
 
 
 
 <div><span style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 25px;margin-top: 5px;font-weight:bold">选择权限：</span></div>
 <div style="width:780px;height:300px;margin-left: 25px;margin-top: 10px;">
  <div style="width:700px;height:50px;border: 1px solid #999;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="gaiLan" name="gaiLan" value="1" onchange='selAll_gaiLan(this)'/>&nbsp;&nbsp;概览</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="gaiLan_kejian" name="gaiLan_kejian" value="1" onchange='sel_kejian(this)'/>&nbsp;&nbsp;可见该栏</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="gaiLan_xinxi" name="gaiLan_xinxi" value="1" onchange='sel_xinxi(this)'/>&nbsp;&nbsp;可见“信息概览”</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="gaiLan_guanyu" name="gaiLan_guanyu" value="1" onchange='sel_guanyu(this)'/>&nbsp;&nbsp;可编辑"关于我们"信息</div>
  		</div>
  </div>
 	  <div style="width:700px;height:50px;border: 1px solid #999;margin-top: 10px;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="notice" name="notice" value="1" onchange='selAll_notice(this)'/>&nbsp;&nbsp;通知公告</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="notice_kejian" name="notice_kejian" value="1" onchange='noticekejian(this)'/>&nbsp;&nbsp;可见该栏</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="notice_new" name="notice_new" value="1" onchange='noticenew(this)'/>&nbsp;&nbsp;新增发布</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="notice_caogao" name="notice_caogao" value="1" onchange='noticecaogao(this)'/>&nbsp;&nbsp;草稿箱</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="notice_chakan" name="notice_chakan" value="1" onchange='noticechakan(this)'/>&nbsp;&nbsp;查看</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="notice_chehui" name="notice_chehui" value="1" onchange='noticechehui(this)'/>&nbsp;&nbsp;撤回</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="notice_del" name="notice_del" value="1" onchange='noticedel(this)'/>&nbsp;&nbsp;删除</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="notice_daochu" name="notice_daochu" value="1" onchange='noticedaochu(this)'/>&nbsp;&nbsp;导出表格</div>
  		</div>
  </div>
  <div style="width:700px;height:50px;border: 1px solid #999;margin-top: 10px;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="activity" name="activity" value="1" onchange='selAll_activity(this)'/>&nbsp;&nbsp;精彩活动</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="activity_kejian" name="activity_kejian" value="1" onchange='activitykejian(this)'/>&nbsp;&nbsp;可见该栏</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="activity_new" name="activity_new" value="1" onchange='activitynew(this)'/>&nbsp;&nbsp;新增发布</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="activity_chakan" name="activity_chakan" value="1" onchange='activitychakan(this)'/>&nbsp;&nbsp;查看</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="activity_chehui" name="activity_chehui" value="1" onchange='activitychehui(this)'/>&nbsp;&nbsp;撤回</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="activity_del" name="activity_del" value="1" onchange='activitydel(this)'/>&nbsp;&nbsp;删除</div>
  		</div>
 </div>
 <div style="width:700px;height:50px;border: 1px solid #999;margin-top: 10px;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="recipe" name="recipe" value="1" onchange='selAll_recipe(this)'/>&nbsp;&nbsp;每周食谱</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="recipe_kejian" name="recipe_kejian" value="1" onchange='recipekejian(this)'/>&nbsp;&nbsp;可见该栏</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="recipe_new" name="recipe_new" value="1" onchange='recipenew(this)'/>&nbsp;&nbsp;新增发布</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="recipe_chakan" name="recipe_chakan"  value="1" onchange='recipechakan(this)'/>&nbsp;&nbsp;查看</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="recipe_chehui" name="recipe_chehui" value="1" onchange='recipechehui(this)'/>&nbsp;&nbsp;撤回</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="recipe_del" name="recipe_del" value="1" onchange='recipedel(this)'/>&nbsp;&nbsp;删除</div>
  		</div>
  </div>
 <div style="width:700px;height:50px;border: 1px solid #999;margin-top: 10px;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="mailbox" name="mailbox" value="1" onchange='selAll_mailbox(this)'/>&nbsp;&nbsp;园长信箱</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="mailbox_kejian" name="mailbox_kejian"  value="1" onchange='mailboxkejian(this)'/>&nbsp;&nbsp;可见该栏</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="mailbox_chakan" name="mailbox_chakan" value="1" onchange='mailboxchakan(this)'/>&nbsp;&nbsp;查看详情</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="mailbox_del" name="mailbox_del" value="1" onchange='mailboxdel(this)'/>&nbsp;&nbsp;删除</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox"  id="mailbox_daochu" name="mailbox_daochu" value="1" onchange='mailboxdaochu(this)'/>&nbsp;&nbsp;导出表格</div>
  		</div>
  </div>
  <div style="width:700px;height:50px;border: 1px solid #999;margin-top: 10px;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="park" name="park" value="1" onchange='selAll_park(this)'/>&nbsp;&nbsp;园区管理</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="park_kejian" name="park_kejian" value="1" onchange='parkkejian(this)'/>&nbsp;&nbsp;可见该栏</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="park_add" name="park_add" value="1" onchange='parkadd(this)'/>&nbsp;&nbsp;添加园区</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="park_xiugai" name="park_xiugai" value="1" onchange='parkxiugai(this)'/>&nbsp;&nbsp;修改园区</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="park_del" name="park_del" value="1" onchange='parkdel(this)'/>&nbsp;&nbsp;删除园区</div>
  		</div>
  </div>
 	<div style="width:700px;height:50px;border: 1px solid #999;margin-top: 10px;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="classes" name="classes" value="1" onchange='selAll_classes(this)'/>&nbsp;&nbsp;班级管理</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="classes_kejian" name="classes_kejian" value="1" onchange='classeskejian(this)'/>&nbsp;&nbsp;可见该栏</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="classes_add" name="classes_add" value="1" onchange='classesadd(this)'/>&nbsp;&nbsp;单条添加</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="classes_addList" name="classes_addList" value="1" onchange='classesaddList(this)'/>&nbsp;&nbsp;批量导入</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="classes_xiugai" name="classes_xiugai" value="1" onchange='classesxiugai(this)'/>&nbsp;&nbsp;修改班级</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="classes_del" name="classes_del" value="1" onchange='classesdel(this)'/>&nbsp;&nbsp;删除班级</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="classes_up" name="classes_up" value="1" onchange='classesup(this)'/>&nbsp;&nbsp;升班</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="classes_zhuxiao" name="classes_zhuxiao" value="1" onchange='classeszhuxiao(this)'/>&nbsp;&nbsp;注销</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="classes_lishi" name="classes_lishi" value="1" onchange='classeslishi(this)'/>&nbsp;&nbsp;历史班级</div>
  		</div>
  </div>
  <div style="width:700px;height:50px;border: 1px solid #999;margin-top: 10px;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="student" name="student" value="1" onchange='selAll_student(this)'/>&nbsp;&nbsp;学生管理</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="student_kejian" name="student_kejian" value="1" onchange='studentkejian(this)'/>&nbsp;&nbsp;可见该栏</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="student_add" name="student_add" value="1" onchange='studentadd(this)'/>&nbsp;&nbsp;单条添加</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="student_addList" name="student_addList" value="1" onchange='studentaddList(this)'/>&nbsp;&nbsp;批量导入</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="student_xiugai" name="student_xiugai" value="1" onchange='studentxiugai(this)'/>&nbsp;&nbsp;修改学生</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="student_del" name="student_del" value="1" onchange='studentdel(this)'/>&nbsp;&nbsp;删除学生</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="student_daochu" name="student_daochu" value="1" onchange='studentdaochu(this)'/>&nbsp;&nbsp;导出表格</div>
  		</div>
  </div>
  <div  id="roleZongY" style="width:700px;height:50px;border: 1px solid #999;margin-top: 10px;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="role" name="role" value="1" onchange='selAll_role(this)'/>&nbsp;&nbsp;角色管理</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="role_kejian" name="role_kejian" value="1" onchange='rolekejian(this)'/>&nbsp;&nbsp;可见该栏</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="role_add" name="role_add" value="1" onchange='roleadd(this)'/>&nbsp;&nbsp;添加角色</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="role_xiugai" name="role_xiugai" value="1" onchange='rolexiugai(this)'/>&nbsp;&nbsp;修改角色</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="role_del" name="role_del" value="1" onchange='roledel(this)'/>&nbsp;&nbsp;删除角色</div>
  		</div>
  </div>
  <div style="width:700px;height:50px;border: 1px solid #999;margin-top: 10px;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="employee" name="employee" value="1" onchange='selAll_employee(this)'/>&nbsp;&nbsp;职工管理</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="employee_kejian" name="employee_kejian"  value="1" onchange='employeekejian(this)'/>&nbsp;&nbsp;可见该栏</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="employee_add" name="employee_add" value="1" onchange='employeeadd(this)'/>&nbsp;&nbsp;单条添加</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox"  id="employee_daoru" name="employee_daoru" value="1" onchange='employeedaoru(this)'/>&nbsp;&nbsp;批量导入</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="employee_xiugai" name="employee_xiugai" value="1" onchange='employeexiugai(this)'/>&nbsp;&nbsp;修改职工</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="employee_del" name="employee_del" value="1" onchange='employeedel(this)'/>&nbsp;&nbsp;删除职工</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="employee_enable" name="employee_enable" value="1" onchange='employeeenable(this)'/>&nbsp;&nbsp;禁用/启用职工</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="employee_daochu" name="employee_daochu" value="1" onchange='employeedaochu(this)'/>&nbsp;&nbsp;导出表格</div>
  		</div>
  </div>
  <div style="width:700px;height:50px;border: 1px solid #999;margin-top: 10px;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="appCheckIn" name="appCheckIn" value="1" onchange='selAll_appCheckIn(this)'/>&nbsp;&nbsp;APP端-考勤</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="appCheckIn_kejian" name="appCheckIn_kejian" value="1" onchange='appCheckInkejian(this)'/>&nbsp;&nbsp;每日考勤栏可见</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="appCheckIn_qingjia" name="appCheckIn_qingjia" value="1" onchange='appCheckInqingjia(this)'/>&nbsp;&nbsp;请假申请栏可见</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="appCheckIn_jieshou" name="appCheckIn_jieshou" value="1" onchange='appCheckInjieshou(this)'/>&nbsp;&nbsp;接收请假申请消息</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="appCheckIn_chuli" name="appCheckIn_chuli" value="1" onchange='appCheckInchuli(this)'/>&nbsp;&nbsp;处理请假申请</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="appCheckIn_chaxunlan" name="appCheckIn_chaxunlan" value="1" onchange='appCheckInchaxunlan(this)'/>&nbsp;&nbsp;统计查询栏可见</div>
  		</div>
  </div>
  <div style="width:700px;height:50px;border: 1px solid #999;margin-top: 10px;">
  		<div style="width:100%;height:24px;margin-left: 20px;margin-top: 5px;"><input type="checkbox" id="appGengduo" name="appGengduo" value="1" onchange='selAll_appGengduo(this)'/>&nbsp;&nbsp;APP端-更多</div>
  		<div style="width:100%;height:24px;margin-left: 20px;">
  			<div style="float:left;"><input type="checkbox" id="appGengduo_gonggao" name="appGengduo_gonggao" value="1" onchange='appGengduogonggao(this)'/>&nbsp;&nbsp;查看通知公告</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="appGengduo_huodong" name="appGengduo_huodong" value="1" onchange='appGengduohuodong(this)'/>&nbsp;&nbsp;查看精彩活动</div>
  			<div style="float:left;margin-left: 20px;"><input type="checkbox" id="appGengduo_shipu" name="appGengduo_shipu" value="1" onchange='appGengduoshipu(this)'/>&nbsp;&nbsp;查看每周食谱</div>
  		</div>
  </div>
				  </div>
				  
				  <!-- 占位 -->
		  		<div style="width: 30px;height: 50px";></div>
		  	
		<!-- 	<select id="customCombobox">  </select> -->
		   	<!-- <div class="fitem">
			   <label>正文：</label>
			   <textarea name="text" id="text" class="text" placeholder="多行输入"></textarea>
		   </div> -->
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="roleId" id="roleId" value=""/>
		   <input type="hidden" name="userid" id="userid" value="">
		   <input type="hidden" name="ssss" id="ssss" value="">
		   <input type="hidden" name="schoolVar" id="schoolVar" value="">
		   <input type="hidden" name="typeNam" id="typeNam" value="">
		   <input type="hidden" name="subRoleIdd" id="subRoleIdd" value="">
		   <input type="hidden" name="subRoleId" id="subRoleId" value="">
		   
	 </form>
</div>

<!------------------------------ 确认取消按钮 ----------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddNotice()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>
<script type="text/javascript">

</script>
<style>
.datagrid-btable .datagrid-cell{padding:6px 4px;overflow: hidden;text-overflow:ellipsis;white-space: nowrap;}  
	    formatter: function(value,row,index){  
       return '<span title='+value+'>'+value+'</span>'  
    }  
</style>
</html>
