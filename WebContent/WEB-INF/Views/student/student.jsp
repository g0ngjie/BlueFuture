<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/css/system/notice.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script>
$(document).ready(function(){


	<% int sum = 0; %>
	
	<c:if test="${student_add == 'student_add'}">
	
		document.getElementById("addStudent").style.display="block";
		
		<% sum ++; %>;
		
	</c:if>
	<c:if test="${student_addList == 'student_addList'}">
	
		<% sum ++; %>;
		
	</c:if>
		<c:if test="${student_daochu == 'student_daochu'}">
	
		<% sum ++; %>;
		
	</c:if>
	<% if(sum == 0) { %>
		
		$("#grid").datagrid({
			toolbar:''
		});
		
	<% } %>



var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
     
});

});

/* 操作 */
function formatOper(value,row,index){

		<%int i = 0;%>
    	var str = "";
		<c:if test="${ student_xiugai == 'student_xiugai'}">
			str += "<a href='javascript:openEdit(\"" + row['studentId'] + "\");'>修改</a>&nbsp;";
			<%i ++;%>
		</c:if>
		<c:if test="${ student_del == 'student_del'}">
			<%if (i != 0) {%>
				str += " | &nbsp;"
			<%}%>
			str += "<a href='javascript:openDel(\"" + row['studentId'] + "\");'>删除</a>&nbsp;";
		</c:if>
		if(str == ""){
	    		str = "<a style='color:#666;'>无此权限</a>";
	    }
		return str;


} 
/* 添加 保存 角色 */
function saveAddNotice()
{	

		
		var schoolId = $('#schoolId').val();
		if(schoolId == "" || schoolId == null){
				layer.msg("请选择园区")
				return null;
			}
		//届数  判定有问题	
		var intoYear = $('#intoYear').combobox('getValue');
		if(intoYear == "" || intoYear == null){
				layer.msg("请选择届数")
				return null;
			}
				
		var grade = $('#gradeName').combobox('getValue');
		if(grade == "" || grade == null){
				layer.msg("请选择年级")
				return null;
			}
			
		var classesId = $('#classesId').val();
		if(classesId == "" || classesId == null){
				layer.msg("请选择班级")
				return null;
			}	
		
		var studentCode = $('#studentCode').val();
		if(studentCode.length != 5){
				layer.msg("请输入5位数字格式的编号");
				return null;
			}
			
		var studentName = $('#studentName').val();
		if(studentName.length > 10){
			layer.msg("请输入真实姓名")
			return null;
		}
		if(studentName == "" || studentName == null){
				layer.msg("请输入学生姓名")
				return null;
			}	
			
		var mode = $('#mode').val();
		if(mode=='add'){
			var parentName = $('#textParentName').val();
			
			if(parentName == "" || parentName == null){
				layer.msg("请输入家长姓名")
				return null;
			}
		}else if(mode=='edit'){
			var parentName1 = $('#textParentName').combobox('getText');
				
			if(parentName1 == "" || parentName1 == null){
					layer.msg("请输入家长姓名")
					return null;
				}
		}
		var sex = $('#sex').combobox('getValue');
		if(sex == ""){
				layer.msg("请选择性别")
				return null;
			} 	
			

		var relation = $('#relation').combobox('getValue');
		if(relation == "" || relation == null){
				layer.msg("请选择亲戚关系")
				return null;
			}


		var tel = $('#tel').val();
		if(tel == "" || tel == null){
				layer.msg("请输入联系方式")
				return null;
			} 
		
			
		$('#formAddNotice').form('submit', {
			url : '<%=basePath%>/student/saveStudent.do',
			onSubmit : function() {
				return $(this).form('validate');
				var index = layer.load(1, {
					  shade: [0.1,'#fff'] //0.1透明度的白色背景
				})
				
			},
			success : function(result) {
				data = eval("(" + result + ")");
				//alert(data.msg)
				if(data.code == 100){
					layer.closeAll('loading');
					$('#adddlg').dialog('close');
					$('#grid').datagrid('reload');
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">保存成功</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
					});
				}
				else
				{
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">'+ data.msg +'</div>',
						timeout: 500,  
						showType:'fade',
						style:{right:'', bottom:''}
						});
				}
			}
		}); 
	
}
/* 编辑角色 */
function openEdit(studentId){

	
	$('#adddlg').dialog({height:550});
	$('#inputParent').css('display','block');
	$('#comboboxParent').css('display','none');
	$('#inputParent').css('display','block');
	$('#sexDv').css('display','block');
	$('#relationDv').css('display','block');
	$('#telDv').css('display','block');
	
	/* $('#textParentName').textbox({editable:false});
	$('#sex').combobox({readonly:true});
	$('#relation').textbox({editable:false});
	$('#tel').textbox({editable:false}); */
	  
	  
	$('#parentName').combobox({    
	    url:'<%=basePath%>/student/listParents.do',    
	       queryParams: {
				"studentId" : studentId, 
			}
	       
	}); 
//	alert(studentId);
	$.post("<%=basePath%>/student/studentEdit.do",{studentId:studentId},function(result){
	
	data = eval("(" + result + ")");
   // alert(result)
    	document.getElementById("studentId").value=data.studentId;
    	document.getElementById("classesId").value=data.classesId;
    	document.getElementById("parentId").value=data.parentId;
    	document.getElementById("relationId").value=data.relationId;
    	
  	//	$('#studentId').val(data.studentId);
  		$('#studentName').val(data.studentName);
		$('#schoolName').combobox('setValue',data.schoolName);
		$('#gradeName').combobox('setValue',data.grade);
		$('#grade').val(data.gra);
		
		$('#studentCode').textbox('setValue',data.studentCode);
		
		$('#classesName').combobox('setValue',data.classesName);
		
		//alert(data.sex)
		$('#sex').combobox('setValue',data.sex);
		
		$('#schoolId').val(data.schoolId);
	//	alert(data.schoolId);
		$('#mode').val("edit");
		$('#adddlg').panel({title: "学生修改"});
		$('#adddlg').dialog('open');
		$("#tel").textbox('setValue',data.phone);
	//	document.getElementById("tel").value=data.phone;
		
		$("#intoYear").combobox('setValue',data.intoYear);
		$("#studentName").textbox('setValue',data.studentName);
		$("#textParentName").textbox('setValue',data.parentName);
		$("#relation").combobox('setValue',data.relation);
		
		$('#yaoqing1').empty();
		$('#yaoDv').css('display','block')
		for(var j = 0; j < data.parentList.length; j++){
			
		//	$('#yaoqing').html(data.parentList[0].mobl);
			var row =$('#yaoqing').clone();//克隆
			row.show();
			$('#yaoqing1').append(row);
			row.html(data.parentList[j].paName+'      '+data.parentList[j].relat+'      '+data.parentList[j].mobl);
		}
		
		
	});
		
}

/* 单删 */
function openDel(studentId)
{	$.messager.confirm('确认','确认删除?',function(row){

	if(row){
	$.post("<%=basePath%>/student/delStudent.do",{delKeys:studentId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
}
})};

/* 添加按钮 */
function add()
{		
		
		$('#yaoDv').css('display','none')
		$('#yaoqing1').empty();
		$('#adddlg').dialog({height:500});
		$('#inputParent').css('display','block'); 
		$('#comboboxParent').css('display','none');
		$('#sexDv').css('display','block');
		$('#relationDv').css('display','block');
		$('#telDv').css('display','block');
		
		$('#textParentName').textbox({editable:true});
		$('#sex').combobox({readonly:false});
		$('#relation').textbox({editable:true});
		$('#tel').textbox({editable:true});
		
		$.post("<%=basePath%>/student/sysUserEmobCode.do",function(result){
		data = eval("(" + result + ")");
		
		 $('#emobCode').val(data.emobCode);
		// alert(data.emobCode);
		});
		
		$('#schoolName').combobox('setValue','');
		
		$('#tel').textbox('setValue','');
		
		$('#sex').combobox('setValue','');
		
		$('#intoYear').combobox('setValue','');
		
		$('#gradeName').combobox('setValue','');
	
		$('#classesName').combobox('setValue','');
		$('#studentName').textbox('setValue','');
		$('#textParentName').textbox('setValue','');
		$('#relation').combobox('setValue','');
		
		(function(){
			var Num="";
			for(var i=0;i<5;i++)
			{
			Num+=Math.floor(Math.random()*10);
			}
			$('#studentCode').textbox('setValue',Num);
		})();	
				
    $('#mode').val("add");
    $('#adddlg').panel({title: "学生添加"});
	$('#adddlg').dialog('open');
}

/* 导出 */
function daochu()
{		
		var schoolName = $('#schoolNam').combobox('getValue');
		var intoYear = $('#intoYear1').combobox('getValue');
		var gradeName = $('#gradeName1').combobox('getValue');
		var classesName = $('#classesName1').combobox('getValue');		
		
		window.parent.location="<%=basePath%>/student/daochuStudent.do?schoolName="+schoolName+"&intoYear="+intoYear+"&gradeName="+gradeName+"&classesName="+classesName;
		
		
}

/* 批量导入视频 */
function listImport(){
	$('#file').val('');
	$('#addReportWord').panel({title: "表格批量导入"});
	$('#addReportWord').dialog('open');
	
}
/* 视频批量导入 */
function saveExcel(){
	
	
	var file = $('#file').val();
	var fileArr=file.split("//");
		var fileTArr=fileArr[fileArr.length-1].toLowerCase().split(".");
		var filetype=fileTArr[fileTArr.length-1];
		if(filetype!="xls"&&filetype!="xlsx"){
			$('#file').focus();
			layer.msg("上传文件必须为xls、xlsx文件！");
			return ;
		}
	if(file == ''){
		layer.msg('请选择文件');
		return ;
	}
	var index = layer.load(1, {
			 	 shade: [0.1,'#fff'] 
	});
	$('#readReportForm').form('submit', {
		url : '<%=basePath%>/student/saveExcel.do',
		onSubmit : function() {
			return $(this).form('validate');
		},
		success : function(result) {
			layer.closeAll('loading');
			if(result == null || "" == result){
				$('#addReportWord').dialog('close')
				$('#grid').datagrid('reload');
				$.messager.show({
						title:'提示',
						msg:'<div class="msgs">导入成功</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
					});
			}else{
				alert(result);
			}
		}
	}); 
	
}

//搜索
function sousuo()
{
	var SsName = $('#SsName').val();
	$('#grid').datagrid('load', {SsName:SsName});
	
}

//下拉框搜索
function schoolNameLi(){

	var schoolName = $('#schoolNam').combobox('getValue');
		
	$('#grid').datagrid('load', {schoolName:schoolName});
	
};		
function schoolIntoyear(){

	var schoolName = $('#schoolNam').combobox('getValue');
	var intoYear = $('#intoYear1').combobox('getValue');
		
	$('#grid').datagrid('load', {schoolName:schoolName,intoYear:intoYear});
	
};

function schoolIntoyearGrade(){

	var schoolName = $('#schoolNam').combobox('getValue');
	var intoYear = $('#intoYear1').combobox('getValue');
	var gradeName = $('#gradeName1').combobox('getValue');
	
		
	$('#grid').datagrid('load', {schoolName:schoolName,intoYear:intoYear,gradeName:gradeName});
	
};
function schoolIntoyearGradeClassName(){

	var schoolName = $('#schoolNam').combobox('getValue');
	var intoYear = $('#intoYear1').combobox('getValue');
	var gradeName = $('#gradeName1').combobox('getValue');
	var classesName = $('#classesName1').combobox('getValue');
		
	$('#grid').datagrid('load', {schoolName:schoolName,intoYear:intoYear,gradeName:gradeName,classesName:classesName});
	
};

//扩展easyui表单的验证  
$.extend($.fn.validatebox.defaults.rules, {  
    //验证汉子  
    CHS: {  
        validator: function (value) {  
            return /^[\u0391-\uFFE5]+$/.test(value);  
        },  
        message: '只能输入汉字'  
    },  
    //移动手机号码验证  
    mobile: {//value值为文本框中的值  
        validator: function (value) {  
            var reg = /^1[3|4|5|8|9]\d{9}$/;  
            return reg.test(value);  
        },  
        message: '输入手机号码格式不准确.'  
    },  
    //国内邮编验证  
    zipcode: {  
        validator: function (value) {  
            var reg = /^[1-9]\d{5}$/;  
            return reg.test(value);  
        },  
        message: '邮编必须是非0开始的6位数字.'  
    },  
    //用户账号验证(只能包括 _ 数字 字母)   
    account: {//param的值为[]中值  
        validator: function (value, param) {  
            if (value.length < param[0] || value.length > param[1]) {  
                $.fn.validatebox.defaults.rules.account.message = '用户名长度必须在' + param[0] + '至' + param[1] + '范围';  
                return false;  
            } else {  
                if (!/^[\w]+$/.test(value)) {  
                    $.fn.validatebox.defaults.rules.account.message = '用户名只能数字、字母、下划线组成.';  
                    return false;  
                } else {  
                    return true;  
                }  
            }  
        }, message: ''  
    }  
})	
function downLoad(){
	window.parent.location="<%=basePath%>/file/download.do?id="+"student";
	<%-- $.post("<%=basePath%>/file/download.do",function(result){
	
	data = eval("(" + result + ")");
	}); --%>
	
}

/* 删除按钮 */
function openshanchu()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		var str = "";
		var rows  = $('#grid').datagrid("getRows");
		var ckArr = $("input[name='ck']");
		for(var i=0; i<ckArr.length; i++)
		{
			if(ckArr[i].checked)
			{
				if(str != "")
				{
					str += ",";
				}
				str += rows[i].studentId;
			}
		}
	
	$.messager.confirm('确认','确认删除?',function(row){

	if(row){
		$.post("<%=basePath%>/student/delStudent.do",{delKeys:str},function(result){
			layer.closeAll('loading');
			data = eval("(" + result + ")");
			if(data.code == 100){
				$('#grid').datagrid('reload');
				$.messager.show({
								title:'提示',
								msg:'删除成功',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
			}
		}); 
		}
	});
	}else{
		$.messager.show({
							title:'提示',
							msg:'请选中您要删除的数据',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

</script>
</head>
<body class="easyui-layout">
<!----------------------------- 功能按钮 --------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style=" margin-bottom:3px;text-align: left;height:35px;width: 100%; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;"> 
				<div style="width:80px; margin-top:5px;text-align: left;height:35px;float: left; margin-left: 7px;"> 
		       		 <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()" id="addStudent" style="display: none; float: left;">单条添加</a>
		        </div>
		        <div style="width:80px;  margin-top:5px;text-align: left;height:35px;float: left; "> 																			<!--原来的删除 -->
		      		  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="openshanchu()" >删除</a>
		      	</div>	
		    	<div style="width:80px;  margin-top:5px;text-align: left;height:35px;float: left;margin-left: -25px; "> 																			<!--原来的删除 -->
		      		  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-redo" plain="true" onclick="listImport()" >批量导入</a>
		      	</div>	
       
			
			
			<!--------------------------------- 搜索框 ----------------------------------->
			 <div style="margin-left:10px;background-color: #f4f4f4;height:20px;height: 35px;width: 200px;float: left;">
				   <div style="float: left;height: 30px;line-height: 30px;">
			   		<input name="SsName" id="SsName" placeholder="&nbsp;&nbsp;输入学生姓名" style="margin-top: 10px;border:1px #C5C5C5 solid;width: 100px;height: 23px"/>
				   </div>
				     <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 10px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 70px;height:23px;margin-top: 10px;border-radius: 5px;"
				     class="easyui-linkbutton" iconCls="icon-search">搜索</div>	   
			 </div>
	
		<!-- 搜索 园区列表 -->
		  		 	<label style="float:left; display:inline-block;width:70px; color:#000;font-size:12px;margin-top:10px; margin-left: 0px;">选择园区名</label>
		  		 	<div style="display:inline-block;width:120px;color:#000;font-size:15px;margin-top: 10px; float: left;" class="fitem">
							<input  id="schoolNam" name="schoolNam" class="easyui-combobox" 
								style="width:110px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; data-options="
							valueField:'schoolId',
							textField:'schoolName',
							url:'<%=basePath%>/student/listSchool.do',
							onSelect: function(rec){
							$('#intoYear1').combobox('setValue','');
							$('#gradeName1').combobox('setValue','');
							$('#classesName1').combobox('setValue','');
							
							$('#schoolNam').val(rec.schoolName);
							schoolNameLi();
							var url='<%=basePath%>/student/listIntoYear?schoolId='+rec.schoolId;
							$('#intoYear1').combobox('reload',url);
							}" />
					</div>
					
					<!-- 搜索 届数列表 -->
					<label style="float:left; display:inline-block;width:60px; color:#000;font-size:12px;margin-top:11px; ">选择届数</label>
		  		 	<div style="display:inline-block;width:80px;color:#000;font-size:15px;margin-top: 10px; float: left;" class="fitem">
							<input  id="intoYear1" name="intoYear1" class="easyui-combobox" 
								style="width:70px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; data-options="
							valueField: 'yearId',
							textField: 'intoYear',
							<%-- url:'<%=basePath%>/student/listIntoYear.do', --%>
							onSelect: function(rec){
							$('#gradeName1').combobox('setValue','');
							$('#classesName1').combobox('setValue','');
							$('#intoYear1').val(rec.intoYear);
												<%-- //?schoolName="+schoolName+"&intoYear="+intoYear+"&gradeName="+gradeName+"&classesName="+classesName; --%>
							schoolIntoyear();
							var url='<%=basePath%>/student/listGrade.do?intoYear='+ rec.intoYear +'&schoolId='+ rec.schoolId;
							$('#gradeName1').combobox('reload',url);
							}" />
					</div>
					
					<!-- 搜索 年级列表 -->
					<label style="float:left; display:inline-block;width:60px; color:#000;font-size:12px;margin-top:11px; ">选择年级</label>
		  		 	<div style="display:inline-block;width:80px;color:#000;font-size:15px; margin-top: 10px; float: left;" class="fitem">
							<input  id="gradeName1" name="gradeName1" class="easyui-combobox" 
								style="width:70px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; data-options="
							valueField:'clasId',
							textField:'gradeName',
							onSelect: function(rec){
							$('#classesName1').combobox('setValue','');
							$('#gradeName1').val(rec.gradeName);
							
							schoolIntoyearGrade();
							var url='<%=basePath%>/student/listClassName?grade='+rec.gradeName +'&schoolId='+ rec.schoolId +'&intoYear='+ rec.intoYear;
							$('#classesName1').combobox('reload',url);
							}" />
					</div>
					
					<!-- 搜索  班级列表 -->
					<label style="float:left; display:inline-block;width:60px; color:#000;font-size:12px;margin-top:11px;  ">选择班级</label>
		  		 	<div style="display:inline-block;width:80px;color:#000;font-size:15px; margin-top: 10px; float: left;" class="fitem">
							<input  id="classesName1" name="classesName1" class="easyui-combobox" 
								style="width:70px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; data-options="
							valueField:'classesId',
							textField:'classesName',
							<%-- url:'<%=basePath%>/student/selectClass.do', --%>
							onSelect: function(rec){
							schoolIntoyearGradeClassName();
							$('#classesName1').val(rec.classesName);
							
							}" />
					</div>

					 <div style="width:100px; margin-top:8px; text-align: left;height:35px;float: left;"> 
	    	 			   <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-redo" plain="true" onclick="daochu()" >导出表格</a>
					 </div> 
	 </div>
</div>


		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后台用户管理' style='width:17px;height:17px;float:left' src='<%=basePath%>/img/titleImg.png'>
		<div style='height:20px;line-height:22px;margin-left:10px;margin-top:-4;float:left'>学生管理</div>"
			url="<%=basePath%>/student/selectstudent.do" toolbar="#tb"
			rownumbers="true" pageSize="20" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="schoolName" width="10%" align="center">园区名</th>
					<th field="intoyear" width="10%" align="center">届数</th>
					<th field="grade" width="11%" align="center">年级</th>
					<th field="className" width="11%" align="center">班级</th>
					<th field="studentName" width="11%" align="center">学生姓名</th>
					<th field="parentName" width="11%" align="center">家长姓名</th>
					<th field="relation" width="11%" align="center">亲戚关系</th>
					<th field="mobile" width="11%" align="center">联系电话</th>
					<th field="oper" width="13%" align="center" formatter="formatOper">操作</th>
				</tr>
			</thead>
		</table>
		
<!------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 420px; height: 500px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" >
	<form id="formAddNotice" name="formAddNotice" method="post">
			
		<!-- 园区名 添加 -->	
				<div class="fitem">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="employee">园区名</label>
				<input id="schoolName" name="schoolName" class="easyui-combobox"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; data-options="
				editable:false,	
				valueField:'schoolId',
				textField:'schoolName',
				url:'<%=basePath%>/system/listSchool.do',
				onSelect: function(rec){
				$('#intoYear').combobox('setValue','');
				$('#gradeName').combobox('setValue','');
				$('#classesName').combobox('setValue','');
				$('#schoolName').val(rec.schoolName);
				$('#schoolId').val(rec.schoolId);
				var url='<%=basePath%>/student/listIntoYear?schoolId='+rec.schoolId;
				$('#intoYear').combobox('reload',url);
				}" />
				</div>
	
				<div class="fitem">
				<label
					style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="province">届数</label>
				<input id="intoYear" name="intoYear" class="easyui-combobox"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"
					; data-options=" 
				editable:false,	
				valueField: 'yearId',
				textField: 'intoYear',
				<%-- url:'<%=basePath%>/student/listIntoYear.do', --%>
				onSelect: function(rec){
				$('#gradeName').combobox('setValue','');
				$('#classesName').combobox('setValue','');
				$('#intoYear').val(rec.intoYear);
				$('#intoYearName').val(rec.intoYear);
				
				var url='<%=basePath%>/student/listGrade.do?intoYear='+ rec.intoYear +'&schoolId='+ rec.schoolId;
				$('#gradeName').combobox('reload',url);
				}" />
			</div>
			
			
			<div class="fitem">
				<label
					style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="city">年级</label>
				<input id="gradeName" name="gradeName" class="easyui-combobox"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"
					; data-options="
				editable:false,	
				valueField:'clasId',
				textField:'gradeName',
				onSelect: function(rec){
				$('#classesName').combobox('setValue','');
				$('#gradeName').val(rec.gradeName);
				$('#grade').val(rec.clasId);
				var url='<%=basePath%>/student/listClassName?grade='+rec.gradeName +'&schoolId='+ rec.schoolId +'&intoYear='+ rec.intoYear;
				$('#classesName').combobox('reload',url);
				}" />
			</div>
			
			<!-- 班级 添加 -->	
			<div class="fitem">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="employee">班级</label>
				<input id="classesName" name="classesName" class="easyui-combobox"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px" data-options="
				editable:false,	
				valueField:'classesId',
				textField:'classesName',
				<%-- url:'<%=basePath%>/student/selectClass.do', --%>
				onSelect: function(rec){
				$('#classesName').val(rec.classesName);
				$('#classesId').val(rec.classesId);
				}" />
			</div>
			
			<!-- 学生编号  输入  -->
		   <div class="fitem" >
			   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">学生编号</label>
			   <input name="studentCode" id="studentCode" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; type="text" />
		   </div>
		   
			<!-- 姓名  输入  -->
		   <div class="fitem" >
			   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">姓名</label>
			   <input name="studentName" id="studentName" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; type="text" />
		   </div>
		   
		   <!-- 家长姓名  输入  -->
		   <!-- 班级 添加 -->	
			<div class="fitem" style="display:none;" id="comboboxParent">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="employee">家长姓名</label>
				<input id="parentName" name="parentName" class="easyui-combobox"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; data-options="
					valueField:'parentId',
					textField:'parentName',
					url:'<%=basePath%>/student/listParents.do',
					onSelect: function(rec){
						$('#sex').combobox('setValue',rec.parentSex);
						$('#relation').textbox('setValue',rec.relativeShip);
						$('#tel').textbox('setValue',rec.mobile);
					}" />
			</div>
		   <div class="fitem" style="display:none;" id="inputParent" >
			   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">家长姓名</label>
			   <input name="textParentName" id="textParentName" class="easyui-textbox" style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; type="text" />
		   </div>
		   
		   <!-- 家长性别 -->
		    <div class="fitem" id="sexDv">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="employee">性别</label>
					<select id="sex" name="sex" class="easyui-combobox" data-options="editable:false"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px">   
						<option></option>
					    <option value="1">男</option>   
					    <option value="0">女</option>
					</select>  
			</div>
		   
		   <!-- 亲戚关系  输入  -->
		   <div class="fitem" id="relationDv">
			   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">亲戚关系</label>
			   <!-- <input name="relation" id="relation" class="easyui-combobox"  style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; type="text" /> -->
		  	   <select id="relation" name="relation" class="easyui-combobox" data-options="editable:false"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px">   
						<option></option>
					    <option value="妈妈">妈妈</option>
					    <option value="爸爸">爸爸</option>
					    <option value="爷爷">爷爷</option>
					    <option value="奶奶">奶奶</option>
					    <option value="外公">外公</option>
					    <option value="外婆">外婆</option>   
					    <option value="其他">其他</option>
			   </select>
		   </div>
		   
		   <!-- 联系电话  输入  -->
		   <div class="fitem" id="telDv">
			   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; ">联系电话</label>
			   <input name="tel" id="tel" validtype="mobile" class="easyui-textbox" style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px "; type="text" />
		   </div>
		   
		   <!-- 被邀请 家长  -->
		   <div class="fitem" id="yaoDv">
			   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 0px;margin-top: 10px;float: left; ">被邀请家人:</label>
		  	   <div id="yaoqing" style="width:160px;vertical-align:middle;font-size:15px;color:#000;margin-left:5px; margin-top: 10px; float: left; font-size: 13px;"></div>
		   </div>
		   	   <div id="yaoqing1" style="width:160px;vertical-align:middle;font-size:15px;color:#000;margin-left:5px; margin-top: 0px; float: left; font-size: 13px;"></div>
		   <!-- <div class="fitem">
			   <label>用户：</label>
			   <input id="user" name="user" class="easyui-combobox" placeholder="请选择" 
			  
	            style="vertical-align:middle;font-size:15px;color:#666;margin-top: -6px;wsidth: 270px;" type="text" />
		   	</div>	 -->
		<!-- 	<select id="customCombobox">  </select> -->
		   	<!-- <div class="fitem">
			   <label>正文：</label>
			   <textarea name="text" id="text" class="text" placeholder="多行输入"></textarea>
		   </div> -->
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="studentId" id="studentId" value=""/>
		   <input type="hidden" name="userid" id="userid" value="">
			<!-- intoyear 届数 -->		   
		   <input type="hidden" name="intoYearName" id="intoYearName" value="">
		   <!-- 年级"grade" -->
		   <input type="hidden" name="grade" id="grade" value="">
		   <!-- 班级名 -->
		   <input type="hidden" name="classesId" id="classesId" value="">
		   <!-- 家长 Id -->
		   <input type="hidden" name="parentId" id="parentId" value="">
		   <!-- 关系 Id -->
		    <input type="hidden" name="relationId" id="relationId" value="">
		   
		   <input type="hidden" name="schoolId" id="schoolId" value=""> 
		   <input type="hidden" name="emobCode" id="emobCode" value=""> 
		    
	 </form>
</div>

<!-- 批量导入视频弹窗 -->
<div id="addReportWord" class="easyui-dialog" closed="true" buttons="#add-butto"  style="width: 480px; height: 200px; display: none;" >
 
  <form  id="readReportForm" method="post" enctype="multipart/form-data"  >  
	  <div style="margin-top: 50px;margin-left: 35px">
	            <label for="file" style="font-size: 14px;">文件:</label> &nbsp; &nbsp;&nbsp;
	            <input id="file" type="file" name="file" style="" />  <a style="color:red;">*请选择excel文件*</a>
	  </div>
  </form>
</div> 		 
<div id="add-butto" style="display: none;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="downLoad()" iconcls="icon-save">模板下载</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveExcel()" iconcls="icon-save">导入</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#addReportWord').dialog('close')"
				iconcls="icon-cancel">取消</a>	
</div>

<!------------------------------ 确认取消按钮 ----------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddNotice()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>
<script type="text/javascript">
 $(function(){
 var uu = 0;
    $('#user').combobox({
     	url:'<%=basePath%>/user/userName.do',
     	valueField: 'userId',
		textField: 'userName',
		editable:false ,
		onLoadSuccess: function(result){
			if(uu==0){
				var data = $(this).combobox('getData');
				data.insert(0, {'userId':'0','userName':'全部'});
				uu++;
				$("#user").combobox("loadData", data);
			}
		}
	});
	
}); 
</script>
</html>
