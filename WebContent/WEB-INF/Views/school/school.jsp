<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/js/pro_common.js" type="text/javascript"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script src="<%=basePath%>/Public/editor/kindeditor.js"></script>
<script src="<%=basePath%>/Public/editor/lang/zh_CN.js"></script>

<script src="<%=basePath%>/Public/editor/easyui_kindeditor.js"></script>
<script>

$(document).ready(function(){


	<% int sum = 0; %>
	
	<c:if test="${park_add == 'park_add'}">
	
		document.getElementById("addPark").style.display="block";
		
		<% sum ++; %>;
		
	</c:if>
	<% if(sum == 0) { %>
		
		$("#grid").datagrid({
			toolbar:''
		});
		
	<% } %>




var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
     
});

});

/*
/* 操作 */
function formatOper(value,row,index){

		<%int i = 0;%>
    	var str = "";
		<c:if test="${ park_xiugai == 'park_xiugai'}">
			str += "<a href='javascript:openbianji(\"" + row['schoolId'] + "\");'>修改</a>&nbsp;";
			<%i ++;%>
		</c:if>
		<c:if test="${ park_del == 'park_del'}">
			<%if (i != 0) {%>
				str += " | &nbsp;"
			<%}%>
			str += "<a href='javascript:opendanshan(\"" + row['schoolId'] + "\");'>删除</a>&nbsp;";
		</c:if>
		if(str == ""){
	    		str = "<a style='color:#666;'>无此权限</a>";
	    }
		return str;

	/*return "<a href='javascript:openbianji(\"" + row['schoolId'] + "\");'>修改</a>"
	+ " | <a href='javascript:opendanshan(\"" + row['schoolId'] + "\");'>删除</a>";*/
} 

/* 添加 保存 园区 */
function saveAddSchool()
{	
	var schoolName = $('#schoolName').val();
	if(schoolName == "" || schoolName == null){
		layer.msg("请输入园区名称");
		return null;
	}
	//判定
		var province = $('#province').combobox('getValue');
		if(province == "" || province == null){
				layer.msg("请选择省份")
				return null;
			}
			
		var city = $('#city').combobox('getValue');
		if(city == "" || city == null){
				layer.msg("请选择市区")
				return null;
			}	
		
		
		
	
	$('#formAddRole').form('submit', {
		url : '<%=basePath%>/school/saveSchool.do',
		onSubmit : function() {
			return $(this).form('validate');
			var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
			
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				
			}
		}
	}); 
}

/* 编辑园区 */
function openbianji(schoolId)
{	
	$.post("<%=basePath%>/school/bjSchool.do",{schoolId:schoolId},function(result){
		data = eval("(" + result + ")");
		$('#schoolId').val(data.schoolId);
		$('#schoolName').val(data.schoolName);
		
		$('#city').combobox('setValue',data.city);
		$('#cityId').val(data.cityId);
		
		$('#province').combobox('setValue',data.province);
		$('#provinceId').val(data.provinceId);
	
		//$('#employeeId').val(data.employeeNameId);
		$('#employee1').css('display','none');
		/* $('#employee').combobox({
			queryParams:{
				"schoolId":schoolId
			}
		}); */
		
		$('#employee').textbox('setValue',data.employeeName);
		$('#employee1').css('display','block');
		
		
		$('#adddlg').panel({title: "编辑园区"});
	    $('#mode').val("edit");	
		//$('#name').combobox('setValue', data.name);
		$('#adddlg').dialog('open');
	}); 
	
}

/* 单删 */
function opendanshan(schoolId)
{	$.messager.confirm('确认','确认删除?',function(row){

	if(row){
	
	$.post("<%=basePath%>/school/delSchool.do",{delKeys:schoolId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		//if(data.code == 100){
			//$('#grid').datagrid('reload');
			/* $.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
							
			}); */
			if(data.code == 100){
					layer.closeAll('loading');
					$('#adddlg').dialog('close');
					$('#grid').datagrid('reload');
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">删除成功</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
					});
				}
				else
				{
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">'+data.msg+'</div>',
						timeout: 500,  
						showType:'fade',
						style:{right:'', bottom:''}
						});
				}
		//}
	})}
	}); 
}




/* 添加按钮 */
function add()
{
    document.getElementById("schoolName").value = "";
	$('#province').combobox('setValue','');
	$('#city').combobox('setValue','');
	
	
     $("#enable   option[value='1']").attr("selected",true);
    $('#mode').val("add");	
    $('#employee1').css('display','none');
    
    
    $('#adddlg').panel({title: "添加园区"});
	$('#adddlg').dialog('open');
}


</script>
</head>
<body class="easyui-layout">
	<!----------------------------- 功能按钮 --------------------------------->
	<div id="tb" style="height:auto;display: none;">
		<div
			style="width:110%; margin-top:5px;margin-left:7px;text-align: left;height:35px;">
			<a href="javascript:void(0)" class="easyui-linkbutton"
				iconCls="icon-add" plain="true" onclick="add()" id="addPark" style="display: none; float: left;">添加</a>
		</div>

	</div>
	<table id="grid" class="easyui-datagrid"
		style="width:100%;height:100%;display: none;"
		title="<img alt='后台用户管理' style='width:17px;height:17px;float:left' src='<%=basePath%>/img/titleImg.png'>
		<div style='height:20px;line-height:22px;margin-left:10px;margin-top:-4;float:left'>园区管理</div>"
		url="<%=basePath%>/school/schoolList.do" toolbar="#tb"
		rownumbers="true" pageSize="20" pagination="true" singleSelect="false"
		loadMsg="正在努力为您加载数据">
		<thead>
			<tr>
				<th field="ck" checkbox="true" id="ck" name="ck"></th>
				<th field="schoolName" width="14%" align="center">园区名</th>
				<th field="provinceName" width="14%" align="center">省份</th>
				<th field="city" width="14%" align="center">城市</th>
				<th field="employeeName" width="14%" align="center">园长</th>
				<th field="createUser" width="14%" align="center">添加人</th>
				<th field="createDate" width="14%" align="center">添加日期</th>
				<th field="oper" width="15%" align="center" formatter="formatOper"
					align="center">操作</th>
			</tr>
		</thead>
	</table>

	<!------------------------------- 添加界面 ----------------------------->
	<div id="adddlg" class="easyui-dialog"
		style="width: 400px; height: 300px; padding: 10px 20px;display: none;"
		closed="true" buttons="#adddlg-buttons">
		<form id="formAddRole" name="formAddRole" method="post">
			<div class="fitem">
				<label
					style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 15px;">园区名</label>
				<input name="schoolName"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;border:1px solid #95B8E7"
					type="text" id="schoolName" />
			</div>


			<div class="fitem">
				<label
					style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="province">省份</label>
				<input id="province" name="province" class="easyui-combobox"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"
					 data-options=" 
					 editable:false,
				valueField: 'areaId',
				textField: 'areaName',
				url:'<%=basePath%>/school/listSysAreaData.do',
				onSelect: function(rec){
				$('#city').combobox('setValue','');
				$('#provinceId').val(rec.areaId);
				$('#province').val(rec.areaName);
				var url='<%=basePath%>school/listCity.do?cityId='+rec.areaId;
				$('#city').combobox('reload',url);
				}" />
			</div>
			<div class="fitem">
				<label
					style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="city">城市</label>
				<input id="city" name="city" class="easyui-combobox"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"
					 data-options="
					 editable:false,
				valueField:'cityId',
				textField:'cityName',
				onSelect: function(rec){
				$('#city').val(rec.cityName);
				$('#cityId').val(rec.cityId);
				}" />
			</div>
			<div class="fitem" id="employee1">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="employee">园长</label>
				<input id="employee" name="employee" class="easyui-textbox"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"
					 data-options="
					 editable:false,
				valueField:'employeeId',
				textField:'employeeName',
				url:'<%=basePath%>/school/listEmployee.do',
				onSelect: function(rec){
				$('#employee').val(rec.employeeName);
				$('#employeeId').val(rec.employeeId);
				}" />
			</div>
			<!-- var url = '<%=basePath%>/school/listEmployee.do?area='+rec.provincekey; -->

			<input type="hidden" name="mode" id="mode" value="" /> 
			<input type="hidden" name="schoolId" id="schoolId" value="" />
			<input type="hidden" name="provinceId" id="provinceId" value="" />
			<input type="hidden" name="cityId" id="cityId" value="" />
			<input type="hidden" name="employeeId" id="employeeId" value="" />
		</form>
	</div>

	<!------------------------------ 确认取消按钮 ----------------------------------->
	<div id="adddlg-buttons" style="height: 30px;display: none;">
		<div style="width:140px;height:35px;float: right;">
			<a href="javascript:void(0)" class="easyui-linkbutton"
				onclick="saveAddSchool()" iconcls="icon-save">保存</a> <a
				href="javascript:void(0)" class="easyui-linkbutton"
				onclick="javascript:$('#adddlg').dialog('close')"
				iconcls="icon-cancel">取消</a>
		</div>
	</div>
</body>
</html>
