<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/css/system/notice.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script>
$(document).ready(function(){


	<% int sum = 0; %>
	
	<c:if test="${employee_add == 'employee_add'}">
	
		document.getElementById("addEmployee").style.display="block";
		
		<% sum ++; %>;
		
	</c:if>
	<c:if test="${employee_enable == 'employee_enable'}">
	
		<% sum ++; %>;
		
	</c:if>
	<c:if test="${employee_daoru == 'employee_daoru'}">
	
		<% sum ++; %>;
		
	</c:if>
		<c:if test="${employee_daochu == 'employee_daochu'}">
	
		<% sum ++; %>;
		
	</c:if>
	<% if(sum == 0) { %>
		
		$("#grid").datagrid({
			toolbar:''
		});
		
	<% } %>





var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
     
});

					
});

/* 操作 */
function formatOper(value,row,index){

		<%int i = 0;%>
    	var str = "";
		<c:if test="${ employee_xiugai == 'employee_xiugai'}">
			str += "<a href='javascript:openEdit(\"" + row['employeeId'] + "\");'>修改</a>&nbsp;";
			<%i ++;%>
		</c:if>
		<c:if test="${ employee_del == 'employee_del'}">
			<%if (i != 0) {%>
				str += " | &nbsp;"
			<%}%>
			str += "<a href='javascript:openDel(\"" + row['employeeId'] + "\");'>删除</a>&nbsp;";
		</c:if>
		if(str == ""){
	    		str = "<a style='color:#666;'>无此权限</a>";
	    }
		return str;

	/*return "<a href='javascript:openEdit(\"" + row['employeeId'] + "\");'>修改</a>"
	+ " | <a href='javascript:openDel(\"" + row['employeeId'] + "\");'>删除</a>";*/
} 
/* 添加 保存 角色 */
function saveAddNotice()
{	

		/* var schoolName = $('#schoolName').combobox('getValue');
		if(schoolName == "" || schoolName == null){
				layer.msg("园区名不能为空")
				return null;
			}  */
			
		var roleName = $('#roleName').combobox('getValue');
		if(roleName == "" || roleName == null){
				layer.msg("角色不能为空")
				return null;
			} 
			
		var rolDat = $('#roleLeData').val();	
		if(rolDat ==1){
		
			var NJ=$('input:radio[name="isNianJ"]:checked').length;	
				//alert(NJ)
				if(NJ == 0){
					layer.msg("请选择所属年级")
					return null;
				}
		}
			
		var sysUserName = $('#sysUserName').textbox('getValue');
		
		if(sysUserName.length > 10){
			layer.msg("请输入真实姓名")
			return null;
		}
		if(sysUserName == "" || sysUserName == null){
				layer.msg("请输入姓名")
				return null;
			} 
		var sex = $('#sex').combobox('getValue');
		if(sex == ""){
				layer.msg("请选择性别")
				return null;
			} 
		
		var tel = $('#tel').textbox('getValue');
		if(tel == "" || tel == null){
				layer.msg("请输入联系方式")
				return null;
			} 
			
		$('#formAddNotice').form('submit', {
			url : '<%=basePath%>/employee/saveEmployee.do',
			onSubmit : function() {
				return $(this).form('validate');
				var index = layer.load(1, {
					  shade: [0.1,'#fff'] //0.1透明度的白色背景
				})
				
			},
			success : function(result) {
				data = eval("(" + result + ")");
				if(data.code == 100){
					layer.closeAll('loading');
					$('#adddlg').dialog('close');
					$('#grid').datagrid('reload');
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">保存成功</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
					});
				}
				else
				{
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">'+ data.msg +'</div>',
						timeout: 500,  
						showType:'fade',
						style:{right:'', bottom:''}
						});
				}
			}
		}); 
	
}
/* 编辑角色 */
function openEdit(employeeId)
{	
	/* var isNianJ = $("input[name=isNianJ]");
   	isNianJ.attr("checked",false);
   	$('#showNinaJ').css('display','none'); */

	$.post("<%=basePath%>/employee/sysUserEmobCode.do",{employeeId:employeeId},function(result){
		data = eval("(" + result + ")");
		
		 $("#emobCode").val(data.emobCode);
		 //alert(data.emobCode);
		});	

	$.post("<%=basePath%>/employee/employeeEdit.do",{employeeId:employeeId},function(result){
		data = eval("(" + result + ")");
		$('#employeeId').val(employeeId);
		
		$('#schoolName').combobox('setValue',data.schoolName);
		$('#schoolId').val(data.schoolId);
		
		$('#roleName').combobox('setValue',data.roleName);
		$('#roleId').val(data.roleId);
		
		//alert(data.isNi);
		if(data.isNi > 0){
			$('#showNinaJ').css('display','block');
			if(data.isNi ==1){
				$("#tuoban1").attr("checked","checked")
			}else if(data.isNi ==2){
				$("#xiaoban2").attr("checked","checked")
			}else if(data.isNi ==3){
				$("#zhongban3").attr("checked","checked")
			}else if(data.isNi ==4){
				$("#daban4").attr("checked","checked")
			}else if(data.isNi ==5){
				$("#xueqian5").attr("checked","checked")
			}else{
				$('#showNinaJ').css('display','none');
			}
		}else{
		
			$('#showNinaJ').css('display','none');
			//$('#showNinaJ').css('display','block');
		}
		
		$('#sysUserName').textbox('setValue',data.userName);
		$('#sysUserName').val(data.userName);
		
		$('#typeList').textbox('setValue',data.typeName);
		$('#subRoleId').val(data.subRoleId);
		
		$('#sex').textbox('setValue',data.sex);
		
		$('#teacherAndTel').combobox('setValue',data.teacherAndTel);
		$('#teacherAndTelId').val(data.teacherAndTelId);
		
		$('#tel').textbox('setValue',data.tel);
		
		$('#mode').val("edit");
		$('#adddlg').panel({title: "职工修改"});
		$('#adddlg').dialog('open');
	}); 
	
}

/* 单删 */
function openDel(employeeId)
{	$.messager.confirm('确认','确认删除?',function(row){

	if(row){
	$.post("<%=basePath%>/employee/delEmployee.do",{delKeys:employeeId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}else
				{
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">'+ data.msg +'</div>',
						timeout: 500,  
						showType:'fade',
						style:{right:'', bottom:''}
						});
				}
	}); 
}
})};

/* 添加按钮 */
function add()
{

   // document.getElementById("Name").value = "";
   // $('#user').combobox('setValue','');
   // document.getElementById("text").value = "";
   	$('#schoolName').combobox('setValue','');
   	$('#roleName').combobox('setValue','');
   	$('#roleName').combobox({  
	   queryParams:{
	   		'schoolId':''
	   }    
	}); 
   	
   	$('#typeList').combobox('setValue','');
   	$('#sex').combobox('setValue','');
   	$('#sysUserName').textbox('setValue','');
   	$('#tel').textbox('setValue','');
   	var isNianJ = $("input[name=isNianJ]");
   	isNianJ.attr("checked",false);
   	$('#showNinaJ').css('display','none');
   	
   	
    $('#mode').val("add");
    $('#adddlg').panel({title: "添加"});
	$('#adddlg').dialog('open');
}

function formatEnable(value,row,index){
	if(value == '0')
		return "<a style='color:#2A60F4'>启用</a>";
	return "<a style='color:#FF0000'>禁用</a>";
	
}

/* 禁用按钮 */
function disable()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].employeeId;
		}
	}
	
	$.post("<%=basePath%>/employee/enable.do",{disableKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'操作成功',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'请选中要启用/禁用的数据',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

/* 批量导入视频 */
function listImport(){
	$('#file').val('');
	$('#addReportWord').panel({title: "表格批量导入"});
	$('#addReportWord').dialog('open');
	
}
/* 视频批量导入 */
function saveExcel(){
	
	
	var file = $('#file').val();
	var fileArr=file.split("//");
		var fileTArr=fileArr[fileArr.length-1].toLowerCase().split(".");
		var filetype=fileTArr[fileTArr.length-1];
		if(filetype!="xls"&&filetype!="xlsx"){
			$('#file').focus();
			layer.msg("上传文件必须为xls、xlsx文件！");
			return ;
		}
	if(file == ''){
		layer.msg('请选择文件');
		return ;
	}
	var index = layer.load(1, {
			 	 shade: [0.1,'#fff'] 
	});
	$('#readReportForm').form('submit', {
		url : '<%=basePath%>/employee/saveExcel.do',
		onSubmit : function() {
			return $(this).form('validate');
		},
		success : function(result) {
			layer.closeAll('loading');
			if(result == null || "" == result){
				$('#addReportWord').dialog('close')
				$('#grid').datagrid('reload');
				$.messager.show({
						title:'提示',
						msg:'<div class="msgs">导入成功</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
					});
			}else{
				alert(result);
			}
		}
	}); 
	
}



/* 导出 */
function daochu()
{		
		var SsName = $('#SsName').val();
		var SsTel = $('#SsTel').val();
		window.parent.location="<%=basePath%>/employee/daochuEmployee.do?SsName="+SsName+"&SsTel="+SsTel;
}

//搜索
function sousuo()
{
	var SsName = $('#SsName').val();
	var SsTel = $('#SsTel').val();
	$('#grid').datagrid('load', {SsName:SsName,SsTel:SsTel});
	
}

//扩展easyui表单的验证  
/* $.extend($.fn.validatebox.defaults.rules, {  
    //验证汉子  
    CHS: {  
        validator: function (value) {  
            return /^[\u0391-\uFFE5]+$/.test(value);  
        },  
        message: '只能输入汉字'  
    },  
    //移动手机号码验证  
    mobile: {//value值为文本框中的值  
        validator: function (value) {  
            var reg = /^1[3|4|5|8|9]\d{9}$/;  
            return reg.test(value);  
        },  
        message: '输入手机号码格式不准确.'  
    },  
    //国内邮编验证  
    zipcode: {  
        validator: function (value) {  
            var reg = /^[1-9]\d{5}$/;  
            return reg.test(value);  
        },  
        message: '邮编必须是非0开始的6位数字.'  
    },  
    //用户账号验证(只能包括 _ 数字 字母)   
    account: {//param的值为[]中值  
        validator: function (value, param) {  
            if (value.length < param[0] || value.length > param[1]) {  
                $.fn.validatebox.defaults.rules.account.message = '用户名长度必须在' + param[0] + '至' + param[1] + '范围';  
                return false;  
            } else {  
                if (!/^[\w]+$/.test(value)) {  
                    $.fn.validatebox.defaults.rules.account.message = '用户名只能数字、字母、下划线组成.';  
                    return false;  
                } else {  
                    return true;  
                }  
            }  
        }, message: ''  
    }  
}) */
function isRoleNa(schoolId){
	$('#roleName').combobox({
		queryParams:{
			'schoolId':schoolId,
		}
	})
}
function chaRoleLevel(roleId){

	$.post("<%=basePath%>/employee/getRoleLevel.do",{roleId:roleId},function(result){
		data = eval("(" + result + ")");
			if(data.roLevel ==1){
				$('#showNinaJ').css('display','block');
				$('#roleLeData').val(1);
			}else{
				$('#showNinaJ').css('display','none');
				$('#roleLeData').val(0);
			}
		});
}	

function downLoad(){
	window.parent.location="<%=basePath%>/file/download.do?id="+"employee";
	
}

/* 删除按钮 */
function openshanchu()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		var str = "";
		var rows  = $('#grid').datagrid("getRows");
		var ckArr = $("input[name='ck']");
		for(var i=0; i<ckArr.length; i++)
		{
			if(ckArr[i].checked)
			{
				if(str != "")
				{
					str += ",";
				}
				str += rows[i].employeeId;
			}
		}
	
	$.messager.confirm('确认','确认删除?',function(row){

	if(row){
		$.post("<%=basePath%>/employee/delEmployee.do",{delKeys:str},function(result){
			layer.closeAll('loading');
			data = eval("(" + result + ")");
			if(data.code == 100){
				$('#grid').datagrid('reload');
				$.messager.show({
								title:'提示',
								msg:'删除成功',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
			}else
			{
					$.messager.show({
						title:'提示',
						msg:'<div class="msgs">'+ data.msg +'</div>',
						timeout: 500,  
						showType:'fade',
						style:{right:'', bottom:''}
						});
				}
			
		}); 
		}
	});
	}else{
		$.messager.show({
							title:'提示',
							msg:'请选中您要删除的数据',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}
</script>
</head>
<body class="easyui-layout">
<!----------------------------- 功能按钮 --------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:110%; margin-top:5px;margin-left:7px;text-align: left;height:35px;"> 
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()" id="addEmployee" style="display: none; float: left;" >单条添加</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="disable()" >启用/禁用</a>
      	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="openshanchu()" >删除</a>
	 </div> 
	
<!--------------------------------- 搜索框 ----------------------------------->
	 <div style="margin-left:10px; text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		   <div style="float: left;height: 30px; "> 
		    <span style="float: left;margin-left:5px; margin-top: 7px;margin-right: 10px;color:#404040" >姓名</span>
	   		<input name="SsName" id="SsName" style="margin-top: 5px;border:1px #C5C5C5 solid;width: 100px;height: 23px"/>
		   </div>
		   <div style="float: left;height: 30px;margin-left: 20px; "> 
		    <span style="float: left;margin-left:5px; margin-top: 7px;margin-right: 10px;color:#404040" >联系电话</span>
	   		<input name="SsTel" id="SsTel" style="margin-top: 5px;border:1px #C5C5C5 solid;width: 150px;height: 23px"/>
		   </div>
	     <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 10px;cursor:pointer;text-align:center; background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 5px;border-radius: 5px;"
	     class="easyui-linkbutton" iconCls="icon-search">搜索</div>	 
	     <div style="width:100px; margin-top:3px;margin-left:20px; text-align: left;height:35px;float: left; "> 
	    	    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-redo" plain="true" onclick="listImport()" >批量导入</a>
			 </div> 
			 
	     <div style="width:100px; margin-top:3px; text-align: left;height:35px;float: left; "> 
	    	    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-redo" plain="true" onclick="daochu()" >导出表格</a>
			 </div>   
	 </div>
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后台用户管理' style='width:17px;height:17px;float:left' src='<%=basePath%>/img/titleImg.png'>
		<div style='height:20px;line-height:22px;margin-left:10px;margin-top:-4;float:left'>职工管理</div>"
			url="<%=basePath%>/employee/selectEmployee.do" toolbar="#tb"
			rownumbers="true" pageSize="20" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="schoolName" width="14%" align="center">园区名</th>
					<th field="roleName" width="14%" align="center">角色</th>
					<th field="userName" width="14%" align="center">姓名</th>
					<th field="sex" width="14%" align="center">性别</th>
					<th field="tel" width="14%" align="center">联系电话</th>
					<th field="enable" width="14%" align="center" formatter="formatEnable">状态</th>
					<th field="oper" width="15%" align="center" formatter="formatOper">操作</th>
				</tr>
			</thead>
		</table>
		
<!------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 430px; height: 420px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" >
	<form id="formAddNotice" name="formAddNotice" method="post">
	
			<!-- 园区名 添加 -->	
				<div class="fitem">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 15px; for="employee">园区名</label>
				<input id="schoolName" name="schoolName" class="easyui-combobox"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; data-options="
					editable:false,
				valueField:'schoolId',
				textField:'schoolName',
				url:'<%=basePath%>/system/listSchool.do',
				onSelect: function(rec){
				$('#schoolName').val(rec.schoolName);
				$('#schoolId').val(rec.schoolId);
				isRoleNa(rec.schoolId);
				}" />
			</div>
			
			
			<!-- 角色名 -->	
				<div class="fitem">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 15px; for="employee">角色</label>
				<input id="roleName" name="roleName" class="easyui-combobox"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; data-options="
					editable:false,
				valueField:'roleId',
				textField:'roleName',
				url:'<%=basePath%>/employee/listRole.do',
				onSelect: function(rec){
					$('#roleName').val(rec.roleName);
					$('#roleId').val(rec.roleId);
					$('#typeList').combobox('setValue','')
					$('#subRoleId').val('')
				var url='<%=basePath%>/employee/listSubRole.do?roleId='+rec.roleId;
					$('#typeList').combobox('reload',url);
					chaRoleLevel(rec.roleId);
				}" />
			</div>
			
			<!-- 角色级别 -->
			 <div id="showNinaJ" style="width:178px;height:80px;border: 1px solid #999;margin-top: 10px;margin-left: 123px;display: none;">
			 	<label class="tile" style="display:inline-block;width:35px;color:#000;font-size:15px;margin-left: 15px;margin-top: 5px;">托班</label>
				<input id="tuoban1" type="radio" name='isNianJ' value='1'>
				<label class="tile" style="display:inline-block;width:35px;color:#000;font-size:15px;margin-left: 15px;margin-top: 5px;">小班</label>
				<input id="xiaoban2" type="radio" name='isNianJ' value='2'>
				<label class="tile" style="display:inline-block;width:35px;color:#000;font-size:15px;margin-left: 15px;margin-top: 5px;">中班</label>
				<input id="zhongban3" type="radio" name='isNianJ' value='3'>
				<label class="tile" style="display:inline-block;width:35px;color:#000;font-size:15px;margin-left: 15px;margin-top: 5px;">大班</label>
				<input id="daban4" type="radio" name='isNianJ' value='4'>
				<label class="tile" style="display:inline-block;width:50px;color:#000;font-size:15px;margin-left: 15px;margin-top: 5px;">学前班</label>
				<input id="xueqian5" type="radio" name='isNianJ' value='5'>
			 </div>
			
			<!-- 类型 -->	
				<div class="fitem">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 15px; for="employee">类型</label>
				<input id="typeList" name="typeList" class="easyui-combobox"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; data-options="
					editable:false,
				valueField:'subRoleId',
				textField:'typeName',
				onSelect: function(rec){
					$('#typeList').val(rec.typeName);
					$('#subRoleId').val(rec.subRoleId);
				}" />
			</div>
			
			
			<!-- 姓名  输入  -->
		   <div class="fitem" >
			   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 15px;">姓名</label>
			   <input name="sysUserName" id="sysUserName" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; type="text" />
		   </div>
		   
		   
		   <div class="fitem">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 15px; for="employee">性别</label>
					<select id="sex" name="sex" class="easyui-combobox" data-options="editable:false"
					style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px">   
						<option></option>
					    <option value="1">男</option>   
					    <option value="0">女</option>   
					</select>  
				</div>
				
				<!-- 联系电话  -->
		   <div class="fitem" >
			   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 15px;">联系电话</label>
			   <input name="tel" id="tel" validtype="mobile" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; type="text" />
		   </div>
			
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="employeeId" id="employeeId" value=""/>
		   <input type="hidden" name="userid" id="userid" value="">
		   <input type="hidden" name="schoolId" id="schoolId" value="">
		   <input type="hidden" name="roleId" id="roleId" value="">
		   <input type="hidden" name="subRoleId" id="subRoleId" value="">
		   <input type="hidden" name="sysUserName" id="sysUserName" value="">
		   <input type="hidden" name="emobCode" id="emobCode" value=""> 
		   <input type="hidden" name="roleLeData" id="roleLeData" value=""> 
		   
	 </form>
</div>


<!-- 批量导入视频弹窗 -->
<div id="addReportWord" class="easyui-dialog" closed="true" buttons="#add-butto"  style="width: 480px; height: 200px; display: none;" >
 
  <form  id="readReportForm" method="post" enctype="multipart/form-data"  >  
	  <div style="margin-top: 50px;margin-left: 35px">
	            <label for="file" style="font-size: 14px;">文件:</label> &nbsp; &nbsp;&nbsp;
	            <input id="file" type="file" name="file" style="" />  <a style="color:red;">*请选择excel文件*</a>
	  </div>
  </form>
</div> 		 
<div id="add-butto" style="display: none;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="downLoad()" iconcls="icon-save">模板下载</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveExcel()" iconcls="icon-save">导入</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#addReportWord').dialog('close')"
				iconcls="icon-cancel">取消</a>	
</div>	


<!------------------------------ 确认取消按钮 ----------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddNotice()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>
<script type="text/javascript">


</script>
</html>
